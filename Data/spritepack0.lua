-- Generated with TexturePacker (http://www.codeandweb.com/texturepacker)
-- with a custom export by Stewart Bracken (http://stewart.bracken.bz)
--
-- $TexturePacker:SmartUpdate:2082a66232ccbca689bfefc7ffdb04d9:f8223a5ccd60d316a6d7da5e0f5ce0ab:5e3e9293d7f39997ec7e7a2c172c8ee9$
--
--[[------------------------------------------------------------------------
-- Example Usage --

function love.load()
	myAtlas = require("spritepack0")
	batch = love.graphics.newSpriteBatch( myAtlas.texture, 100, "stream" )
end
function love.draw()
	batch:clear()
	batch:bind()
		batch:add( myAtlas.quads['mySpriteName'], love.mouse.getX(), love.mouse.getY() )
	batch:unbind()
	love.graphics.draw(batch)
end

--]]------------------------------------------------------------------------

local TextureAtlas = {}
local Quads = {}
local Offsets = {}
local Texture = love.graphics.newImage( "assets/art/" .. "spritepack0.png" )
Texture:setFilter("nearest", "nearest")


Quads["Ally"] = love.graphics.newQuad(22, 246, 8, 8, 256, 256 )
Offsets["Ally"] = { 0, 0, 8, 8 }

Quads["Arrow1"] = love.graphics.newQuad(160, 38, 5, 5, 256, 256 )
Offsets["Arrow1"] = { 0, 0, 5, 5 }

Quads["Beam"] = love.graphics.newQuad(2, 152, 8, 32, 256, 256 )
Offsets["Beam"] = { 0, 0, 8, 32 }

Quads["Beam2"] = love.graphics.newQuad(2, 186, 8, 32, 256, 256 )
Offsets["Beam2"] = { 0, 0, 8, 32 }

Quads["Beamer1"] = love.graphics.newQuad(32, 204, 14, 14, 256, 256 )
Offsets["Beamer1"] = { 0, 0, 14, 14 }

Quads["Beamer2"] = love.graphics.newQuad(48, 204, 14, 14, 256, 256 )
Offsets["Beamer2"] = { 0, 0, 14, 14 }

Quads["Beamer3"] = love.graphics.newQuad(64, 204, 14, 14, 256, 256 )
Offsets["Beamer3"] = { 0, 0, 14, 14 }

Quads["BlockedPortal1"] = love.graphics.newQuad(72, 220, 16, 16, 256, 256 )
Offsets["BlockedPortal1"] = { 0, 0, 16, 16 }

Quads["BlockedPortal2"] = love.graphics.newQuad(72, 238, 16, 16, 256, 256 )
Offsets["BlockedPortal2"] = { 0, 0, 16, 16 }

Quads["BlockedPortal3"] = love.graphics.newQuad(90, 220, 16, 16, 256, 256 )
Offsets["BlockedPortal3"] = { 0, 0, 16, 16 }

Quads["Bomb1"] = love.graphics.newQuad(32, 246, 8, 8, 256, 256 )
Offsets["Bomb1"] = { 0, 0, 8, 8 }

Quads["Bomb2"] = love.graphics.newQuad(42, 246, 8, 8, 256, 256 )
Offsets["Bomb2"] = { 0, 0, 8, 8 }

Quads["Bomb3"] = love.graphics.newQuad(52, 246, 8, 8, 256, 256 )
Offsets["Bomb3"] = { 0, 0, 8, 8 }

Quads["Bomb4"] = love.graphics.newQuad(62, 246, 8, 8, 256, 256 )
Offsets["Bomb4"] = { 0, 0, 8, 8 }

Quads["Bomb5"] = love.graphics.newQuad(58, 194, 8, 8, 256, 256 )
Offsets["Bomb5"] = { 0, 0, 8, 8 }

Quads["Bouncer1"] = love.graphics.newQuad(162, 248, 6, 6, 256, 256 )
Offsets["Bouncer1"] = { 0, 0, 6, 6 }

Quads["Bouncer2"] = love.graphics.newQuad(170, 248, 6, 6, 256, 256 )
Offsets["Bouncer2"] = { 0, 0, 6, 6 }

Quads["Chest01"] = love.graphics.newQuad(140, 160, 14, 12, 256, 256 )
Offsets["Chest01"] = { 0, 0, 14, 12 }

Quads["Chest02"] = love.graphics.newQuad(214, 70, 14, 12, 256, 256 )
Offsets["Chest02"] = { 0, 0, 14, 12 }

Quads["Chest03"] = love.graphics.newQuad(239, 78, 14, 12, 256, 256 )
Offsets["Chest03"] = { 0, 0, 14, 12 }

Quads["Corner1"] = love.graphics.newQuad(80, 204, 14, 14, 256, 256 )
Offsets["Corner1"] = { 0, 0, 14, 14 }

Quads["Corner2"] = love.graphics.newQuad(96, 204, 14, 14, 256, 256 )
Offsets["Corner2"] = { 0, 0, 14, 14 }

Quads["CursorKeyLeft"] = love.graphics.newQuad(58, 174, 16, 18, 256, 256 )
Offsets["CursorKeyLeft"] = { 0, 0, 16, 18 }

Quads["CursorKeyRight"] = love.graphics.newQuad(58, 174, 16, 18, 256, 256 )
Offsets["CursorKeyRight"] = { 0, 0, 16, 18 }

Quads["DirectionalArrow"] = love.graphics.newQuad(76, 144, 6, 4, 256, 256 )
Offsets["DirectionalArrow"] = { 0, 0, 6, 4 }

Quads["EnemyBug1"] = love.graphics.newQuad(90, 238, 16, 16, 256, 256 )
Offsets["EnemyBug1"] = { 0, 0, 16, 16 }

Quads["EnemyBug2"] = love.graphics.newQuad(108, 220, 16, 16, 256, 256 )
Offsets["EnemyBug2"] = { 0, 0, 16, 16 }

Quads["EnemyBullet1"] = love.graphics.newQuad(148, 69, 5, 5, 256, 256 )
Offsets["EnemyBullet1"] = { 0, 0, 5, 5 }

Quads["EnemyBullet2"] = love.graphics.newQuad(148, 76, 5, 5, 256, 256 )
Offsets["EnemyBullet2"] = { 0, 0, 5, 5 }

Quads["EnemyFly1"] = love.graphics.newQuad(108, 238, 16, 16, 256, 256 )
Offsets["EnemyFly1"] = { 0, 0, 16, 16 }

Quads["EnemyFly2"] = love.graphics.newQuad(126, 220, 16, 16, 256, 256 )
Offsets["EnemyFly2"] = { 0, 0, 16, 16 }

Quads["EnemyFly3"] = love.graphics.newQuad(126, 238, 16, 16, 256, 256 )
Offsets["EnemyFly3"] = { 0, 0, 16, 16 }

Quads["EnemyFrog"] = love.graphics.newQuad(76, 54, 16, 16, 256, 256 )
Offsets["EnemyFrog"] = { 0, 0, 16, 16 }

Quads["EnemyFrogSit"] = love.graphics.newQuad(78, 2, 16, 16, 256, 256 )
Offsets["EnemyFrogSit"] = { 0, 0, 16, 16 }

Quads["EnemyWizard1"] = love.graphics.newQuad(78, 20, 16, 16, 256, 256 )
Offsets["EnemyWizard1"] = { 0, 0, 16, 16 }

Quads["EnemyWizard2"] = love.graphics.newQuad(76, 72, 16, 16, 256, 256 )
Offsets["EnemyWizard2"] = { 0, 0, 16, 16 }

Quads["EnemyWizard3"] = love.graphics.newQuad(76, 90, 16, 16, 256, 256 )
Offsets["EnemyWizard3"] = { 0, 0, 16, 16 }

Quads["EnemyWizard4"] = love.graphics.newQuad(76, 108, 16, 16, 256, 256 )
Offsets["EnemyWizard4"] = { 0, 0, 16, 16 }

Quads["EnemyWizard5"] = love.graphics.newQuad(76, 126, 16, 16, 256, 256 )
Offsets["EnemyWizard5"] = { 0, 0, 16, 16 }

Quads["EnemyWizard6"] = love.graphics.newQuad(82, 150, 16, 16, 256, 256 )
Offsets["EnemyWizard6"] = { 0, 0, 16, 16 }

Quads["Flame0"] = love.graphics.newQuad(78, 192, 10, 10, 256, 256 )
Offsets["Flame0"] = { 0, 0, 10, 10 }

Quads["Flame1"] = love.graphics.newQuad(90, 192, 10, 10, 256, 256 )
Offsets["Flame1"] = { 0, 0, 10, 10 }

Quads["Flea1"] = love.graphics.newQuad(102, 192, 10, 10, 256, 256 )
Offsets["Flea1"] = { 0, 0, 10, 10 }

Quads["Flea2"] = love.graphics.newQuad(114, 192, 10, 10, 256, 256 )
Offsets["Flea2"] = { 0, 0, 10, 10 }

Quads["Freeze0"] = love.graphics.newQuad(94, 38, 16, 16, 256, 256 )
Offsets["Freeze0"] = { 0, 0, 16, 16 }

Quads["Freeze1"] = love.graphics.newQuad(96, 2, 16, 16, 256, 256 )
Offsets["Freeze1"] = { 0, 0, 16, 16 }

Quads["Freeze2"] = love.graphics.newQuad(96, 20, 16, 16, 256, 256 )
Offsets["Freeze2"] = { 0, 0, 16, 16 }

Quads["Freeze3"] = love.graphics.newQuad(94, 56, 16, 16, 256, 256 )
Offsets["Freeze3"] = { 0, 0, 16, 16 }

Quads["Genocide1"] = love.graphics.newQuad(2, 220, 8, 32, 256, 256 )
Offsets["Genocide1"] = { 0, 0, 8, 32 }

Quads["Genocide2"] = love.graphics.newQuad(12, 152, 8, 32, 256, 256 )
Offsets["Genocide2"] = { 0, 0, 8, 32 }

Quads["Genocide3"] = love.graphics.newQuad(12, 186, 8, 32, 256, 256 )
Offsets["Genocide3"] = { 0, 0, 8, 32 }

Quads["Gold1"] = love.graphics.newQuad(167, 48, 7, 8, 256, 256 )
Offsets["Gold1"] = { 0, 0, 7, 8 }

Quads["Gold2"] = love.graphics.newQuad(242, 38, 7, 8, 256, 256 )
Offsets["Gold2"] = { 0, 0, 7, 8 }

Quads["Gold3"] = love.graphics.newQuad(242, 48, 7, 8, 256, 256 )
Offsets["Gold3"] = { 0, 0, 7, 8 }

Quads["Gold4"] = love.graphics.newQuad(167, 58, 7, 8, 256, 256 )
Offsets["Gold4"] = { 0, 0, 7, 8 }

Quads["Gold5"] = love.graphics.newQuad(156, 61, 7, 8, 256, 256 )
Offsets["Gold5"] = { 0, 0, 7, 8 }

Quads["Grunt1"] = love.graphics.newQuad(112, 204, 14, 14, 256, 256 )
Offsets["Grunt1"] = { 0, 0, 14, 14 }

Quads["Grunt2"] = love.graphics.newQuad(78, 38, 14, 14, 256, 256 )
Offsets["Grunt2"] = { 0, 0, 14, 14 }

Quads["Grunt3"] = love.graphics.newQuad(128, 203, 14, 14, 256, 256 )
Offsets["Grunt3"] = { 0, 0, 14, 14 }

Quads["Grunt4"] = love.graphics.newQuad(150, 174, 14, 14, 256, 256 )
Offsets["Grunt4"] = { 0, 0, 14, 14 }

Quads["Grunt5"] = love.graphics.newQuad(162, 200, 14, 14, 256, 256 )
Offsets["Grunt5"] = { 0, 0, 14, 14 }

Quads["Grunt6"] = love.graphics.newQuad(162, 216, 14, 14, 256, 256 )
Offsets["Grunt6"] = { 0, 0, 14, 14 }

Quads["Grunt7"] = love.graphics.newQuad(162, 232, 14, 14, 256, 256 )
Offsets["Grunt7"] = { 0, 0, 14, 14 }

Quads["Grunt8"] = love.graphics.newQuad(178, 38, 14, 14, 256, 256 )
Offsets["Grunt8"] = { 0, 0, 14, 14 }

Quads["Heart"] = love.graphics.newQuad(148, 61, 6, 6, 256, 256 )
Offsets["Heart"] = { 0, 0, 6, 6 }

Quads["Homer1"] = love.graphics.newQuad(68, 194, 8, 8, 256, 256 )
Offsets["Homer1"] = { 0, 0, 8, 8 }

Quads["Homer2"] = love.graphics.newQuad(100, 164, 8, 8, 256, 256 )
Offsets["Homer2"] = { 0, 0, 8, 8 }

Quads["Homer3"] = love.graphics.newQuad(110, 164, 8, 8, 256, 256 )
Offsets["Homer3"] = { 0, 0, 8, 8 }

Quads["Homer4"] = love.graphics.newQuad(120, 164, 8, 8, 256, 256 )
Offsets["Homer4"] = { 0, 0, 8, 8 }

Quads["Homer5"] = love.graphics.newQuad(130, 164, 8, 8, 256, 256 )
Offsets["Homer5"] = { 0, 0, 8, 8 }

Quads["InfoTile"] = love.graphics.newQuad(2, 2, 48, 48, 256, 256 )
Offsets["InfoTile"] = { 0, 0, 48, 48 }

Quads["InfoTile2"] = love.graphics.newQuad(2, 52, 48, 48, 256, 256 )
Offsets["InfoTile2"] = { 0, 0, 48, 48 }

Quads["InfoTile3"] = love.graphics.newQuad(2, 102, 48, 48, 256, 256 )
Offsets["InfoTile3"] = { 0, 0, 48, 48 }

Quads["Key1"] = love.graphics.newQuad(150, 45, 14, 14, 256, 256 )
Offsets["Key1"] = { 0, 0, 14, 14 }

Quads["Key2"] = love.graphics.newQuad(194, 38, 14, 14, 256, 256 )
Offsets["Key2"] = { 0, 0, 14, 14 }

Quads["Key3"] = love.graphics.newQuad(210, 38, 14, 14, 256, 256 )
Offsets["Key3"] = { 0, 0, 14, 14 }

Quads["Kiss"] = love.graphics.newQuad(137, 192, 8, 8, 256, 256 )
Offsets["Kiss"] = { 0, 0, 8, 8 }

Quads["Lightning1"] = love.graphics.newQuad(12, 220, 8, 32, 256, 256 )
Offsets["Lightning1"] = { 0, 0, 8, 32 }

Quads["Lightning2"] = love.graphics.newQuad(22, 152, 8, 32, 256, 256 )
Offsets["Lightning2"] = { 0, 0, 8, 32 }

Quads["Lightning3"] = love.graphics.newQuad(22, 186, 8, 32, 256, 256 )
Offsets["Lightning3"] = { 0, 0, 8, 32 }

Quads["NearestCreatureIcon"] = love.graphics.newQuad(147, 192, 8, 8, 256, 256 )
Offsets["NearestCreatureIcon"] = { 0, 0, 8, 8 }

Quads["NearestPortalIcon"] = love.graphics.newQuad(126, 192, 9, 9, 256, 256 )
Offsets["NearestPortalIcon"] = { 0, 0, 9, 9 }

Quads["Negate0"] = love.graphics.newQuad(94, 74, 16, 16, 256, 256 )
Offsets["Negate0"] = { 0, 0, 16, 16 }

Quads["Negate1"] = love.graphics.newQuad(94, 92, 16, 16, 256, 256 )
Offsets["Negate1"] = { 0, 0, 16, 16 }

Quads["Negate2"] = love.graphics.newQuad(94, 110, 16, 16, 256, 256 )
Offsets["Negate2"] = { 0, 0, 16, 16 }

Quads["Ogre1"] = love.graphics.newQuad(94, 128, 16, 16, 256, 256 )
Offsets["Ogre1"] = { 0, 0, 16, 16 }

Quads["Ogre2"] = love.graphics.newQuad(100, 146, 16, 16, 256, 256 )
Offsets["Ogre2"] = { 0, 0, 16, 16 }

Quads["Ogre3"] = love.graphics.newQuad(114, 174, 16, 16, 256, 256 )
Offsets["Ogre3"] = { 0, 0, 16, 16 }

Quads["Ogre4"] = love.graphics.newQuad(114, 174, 16, 16, 256, 256 )
Offsets["Ogre4"] = { 0, 0, 16, 16 }

Quads["Ogre5"] = love.graphics.newQuad(112, 38, 16, 16, 256, 256 )
Offsets["Ogre5"] = { 0, 0, 16, 16 }

Quads["Ogre6"] = love.graphics.newQuad(114, 2, 16, 16, 256, 256 )
Offsets["Ogre6"] = { 0, 0, 16, 16 }

Quads["Pickup1"] = love.graphics.newQuad(167, 68, 7, 8, 256, 256 )
Offsets["Pickup1"] = { 0, 0, 7, 8 }

Quads["Pickup10"] = love.graphics.newQuad(156, 71, 7, 8, 256, 256 )
Offsets["Pickup10"] = { 0, 0, 7, 8 }

Quads["Pickup11"] = love.graphics.newQuad(242, 58, 7, 8, 256, 256 )
Offsets["Pickup11"] = { 0, 0, 7, 8 }

Quads["Pickup12"] = love.graphics.newQuad(242, 68, 7, 8, 256, 256 )
Offsets["Pickup12"] = { 0, 0, 7, 8 }

Quads["Pickup13"] = love.graphics.newQuad(148, 131, 7, 8, 256, 256 )
Offsets["Pickup13"] = { 0, 0, 7, 8 }

Quads["Pickup14"] = love.graphics.newQuad(157, 131, 7, 8, 256, 256 )
Offsets["Pickup14"] = { 0, 0, 7, 8 }

Quads["Pickup15"] = love.graphics.newQuad(167, 189, 7, 8, 256, 256 )
Offsets["Pickup15"] = { 0, 0, 7, 8 }

Quads["Pickup16"] = love.graphics.newQuad(176, 189, 7, 8, 256, 256 )
Offsets["Pickup16"] = { 0, 0, 7, 8 }

Quads["Pickup17"] = love.graphics.newQuad(176, 70, 7, 8, 256, 256 )
Offsets["Pickup17"] = { 0, 0, 7, 8 }

Quads["Pickup18"] = love.graphics.newQuad(166, 129, 7, 8, 256, 256 )
Offsets["Pickup18"] = { 0, 0, 7, 8 }

Quads["Pickup19"] = love.graphics.newQuad(175, 129, 7, 8, 256, 256 )
Offsets["Pickup19"] = { 0, 0, 7, 8 }

Quads["Pickup2"] = love.graphics.newQuad(188, 70, 7, 8, 256, 256 )
Offsets["Pickup2"] = { 0, 0, 7, 8 }

Quads["Pickup20"] = love.graphics.newQuad(188, 80, 7, 8, 256, 256 )
Offsets["Pickup20"] = { 0, 0, 7, 8 }

Quads["Pickup21"] = love.graphics.newQuad(230, 70, 7, 8, 256, 256 )
Offsets["Pickup21"] = { 0, 0, 7, 8 }

Quads["Pickup22"] = love.graphics.newQuad(230, 80, 7, 8, 256, 256 )
Offsets["Pickup22"] = { 0, 0, 7, 8 }

Quads["Pickup23"] = love.graphics.newQuad(230, 90, 7, 8, 256, 256 )
Offsets["Pickup23"] = { 0, 0, 7, 8 }

Quads["Pickup24"] = love.graphics.newQuad(188, 90, 7, 8, 256, 256 )
Offsets["Pickup24"] = { 0, 0, 7, 8 }

Quads["Pickup25"] = love.graphics.newQuad(180, 112, 7, 8, 256, 256 )
Offsets["Pickup25"] = { 0, 0, 7, 8 }

Quads["Pickup26"] = love.graphics.newQuad(180, 100, 7, 8, 256, 256 )
Offsets["Pickup26"] = { 0, 0, 7, 8 }

Quads["Pickup27"] = love.graphics.newQuad(184, 122, 7, 8, 256, 256 )
Offsets["Pickup27"] = { 0, 0, 7, 8 }

Quads["Pickup3"] = love.graphics.newQuad(189, 112, 7, 8, 256, 256 )
Offsets["Pickup3"] = { 0, 0, 7, 8 }

Quads["Pickup4"] = love.graphics.newQuad(189, 100, 7, 8, 256, 256 )
Offsets["Pickup4"] = { 0, 0, 7, 8 }

Quads["Pickup5"] = love.graphics.newQuad(198, 100, 7, 8, 256, 256 )
Offsets["Pickup5"] = { 0, 0, 7, 8 }

Quads["Pickup6"] = love.graphics.newQuad(198, 110, 7, 8, 256, 256 )
Offsets["Pickup6"] = { 0, 0, 7, 8 }

Quads["Pickup7"] = love.graphics.newQuad(239, 92, 7, 8, 256, 256 )
Offsets["Pickup7"] = { 0, 0, 7, 8 }

Quads["Pickup8"] = love.graphics.newQuad(230, 100, 7, 8, 256, 256 )
Offsets["Pickup8"] = { 0, 0, 7, 8 }

Quads["Pickup9"] = love.graphics.newQuad(239, 102, 7, 8, 256, 256 )
Offsets["Pickup9"] = { 0, 0, 7, 8 }

Quads["PickupFade1"] = love.graphics.newQuad(230, 110, 7, 8, 256, 256 )
Offsets["PickupFade1"] = { 0, 0, 7, 8 }

Quads["PickupFade2"] = love.graphics.newQuad(239, 112, 7, 8, 256, 256 )
Offsets["PickupFade2"] = { 0, 0, 7, 8 }

Quads["PickupFade3"] = love.graphics.newQuad(186, 132, 7, 8, 256, 256 )
Offsets["PickupFade3"] = { 0, 0, 7, 8 }

Quads["PickupFade4"] = love.graphics.newQuad(193, 122, 7, 8, 256, 256 )
Offsets["PickupFade4"] = { 0, 0, 7, 8 }

Quads["Player0"] = love.graphics.newQuad(114, 20, 16, 16, 256, 256 )
Offsets["Player0"] = { 0, 0, 16, 16 }

Quads["Player1"] = love.graphics.newQuad(112, 56, 16, 16, 256, 256 )
Offsets["Player1"] = { 0, 0, 16, 16 }

Quads["Player2"] = love.graphics.newQuad(112, 74, 16, 16, 256, 256 )
Offsets["Player2"] = { 0, 0, 16, 16 }

Quads["Player3"] = love.graphics.newQuad(112, 92, 16, 16, 256, 256 )
Offsets["Player3"] = { 0, 0, 16, 16 }

Quads["Player4"] = love.graphics.newQuad(112, 110, 16, 16, 256, 256 )
Offsets["Player4"] = { 0, 0, 16, 16 }

Quads["Player5"] = love.graphics.newQuad(112, 128, 16, 16, 256, 256 )
Offsets["Player5"] = { 0, 0, 16, 16 }

Quads["Player6"] = love.graphics.newQuad(118, 146, 16, 16, 256, 256 )
Offsets["Player6"] = { 0, 0, 16, 16 }

Quads["Player7"] = love.graphics.newQuad(132, 174, 16, 16, 256, 256 )
Offsets["Player7"] = { 0, 0, 16, 16 }

Quads["Player8"] = love.graphics.newQuad(144, 202, 16, 16, 256, 256 )
Offsets["Player8"] = { 0, 0, 16, 16 }

Quads["PlayerBox8x8"] = love.graphics.newQuad(157, 190, 8, 8, 256, 256 )
Offsets["PlayerBox8x8"] = { 0, 0, 8, 8 }

Quads["PlayerInDanger"] = love.graphics.newQuad(144, 220, 16, 16, 256, 256 )
Offsets["PlayerInDanger"] = { 0, 0, 16, 16 }

Quads["PlayerUpgraded"] = love.graphics.newQuad(144, 238, 16, 16, 256, 256 )
Offsets["PlayerUpgraded"] = { 0, 0, 16, 16 }

Quads["Popper1"] = love.graphics.newQuad(226, 38, 14, 14, 256, 256 )
Offsets["Popper1"] = { 0, 0, 14, 14 }

Quads["Popper2"] = love.graphics.newQuad(178, 54, 14, 14, 256, 256 )
Offsets["Popper2"] = { 0, 0, 14, 14 }

Quads["Popper3"] = love.graphics.newQuad(194, 54, 14, 14, 256, 256 )
Offsets["Popper3"] = { 0, 0, 14, 14 }

Quads["Popper4"] = love.graphics.newQuad(210, 54, 14, 14, 256, 256 )
Offsets["Popper4"] = { 0, 0, 14, 14 }

Quads["Popper5"] = love.graphics.newQuad(226, 54, 14, 14, 256, 256 )
Offsets["Popper5"] = { 0, 0, 14, 14 }

Quads["Popper6"] = love.graphics.newQuad(148, 83, 14, 14, 256, 256 )
Offsets["Popper6"] = { 0, 0, 14, 14 }

Quads["Popper7"] = love.graphics.newQuad(148, 83, 14, 14, 256, 256 )
Offsets["Popper7"] = { 0, 0, 14, 14 }

Quads["Portal1"] = love.graphics.newQuad(162, 2, 16, 16, 256, 256 )
Offsets["Portal1"] = { 0, 0, 16, 16 }

Quads["Portal2"] = love.graphics.newQuad(132, 2, 16, 16, 256, 256 )
Offsets["Portal2"] = { 0, 0, 16, 16 }

Quads["Portal3"] = love.graphics.newQuad(132, 20, 16, 16, 256, 256 )
Offsets["Portal3"] = { 0, 0, 16, 16 }

Quads["QuestionMark"] = love.graphics.newQuad(150, 2, 8, 11, 256, 256 )
Offsets["QuestionMark"] = { 0, 0, 8, 11 }

Quads["QuestionMark1"] = love.graphics.newQuad(150, 15, 8, 11, 256, 256 )
Offsets["QuestionMark1"] = { 0, 0, 8, 11 }

Quads["QuestionMark2"] = love.graphics.newQuad(150, 28, 8, 11, 256, 256 )
Offsets["QuestionMark2"] = { 0, 0, 8, 11 }

Quads["Rocket"] = love.graphics.newQuad(167, 38, 8, 8, 256, 256 )
Offsets["Rocket"] = { 0, 0, 8, 8 }

Quads["Shadow0"] = love.graphics.newQuad(180, 2, 16, 16, 256, 256 )
Offsets["Shadow0"] = { 0, 0, 16, 16 }

Quads["Shield1"] = love.graphics.newQuad(32, 152, 24, 24, 256, 256 )
Offsets["Shield1"] = { 0, 0, 24, 24 }

Quads["Shield2"] = love.graphics.newQuad(32, 178, 24, 24, 256, 256 )
Offsets["Shield2"] = { 0, 0, 24, 24 }

Quads["Shield3"] = love.graphics.newQuad(52, 2, 24, 24, 256, 256 )
Offsets["Shield3"] = { 0, 0, 24, 24 }

Quads["Shield4"] = love.graphics.newQuad(52, 28, 24, 24, 256, 256 )
Offsets["Shield4"] = { 0, 0, 24, 24 }

Quads["Siren"] = love.graphics.newQuad(198, 2, 16, 16, 256, 256 )
Offsets["Siren"] = { 0, 0, 16, 16 }

Quads["Siren2"] = love.graphics.newQuad(216, 2, 16, 16, 256, 256 )
Offsets["Siren2"] = { 0, 0, 16, 16 }

Quads["Siren3"] = love.graphics.newQuad(234, 2, 16, 16, 256, 256 )
Offsets["Siren3"] = { 0, 0, 16, 16 }

Quads["Siren4"] = love.graphics.newQuad(162, 20, 16, 16, 256, 256 )
Offsets["Siren4"] = { 0, 0, 16, 16 }

Quads["Skull"] = love.graphics.newQuad(136, 38, 12, 12, 256, 256 )
Offsets["Skull"] = { 0, 0, 12, 12 }

Quads["SpecialChest01"] = love.graphics.newQuad(214, 84, 14, 12, 256, 256 )
Offsets["SpecialChest01"] = { 0, 0, 14, 12 }

Quads["SpecialChest02"] = love.graphics.newQuad(198, 86, 14, 12, 256, 256 )
Offsets["SpecialChest02"] = { 0, 0, 14, 12 }

Quads["SpecialChest03"] = love.graphics.newQuad(214, 98, 14, 12, 256, 256 )
Offsets["SpecialChest03"] = { 0, 0, 14, 12 }

Quads["Spider"] = love.graphics.newQuad(180, 20, 16, 16, 256, 256 )
Offsets["Spider"] = { 0, 0, 16, 16 }

Quads["Spider2"] = love.graphics.newQuad(198, 20, 16, 16, 256, 256 )
Offsets["Spider2"] = { 0, 0, 16, 16 }

Quads["SpinningBlade"] = love.graphics.newQuad(52, 54, 22, 22, 256, 256 )
Offsets["SpinningBlade"] = { 0, 0, 22, 22 }

Quads["SwordSlash0"] = love.graphics.newQuad(76, 174, 36, 16, 256, 256 )
Offsets["SwordSlash0"] = { 0, 0, 36, 16 }

Quads["SwordSlashUpgrade0"] = love.graphics.newQuad(22, 220, 48, 24, 256, 256 )
Offsets["SwordSlashUpgrade0"] = { 0, 0, 48, 24 }

Quads["Swordsman1"] = love.graphics.newQuad(148, 99, 14, 14, 256, 256 )
Offsets["Swordsman1"] = { 0, 0, 14, 14 }

Quads["Swordsman2"] = love.graphics.newQuad(148, 115, 14, 14, 256, 256 )
Offsets["Swordsman2"] = { 0, 0, 14, 14 }

Quads["Swordsman3"] = love.graphics.newQuad(154, 141, 14, 14, 256, 256 )
Offsets["Swordsman3"] = { 0, 0, 14, 14 }

Quads["Swordsman4"] = love.graphics.newQuad(156, 157, 14, 14, 256, 256 )
Offsets["Swordsman4"] = { 0, 0, 14, 14 }

Quads["TargetCursor"] = love.graphics.newQuad(52, 78, 22, 22, 256, 256 )
Offsets["TargetCursor"] = { 0, 0, 22, 22 }

Quads["TargetCursor1"] = love.graphics.newQuad(52, 102, 22, 22, 256, 256 )
Offsets["TargetCursor1"] = { 0, 0, 22, 22 }

Quads["TargetCursor2"] = love.graphics.newQuad(52, 126, 22, 22, 256, 256 )
Offsets["TargetCursor2"] = { 0, 0, 22, 22 }

Quads["TargetCursor3"] = love.graphics.newQuad(58, 150, 22, 22, 256, 256 )
Offsets["TargetCursor3"] = { 0, 0, 22, 22 }

Quads["Warp1"] = love.graphics.newQuad(216, 20, 16, 16, 256, 256 )
Offsets["Warp1"] = { 0, 0, 16, 16 }

Quads["Warp2"] = love.graphics.newQuad(234, 20, 16, 16, 256, 256 )
Offsets["Warp2"] = { 0, 0, 16, 16 }

Quads["Warp3"] = love.graphics.newQuad(130, 52, 16, 16, 256, 256 )
Offsets["Warp3"] = { 0, 0, 16, 16 }

Quads["Warp4"] = love.graphics.newQuad(130, 70, 16, 16, 256, 256 )
Offsets["Warp4"] = { 0, 0, 16, 16 }

Quads["Wraith"] = love.graphics.newQuad(130, 88, 16, 16, 256, 256 )
Offsets["Wraith"] = { 0, 0, 16, 16 }

Quads["Wraith2"] = love.graphics.newQuad(130, 106, 16, 16, 256, 256 )
Offsets["Wraith2"] = { 0, 0, 16, 16 }

Quads["Wraith3"] = love.graphics.newQuad(130, 124, 16, 16, 256, 256 )
Offsets["Wraith3"] = { 0, 0, 16, 16 }

Quads["Wraith4"] = love.graphics.newQuad(136, 142, 16, 16, 256, 256 )
Offsets["Wraith4"] = { 0, 0, 16, 16 }

Quads["Zombie1"] = love.graphics.newQuad(166, 173, 14, 14, 256, 256 )
Offsets["Zombie1"] = { 0, 0, 14, 14 }

Quads["Zombie2"] = love.graphics.newQuad(164, 81, 14, 14, 256, 256 )
Offsets["Zombie2"] = { 0, 0, 14, 14 }

Quads["Zombie3"] = love.graphics.newQuad(164, 97, 14, 14, 256, 256 )
Offsets["Zombie3"] = { 0, 0, 14, 14 }

Quads["Zombie4"] = love.graphics.newQuad(164, 113, 14, 14, 256, 256 )
Offsets["Zombie4"] = { 0, 0, 14, 14 }

Quads["Zombie5"] = love.graphics.newQuad(170, 139, 14, 14, 256, 256 )
Offsets["Zombie5"] = { 0, 0, 14, 14 }

Quads["Zombie6"] = love.graphics.newQuad(172, 155, 14, 14, 256, 256 )
Offsets["Zombie6"] = { 0, 0, 14, 14 }

Quads["Zombie7"] = love.graphics.newQuad(182, 171, 14, 14, 256, 256 )
Offsets["Zombie7"] = { 0, 0, 14, 14 }

Quads["Zombie8"] = love.graphics.newQuad(198, 70, 14, 14, 256, 256 )
Offsets["Zombie8"] = { 0, 0, 14, 14 }


function TextureAtlas:getDimensions(quadName)
	local quad = self.quads[quadName]
	if not quad then
		return nil 
	end
	local x, y, w, h = quad:getViewport()
    return w, h
end

function TextureAtlas:getCenterOffsetXY(quadName)
	local quad = self.quads[quadName]
	if not quad then return nil end
	local offset = self.offsets[quadName]
	if not offset then return nil end

	local x, y, w, h = quad:getViewport()
	
	local ox, oy, ow, oh = offset[1], offset[2], offset[3], offset[4]
	local hw = ow / 2
	local hh = oh / 2
	hw = hw - ox
	hh = hh - oy
    return hw, hh
end


TextureAtlas.offsets = Offsets
TextureAtlas.quads = Quads
TextureAtlas.texture = Texture

return TextureAtlas
