-- Generated with TexturePacker (http://www.codeandweb.com/texturepacker)
-- with a custom export by Stewart Bracken (http://stewart.bracken.bz)
--
-- $TexturePacker:SmartUpdate:9f9a55755a89dad2777ef7c551351843:89129cc6cd7ee3af253b9b08903b72d8:64b1efae70cfdf3b8ad901c3c01cb45f$
--
--[[------------------------------------------------------------------------
-- Example Usage --

function love.load()
	myAtlas = require("tinyfigures0")
	batch = love.graphics.newSpriteBatch( myAtlas.texture, 100, "stream" )
end
function love.draw()
	batch:clear()
	batch:bind()
		batch:add( myAtlas.quads['mySpriteName'], love.mouse.getX(), love.mouse.getY() )
	batch:unbind()
	love.graphics.draw(batch)
end

--]]------------------------------------------------------------------------

local TextureAtlas = {}
local Quads = {}
local Offsets = {}
local Texture = love.graphics.newImage( "assets/art/" .. "tinyfigures0.png" )
Texture:setFilter("nearest", "nearest")


Quads["0"] = love.graphics.newQuad(2, 2, 4, 7, 32, 32 )
Offsets["0"] = { 0, 0, 4, 7 }

Quads["1"] = love.graphics.newQuad(14, 11, 3, 7, 32, 32 )
Offsets["1"] = { 0, 0, 4, 7 }

Quads["2"] = love.graphics.newQuad(2, 11, 4, 7, 32, 32 )
Offsets["2"] = { 0, 0, 4, 7 }

Quads["3"] = love.graphics.newQuad(2, 20, 4, 7, 32, 32 )
Offsets["3"] = { 0, 0, 4, 7 }

Quads["4"] = love.graphics.newQuad(8, 2, 4, 7, 32, 32 )
Offsets["4"] = { 0, 0, 4, 7 }

Quads["5"] = love.graphics.newQuad(8, 11, 4, 7, 32, 32 )
Offsets["5"] = { 0, 0, 4, 7 }

Quads["6"] = love.graphics.newQuad(8, 20, 4, 7, 32, 32 )
Offsets["6"] = { 0, 0, 4, 7 }

Quads["7"] = love.graphics.newQuad(14, 2, 4, 7, 32, 32 )
Offsets["7"] = { 0, 0, 4, 7 }

Quads["8"] = love.graphics.newQuad(20, 2, 4, 7, 32, 32 )
Offsets["8"] = { 0, 0, 4, 7 }

Quads["9"] = love.graphics.newQuad(26, 2, 4, 7, 32, 32 )
Offsets["9"] = { 0, 0, 4, 7 }


function TextureAtlas:getDimensions(quadName)
	local quad = self.quads[quadName]
	if not quad then
		return nil 
	end
	local x, y, w, h = quad:getViewport()
    return w, h
end

function TextureAtlas:getCenterOffsetXY(quadName)
	local quad = self.quads[quadName]
	if not quad then return nil end
	local offset = self.offsets[quadName]
	if not offset then return nil end

	local x, y, w, h = quad:getViewport()
	
	local ox, oy, ow, oh = offset[1], offset[2], offset[3], offset[4]
	local hw = ow / 2
	local hh = oh / 2
	hw = hw - ox
	hh = hh - oy
    return hw, hh
end


TextureAtlas.offsets = Offsets
TextureAtlas.quads = Quads
TextureAtlas.texture = Texture

return TextureAtlas
