-- Generated with TexturePacker (http://www.codeandweb.com/texturepacker)
-- with a custom export by Stewart Bracken (http://stewart.bracken.bz)
--
-- $TexturePacker:SmartUpdate:6289b63db73d6448874a61bd6ecd1943:d3b3cf22129d8943ab643f272db74c8f:02b817e3a8464cb5472a8fb752e77c98$
--
--[[------------------------------------------------------------------------
-- Example Usage --

function love.load()
	myAtlas = require("tileset0")
	batch = love.graphics.newSpriteBatch( myAtlas.texture, 100, "stream" )
end
function love.draw()
	batch:clear()
	batch:bind()
		batch:add( myAtlas.quads['mySpriteName'], love.mouse.getX(), love.mouse.getY() )
	batch:unbind()
	love.graphics.draw(batch)
end

--]]------------------------------------------------------------------------

local TextureAtlas = {}
local Quads = {}
local Offsets = {}
local Texture = love.graphics.newImage( "assets/art/" .. "tileset0.png" )
Texture:setFilter("nearest", "nearest")


Quads["Tile1"] = love.graphics.newQuad(2, 2, 16, 16, 128, 128 )
Offsets["Tile1"] = { 0, 0, 16, 16 }

Quads["Tile10"] = love.graphics.newQuad(2, 20, 16, 16, 128, 128 )
Offsets["Tile10"] = { 0, 0, 16, 16 }

Quads["Tile11"] = love.graphics.newQuad(2, 38, 16, 16, 128, 128 )
Offsets["Tile11"] = { 0, 0, 16, 16 }

Quads["Tile12"] = love.graphics.newQuad(2, 56, 16, 16, 128, 128 )
Offsets["Tile12"] = { 0, 0, 16, 16 }

Quads["Tile13"] = love.graphics.newQuad(2, 74, 16, 16, 128, 128 )
Offsets["Tile13"] = { 0, 0, 16, 16 }

Quads["Tile14"] = love.graphics.newQuad(2, 92, 16, 16, 128, 128 )
Offsets["Tile14"] = { 0, 0, 16, 16 }

Quads["Tile15"] = love.graphics.newQuad(2, 110, 16, 16, 128, 128 )
Offsets["Tile15"] = { 0, 0, 16, 16 }

Quads["Tile16"] = love.graphics.newQuad(20, 2, 16, 16, 128, 128 )
Offsets["Tile16"] = { 0, 0, 16, 16 }

Quads["Tile17"] = love.graphics.newQuad(38, 2, 16, 16, 128, 128 )
Offsets["Tile17"] = { 0, 0, 16, 16 }

Quads["Tile18"] = love.graphics.newQuad(56, 2, 16, 16, 128, 128 )
Offsets["Tile18"] = { 0, 0, 16, 16 }

Quads["Tile19"] = love.graphics.newQuad(74, 2, 16, 16, 128, 128 )
Offsets["Tile19"] = { 0, 0, 16, 16 }

Quads["Tile2"] = love.graphics.newQuad(92, 2, 16, 16, 128, 128 )
Offsets["Tile2"] = { 0, 0, 16, 16 }

Quads["Tile20"] = love.graphics.newQuad(110, 2, 16, 16, 128, 128 )
Offsets["Tile20"] = { 0, 0, 16, 16 }

Quads["Tile21"] = love.graphics.newQuad(20, 20, 16, 16, 128, 128 )
Offsets["Tile21"] = { 0, 0, 16, 16 }

Quads["Tile22"] = love.graphics.newQuad(20, 38, 16, 16, 128, 128 )
Offsets["Tile22"] = { 0, 0, 16, 16 }

Quads["Tile23"] = love.graphics.newQuad(20, 56, 16, 16, 128, 128 )
Offsets["Tile23"] = { 0, 0, 16, 16 }

Quads["Tile24"] = love.graphics.newQuad(20, 74, 16, 16, 128, 128 )
Offsets["Tile24"] = { 0, 0, 16, 16 }

Quads["Tile25"] = love.graphics.newQuad(20, 92, 16, 16, 128, 128 )
Offsets["Tile25"] = { 0, 0, 16, 16 }

Quads["Tile26"] = love.graphics.newQuad(20, 110, 16, 16, 128, 128 )
Offsets["Tile26"] = { 0, 0, 16, 16 }

Quads["Tile3"] = love.graphics.newQuad(38, 20, 16, 16, 128, 128 )
Offsets["Tile3"] = { 0, 0, 16, 16 }

Quads["Tile4"] = love.graphics.newQuad(56, 20, 16, 16, 128, 128 )
Offsets["Tile4"] = { 0, 0, 16, 16 }

Quads["Tile5"] = love.graphics.newQuad(74, 20, 16, 16, 128, 128 )
Offsets["Tile5"] = { 0, 0, 16, 16 }

Quads["Tile6"] = love.graphics.newQuad(92, 20, 16, 16, 128, 128 )
Offsets["Tile6"] = { 0, 0, 16, 16 }

Quads["Tile7"] = love.graphics.newQuad(110, 20, 16, 16, 128, 128 )
Offsets["Tile7"] = { 0, 0, 16, 16 }

Quads["Tile8"] = love.graphics.newQuad(38, 38, 16, 16, 128, 128 )
Offsets["Tile8"] = { 0, 0, 16, 16 }

Quads["Tile9"] = love.graphics.newQuad(38, 56, 16, 16, 128, 128 )
Offsets["Tile9"] = { 0, 0, 16, 16 }


function TextureAtlas:getDimensions(quadName)
	local quad = self.quads[quadName]
	if not quad then
		return nil 
	end
	local x, y, w, h = quad:getViewport()
    return w, h
end

function TextureAtlas:getCenterOffsetXY(quadName)
	local quad = self.quads[quadName]
	if not quad then return nil end
	local offset = self.offsets[quadName]
	if not offset then return nil end

	local x, y, w, h = quad:getViewport()
	
	local ox, oy, ow, oh = offset[1], offset[2], offset[3], offset[4]
	local hw = ow / 2
	local hh = oh / 2
	hw = hw - ox
	hh = hh - oy
    return hw, hh
end


TextureAtlas.offsets = Offsets
TextureAtlas.quads = Quads
TextureAtlas.texture = Texture

return TextureAtlas
