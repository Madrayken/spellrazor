local class = require 'library/middleclass'

fifo = class('fifo')

function fifo:initialize()
  self.queue = {}
end

-- Pushes a value at the tail of the heap
function fifo:push(value) 
  table.insert(self.queue, value) 
end

function fifo:length()
  return #self.queue
end

-- Remove and return the value at the head of the heap
function fifo:pop()
  local head = self.queue[1]
  table.remove(self.queue, 1)
  return head
end

-- Clears the heap
function fifo:clear(f) self.queue = {} end

-- Checks if the heap is empty
function fifo:isEmpty() return #self.queue == 0 end

--return fifo