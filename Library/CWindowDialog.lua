local class = require 'library.middleclass'

CWindowDialog = class('CWindowDialog', CWindow)

function CWindowDialog:initialize(name, array_of_dialogs, text, input_triplets, default_option_index)
	CWindow.initialize(self, name, array_of_dialogs) -- initialize the parent    	

  self.text = text
  self.inputTriplets = input_triplets -- {text, key, function, optional explanation text}
  if default_option_index then
    self.cursor = default_option_index
  else
    self.cursor = 1
  end
  
	--setGlitchingForTime(1)
  
  self.top = 0
  self.height = 0
  self.width = 0
  
  self.time = 0
  self:calculateWidthAndHeight()
end
-------------------------
function CWindowDialog:keypressed(key)
	if CWindow.keypressed(self, key) == true then return true end
	
	if self.inputTriplets == nil then
		return true
	end
	
	local count = #self.inputTriplets
	if key == "up" or key == "kp8" then self.cursor = self.cursor - 1 end
	if key == "down" or key == "kp2" then self.cursor = self.cursor + 1 end
	if self.cursor > #self.inputTriplets then self.cursor = 1 end
	if self.cursor < 1 then self.cursor = #self.inputTriplets end
	  
	if key == "return" or key == "enter" or key == "kpenter" then
		local v = self.inputTriplets[self.cursor]
		if v[3] ~= nil then 	
			v[3](nil, self.cursor) -- Note - we're disallowing the passing of the table this function is part of!
		end
    self:close()
    return true
  end
  
  return true
end

function CWindowDialog:calculateWidthAndHeight()
  local lines = splitline(self.text, "\n") 
  self.top = (gCanvasHeight / 2) - self.lineHeight * (#lines + 4)
	local	y = self.top + self.lineHeight * 2
	y = y + self.lineHeight * #lines
	y = y + #self.inputTriplets * self.lineHeight
  y = y + self.lineHeight * 2

  local max_lines = 0
  for i, v in ipairs(self.inputTriplets) do
    if v[4] ~= nil then
      lines = splitline(v[4], "\n") 
      max_lines = math.max(max_lines, #lines)
    end
  end
  y = y + self.lineHeight * max_lines
  
  y = y + self.lineHeight * 2
  self.height = y - self.top
  self.top = gCanvasHeight /2 - self.height / 2
  self.width = 300
end

-------------------------
function CWindowDialog:draw()
  self.time = self.time + love.timer.getDelta()
  love.graphics.setFont(bigFont)
	love.graphics.setColor(5,0,0,255)
  local width = gCanvasWidth * 0.6
  local height = gCanvasWidth * 0.
	love.graphics.rectangle("fill", 2 * (gCanvasWidth / 2 - self.width / 2), 2 * self.top, 2 * self.width, 2 * self.height) 
  
  local r1, g1, b1 = HSL(((self.time) % 1) / 1 * 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)

	love.graphics.setColor(r1,g1,b1,255)
  love.graphics.setLineWidth(2)
  love.graphics.rectangle("line", 2 * (gCanvasWidth / 2 - self.width / 2), 2 * self.top, 2 * self.width, 2 * self.height) 
  love.graphics.setLineWidth(1)

	
  local wid = 1000
  local lines = splitline(self.text, "\n") 
	local	y = self.top + self.lineHeight * 2

	love.graphics.setColor(gColourList[CYAN_INDEX + 1])

	for i, v in ipairs(lines) do		
		love.graphics.printf(v, 2 * (-(wid / 2) + (gCanvasWidth / 2)), 2 * y, 2 * wid, 'center')
		y = y + self.lineHeight
	end
	
--	y = y + self.lineHeight * 
	local str = ""
	local number_of_options = #self.inputTriplets
	for i, v in ipairs(self.inputTriplets) do
		str = v[1]
		y = y + self.lineHeight
		if self.cursor == i then
			love.graphics.setColor(255, 255, 255)
			str = "> " .. str .. " <" 
		else
			love.graphics.setColor(0,128,255)
		end
		love.graphics.printf(str, 2 * (-(wid / 2) + (gCanvasWidth / 2)), 2 * y, 2 * wid, 'center')		
	end
  
  y = y + self.lineHeight * 2

  love.graphics.setColor(255, 255, 255)
  if self.inputTriplets[self.cursor][4] then
    lines = splitline(self.inputTriplets[self.cursor][4], "\n") 

    for i, v in ipairs(lines) do		
      love.graphics.printf(v, 2 * (-(wid / 2) + (gCanvasWidth / 2)), 2 * y, 2 * wid, 'center')
      y = y + self.lineHeight
    end
  end
	love.graphics.setColor(255, 255, 255)
end



