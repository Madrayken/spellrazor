-- Tools
----------------------------
gAround4Cardinal = {{0,-1}, {1,0}, {0,1}, {-1,0}} 
gAround4Diagonal = {{-1,-1}, {1,-1}, {1,1}, {-1,1}} 
gAround8 = {{-1,-1}, {0,-1},  {1,-1}, {1,0}, {1,1}, {0,1}, {-1,1}, {-1,0}} 


gToolRandomNumbers = {}
gToolRandomNumberPointer = 1
function setupRandomNumbers(n)
	for i = 1, n do
		gToolRandomNumbers[i] = math.random()
	end
end

function getRandomNumber()
	local r = gToolRandomNumbers[gToolRandomNumberPointer]
	gToolRandomNumberPointer = gToolRandomNumberPointer + 1
	if gToolRandomNumberPointer > #gToolRandomNumbers then gToolRandomNumberPointer = 1 end
end

function getFirstMemberAndRemove( t )
  print("Removing first member: " .. t)
	if t == nil then print("table is null") end
	local c = t[1]
	table.remove(t,1)
end

function getRandomMember( t )
    if t == nil then print("Variable is null") end
    local choice = nil
    local n = 0
    for i, o in pairs(t) do
			n = n + 1
			if math.random() < (1/n) then
				choice = o      
			end
    end
    return choice 
end

function getRandomArrayMemberAndRemove_inplace(t)
	local i = math.random(#t)
	local r = deepcopy(t[i])
	table.remove(t, i)
	return r
end


function getRandomTableMemberAndRemove_inplace( t )
    local choice = nil
    local n = 0
    key = nil
    for i, o in pairs(t) do
        n = n + 1
        if math.random() < (1/n) then
            choice = o      
            key = i
        end
    end
    
    if key ~= nil then
        t[key] = nil
    end
    
    return choice 
end


function getRandomisedArray(list)
    t = deepcopy(list)
    r = {}
    
    for i = 1, #list do
        table.insert(r, getRandomTableMemberAndRemove_inplace(t))
    end
    
    return r
end

function randomiseTable_inplace(tab)
  local keys = {}
	local vals = {}
	for k, v in pairs(tab) do
		table.insert(keys, k)
		table.insert(vals, v)
	end
	keys = getRandomisedArray(keys)
  for i, v in ipairs(keys) do
    tab[v] = vals[i]
  end
end


function getLengthOfTable(t)
    if t == nil then return nil end
    
    length = 0
    for k, v in pairs(t) do
        length = length + 1
    end
    return length    
end

function removeFirstMemberWithValue_inplace(tab, rm)    
	if tab == nil then love.errhand("Passing a null table") end
	local index = getIndexOf(tab, rm)
	if index ~= nil then
		table.remove(tab, index) -- remove by value
	end	
end

function getDice(count, type_of_dice)
	if type_of_dice == nil  then type_of_dice = 6  end
    local total = 0
    for i = 0, count do    
        total = total + math.random(1,type_of_dice)
    end
    return total
end


function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function insertItemIfNotPresent(t, val, pos)
	if pos == nil then
		if getIndexOf(t, val) == nil then
			table.insert(t, val)
		end
	else
		if getIndexOf(t, val) == nil then
			table.insert(t, pos, val)
		end
	end
end


-- return the first integer index holding the value 
function getIndexOf(t,val)
    if t == nil then return nil end
    
    for k,v in ipairs(t) do 
--        print("Looking for " .. val .." Found: " .. v)
        if v == val then 
--            print("Found " .. val .. " at " .. k)
            
            assert(type(k) == "number", "The table getIndex is pointed to has non_numeric keys. Use 'getKeyOf' instead")
            return k 
        end
    end
    return nil
end

-- return any key holding the value 
function getKeyOf(t,val)
    for k,v in pairs(t) do 
        if v == val then return k end
    end
    return nil
end

-- return all keys
function getAllKeys(t)
    s = {}
    for k,v in pairs(t) do table.insert(s, k) end
    return s
end

-- return all keys holding the value
function getAllKeysOf(t,val)
    local s={}
    for k,v in pairs(t) do 
        if v == val then s[#s+1] = k end
    end
    return s
end

-- invert a table so that each value is the key holding one key to that value 
-- in the original table.
function invertTable(t)
    local i={}
    for k,v in pairs(t) do 
        i[v] = k
    end
    return i
end

function roundRect(x,y,w,h,r)
    pushStyle()
    font(GLOBAL_FONT)
    fontSize(FONT_SIZE)
    noSmooth()

    insetPos = vec2(x+r,y+r)
    insetSize = vec2(w-2*r,h-2*r)
    
    --Copy fill into stroke
    local red,green,blue,a = fill()
    stroke(red,green,blue,a)
    
    rectMode(CORNER)
    rect(insetPos.x,insetPos.y,insetSize.x,insetSize.y)
    
    if r > 0 then
        smooth()
        lineCapMode(ROUND)
        strokeWidth(r*2)

        line(insetPos.x, insetPos.y, 
             insetPos.x + insetSize.x, insetPos.y)
        line(insetPos.x, insetPos.y,
             insetPos.x, insetPos.y + insetSize.y)
        line(insetPos.x, insetPos.y + insetSize.y,
             insetPos.x + insetSize.x, insetPos.y + insetSize.y)
        line(insetPos.x + insetSize.x, insetPos.y,
             insetPos.x + insetSize.x, insetPos.y + insetSize.y)            
    end
    popStyle()
end

function math.averageAngles(...)
	local x,y = 0,0
	for i=1,select('#',...) do local a= select(i,...) x, y = x+math.cos(a), y+math.sin(a) end
	return math.atan2(y, x)
end
 
 
-- Returns the distance between two points.
function math.dist(x1,y1, x2,y2) return ((x2-x1)^2+(y2-y1)^2)^0.5 end
function math.dist2(x1,y1, x2,y2) return ((x2-x1)^2+(y2-y1)^2) end

-- Distance between two 3D points:
function math.dist(x1,y1,z1, x2,y2,z2) return ((x2-x1)^2+(y2-y1)^2+(z2-z1)^2)^0.5 end
 
 
-- Returns the angle between two points.
function math.angle(x1,y1, x2,y2) return math.atan2(y2-y1, x2-x1) end
 
 
-- Returns the closest multiple of 'size' (defaulting to 10).
function math.multiple(n, size) size = size or 10 return math.round(n/size)*size end
 
 
-- Clamps a number to within a certain range.
function math.clamp(low, n, high) return math.min(math.max(low, n), high) end
 
 
-- Linear interpolation between two numbers.
function lerp(a,b,t) return (1-t)*a + t*b end
function lerp2(a,b,t) return a+(b-a)*t end
 
-- Cosine interpolation between two numbers.
function cerp(a,b,t) local f=(1-math.cos(t*math.pi))*.5 return a*(1-f)+b*f end
 
 
-- Normalize two numbers.
function math.normalize(x,y) local l=(x*x+y*y)^.5 if l==0 then return 0,0,0 else return x/l,y/l,l end end
 
 
-- Returns 'n' rounded to the nearest 'deci'th (defaulting whole numbers).
function math.round(n, deci) deci = 10^(deci or 0) return math.floor(n*deci+.5)/deci end
 
 
-- Randomly returns either -1 or 1.
function math.rsign() return math.random(2) == 2 and 1 or -1 end
 
 
-- Returns 1 if number is positive, -1 if it's negative, or 0 if it's 0.
function math.sign(n) return n>0 and 1 or n<0 and -1 or 0 end
 
 
-- Gives a precise random decimal number given a minimum and maximum
function math.prandom(min, max) return math.random() * (max - min) + min end
 
 
-- Checks if two line segments intersect. Line segments are given in form of ({x,y},{x,y}, {x,y},{x,y}).
function checkIntersect(l1p1, l1p2, l2p1, l2p2)
	local function checkDir(pt1, pt2, pt3) return math.sign(((pt2.x-pt1.x)*(pt3.y-pt1.y)) - ((pt3.x-pt1.x)*(pt2.y-pt1.y))) end
	return (checkDir(l1p1,l1p2,l2p1) ~= checkDir(l1p1,l1p2,l2p2)) and (checkDir(l2p1,l2p2,l1p1) ~= checkDir(l2p1,l2p2,l1p2))
end
 
-- Checks if two lines intersect (or line segments if seg is true)
-- Lines are given as four numbers (two coordinates)
function findIntersect(l1p1x,l1p1y, l1p2x,l1p2y, l2p1x,l2p1y, l2p2x,l2p2y, seg1, seg2)
	local a1,b1,a2,b2 = l1p2y-l1p1y, l1p1x-l1p2x, l2p2y-l2p1y, l2p1x-l2p2x
	local c1,c2 = a1*l1p1x+b1*l1p1y, a2*l2p1x+b2*l2p1y
	local det,x,y = a1*b2 - a2*b1
	if det==0 then return false, "The lines are parallel." end
	x,y = (b2*c1-b1*c2)/det, (a1*c2-a2*c1)/det
	if seg1 or seg2 then
		local min,max = math.min, math.max
		if seg1 and not (min(l1p1x,l1p2x) <= x and x <= max(l1p1x,l1p2x) and min(l1p1y,l1p2y) <= y and y <= max(l1p1y,l1p2y)) or
		   seg2 and not (min(l2p1x,l2p2x) <= x and x <= max(l2p1x,l2p2x) and min(l2p1y,l2p2y) <= y and y <= max(l2p1y,l2p2y)) then
			return false, "The lines don't intersect."
		end
	end
	return x,y
end

function getClosestPointToLine( A,  B,  P, segmentClamp )
    local AP = { x=P.x - A.x, y=P.y - A.y }
    local AB = { x=B.x - A.x, y=B.y - A.y }
    local ab2 = AB.x*AB.x + AB.y*AB.y
    local ap_ab = AP.x*AB.x + AP.y*AB.y
    local t = ap_ab / ab2
 
    if (segmentClamp or true) then
         if (t < 0.0) then
                t = 0.0
         elseif (t > 1.0) then
                t = 1.0
         end
    end
 
    local Closest = { x=A.x + AB.x * t, y=A.y + AB.y * t }
 
    return Closest
end






function randomIntNormalBetween(min,max)
  local v =  min + math.floor((max - min) * 0.5 * (1 + math.sqrt(-2 * math.log(math.random())) * math.cos(2 * math.pi * math.random()) * 0.5))
	return math.min(math.max(v,min), max)
end

function getClampInclusive(x, low, high)
	if x < low then return low
	elseif x > high then return high end
	return x
end

-- From Love2d member tael
function HSL(h, s, l)
   if s == 0 then return l,l,l end
   h, s, l = h/256*6, s/255, l/255
   local c = (1-math.abs(2*l-1))*s
   local x = (1-math.abs(h%2-1))*c
   local m,r,g,b = (l-.5*c), 0,0,0
   if h < 1     then r,g,b = c,x,0
   elseif h < 2 then r,g,b = x,c,0
   elseif h < 3 then r,g,b = 0,c,x
   elseif h < 4 then r,g,b = 0,x,c
   elseif h < 5 then r,g,b = x,0,c
   else              r,g,b = c,0,x
   end
   return math.ceil((r+m)*256),math.ceil((g+m)*256),math.ceil((b+m)*256)
end

-------------------------------
function generateSpiralTable(sizeX, sizeY)
	local spiral_table = {}
	local x = 0
	local y = 0
	dx = 0
	dy = -1
	
	local size = math.pow(math.max(sizeX, sizeY), 2)
	for i = 1, size do
		if (-sizeX/2 < x and x <= sizeX/2) and (-sizeY/2 < y and y <= sizeY/2) then
			table.insert(spiral_table, {x, y})
		end
		if x == y or (x < 0 and x == -y) or (x > 0 and x == 1-y) then
			dx, dy = -dy, dx
		end
		x, y = x+dx, y+dy
	end
	return spiral_table, size
end

--------------
-- Probability sets are in the form
-- {true/false, {item1, probabilityValue}, {item2, probabilityValue} ... etc}
-- The true/false thing here is whether the set is finite or not
function getRandomThingFromProbabilitySet(rewards)
  if rewards == nil then return nil end
  local remove_reward_once_found = false
  -- Deal with getting a reward from a finite pool
  if rewards[1] == true then remove_reward_once_found = true end
  
  local reward_set = deepcopy(rewards)
  table.remove(reward_set, 1)  -- [1] is the true/false value - but we know that now, so kill it
  if #reward_set == 0 then return nil end
  -- Get single reward
  if #reward_set == 1 then
		local r = reward_set[1][1]
		if remove_reward_once_found then table.remove(rewards, 2) end
    return r
  end
  
  -- Okay, now we have an array of items with their probabilities
  -- Figure out the size of the 'probability space'
  local total_probability = 0;
  local reward_index = 1

  for i, reward_tuple in ipairs(reward_set) do
    total_probability = total_probability + reward_tuple[2] -- used to figure out which one we got
  end
  local threshold = math.random() * total_probability

  -- Then go through again and see which one is the last to break it
  total_probability = 0
  for i, reward_tuple in ipairs(reward_set) do
    total_probability = total_probability + reward_tuple[2] -- used to figure out which one we got
    if total_probability > threshold then
      reward_index = i -- remember where we were last time
      break
    end
  end
  
	if remove_reward_once_found then
		table.remove(rewards, reward_index + 1)
	end
	
  return reward_set[reward_index][1]    
end

-------------------------
function replaceChar(str, pos, r)
    return table.concat{str:sub(1,pos-1), r, str:sub(pos+1)}
end

function insertChar(str, pos, r)
    return table.concat{str:sub(1,pos-1), r, str:sub(pos)}
end

-------------------------
function splitline(inputstr, sep)
	if sep == nil then
		sep = "%s"
	end
	local t={} ; i=1
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		t[i] = str
		i = i + 1
	end
	return t
end

-------------------------
local digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
local floor,insert = math.floor, table.insert
function toBaseN(n,b)
    n = floor(n)
    if not b or b == 10 then return tostring(n) end
    local t = {}
    local sign = ""
    if n < 0 then
      sign = "-"
			n = -n
    end
    repeat
			local d = (n % b) + 1
			n = floor(n / b)
			insert(t, 1, digits:sub(d,d))
    until n == 0
    return sign .. table.concat(t,"")
end
-------------------------
function fromBaseN(n,b)
	local sign = 1
	if n:sub(1,1) == "-" then 
		sign = -1 
		n = n:sub(2, string.len(n)) -- remove the preceeding minus
	end
	
	local total = 0
	for i = 1, string.len(n) do -- Go through each digit
		local c = n:sub(i,i) -- get the first figure
		for j = 1, string.len(digits) do
			local d = digits:sub(j,j)
			if d == c then 
				total = total * b; 
				total = total + (j-1); 
				break 
			end
		end
	end

	return total * sign
end
-------------------------
local A1, A2 = 727595, 798405  -- 5^17=D20*A1+A2
local D20, D40 = 1048576, 1099511627776  -- 2^20, 2^40
local X1, X2 = 0, 1
function rand()
    local U = X2*A2
    local V = (X1*A2 + X2*A1) % D20
    V = (V*D20 + U) % D40
    X1 = math.floor(V/D20)
    X2 = V - X1*D20
    return V/D40
end
-------------------------
local rand_asciis = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "A","B","C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N","P","Q","R","S","T","U","V","W","X","Y","Z"}
function getAlphanumericPasswordOfLength(l)	
	math.randomseed(os.clock()*100000000000)
	local pw = ""
	for i = 1, l do
		index = math.floor(math.random(#rand_asciis))
		pw = pw .. rand_asciis[index] 
	end
return pw
end
-------------------------
function math.roundToNearestPO2(x)
  return 2 ^ math.ceil(math.log(x)/math.log(2))
end
