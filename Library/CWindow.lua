local class = require "library.middleclass"

CWindow = class('CWindow')

function CWindow:initialize(name, array_of_dialogs, text)
	self.windowTime = 0
  self.name = name
	self.lineHeight = 10 -- Assumed from the font

  table.insert(array_of_dialogs, self)
  self.tableID = #array_of_dialogs
  self.arrayofDialogs = array_of_dialogs
	self.time = 0
end

-------------------------
function CWindow:update(dt)
	self.windowTime = self.windowTime + dt
end

function CWindow:textinput()
end

function CWindow:keypressed(key)
  if key == "escape" then
    self:close() 
    return true
  end
  return false -- ignore all other inputs!
end

function CWindow:close()
  self.state = DIALOG_STATE_CLOSED
  for i, v in ipairs(self.arrayofDialogs) do
    if v.tableID == self.tableID then
      table.remove(self.arrayofDialogs, i)
      break
    end
  end
end