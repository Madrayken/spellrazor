require "library.Tools"
local class = require 'library.middleclass'
local utf8 = require("utf8")

local textLines = {}
local colourTextLines = {}
local input = ""
local inputHistory = {}
local inputHistoryIndex = 0
local cursorPos = 0

local keyHoldData = {}


local KEY_INITIAL_PRESS_TIME_BEFORE_REPEAT = 0.5
local KEY_REPEAT_INTERVAL = 0.08

local SLOWMO_COST = 10
local TIME_COST = 25
local MAX_INPUT_HISTORY = 20

-- ALL ConsoleResponses keys should be LOWER CASE ONLY!!!!!!
local consoleResponses = {};

consoleResponses["xy"] 					= {"I've found a weird rip in the data at this coordinate. I'll record this information under [XY] for later. This glitch coincided with my high score being corrupted yet again. The new command [resetscores] will uncorrupt the table once and for all, I hope. And I'll be on the top of the table again, which is a nice bonus. It'll be interesting to see what killed me on that level!", 	{"XY", "resetscores"}} 

consoleResponses["palace"] 			= {"The Palace. The Palace of Worms? I seem to have seen that phrase before. I'm guessing it's a location of some significance.", 				{"palace"}} 

consoleResponses["arcesso"] 		= {"Arcesso. It's Latin and means 'call out' or something similar. Sounds like a command word to me. 'Arcesso Christie Brinkley!'", 																			{"Arcesso"}} 

consoleResponses["ritual"] 			= {"The ritual seems to have been pretty simple:\n'[Harbingers] gather at the [Palace] and - standing at [4,4] - recite the command and her name in a single breath'.\nFor all its silliness... perhaps my subconscious is giving me tools to fix my own psyche? I've got the location and [coordinates] figured out, and I know her name, which only leaves the 'command' part. I think I'm looking for a word in Latin.", 	{"palace", "4,4"}} 

consoleResponses["alastar"] 		= {"I found two references to [Alastar] at the university library. One came with a woodcut illustration. It's Her.\n\nApparently, [Alastar] is the name of a  goddess/demon worshipped in Mesopotamia around 2335 BCE. References to her peaked during the late 1800s when she was linked to certain artists and poets - the [Harbingers]. One passage describes absinthe-fuelled parties in Paris where [Harbingers] would 'gather at [4,4]' (her holy number?) and perform a short [ritual] in order to gain 'otherworldly inspiration'.\nI had assumed her name was meaningless nonsense. On reflection, I must have read the name in some 'Big Book of Monsters' when I was a kid or something, and kept it locked in my subconscious until now.\nI'm really unsettled, though. That face...", 	{"Alastar", "ritual", "demon", "4,4", "Harbingers"}} 

consoleResponses["ascension"] 	= {"[Ascension] I wonder if it's one of the hidden exits I was looking for... I'll need to [redirect] a portal and find out." , 											{"redirect", "Ascension"}} 
consoleResponses["abaddon"] 		= {"[Abaddon] is apparently both an [angel]/[demon] and a place. I wonder if it's a key to one of those hidden areas I was talking about beforehand. I guess I could [redirect] a portal and see?" , 	{"redirect", "Abaddon"}} 

consoleResponses["names"] 			= {"The mystery regions will have names. They'll be single words, not sentences. Whatever this glitching is it has infected other aspects of the game:  high-scores as well as the levels. Nobody here even knows the chipset, so I have to ask: am I doing this? Have I lost it? Am I glitching my own game? Why would I do that? I need to think outside the box..." , 	{}} 

consoleResponses["flotilla"] 		= {"August-07-1981\nThings are bad. The meds and dreams are significantly affecting my mood. Work is unravelling into chaos. It's not just the code that's glitching now. It's the data, too. Something in there is self-modifying. I know there are at least 3 levels in the game right now that I did NOT create. I've added a [redirect] command which will allow me to point exit portals at these new regions once I know their [names]. I'm going to scour the game's text for anything that looks like it could be a level code." , 	{"redirect", "names", "flotilla"}} 

consoleResponses["keys"] 				= {"Everything you've been typing into the console is a [key], Duncan. Why did you need a reminder of that?" , 	{}}
consoleResponses["key"] 				= {"Her number. Level 4. I'll keep the key nice and simple for now. A single word. Something... nautical." , 	{}}
consoleResponses["glitched"] 		= {"Bits and pieces of corrupt code and data seem to pop up all over the place. But it's not entirely random. I keep seeing patterns, and words. Scientists say human brains find patterns in random noise all the time, so perhaps I'm imagining it." , 	{}}

consoleResponses["crazy"] 			= {"If someone's messing with my code I'll need to find a better way to protect my secrets. I *think* I'm going to encode command [keys] into the walls. What with the rest of the glitches and other random messages turning up in the walls, I doubt anyone will find mine. I'll hide a [key] on level 4. Let's see if that makes things more secure." , 	{"key", "keys"}}

consoleResponses["die"] 				= {"I'm going to assume this comment isn't a threat and has more to do with 'death' in the game. I'm only in the high score table once (at the top), so it could be linked to how I died when I added that top score. But what do you know... the score has [glitched], and I don't remember how I ended that game, so I don't even have the answer to what killed me." , 	{}} 

consoleResponses["coordinates"] = {"I'm glad I added the [info] command or else I'd be counting spaces, and that's just horrible." , 										{"info"}} 
consoleResponses["grid"] 				= {"[Grid], as in [Euclidean] space accessed with an 'X,Y' coordinate. If [info] shows the sound is showing up at, say '3,2' I'll simply record my notes under '3,2'. Easy. For now I'll keep hunting..." , 										{"Euclidean"}} 
consoleResponses["noise"] 			= {"It was a looped screaming/whispering sound. Scared the life out of me the first time I heard it. I'll store a note using the [grid] [coordinates] of the noise as reference." , 										{"grid", "coordinates"}} 
consoleResponses["nevermore"] 	= {"I heard a weird [noise] on the level where this message shows up. I'm sure the [coordinates] where I heard it are somehow meaningful. I'll make a note of them in this console when I've found them in the [info], but I guess I'll have to move over EVERY grid square to find it. Ugh." , 	{"noise", "coordinates", "nevermore"}} 

consoleResponses["paranoia"] 		= {"Is it still paranoia if it's true? I found something super-creepy in my code and I didn't write it. I raised it with Dave, and he just took the opportunity to rag on me about the unfinished state of the game. I guess it's easier to blame my [crazy] rather than try to figure out who's actually done this. I know it's not me. I wouldn't write 'How did you [die], Duncan?' in my own comments." , 	{"die", "crazy"}}

consoleResponses["july-20"] 	= {"July-20-1981\n[Strindberg]'s gone! I was standing outside his office for about 10 minutes (like an idiot) before I noticed that the door was open. [Strindberg]'s office had been thoroughly cleared out, and there was a month of unopened mail on the floor.\nI'm back at work now and I'm really irritated. Taking off like that, without a word or a letter to his patients seems really unethical. I really wanted to talk about getting off the Miralazine. Hey - a tip to any psychiatrists reading this: when your patient reports experiencing mild [paranoia], don't disappear without a word. It makes them antsy." , 	{"paranoia"}} 

consoleResponses["position"] 		= {"Glad I created that [info] command. It makes finding [coordinates] much easier." , 										{"info"}} 
consoleResponses["4,4"] 				= {"Maybe a link to the player [position]? I can't think what this might mean right now, but I have a feeling it'll be useful much later when I know what all this weirdness is really about." , 			{"position"}} 
consoleResponses["44"] 					= {"More accurately, it was said as '[4,4]' - with a gap between the two digits. I guess it could also be a [grid] coordinate?" , 		{"grid", "4,4"}} 

consoleResponses["nightmares"] 	= {"They start off just like before. But now, when She turns, I get to see her face. It is a smooth, slightly contoured expanse of featureless flesh. As I move toward her that blank expanse of skin splits, forming a wide horizontal gash,  a rudimentary mouth. And we kiss. The drugs are clearly working. They remove the horror from the memory. The result is that it all feels oddly theatrical and even religious - like kissing the Pope's ring. When I awaken (slowly and calmly - thanks, meds!) I'm always left with a number stuck in my mind: '[44]'." , 				{"44"}} 

consoleResponses["june-20"] 		= {"June-20-1981\nThe headaches have gone, and I'm now able to sleep through the night. Surprisingly, the dreams themselves are actually worse. They've become fully-fledged [nightmares]. However, my emotional reaction to Her is muted, and the encounters don't wake me up any more. It's like the drugs have stopped my body manufacturing 'terror' chemicals. There are other side-effects, though. I constantly feel like someone is watching me... but I'm too emotionally numb to feel all that bothered about it. I asked if feeling paranoid indicated we should alter the dosage, but [Strindberg] told me to wait until next month ([July-20]) before making any changes." , 				{"Nightmares", "July-20"}} 

consoleResponses["september-2"] 		= {"September-2-1981\nSomething is... different. I can feel it. It's like I'm vibrating in sympathetic response to an inaudible frequency. It's difficult to concentrate, but I now have a latitude and longitude to work with. I'll check the US maps. I need to know the name of my destination before I take the final step." , 				{}} 

consoleResponses["tesla"] 				= {"What a genius. I would have loved to have seen Wardenclyffe completed, in all its blazing glory. Life certainly wasn't fair to the guy. If it was rough for him, how much hope is there for the rest of us?" , 				{"tesla"}} 
consoleResponses["angels"] 				= {"your prayers are worth nothing for she writhes in the £$^&@%^^£//orms" , 				{"angels"}} 
consoleResponses["angel"] 				= {"your prayers are worth nothing for she writhes in the £$^&@%^^£//orms" , 				{"angel"}} 
consoleResponses["demons"] 				= {"your curses are worth nothing for she writhes in the £$^&@%^^£//orms" , 				{"demons"}} 
consoleResponses["demon"] 				= {"your curses are worth nothing for she writhes in the £$^&@%^^£//orms" , 				{"demon"}} 

consoleResponses["harbinger"] 		= {"$% is nothing but he who comes before and opens the way to the palace of worms sh@$%&!" , 				{}} 
consoleResponses["harbingers"] 		= {"$% are nothing but those who come before and open the way to the palace of worms sh@$%&!" , 				{}} 
consoleResponses["miralazine"] 		= {"Never heard of it before so I looked it up in MIMs. Apparently it's a 'hypnotic', which is just a fancy name for a sleep aid. I'm not allowed any alcohol for at least 5 hours before taking it! Brutal. I hope it's worth it. Edit: it works!" , 				{}} 


consoleResponses["strindberg"] 			= {"If Jonathan hadn't recommended him so highly, I'd have dropped him months ago. He looks young, but he can't be. The certificate on his wall shows that he graduated from Johns Hopkins about 20 years ago, and is still involved with some weird arts club called 'The [Harbingers]'. I guess his family has money." , 				{"Harbinger"}} 

consoleResponses["may-20"] 			= {"May-20-1981\nI talked to Dr. [Strindberg] about getting something to help me sleep (and ideally stop the headaches, too). I mentioned in passing that the kids we tested the game on last month shared some of my symptoms. He froze for about 5 seconds, and then just nodded in his usual non-committal way. He said absolutely nothing. It was weird. I really don't like this guy much. On the other hand I now have a prescription for [Miralazine] (30mg). He actually made me promise I'd come back and see him on [June-20] at exactly the same time: 4:40pm. He made it sound like there'd be horrific side-effects if we didn't closely monitor dosage. Frankly, I don't care. Bring on the chemistry. Bring on sleep." , 				{"Miralazine", "June-20"}} 

consoleResponses["randomizer"] 		= {"It's now possible to randomize the player's spells using the [shuffle] command in this console. I've made it so that players lose 25% of their spells in order to deter cheaters, but it's still useful for playing about with different spell-limitations." , 	{"shuffle"}} 

consoleResponses["6502"] = {"I didn't design this chip. I have no idea who did. I assume it was the guy working here before me? It's an odd thing, anyway. Lots of commands that don't seem to do anything, as well as a built-in ROM source filled with data tables of all kinds. Very strange, but super fast! I can do stuff with Spellrazor that'll leave those Midway guys scratching their heads." , 	{}} 

consoleResponses["april-3"] 		= {"April-3-1981\nIt's 9pm and I'm off home. The [randomizer] function is in and Jeff dropped off the test report just after 8pm. I gave it a quick skim and the good news is that we're funded. The bad news is that the 27 fire buttons seem to be right on the edge of putting some players off. This is much as I expected.\nWorse, some players also reported audio-visual distortions, headaches and nausea. I know I'm doing some wacky scale and rotation stuff with the [6502] variant I'm using, but it wouldn't cause reactions like this. Worryingly, I have experienced several of the same symptoms." , {"randomizer"}} 

consoleResponses["sleep"]  		= {"I'm having pretty much the same dream every night. A tall, skeletally thin woman standing with her back to me. Horrible cracks and pops as she begins to turn her head. An overwhelming sense of dread, like I really don't want to see her face... and I always wake up at this point. I started taking cough-medicine to knock me out, but it doesn't really work. I'm going ask Dr. [Strindberg] for something stronger when I see him on [May-20]." , 	{"May-20", "Strindberg"}} 

consoleResponses["test"] 		= {"They'll unleash 10 random kids on Spellrazor for 10-20 mins each. I earn a point for each minute they play over the ten minute mark. A total of over 100 points means the game gets further funding. Or 'I get paid' if you want to be cynical. Bring on the cynicism. It keeps me in decent coffee." , 	{}} 

consoleResponses["white"] 	  	= {"Yes, [white]." , 	{}} 
consoleResponses["secrets"] 	  	= {"March-02-1981\nI've added this console in order to access secret functions. Anything shown in [white] is a command. For example there's a new, undocumented [info] command which allows me to see exactly where players are in the dungeon and other useful details. It'll make bug-squashing easier after the initial [test] round (booked for [April-3]). The sooner the test is done the sooner I can finish the game and get some [sleep]." , 	{"white", "info", "April-3", "sleep", "test"}} 

consoleResponses["eraserhead"] 	  = {"I feel like Henry. I am moving around from task to task, never seeing the whole, yet taking comfort in the simple squalor of my apartment. But that comfort is illusory. It can't go on. I'm increasingly feeling that there must be more to life than this. Not because of some spiritual lack, but because I can feel it. I'm close. I'm close to seeing the man in the moon, and the production line that makes erasers out of skulls. Close." , 	{"Eraserhead"}} 
consoleResponses["visage"] 	    = {"Fade to Grey. It sounds like the future. I hear it was written by the guys in Ultravox, on equipment loaned to them by Gary Numan. I love how cold and emotionless it sounds, while still evoking a weird, dark serenity. Atmosphere without humanity. Like I said, the future." , 	{"Visage"}} 
consoleResponses["coffee"] 	      = {"Oily micro-droplets of stimulation floating as a black suspended solution. Coffee is a ritual and an effect. Despite being a drug it is not one of vagary or release from reality. It is a drill; a tool by which we can move inward and do more, not less. I wonder if we'd have achieved so much without it." , 	{"Coffee"}} 
consoleResponses["dmb"] 	         = {"That's me. How did I die? And finally, where?" , 	{}} 
consoleResponses["euclidean"] 	  = {"The version of space that makes sense. Is our universe truthfully made up of such simple mathematics? If it were not, would our senses even be able to detect it? We cannot see into the ultraviolet, but for butterflies and moths, it's just part of their world. If the world coiled in upon itself, and twisted into incomprehensible geometries, would we even know? Could we?" , 	{}} 
consoleResponses["summoning"] 	  = {"Summoning [demons] always sounds like a bad idea, no matter what your desires. But at the same time, I think we crave communion with the alien, the angelic, and the infernal; things that are not 'us'. We understand that our monkey brains drive us witlessly toward violence, toward nuclear annihilation (just the biggest, newest stick we've picked up to bash skulls with). We know, deep down that even contact with something uncaring, 'other', even if it were hostile, would show us that there's something out there that doesn't think like us. It would tell us our way is not the only way, and given the world... that seems like a message of hope." , 	{"summoning", "demons"}} 
consoleResponses["effect"] 	      = {"We neglect effect. When we say 'The ends justify the means' we never mean it; it's an implicitly ironic statement. It's said wryly, as if anyone believing it is a fool. And yet 'effect' is all we have. Our world and those who live here are merely accumulations of jagged effect, wrapped in a comfort-blanket of reason. [Proust] knew this, but who reads [Proust] nowadays?", 	{"effect", "Proust"}} 
consoleResponses["cause"] 	      = {"We deify cause. We seek to understand the 'why' even when it leads to further confusion. Why did this thing happen? Why did that serial killer do those things? Why did that country allow itself to be ruled by a dictator? We never arrive at a satisfactory answer. The only truth is that life is chaos. In searching for the reasons 'why' we miss the opportunity to focus on the 'what'. I guess a lot of people believe in the con of control and that makes them happier... for a short, shallow time at least." , 	{"cause"}} 
consoleResponses["death"] 	      = {"Death is inevitable and our time as experiential beings is bounded by it on one end. But time is elastic. Relative. Somewhere between these two facts lies a darker truth about perception and value. If we conquer time we won't necessarily conquer our linear perception of it. If we conquer death it won't necessarily increase the value of life. We are stuck on a roller coaster to oblivion and should really just enjoy the ride rather than examining the paintwork and the scuff marks on the seat." , 	{"death"}} 
consoleResponses["proust"] 	      = {"Consider the madelaine and the phone box. You can learn a lot about a man from his words; in philosophy that can often be more illuminating than the philosophy being sold. The problem is that - being reflective and written by humans - philosophers assume that understanding human nature is somehow important. If we could explain [Proust]'s madelaine and phone box to a dog they would find the ideas meaningless. That's not because dogs are somehow less alive - quite the opposite in fact. Dogs are simply less self-reflective, less self-obsessed, and happier. The fact that so many philosophers drank and took drugs should be a huge clue as to how pointless many of their musings are. Never trust a sick doctor." , 	{"Proust"}}

spellDescriptions = {}
spellDescriptions["A"] = {"[A]RROW", 			"  Flies straight toward an enemy, causes damage on contact, then disappears."}
spellDescriptions["B"] = {"[B]OUNCER", 		"  A bouncy ball of energy that flies straight toward enemies, causes damage on contact, or bounces off walls if it misses."}
spellDescriptions["C"] = {"[C]OMPANION", 	"  Summons a small orbiting sprite-bot who casts spells at nearby enemies."}
spellDescriptions["D"] = {"[D]EATHTOUCH", "  Creates a whirling circle of blades around the caster that cause damage on contact."}
spellDescriptions["E"] = {"[E]-BEAM", 		"	 A linear beam of pure energy capable of cutting through multiple enemies at once."}
spellDescriptions["F"] = {"[F]REEZE", 		"  Stops all visible enemies from moving. Beware: they can still cast spells!"}
spellDescriptions["G"] = {"[G]ENOCIDE", 	"  Kills every creature of the same type as the one you hit."}
spellDescriptions["H"] = {"[H]OMER", 			"  Homes in on current target, causes damage on contact, then disappears."}
spellDescriptions["I"] = {"[I]NVISO", 		"  Stops enemies detecting the caster for a period."}
spellDescriptions["J"] = {"[J]UMP", 			"  Jump over anything (including walls) and land with a small impact explosion, killing unwary enemies in its radius."}
spellDescriptions["K"] = {"[K]ISS", 			"  Seduces target, causing them to become your loyal slave for the entire level."}
spellDescriptions["L"] = {"[L]IGHTNING", 	"  Instantaneously blasts the current target with a bolt of pure energy, and chains attacks to others nearby."}
spellDescriptions["M"] = {"[M]AP", 				"  Reveals unmapped rooms on the level."}
spellDescriptions["N"] = {"[N]EGATE", 		"  Stops all visible enemies casting spells for a period. Beware: they can still move!"}
spellDescriptions["O"] = {"[O]GRE", 			"  Summon a powerful Ogre-bot to fight for you: Warning - it will eventually turn on you!"}
spellDescriptions["P"] = {"[P]ORTAL", 		"  Forces open the exit portal in the current room, regarless of whether enemies are still present."}
spellDescriptions["Q"] = {"[Q]UAKEBOMB", 	"  A timed explosive device that causes damage to everything in the surrounding area, including you. Drop and RUN!"}
spellDescriptions["R"] = {"[R]OCKET", 		"  A slow, cumbersome, and highly explosive magical missile. Use with care."}
spellDescriptions["S"] = {"[S]HIELD", 		"  Adds 1 shield to the caster. Can stack more than one if you buy [shieldup]."}
spellDescriptions["T"] = {"[T]ELEPORT", 	"  Causes the caster to warp to an unmapped room, or a random location if the whole level is mapped."}
spellDescriptions["U"] = {"[U]LTRASWORD", "  Bigger. Longer. Fatter. Sword-er. Also goes through walls, which is nice."}
spellDescriptions["V"] = {"[V]OID", 			"  Create an area of safety around the caster that pushes all enemy things away, spells included."}
spellDescriptions["W"] = {"[W]ARP", 			"  Transports all creatures within range to your position after a brief delay. Use with care... and a followup."}
spellDescriptions["X"] = {"[X]RAY", 			"  Allows the caster to see any robot within range, regardless of walls. Useful in combination with other spells such as [W]."}
spellDescriptions["Y"] = {"[Y]SHOT", 			"  Three pronged spray of small-arms fire radiating from the caster. Lasts several seconds. Useful in rapidly clearing out multiple rooms."}
spellDescriptions["Z"] = {"[Z]AP", 				"  Electrifies the current room's floor, blasting any enemies inside."}

local creatureDescriptions = {}
creatureDescriptions["BUG"] 				= {"Bug", 				"  Annoying service droid in the shape of a scarab. Heads toward anything it considers 'trash'. Don't take it personally. Especially not the 'died because you touched it' part."}
creatureDescriptions["FROG"] 				= {"Frog", 				"  Hopping green message droid. Hostile towards anyone it believes interferes with its delivery of credit card offers. Mostly harmless... apart from killing on contact."}
creatureDescriptions["FLY"] 				= {"Fly", 				"  Flying bug-bot used to filter the air in the facility. Heads toward anything it considers an 'impurity'. It's not you, it's him. He doesn't like you."}
creatureDescriptions["GRUNT"] 			= {"Grunt", 			"  Hovering security bot with erratic behaviour. Armed with small caliber single shot firearm and a few homing rounds. Tends to shoot before asking, largely because it can't talk."}
creatureDescriptions["SWORDSMAN"] 	= {"Swordsman",		"  Close quarters attacker designed to inflict damage to things with whirling steel blades. Who in their right mind makes these things? It can't make a salad. It just cuts things."}
creatureDescriptions["SPIDER"] 			= {"Spider", 			"  Robot designed to find and explode landmines. It does this by hopping, landing, and then exploding. Yes, to remove the threat of explosions, it causes explosions. (sigh)"}
creatureDescriptions["POPPER"] 			= {"Popper", 			"  Housing pod for up to a dozen [Fly] bots. Explodes when destroyed. Also fires homing shots when it's bored. It has no arms, so doing the crossword is tricky."}
creatureDescriptions["BEAMER"] 			= {"Beamer", 			"  Malfunctioning party lighting, or evil military side-project? You decide. Fires a long, deadly E-Beam in the cardinal directions when threatened. It is also quite angsty."}
creatureDescriptions["WRAITH"] 			= {"Wraith", 			"  Remember the [Swordsman]? Now imagine this with stealth capability rendering it untargetable. Yup. Someone was paid to make this. Not me. I'm just writing the manual."}
creatureDescriptions["CORNER"] 			= {"Corner", 			"  Imagine the world's worst draft excluder. Imagine that rather than stopping breezes under doors, it shoots beams across thresholds to rooms. That's what this does. Also knows French."}
creatureDescriptions["SIREN"] 			= {"Siren", 			"  Tired of being able to cast spells? Want to rely exclusively on your sword? Then a [Siren] is just the job. Not so hazardous by itself, its Negate ability makes simple droids deadly."}
creatureDescriptions["OGRE"] 				= {"Ogre", 				"  Initially friendly to summoners, this heavily armoured robot is fitted with a semi-automatic firearm. Quick and lethal fun for all the family... of Mansons."}
creatureDescriptions["OGRET"] 			= {"Ogret", 			"  NOT initially friendly. The female version of an [Ogre]. Just as nasty, despite receiving only .7 of the energy their male counterparts enjoy."}
creatureDescriptions["SECURITYBOT"] = {"SecurityBot", "  Oh dear God. You did what? Why did you do that? Look! It's coming for you! Run! Run away! So many guns! And shields! So man..."}
creatureDescriptions["MAGUS"] 			= {"Magus", 			"  A variant of the common [Securitybot]. Differs primarily in its weapon loadout."}
local logins = {}
gLoggedInAs = "guest"

-------------------------
local gCommandEnteredAfterOpenedTerminal = false
CWindowTerminal = class('CWindowTerminal', CWindow)
function CWindowTerminal:initialize(name, array_of_dialogs)
	CWindow.initialize(self, name, array_of_dialogs) -- initialize the parent  
	self.maxLines = 29
	self.maxLength = 73
	if gFirstTimeConsoleOpened then
			self:printHelp()
      	self:addLine("Current gold: " .. gGold .. ", spell cost: " .. gSpellCost)
      gFirstTimeConsoleOpened = false
  elseif gCommandEnteredAfterOpenedTerminal == true then
    gCommandEnteredAfterOpenedTerminal = false
    self:addLine("--------------------------------------------------------------")
    self:addLine("Type [?] for commands, [Up]/[Down] to see previous commands entered.")
    self:addLine("Current gold: " .. gGold .. ", spell cost: " .. gSpellCost)
  end
  
  self.ignoreFirstReturn = true
	setGlitchingForTime(0.5)
end

-------------------------
function CWindowTerminal:update(dt)
	CWindow.update(self, dt)
	
	for k, v in pairs(keyHoldData) do -- v[1] is function, [v2] is initialDelay, v[3] is repeatTime
		if love.keyboard.isDown(k) then 
			v[2] = math.min(v[2] + dt, KEY_INITIAL_PRESS_TIME_BEFORE_REPEAT)
		else
			v[2] = 0; v[3] = 0
		end
	
		if v[2] == KEY_INITIAL_PRESS_TIME_BEFORE_REPEAT then
			v[3] = v[3] + dt
			if v[3] >= KEY_REPEAT_INTERVAL then
				v[3] = 0
				v[1]() -- Perform the function attached
			end
		end 
	end
end
-------------------------
local function stripBracketLines(text)
	local out_1 = ""
	local out_2 = ""
	local in_bracket = false
	local len = string.len(text)
	for i = 1, len do
		local letter = text:sub(i,i)
		if letter == "[" then 
			in_bracket = true 
		elseif letter == "]" then 
			in_bracket = false 
		elseif letter == " " then 
			out_1 = out_1 .. " "
			out_2 = out_2 .. " "
		elseif letter == "\n" then 
			out_1 = out_1 .. "\n"
			out_2 = out_2 .. "\n"
		elseif in_bracket == true then 
			out_1 = out_1 .. "_" -- We have to put something there or the word splitting doesn't work
			out_2 = out_2 .. letter
		else
			out_1 = out_1 .. letter
			out_2 = out_2 .. "_"  -- We have to put something there or the word splitting doesn't work
		end
	end
	return out_1, out_2
end
-------------------------
function CWindowTerminal:close()
--			gameState:startEndgame()	
	
	if gRitualSaidCorrectly then 
		gRitualSaidCorrectly = false
		gameState:startEndgame()	
	end
	CWindow.close(self)
end
-------------------------
function CWindowTerminal:addLine(total_text)
	-- FIX THIS SO IT SPLITS LINES AFTER THEY'VE BEEN COLOURISED
	local texts = splitline(total_text, '\n')

	for i2, text in ipairs(texts) do
		limit = self.maxLength
		indent = ""

		local t1, t2 = stripBracketLines(text)
		
		local here = 0
		local str = t1:gsub( "(%s+)()(%S+)()",
			function( sp, st, word, fi )
				 if fi-here > limit then
						here = st
						return "\n"..word
				 end
			end)
		t1 = splitline(str, '\n')
		
		here = 0
		str = t2:gsub( "(%s+)()(%S+)()",
			function( sp, st, word, fi )
				 if fi-here > limit then
						here = st
						return "\n"..word
				 end
			end)
		t2 = splitline(str, '\n')
		
		local line_count = #t1
		for i = 1, line_count do

			
			if t1[i] ~= nil then t1[i] = t1[i]:gsub("_", " "); table.insert(textLines, 					"  " .. t1[i]) else table.insert(textLines, 				"  ") end
			if t2[i] ~= nil then t2[i] = t2[i]:gsub("_", " "); table.insert(colourTextLines,  	"  " .. t2[i]) else table.insert(colourTextLines, 	"  ") end
			if #textLines > self.maxLines then
				table.remove(textLines, 1)
				table.remove(colourTextLines, 1)
			end
		end
	end
end

-------------------------
function CWindowTerminal:textinput(key)
	if key == "%" or key == "(" or key == ")" or key == "[" or key == "]" or key == "{" or key == "}" or string.match(gSupportedCharacterString, key) then	
		if string.len(input) < self.maxLength then
	--		input = input .. key

			if string.len(input) <= cursorPos then 
				input = input .. key
			else
				input = insertChar(input, cursorPos+1, key)
			end
			playSound("Typing",1)
			cursorPos = cursorPos + 1
		end
	end
	self.ignoreFirstReturn = false
end

-------------------------
function CWindowTerminal:keypressed(key)
	if CWindow.keypressed(self, key) == true then return true end
	
  if (key == "return" or key == "enter" or key == "kpenter") and self.ignoreFirstReturn == false then 
		cursorPos = 0
		if self:parse(input) ~= false then
			table.insert(inputHistory, input)
			if #inputHistory > MAX_INPUT_HISTORY then
				table.remove(inputHistory, 1)
			end
			inputHistoryIndex = #inputHistory
			input = ""
		end
		playSound("Typing",1) 
		return true
	end
	
	if key == "left" then cursorLeft() end
	if key == "right" then cursorRight() end
	
	if key == "backspace" then backspace() end

	if #inputHistory > 0 then
		if key == "up" or key == "kp8" then
			if inputHistoryIndex >= 1 then 
				input = inputHistory[inputHistoryIndex]
				cursorPos = utf8.offset(input, -1)
				if cursorPos == nil then cursorPos = 0 end -- Deal with the fact that utf8.offset sometimes returns nil
				if inputHistoryIndex > 1 then
					inputHistoryIndex = inputHistoryIndex - 1
				end
			end
		elseif key == "down" or key == "kp2" then
			if inputHistoryIndex < #inputHistory then 
				inputHistoryIndex = inputHistoryIndex + 1
			end
			input = inputHistory[inputHistoryIndex]
			cursorPos = utf8.offset(input, -1)
		end
	end
	self.ignoreFirstReturn = false

  return true
end

-------------------------
function backspace()
	local byteoffset = utf8.offset(input, -1) -- Find the end of the string
	if byteoffset then
	-- remove the last UTF-8 character.
	 -- string.sub operates on bytes rather than UTF-8 characters, so we couldn't do string.sub(text, 1, -2).
--		input = string.sub(input, 1, byteoffset - 1)
		if cursorPos == byteoffset then
			input = string.sub(input, 1, byteoffset - 1)
		elseif cursorPos > 0 then
			input = string.sub(input, 1, cursorPos-1) .. string.sub(input, cursorPos+1, byteoffset)
		end
		if cursorPos > 0 then
			cursorPos = cursorPos - 1
		end
		playSound("Typing",1) 
		return true
	end
end

-------------------------
function cursorLeft()
	if cursorPos > 0 then playSound("Typing",1); cursorPos = cursorPos - 1 end
end
-------------------------
function cursorRight()
	if cursorPos < string.len(input) then cursorPos = cursorPos + 1; playSound("Typing",1) end
end
-------------------------
function CWindowTerminal:parse(line)
  gCommandEnteredAfterOpenedTerminal = true
	if line == "" then return false end
	
	local pure_line = string.format( "%s", line:match( "^%s*(.-)%s*$" )) 

	--- Give the special logged in commands priority
	if pure_line ~= nil and pure_line ~= "" then		
		-- If we're entering a glitch coordinate then...
		if gWhisperNodePosition ~= nil then
			coord = pure_line:gsub("%s+", "")
			if coord == gWhisperNodePosition.x .. "," .. gWhisperNodePosition.y then pure_line = "xy" end
		end
			
		local lower_pure_line = pure_line:lower()

		if lower_pure_line == "clinton road" or lower_pure_line == "clinton" then
			setGlitchingForTime(-5)
			self:addLine("----------------------------------------------------------------------")
			self:addLine("September-3-1981\nI now see her face wherever I go. Yet I feel calmer, and more purposeful than I ever have.\nI'm taking off for a while. There's a place I need to be. I know I might not be coming back, but that's okay. We shouldn't pity the purposeful.\nThere is nothing for me here now. The Spellrazor project has been terminated and the company is letting me go. I can understand why. Given the second round of test results and the fact that I've not really been myself lately it's not a surprise.\nI managed to retain a printout of the assembly code, plus a bunch of diagrams. I'll be mailing them to Jonathan before I leave. I've told him to hold on to them and keep them secret... for now. I can't bear to let Spellrazor disappear. Part of me - the part that now looks at the human race as fleeting ephemera - believes that this project and it's message deserve to be seen and heard. Not today, but maybe in a little while.\nUntil then I'll be waiting here with the others on the Road.\n - DMB")
			return true
		end
    
    if unlockAllDoorsWithpassword(pure_line:upper()) then 
			self:addLine("Door opened. Security reset.")
			playSound("Success", 1)
			gVaultRoomID = 0 -- Stop anything reacting to our presence here
			gameState:resetTrace() 
      return
--		else
--			self:addLine("Invalid password! Securitybot summoned!")
--			gameState:reduceTraceTimeBy(100)
--			playSound("Fail", 1)
		end
    

		---------------------------------------------------------
		if consoleResponses[lower_pure_line] ~= nil then
			local line = consoleResponses[lower_pure_line][1]
			setGlitchingForTime(0.25)
			self:addLine("----------------------------------------------------------------------")
			self:addLine(line)
			self:addNewCommandsFromList(consoleResponses[lower_pure_line][2])
			return
		end
	end
--[[	if pure_line ~= nil and pure_line ~= "" and gLoggedInAs ~= "Guest" and logins[gLoggedInAs] ~= nil then
		local s = rems[gLoggedInAs]
		if s[lower_pure_line] ~= nil then
			setGlitchingForTime(0.5)
			local line = rems[gLoggedInAs][lower_pure_line]
			self:addLine("----------------------------------------------------------------------")
			self:addLine(line)
			return
		end
	end]]
	-------------------------------------------------
	local commands = splitline(line, " ") 
	local var = nil; var2 = nil
	local command = commands[1]
	
	if command == nil or command == " " then return false end
	if #commands > 1 then var = commands[2]; string.format( "%s", var:match( "^%s*(.-)%s*$" ) )end
	if #commands > 2 then var2 = commands[3]; string.format( "%s", var2:match( "^%s*(.-)%s*$" ) ) end
	command = string.format( "%s", command:match( "^%s*(.-)%s*$" ) )
	command = string.lower(command)
	command_upper = string.upper(command)
	
	if var ~= nil then var = var:lower() end
	if var2 ~= nil then var2 = var2:lower() end

	if command == "arcesso" and var ~= nil then
		if tonumber(var) == nil then
			if var:lower() == "alastar" then
				if gCurrentLevelName == "PALACE" and math.floor(gPlayer.mapX / NODE_SIZE) == 4 and math.floor(gPlayer.mapY / NODE_SIZE) == 4 then
					self:addLine("She hears your call.")
					gRitualSaidCorrectly = true
					setGlitchingForTime(1)
					return
				else			 
					self:addLine("She cannot hear your call.")
--					gRitualSaidCorrectly = false
				end
			end
		end
	elseif command == "redirect" then
		if gPortalIsOpen then
			if var == nil then
				self:addLine("ERROR: [redirect] requires a destination name.")
			elseif gSpecialLevelDefinitions[var] ~= nil then
				gNextLevelHasPredefinedName = var 
				self:addLine("Portal redirected to " .. var)
				playSound("GotKey", 1)
			else
				self:addLine("ERROR: Realm '" .. var .. "' unknown.")
				gNextLevelHasPredefinedName = nil
			end
		else
			self:addLine("ERROR: No exit portal present on level. Please clear all threats and try again.")
			gNextLevelHasPredefinedName = nil
		end
		return
	elseif creatureDescriptions[command_upper] ~= nil then
		local str = creatureDescriptions[command_upper]
		self:addLine("------------ " .. str[1] .. " -------------")
		self:addLine(str[1])
		self:addLine(str[2])
	elseif spellDescriptions[command_upper] ~= nil then
		local str = spellDescriptions[command_upper][1]
		if gSpellsFound[command_upper] ~= true then
			str = str .. " --- (Undiscovered)"
		else
			str = str .. " --- (Discovered)"
		end
		self:addLine("------------ " .. str)
		self:addLine(spellDescriptions[command_upper][2])
	elseif command == "help" or command == "commands" then
		self:printHelp()
	elseif command == "resetscores" then
		resetScores()
		self:addLine("High-scores reset to factory default. Duncan is on top again.")
	elseif command == "controls" then
		self:printControls()
	elseif command == "?" then
		self:printHelp()
	elseif command == "gold" or command == "$" then
		if gGold < gSpellCost then 
			self:addLine("Gold: " .. gGold .. " ...pitiful. Spell cost: " .. gSpellCost)
		elseif gGold > 20 then 
			self:addLine("Gold: " .. gGold .. " ...criminy. Spell cost: " .. gSpellCost)
		else
			self:addLine("Gold: " .. gGold .. " ...not bad. Spell cost: " .. gSpellCost)
		end
	elseif command == "buy" then
		gConsoleCommandsOpened["buy"] = true
		if var == nil then self:addLine("ERROR: Use form '[buy] <amount> <letter>' or '[buy] <letter>'\ne.g. '[buy 3 f]'") return end
		if var2 ~= nil and tonumber(var) ~= nil then
			self:buy(tonumber(var), string.upper(var2))
		else
			self:buy(1, string.upper(var))
		end
	elseif command == "sell" then
		gConsoleCommandsOpened["buy"] = true
		if var == nil then self:addLine("ERROR: Use form '[sell] <amount> <letter>' or '[sell] <letter>'\ne.g. '[sell 3 f]'") return end
		if var2 ~= nil and tonumber(var) ~= nil then
			self:sell(tonumber(var), string.upper(var2))
		else
			self:sell(1, string.upper(var))
		end
	elseif command == "shuffle" then
		gConsoleCommandsOpened["shuffle"] = true
		self:randomiseSpells()
    self:addLine("Shuffled")
	elseif command == "spells" then
		self:listSpells()
	elseif command == "creatures" then
		self:listCreatures()
	elseif command == "hello" or command == "hi" or command == "hey" then
		self:addLine("Hello to you, too.")
	elseif command == "!"  then
		self:addLine("It is, indeed, a situation worthy of exclamation.")
	elseif command == "fuck" or command == "cunt" or command == "shit" or command == "wanker" then
		self:addLine("Danger time reduced")
		playSound("Fail", 1)
		gameState:reduceTraceTimeBy(100)
  elseif command == "create" then
    if commands[2] == nil or commands[2] == "" then self:addLine("Format is: create (name) - case sensitive") end
    if createEnemyWithName(commands[2]) == false then self:addLine("No creature with name: " .. commands[2]) end
	elseif command == "shieldup" then
		if gMaxShieldLevel >= 4 then self:addLine("Max shield is already 4") return end
		cost = gShieldCosts[gMaxShieldLevel]
		if gGold < gShieldCosts[gMaxShieldLevel] then self:addLine("You need " .. cost - gGold .. " more Gold for that.") return end
		gGold = gGold - cost
		gMaxShieldLevel = gMaxShieldLevel + 1
		gShieldsLowSoundTimer = 0
		gPlayer:setMaxShieldLevel(gMaxShieldLevel)
    gameState:removeShieldWarning()
		playSound("Shield",1)
		self:addLine("You can now stack up to " .. gMaxShieldLevel .. " [S]hield spells.") self:addLine("You now have " .. gGold .. " Gold.") return
--	elseif command == "slowmo" then
--		if gBoughtSlowmo == true then 
--			gSlowmo = not gSlowmo
--			if gSlowmo == true then self:addLine("Slowmo ON") else self:addLine("Slowmo OFF") end 
--			return 
--	elseif command == "open" then
--		if var == nil then self:addLine("ERROR: use the format '[open] xxxx' where 'xxxx' is the 4-letter code you can find on the same level as the locked door.") return end
--		if unlockAllDoorsWithpassword(var:upper()) then 
--			self:addLine("Door opened. Security reset.")
--			playSound("Success", 1)
--			gVaultRoomID = 0 -- Stop anything reacting to our presence here
--			gameState:resetTrace() 
--		else
--			self:addLine("Invalid password! Securitybot summoned!")
--			gameState:reduceTraceTimeBy(100)
--			playSound("Fail", 1)
--		end
--[[			end
		if gGold < SLOWMO_COST then self:addLine("You need " .. SLOWMO_COST - gGold .. " more gold for that.") return end
		gGold = gGold - SLOWMO_COST
		gBoughtSlowmo = true
		gSlowmo = true
		self:addLine("Slowmo ON")
		self:addLine("You can now use 'slowmo' to make the game slow to a crawl whenever you stand still.") 
		self:addLine("You now have " .. gGold .. " Gold.") 
		return]]
--[[	elseif command == "portals" then
		for k, v in pairs(logins) do
			if v[4] 
		end]]
--[[	elseif command == "logout" then
		if gLoggedInAs == "guest" then self:addLine("Already logged out. Current username is 'GUEST'") return end
		gLoggedInAs = "guest"
		self:addLine("Logged out. Current username is 'GUEST'") return
	elseif command == "login" then
		if gLoggedInAs ~= "guest" then self:addLine("Already logged in as: " .. string.upper(gLoggedInAs) .. ". Logout first") return end
		if var == nil then self:addLine("ERROR: Use form '[login] <id> <password>' where <id> is a 3-letter id or user's initials") return end
		if var2 == nil then self:addLine("ERROR: Use form '[login] <id> <password>' where <id> is a 3-letter id or user's initials") return end
		
		var = string.lower(var)
		var2 = string.lower(var2)
		if logins[var] ~= nil then
			if var2 ~= logins[var][1] then 
				self:addLine("Incorrect id/password combination") return 
			else 
				self:login(var) return 
			end
		else
			self:addLine("Incorrect id/password combination") return 
		end]]
	------------------------------------------ Debug commands start
	elseif command == "autoscreenshots" then
		if gTrailerTakeRandomScreenshots == false then
			gTrailerTakeRandomScreenshots = true
			gTrailerRandomScreenshotTimeLeft = MAX_SCREENSHOT_DELAY
		else
			gTrailerTakeRandomScreenshots = false
			gTrailerRandomScreenshotTimeLeft = 0
		end
	elseif command == "togglehud" then
		gGUIOff = not gGUIOff
	elseif command == "gotolevel" then
		gTrailerForcedLevelNumber = nil
		if var ~= nil and tonumber(var) ~= nil then gTrailerForcedLevelNumber = tonumber(var) end
	elseif command == "time" then
		gConsoleCommandsOpened["time"] = true
--		gHasCheated = true
		self:addTime()
	elseif command == "mapall" then
		self:addLine("Level mapped")
		gHasCheated = true
		mapWholeLevel()
	elseif command == "greed" then
		if var == nil or tonumber(var) == nil then var = 50 end
		gGold = gGold + var
		self:addLine("Gold: " .. gGold)
		gHasCheated = true
	elseif command == "info" and gPlayer ~= nil then
		gConsoleCommandsOpened["info"] = true
    self:addLine("--------------------------------- Info ---------------------------------")
		self:addLine("Zone:         " .. gCurrentLevelName)
		self:addLine("Position:     " .. math.floor(gPlayer.mapX / NODE_SIZE) .. ", " .. math.floor(gPlayer.mapY / NODE_SIZE))
    self:addLine("Code:         " .. gPlayer.mapX .. ", " .. gPlayer.mapY)
		self:addLine("RoomID:       " .. gPlayer.currentRoomID)
		self:addLine("Gold:         " .. gGold .. ", spell cost: " .. gSpellCost)
		self:addLine("Spell cost:   " .. gSpellCost)
		self:addLine("Shield max:   " .. gMaxShieldLevel)
--		self:addLine("Player cell:       " .. gPlayer.mapX .. ", " .. gPlayer.mapY)
		self:addLine("Enemies left: " .. countEnemies())
	elseif command == "pause" then
		gPause = not gPause
		if gPause then self:addLine("Pause ON") else self:addLine("Pause OFF") end
	elseif command == "debugroom" then
		if var == nil or tonumber(var) == nil then self:addLine("ERROR: Use form 'debugroom roomID'") return end
		var = tonumber(var)
		if getRoomFromID(var) == nil then 
			self:addLine("No room with ID: " .. var) 
		else
			gDebugRoomID = var
			self:addLine("Room " .. gDebugRoomID .. " debugged")
			gMapImage = getPixelMapOfLevel()
		end
	elseif command == "rooms" then
		for i, v in ipairs(getRoomIDArray()) do
			local room = getRoomFromID(v)
			self:addLine("ID: " .. v .. "> " .. room.x1 .. ", " .. room.y1 .. ", " .. room.x2 .. ", " .. room.y2)
		end
	elseif command == "securityoff" then
		gHasCheated = true
		gSecurityOff = not gSecurityOff 
		if gSecurityOff then self:addLine("Security OFF") else self:addLine("Security ON") end
	elseif command == "iamharbinger" then
    self:addLine("Thou art.")
		gHasCheated = true
		addAllSpells()
--	elseif command == "debug" then
--		gHasCheated = true
--		gDebug = not gDebug
--		if gDebug then self:addLine("Debug ON") gSecurityOff = true addAllSpells() else self:addLine("Debug OFF") end
	elseif command == "deletesave" or command == "deletesaves" then
		deleteSave(true)
		self:addLine("Save deleted")
	elseif command == "exit" or command == "quit" then
		self:close() return 
	elseif command == "testending" then
		gRitualSaidCorrectly = true
		self:close() return 
	elseif command == "suicide" then
		gameState:suicide() return
    self:addLine("You are dead.")
	------------------------------------------ Debug commands end
	else		
		local l = logins[gLoggedInAs]
		if l ~= nil and l[3] ~= nil then 
			f = l[3][command]
			if f~= nil then
				local p1 = var; if p1 ~= nil then p1:lower() end
				local p2 = var2; if p2 ~= nil then p2:lower() end
				f(self, p1, p2)
				setGlitchingForTime(0.5)
				return
			end
		else
			if command == "redirect" then 
				self:addLine("ERROR: [redirect] only available to gods. Please [login] with <id> and <password>")
			else
				self:addLine("ERROR: command '" .. pure_line .. "' not understood.")
			end
		end
	end
end


-------------------------
function CWindowTerminal:draw()
	local colour = GREEN_INDEX
	love.graphics.setFont(bigFont)
--	love.graphics.setFont(beebTileFont)
	
	local x_inset = 0.02
	local x = math.floor(gCanvasWidth * x_inset)
	local	y = math.floor(gCanvasHeight * 0.08)

--	love.graphics.setColor(0,0,0,150)
--	love.graphics.rectangle("fill", 0, 0, gCanvasWidth * 2, gCanvasHeight * 2)

	love.graphics.setColor(0,0,0,240)
  local w = gCanvasWidth * 0.98
	love.graphics.rectangle("fill", 2 * (gCanvasWidth/2 - w/2), 46, 2 * w, gCanvasHeight * 2 * 0.85)
  
	local wid = 1000
  love.graphics.setColor(0,0,0,255)
  love.graphics.rectangle("fill", 2 * ((gCanvasWidth / 2) - 120), 2 * (y - 24), 2*2*120, 2*2*8)
	local ind = gRainbowSetWithWhite[(math.floor(gTime * 10) % #gRainbowSetWithWhite) + 1]
	love.graphics.setColor(gColourList[ind+1])	love.graphics.printf("Press 'Esc' to exit Console", 2 * (-(wid / 2) + (gCanvasWidth / 2)), 2 * (y - 20), 2 * wid, 'center')
  love.graphics.setLineWidth(2)
  love.graphics.rectangle("line", 2 * (gCanvasWidth/2 - w/2), 46, 2 * w, gCanvasHeight * 2 * 0.85)
  love.graphics.setLineWidth(1)

	love.graphics.setColor(gColourList[colour + 1])

	local top_y = y
  
	for i, v in ipairs(textLines) do
		love.graphics.print(v, x * 2,y * 2)
		y = y + self.lineHeight
	end
	
	y = top_y
  love.graphics.setColor(gColourList[WHITE_INDEX + 1])

	for i, v in ipairs(colourTextLines) do
		love.graphics.print(v, x * 2,y * 2)
		y = y + self.lineHeight
	end
		
	local str = ">>" .. input
	love.graphics.setColor(gColourList[colour + 1])
	love.graphics.print(str, x * 2,y * 2)
	
	str = ""

	love.graphics.setColor(gColourList[colour+1])
	
	if math.floor(self.windowTime * 2) % 2 == 0 then
		love.graphics.rectangle("fill", 2 * (x + (cursorPos + 2) * 8), 2 * y, 1 * 2, 8 * 2)
	end
--	love.graphics.setShader()
	love.graphics.setFont(beebTileFont)

end

-------------------------
-- Console functions
-------------------------
function CWindowTerminal:printControls()
  self:addLine("------------------------------- Controls -------------------------------")
	self:addLine("- ARROWS to move")
	self:addLine("- SPACE to use Sword")
	self:addLine("- A-Z for spells: Note - spells auto-target")	
	self:addLine("- CTRL to cycle Slowmo types")
  self:addLine("- DELETE/BACKSPACE to Pause")

	self:addLine("- ESC to quit")
	self:addLine("- TAB for fullscreen")
	self:addLine("- F1 for screenshot")
	self:addLine("- RETURN to open console - (hence seeing this!)")	
end

function CWindowTerminal:printHelp()
	self:addLine("------------------------------- Commands -------------------------------")
  self:addLine("[?]             = show this list of commands")
	self:addLine("[secrets]       = access developer's secret commands")
	
  if gMaxShieldLevel < 4 then 
		self:addLine("[shieldup]      = Increase max shield level for " .. gShieldCosts[gMaxShieldLevel] .. " Gold")
	end
  
  self:addLine("[controls]      = show in-game [controls]")
  
--	self:addLine("[gold]         = show amount of [Gold] held")
	self:addLine("[spells]        = list your [spells]")
	self:addLine("[<x>]           = give info on the spell with letter <x> E.g. '[a]'")
	
	self:addLine("[creatures]     = list all enemies")
	self:addLine("[<creature>]    = give info on a specific <creature> E.g. '[fly]'")

	if gConsoleCommandsOpened["shuffle"] then
		self:addLine("[shuffle]     = randomly shuffle your spells, losing 25% of them!")
	end

	self:addLine("[sell <x>]      = [sell] spell <x> for 1 Gold. E.g. '[sell a]'")
	self:addLine("[sell <n> <x>]  = [sell] <n> of <x> for 1 Gold. E.g. '[sell 3 a]'")
	self:addLine("[buy <x>]       = [buy] a spell for " .. gSpellCost .." Gold. E.g. '[buy d]'")
	self:addLine("[buy <n> <x>]   = [buy] <n> of <x> for " .. gSpellCost .." Gold each. E.g. '[buy 2 z]'")
--	self:addLine("[time]          = use " .. TIME_COST .." Gold to reset time limit")
	
	self:addLine("[xxxx]          = Open doors locked with 4-character codes. E.g. '[p12w]'" )
--	self:addLine("[slowmo]        = toggle [slowmo] mode")
	if gConsoleCommandsOpened["resetscores"] then 
		self:addLine("[resetscores]   = reset high-scores to factory defaults")
	end
	

	
--	self:addLine(  "[login <n><p>] = Log in as authorised user. E.g. '[login] dmb asgard'")
--	self:addLine(  "[logout]       = Log back in with guest account")
	self:addLine(	"[suicide]       = kill yourself, but enter your high score if appropriate")
  self:addLine(  "[exit]          = [exit] this debug console")

	local length = getLengthOfTable(gConsoleCommandsOpened)
	if length > 0 then 
		self:addLine("   ")
		self:addLine("Special Commands:")
		local line = ""
		local i = 0
		for k, v in pairs(gConsoleCommandsOpened) do
			if i > 0 then line = line .. ", " end
			line = line .. "[" .. k .."]"
			i = i + 1
		end
		self:addLine(line)
	end
	
	if gLoggedInAs ~= "guest" and logins[gLoggedInAs] ~= nil then
		self:addLine("   ")
		self:addLine("Special Commands:")
		self:addLine("[REM]")

		local l = logins[gLoggedInAs][3]
		if l ~= nil then 
			for k, v in pairs(l) do
				self:addLine("[" .. k .."]")
			end
		end
	end
end
-------------------------
function CWindowTerminal:addTime()
	if gGold < TIME_COST then 
		self:addLine("You need " .. TIME_COST .." Gold for that. You have " .. gGold .. "!") 
	else 
    gGold = gGold - TIME_COST
		self:addLine("Time reset! You now have " .. gGold .. " Gold"); 
		gameState:resetTrace() 
		playSound("Success", 1)
	end
end
-------------------------
function CWindowTerminal:buy(n, v)
	if gSpellsCarried[v] == nil then 
		self:addLine("No such spell as [" .. v .. "]")
		return
	end
	
	if gSpellsFound[v] ~= true then
		self:addLine("You need to discover at least 1 [" .. v .. "] before you can buy more.")
		return
	end
	
	local gold_needed = 0
	for i = 1, n do
		gold_needed = gold_needed + gSpellCost + (i-1)
	end
	
	if gGold >= gold_needed then
		if gameState:buySpell(n, v) == true then
			gGold = gGold - gold_needed
			gSpellCost = gSpellCost + n
			self:addLine("You now have " .. gSpellsCarried[v] .. " '" .. spellDescriptions[v][1] .. "'. Gold now " .. gGold .. ". Spell cost now: " .. gSpellCost)
		else
			self:addLine("You cannot carry " .. gSpellsCarried[v]  + n .. " of those.")
		end
	else
		if gGold < gSpellCost * n then self:addLine("Need " .. gold_needed .. " Gold. You have " .. gGold.. ".") end
	end
end

-------------------------
function CWindowTerminal:sell(n, v)
	if gSpellsCarried[v] == nil then 
		self:addLine("No such spell as [" .. v .. "]")
		return
	end
	if gSpellsCarried[v] >= n then
		gGold = gGold + n
		gameState:sellSpell(n, v)
		self:addLine("Sold " .. n .. " [" .. spellDescriptions[v][1] .. "]. Gold now " .. gGold.. ".")
	else
		self:addLine("You only have " ..  gSpellsCarried[v] .. " [" .. v .. "].")
	end
end


function CWindowTerminal:listSpells()
	self:addLine("-------------- Spells -------------")

	for i, k in ipairs(gUpperCaseList) do
		local v = gSpellsCarried[k]
		if spellDescriptions[k][1] ~= nil and v > 0 then 
			if v >= 10 then 
				self:addLine(v .. " " .. spellDescriptions[k][1])
			else
				self:addLine(v .. "  " .. spellDescriptions[k][1])
			end
		end
	end
end

function CWindowTerminal:listCreatures()
  self:addLine("------------------------------- Creatures ------------------------------")
  self:addLine("Type any creature's name for more information:")
	for k, v in pairs(creatureDescriptions) do
		self:addLine(" - [" .. k .. "]")
	end
end

	
-------------------------
-- name, 
function CWindowTerminal:login(user_lookup)
	local user_info = logins[user_lookup]
	gLoggedInAs = user_lookup
	self:addLine("------------------------- Logged in as: " .. string.upper(user_lookup) .. " --------------------------")
	self:addLine("Welcome back " .. user_info[2] .. ".")
	self:addLine("Type [?], [help], or [commands] to see commands. Type [secrets] for hidden options.")
	setGlitchingForTime(0.5)
end

-------------------------
function CWindowTerminal:redirect(var1, var2)
	if gPortalsOpened[gLoggedInAs] == nil then
		if specialPortalExistsOnLevel(gCurrentLevel) then			
			if gPortalIsOpen then 
				self:addLine("Glitched portal [open]ed for user with ID: " .. gLoggedInAs)
				playSound("GotKey", 1)
				gPortalsOpened[gLoggedInAs] = true
			else
				self:addLine("Glitched portal has yet to be generated. Remove all threats first, then try [open]again.")
			end
		else
			self:addLine("No glitched portal present on this level. Please try later.")
		end
	else
		self:addLine("One glitched portal already [open]ed by this user. Log in with a different account to [redirect] more portals.")
	end
end

-------------------------
function CWindowTerminal:addNewCommandsFromList(list)
	local new_commands = ""
	local new_command_count = 0
	for i, v in ipairs(list) do
		local text = v:lower()
		if gConsoleCommandsOpened[text] == nil then
			gConsoleCommandsOpened[text] = true
			if new_command_count ~= 0 then 
        new_commands = new_commands .. ", " 
      end
			new_commands = new_commands ..  "[" .. v .. "]"
			new_command_count = new_command_count + 1
		end
	end
	
	if new_command_count ~= 0 then
		self:addLine("New commands discovered: " .. new_commands)
		playSound("GotKey")
    saveData()
	end
end

function CWindowTerminal:randomiseSpells()
	local total = 0
	for k, v in pairs(gSpellsCarried) do
		total = total + v
		gSpellsCarried[k] = 0
	end
	
	total = math.floor(total * 0.75)
	if total > 0 then
		for i = 1, total do
			local index = gLimitedUpperCaseList[math.random(#gLimitedUpperCaseList)]
			addSpellsWithLetter(index)
		end
	end
	gSpellsAltered = true
	playSound("ConsoleAltersSomething")
end

-------------------------
function clearConsoleText()
	textLines = 		 {  "              ------========{{  SPELLRAZOR  }}========------",
										"              |    Debug Console (c)1981 Duncan M Bower    |", 
										"              | 'Secrets' for use of authorised staff only |", 
										"              |  (And that's probably not you, now is it?) |", 
										"              ------==================================------"," "}
	colourTextLines ={"                                ",
										"              ", 
										"                 Secrets ", 
										"              ", 
										"             "," "}
end

-------------------------
function specialPortalExistsOnLevel(level)
	local is_critical_level = false
	for k, v in pairs(logins) do
		if v[4] == level then is_critical_level = true end 
	end
	return is_critical_level
end	
	
	
function portalHasBeenOpenedOnLevel(level)
	local is_critical_level = false
	for k, v in pairs(logins) do
		if v[4] == level then is_critical_level = true end 
	end
	if is_critical_level == false then return true end -- Let's just say that critical portals 
	
	
	for k, v in pairs(logins) do
		if v[4] == level and gPortalsOpened[k] == true then return true end
	end
	
	return false
end




keyHoldData["backspace"] = {backspace, 0, 0}
keyHoldData["left"] 		= {cursorLeft, 0, 0}
keyHoldData["right"] = {cursorRight, 0, 0}
