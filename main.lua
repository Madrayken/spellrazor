#/usr/bin/env love
--debug = true
PROJECT_NAME = "Spellrazor"
VERSION_NUMBER = "1.0.1 - 'Albatross'"

TRAILER_MODE_ON = false
SCALE 					= 2

gCanvasType = "nearest" -- "linear"

vector = require "library.hump.vector"
GameState = require "library.hump.gamestate"
require "library.TEsound" 
require "library.Tools"
require "game.SpecialEffects"

require "game.StateMenu"
require "game.StateGame"
require "game.StateHighScoreInput"
require "game.StateIntro"

require "game.CActor"
require "game.CActorMapFeature"
require "game.CActorPickup"
require "game.CActorMobile"
require "game.CWorldMessage"
require "game.Map"
require "game.Spells"
require "game.Rewards"
require "game.Enemies"
require "game.Levels"
require "game.Sounds"

require "library.CWindow"
require "library.CWindowDialog"
require "library.CWindowTerminal"

local utf8 = require("utf8")
local serpent = require "library.serpent"

SAVE_NAME = "spellrazor.sav"

spriteAtlas 		   = require "data.spritepack0"
tileAtlas 			   = require "data.tileset0"
tinyFiguresAtlas   = require "data.tinyfigures0"

WHITE_INDEX = 0
BLACK_INDEX = 1
RED_INDEX = 2
GREEN_INDEX = 6
BLUE_INDEX = 10
YELLOW_INDEX = 14
CYAN_INDEX = 18
MAGENTA_INDEX = 22
GREY_INDEX = 26
GLITCH_ALPHA_VALUE = 255

gColourList = {
	{255, 255, 255}, {0, 0, 0}, 
	{255, 0, 0}, {128, 0, 0}, {64, 0, 0}, {32, 0, 0},
	{0, 255, 0}, {0, 128, 0}, {0, 64, 0}, {0, 32, 0},
	{0, 0, 255}, {0,0, 128}, {0,0, 64}, {0,0, 32},
	{255, 255, 0}, {128, 128, 0}, {64, 64, 0}, {32, 32, 0},
	{0, 255, 255}, {0, 128, 128}, {0, 64, 64}, {0, 32, 32},
	{255, 0, 255}, {128, 0, 128}, {64, 0, 64}, {32, 0, 32},
	{128, 128, 128}, {64, 64, 64}, {32, 32, 32},
}

DEFAULT_GAME_TYPE_INDEX = 1
DEFAULT_SEED_TYPE_INDEX = 2
gDefaultGameTypeIndex = DEFAULT_GAME_TYPE_INDEX
gDefaultSeedTypeIndex = DEFAULT_SEED_TYPE_INDEX
gFirstTimeConsoleOpened = true

gFirstTimePlayed = true
MAX_HIGHSCORES = 15

gRainbowSetDark = {BLUE_INDEX, MAGENTA_INDEX, RED_INDEX}
gRainbowSetWithWhite = {BLUE_INDEX, MAGENTA_INDEX, CYAN_INDEX, GREEN_INDEX, YELLOW_INDEX, WHITE_INDEX}
gRainbowSetReds = {YELLOW_INDEX, RED_INDEX, RED_INDEX + 1, MAGENTA_INDEX, MAGENTA_INDEX + 1}
gRainbowSetYellows = {YELLOW_INDEX, YELLOW_INDEX+1, YELLOW_INDEX + 2}
gRainbowSetBlues = {CYAN_INDEX, CYAN_INDEX + 1, BLUE_INDEX}

gStartedFinalRitual = false
gTotalRitualPeriod = 60*3
gLeavingLevelCounter = 0

gScreenCanvas = nil 
gStretchableCanvas = nil

TRON_BACKGROUND_SIZE = 512

gFreezeUntilEnemySpotted = false
gDebugText = ""
gDebug = false
gHighScores = {}
gDialogArray = {}
gPortalsOpened = {}
gConsoleCommandsOpened = {}
gHasCheated = false

gJustFinishedGame = false

gNumberOfTileTypes = 0
gNumberOfColourTypes = #gColourList

gScreenRotate = 0
gSeedCode = ""

gTrailerForcedLevelNumber = nil
gTrailerScanLines = true
gTrailerBloom = true
gTrailerTakeRandomScreenshots = false
gTrailerRandomScreenshotTimeLeft = 0
MAX_SCREENSHOT_DELAY = 5
gGUIOff = false

gTileSize 			= 16 -- How large is a tile in 'pixels'
gCanvasWidth = math.floor(1280/2)
gCanvasHeight = math.floor(720/2)
gTilesDisplayWidth 	= math.ceil(gCanvasWidth/gTileSize) + 1-- How many tiles of information we want to display
gTilesDisplayHeight = math.ceil(gCanvasHeight/gTileSize) + 1 -- How many tiles of information we want to display
gGenerativeCanvasSize = 0
gIsFullScreen = false

gColourAdd = 0
gShaderZoom = 0
gChromaticAbberation = 0
gTotalGlitchingPeriod = 0
gTimeUntilNextGlitch = 0
gTimeUntilGlitchEnds = 0

MIN_GLITCH_TIME = 0.2
MAX_GLITCH_TIME = 0.4

MIN_GLITCH_GAP = 0.5 -- 0.2
MAX_GLITCH_GAP = 5 	-- 0.7

gDeathMessage = ""
MAX_CHROMATIC_ABBERATION_AMOUNT = 0.02

gFixedFrameCount = 0
gTime = 0
gDrawCount = 0

gShaderBloom = nil
gShaderBloomWithoutScanlines = nil
gShaderFullScreenPostProcess = nil
gShaderFullScreenPostProcessChrom = nil
gShaderFullScreenPostProcessXRay = nil
gShaderFullScreenPostProcessZoom = nil
gShaderLandscapeRecolour = nil
gShaderSprites = nil
gShaderTitleRecolour = nil
gShaderGridColour = nil
gShaderConsole = nil
gShaderTronBackground = nil
--gShaderGenerative = nil
gShaderScrolling = nil
gBackgroundMusic = nil
gFrontEndMusic = nil

gGoddessImage = nil
gScreenModes = {}
gMode = 1 -- Which of the available screen sizes do we choose? 1 is eventually sorted to be the largest
local gGlitchSounds = {}
local currentGlitchSound = nil

gSupportedCharacterString = ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`' .. 'abcdefghijklmnopqrstuvwxyz{|}~'

----------------------------------
-- START
----------------------------------
function null()
end

function love.conf(t)
    t.modules.joystick = false
    t.modules.physics = false
    t.window.msaa = 0                   -- The number of samples to use with multi-sampled antialiasing (number)
end

gSupportsNormalCanvas = false
gCanvasStrs = "Error: This graphics card and drivers do not support 'normal' mode. Your card only supports these formats:\n\n"

function checkCanvasesSupported()
  local canvasformats = love.graphics.getCanvasFormats()
  local str = ""
  gSupportsNormalCanvas = false
  for formatname, formatsupported in pairs(canvasformats) do
    str = string.format("- %s", formatname)
    if formatsupported  == true then
      if formatname == "normal" then 
        gSupportsNormalCanvas = true 
      end
      gCanvasStrs = gCanvasStrs .. str .. "\n"
    end
  end
  gCanvasStrs = gCanvasStrs .."\nPlease let the developer know so he can tailor the game to suit your system.\n"
end

local steamworks = {}
function love.load()
--	love.graphics.setShader = null
--	love.graphics.newShader = null
	-- Enable debugging
  if arg[#arg] == "-debug" then require("mobdebug").start() end
  if arg[2] == "compress" then
    local str = "zip -r " .. PROJECT_NAME .. ".love main.lua assets data game library shaders libsteam_api.dylib steam_api.dll steam_api.lib steam_api.h steam_appid.txt"
    os.execute(str)
  end
  
--  steamworks = require "library.steam.libsteamworks"

  gCanvasMode = "normal"
  --gCanvasMode = "rgb10a2"
	love.graphics.setBackgroundColor(0,0,0,255)

  checkCanvasesSupported()

  if not gSupportsNormalCanvas then return end
  
	gLastInsertedScore = 0
	love.mouse.setVisible(false)
	
	gSpiralTable, gSpiralTableSize    = generateSpiralTable(16,16)
	gShaderBloom                      = love.graphics.newShader("shaders/Bloom.fs")
	gShaderBloomWithoutScanlines      = love.graphics.newShader("shaders/BloomWithoutScanlines.fs")

	gShaderTronBackground             = love.graphics.newShader("shaders/TronBackground.fs")
	gShaderFullScreenPostProcess      = love.graphics.newShader("shaders/FullScreenPostProcess.fs")  
	gShaderFullScreenPostProcessChrom = love.graphics.newShader("shaders/FullScreenPostProcessChrom.fs")
	gShaderFullScreenPostProcessXRay  = love.graphics.newShader("shaders/FullScreenPostProcessXRay.fs")
	gShaderFullScreenPostProcessZoom  = love.graphics.newShader("shaders/FullScreenPostProcessZoom.fs")
	gShaderLandscapeRecolour          = love.graphics.newShader("shaders/Landscape.fs")
	gShaderSprites                    = love.graphics.newShader("shaders/Sprites.fs")
	gShaderTitleRecolour              = love.graphics.newShader("shaders/Title.fs")
	gShaderGridColour                 = love.graphics.newShader("shaders/GridColour.fs")
	gShaderConsole                    = love.graphics.newShader("shaders/Console.fs")
	gShaderScrolling                  = love.graphics.newShader("shaders/Scrolling.fs")
  
	gShaderLandscapeRecolour          = love.graphics.newShader("shaders/Landscape.fs")
	gShaderSprites                    = love.graphics.newShader("shaders/Sprites.fs")
	gShaderTitleRecolour              = love.graphics.newShader("shaders/Title.fs")
	gShaderGridColour                 = love.graphics.newShader("shaders/GridColour.fs")
	gShaderConsole                    = love.graphics.newShader("shaders/Console.fs")
	gShaderScrolling                  = love.graphics.newShader("shaders/Scrolling.fs")

	spriteBatch = love.graphics.newSpriteBatch( spriteAtlas.texture, 150, "static" )
		
	gNumberOfTileTypes = 0
	for k, v in pairs(tileAtlas.quads) do
		gNumberOfTileTypes = gNumberOfTileTypes + 1
	end
	
	tileBatch = love.graphics.newSpriteBatch(tileAtlas.texture, gNumberOfTileTypes * gNumberOfColourTypes, "static")
	
	tinyFiguresBatch = love.graphics.newSpriteBatch(tinyFiguresAtlas.texture, 52, "static")

	gScreenWidth 	  = gCanvasWidth * SCALE
	gScreenHeight 	= gCanvasHeight * SCALE  
  
  local po2Size
  
  po2width  = math.roundToNearestPO2(gCanvasWidth)
  po2height = math.roundToNearestPO2(gCanvasHeight)
  
	lowRezCanvas = love.graphics.newCanvas(po2width,po2height,gCanvasMode,0)
--  lowRezCanvas = love.graphics.newCanvas(gCanvasWidth,gCanvasHeight,gCanvasMode,0)
  lowRezCanvas:setFilter(gCanvasType, gCanvasType) -- Pixel perfect
  
  	
  po2width  = math.roundToNearestPO2(gScreenWidth)
  po2height = math.roundToNearestPO2(gScreenHeight)
  
	gScreenCanvas = love.graphics.newCanvas(po2width, po2height, gCanvasMode,0)
--	gScreenCanvas = love.graphics.newCanvas(gScreenWidth, gScreenHeight, gCanvasMode,0)
--	gScreenCanvas:setFilter("nearest", "nearest") -- Pixel perfect
	
--	gStretchableCanvas = love.graphics.newCanvas(gScreenWidth, gScreenHeight, gCanvasMode,0) -- NOT pixel-perfect, as most displays aren't a nice multiple of the desired screen
--  gStretchableCanvas = love.graphics.newCanvas(po2width, po2height, gCanvasMode,0) -- NOT pixel-perfect, as most displays aren't a nice multiple of the desired screen
	
	gGenerativeCanvasSize = math.floor((gCanvasWidth / 2) * 1.67)
  gGenerativeCanvasSize = math.floor(math.roundToNearestPO2(gGenerativeCanvasSize) / 2)
	generativeCanvas = love.graphics.newCanvas(gGenerativeCanvasSize, gGenerativeCanvasSize, gCanvasMode,0) -- It's square!
	generativeCanvas:setFilter(gCanvasType, gCanvasType) -- Pixel perfect
	generativeCanvas:setWrap("repeat", "repeat")
	
	tronBackgroundCanvas = love.graphics.newCanvas(TRON_BACKGROUND_SIZE, TRON_BACKGROUND_SIZE, gCanvasMode, 0)
	tronBackgroundCanvas:setFilter(gCanvasType, gCanvasType)
	tronBackgroundCanvas:setWrap("repeat", "repeat")

  po2width  = math.roundToNearestPO2(gTileSize * gNumberOfTileTypes)
  po2height = math.roundToNearestPO2(gTileSize * #gColourList)
	tileCanvas = love.graphics.newCanvas(po2width, po2height, gCanvasMode, 0)
	
		-- Pick a screen mode
	gScreenModes = love.window.getFullscreenModes()
	table.sort(gScreenModes, function(a, b) return a.width*a.height < b.width*b.height end)   -- sort from smallest to largest
	
--	for i = 1, #gScreenModes do
--		if gScreenModes[i].height >= gScreenHeight and gScreenModes[i].width >= gScreenWidth then gMode = i - 1 break end
--	end
	gMode = 1
	print("Screen: " .. gScreenModes[gMode].width .. ", " .. gScreenModes[gMode].height)
	
	love.window.setMode(gScreenModes[gMode].width, gScreenModes[gMode].height, {fullscreentype = "desktop", resizable=true, vsync=true, minwidth=gScreenWidth, minheight=gScreenHeight})
	
	love.window.setTitle("SpellRazor")
	love.graphics.clear()
	font = love.graphics.newFont("assets/vher10x10v4-1.ttf", 10)
	
	beebTileFont = love.graphics.newImageFont("assets/beeb.png", gSupportedCharacterString, 1)
  beebTileFont:setFilter("linear", "linear") -- Pixel perfect

	bigFont = love.graphics.newImageFont("assets/beebx2.png", gSupportedCharacterString, 1)
  bigFont:setFilter("nearest", "nearest") -- Pixel perfect

	love.graphics.setFont(beebTileFont)
  GameState.switch(introState)

--	playSound("Welcome")
		
	for i = 1, 6 do
		gGlitchSounds[i] = love.audio.newSource("assets/Glitch" .. i .. ".wav", "static")
		gGlitchSounds[i]:setLooping(true)
	end
	
	gGoddessImage = love.graphics.newImage("assets/Goddess.png")
	loadData()
  
  if arg[2] == "fullscreen" then
    gIsFullScreen = true 
    love.window.setFullscreen(true)  
  end
end

-------------------------------	
-- Default Love stuff
-------------------------------	
function love.update(dt)
  if not gSupportsNormalCanvas then return end 
  
	-- Watch out to see if we should be randomly taking screenshots
	if gTrailerTakeRandomScreenshots == true then
		gTrailerRandomScreenshotTimeLeft = gTrailerRandomScreenshotTimeLeft - (dt * (math.random() * 0.2 + 0.8)) -- Slightly randomise timings of screenshots
		if gTrailerRandomScreenshotTimeLeft < 0 then
			gTrailerRandomScreenshotTimeLeft = MAX_SCREENSHOT_DELAY
			takeScreenshot()
		end
	end	
	
	---------------------------------------------
	gTime = gTime + dt
	---- Total glitching period
	if gTotalGlitchingPeriod > 0 then		
		gTotalGlitchingPeriod = gTotalGlitchingPeriod - dt
		if gTotalGlitchingPeriod <= 0 then
			gTotalGlitchingPeriod = 0
			gTimeUntilGlitchEnds = 0
			gTimeUntilNextGlitch = 0

			if currentGlitchSound ~= nil then
				currentGlitchSound:stop()
			end		
		end
	end
	
	if gTimeUntilGlitchEnds > 0 then
		gTimeUntilGlitchEnds = gTimeUntilGlitchEnds - dt
		if gTimeUntilGlitchEnds <= 0 then
			gTimeUntilGlitchEnds = 0
			if currentGlitchSound ~= nil then
				currentGlitchSound:stop()
			end		
		end
	else
		if gTimeUntilNextGlitch > 0 then
			gTimeUntilNextGlitch = gTimeUntilNextGlitch - dt
			if gTimeUntilNextGlitch <= 0 then
				performGlitch()
			end
		end
	end
	
	TEsound.cleanup()
	
	if #gDialogArray > 0 then
		for i, v in ipairs(gDialogArray) do
			v:update(dt)
		end
		
		if gStartedFinalRitual then gBackgroundMusic:pause() end
		return
	end
	
	if gStartedFinalRitual then gBackgroundMusic:play() end
	
	if gStartedFinalRitual == false then 
		GameState.update(dt)
	end
	
	if gStartedFinalRitual then
		updateEndgameBackground(dt)
	end
end

-------------------------
function setGlitchingForTime(time)
	if time > 0 then
		if gTotalGlitchingPeriod <= 0 then 
			performGlitch()
		end
		gTotalGlitchingPeriod = time
	elseif time < 0 then
		time = -time
		performGlitch(time)
	else
		gTotalGlitchingPeriod = 0
		gTimeUntilNextGlitch = 0
		if currentGlitchSound ~= nil then
			currentGlitchSound:stop()
		end
	end
end
---------------------------
function performGlitch(time)
	math.randomseed(os.clock()*100000000000)
	if time ~= nil then
		gTimeUntilNextGlitch 	= math.random(MIN_GLITCH_GAP, MAX_GLITCH_GAP)
		gTimeUntilGlitchEnds 	= time
	else
		gTimeUntilNextGlitch 	= 0
		gTimeUntilGlitchEnds 	= math.random(MIN_GLITCH_TIME, MAX_GLITCH_TIME)	
	end
	local i = math.random(6)
	if currentGlitchSound ~= nil then
		currentGlitchSound:stop()
	end
	currentGlitchSound = gGlitchSounds[i]
	currentGlitchSound:play()
end

------------------------------- 
function createBitmapFontTexture()	
	local f = love.graphics.newFont("assets/Beeb.ttf", 8)
  f:setFilter("nearest", "nearest")
	love.graphics.setFont(f)

	local beebFontCanvas = love.graphics.newCanvas(8 * 128, 8)
	beebFontCanvas:setFilter("nearest", "nearest")
	love.graphics.setCanvas(beebFontCanvas) --This sets the draw target to the canvas
	love.graphics.clear(0,0,0,0)

	local len = string.len(gSupportedCharacterString) 
	love.graphics.setLineWidth(1)
	for x = 1, len do
		love.graphics.setColor(255,255,255,255)
		local x2 = (x-1) * 8
		love.graphics.print(string.sub(gSupportedCharacterString, x, x), x2, 2)
	end
	love.graphics.setCanvas() --This sets the draw target to the canvas
	local canvas_data = beebFontCanvas:newImageData() 
	
	love.graphics.setColor(255,0,0,255)
	for y = 0, 7 do
		for x = 1, len do
			local x2 = (x-1) * 8
			canvas_data:setPixel(x2, y, 255, 0 ,0, 255)
		end
	end
		
	canvas_data:encode("png", "beeb.png")
  ------
  local bigBeebFontCanvas = love.graphics.newCanvas(2 * 8 * 128, 8 * 2)
	bigBeebFontCanvas:setFilter("nearest", "nearest")
	love.graphics.setCanvas(bigBeebFontCanvas) --This sets the draw target to the canvas
	love.graphics.clear(0,0,0,0)

  len = string.len(gSupportedCharacterString) 
	love.graphics.setLineWidth(1)
	for x = 1, len do
		love.graphics.setColor(255,255,255,255)
		local x2 = (x-1) * 8
		love.graphics.print(string.sub(gSupportedCharacterString, x, x), (2 * x2), (2 * 2), 0, 2, 2)
	end
	love.graphics.setCanvas() --This sets the draw target to the canvas
  canvas_data = bigBeebFontCanvas:newImageData() 

	love.graphics.setColor(255,0,0,255)
	for y = 0, 15 do
		for x = 1, len do
			local x2 = (x-1) * 8
			canvas_data:setPixel(2 * x2, y, 255, 0 ,0, 255)
		end
	end
  
  canvas_data:encode("png", "beebx2.png")  
end	
-----------------------------------------
function createTilesetTexture()
	tileBatch:clear()
--	tileBatch:bind()

	tileCanvas:clear(0,0,0)
	love.graphics.setCanvas(tileCanvas) --This sets the draw target to the canvas

	for y = 1, #gColourList do
		tileBatch:setColor(gColourList[y][1], gColourList[y][2], gColourList[y][3])
		for x = 1, gNumberOfTileTypes do
			local mx = (x - 1) * gTileSize
			local my = (y - 1) * gTileSize
			local name = "Tile" .. x
			tileBatch:add( tileAtlas.quads[name], mx, my)
		end
	end

--	tileBatch:unbind()
	
	love.graphics.draw(tileBatch) -- test
	love.graphics.setCanvas() 

	local canvas_data = tileCanvas:getImageData() 
	canvas_data:encode("ColouredTileset.png","png")
end

-------------------------
function love.draw()
  if not gSupportsNormalCanvas then
    love.graphics.print(gCanvasStrs, 0, 0)
    return
  end
  
	gDrawCount = gDrawCount + 1
	-- Draw the tileset to the canvas
--	createBitmapFontTexture()
--	createTilesetTexture()
	
	--------------------------------------------------------------------------------------------
	----- THE MAIN DRAW SEGMENT. DRAWS THE LEVEL / TITLESCREEN TO THE LOW REZ BUFFER -----------
	love.graphics.setCanvas(lowRezCanvas) --This sets the draw target to the lowRezCanvas
	if gTimeUntilGlitchEnds == 0 then	
		love.graphics.clear(0,0,0,255)
		GameState.draw()	-- Draw the actual game/menu onto the canvas
	end
	
	-- Now blit this low-rez canvas onto the gScreenCanvas - potentially rotated around
	love.graphics.setCanvas(gScreenCanvas) --This sets the target back to the screen	
  lowRezCanvas:setWrap("repeat", "repeat")
	--gShaderZoom = -1 + math.sin(gTime) * 2
  -- gShaderZoom = 4
	local z = gShaderZoom + 1
  z = z * z
  
  if gShaderZoom~=0 then
		love.graphics.setShader(gShaderFullScreenPostProcessZoom)
		if gShaderFullScreenPostProcessZoom ~= nil then
			gShaderFullScreenPostProcessZoom:send("colourAdd", gColourAdd)
			gShaderFullScreenPostProcessZoom:send("chromaticAbberation", gChromaticAbberation)
			gShaderFullScreenPostProcessZoom:send("zoom", z)
			if gXRayTime > (MAX_X_RAY_TIME * 0.1) then 	
				gShaderFullScreenPostProcessZoom:send("greenChrome", 1) 
			else 
				gShaderFullScreenPostProcessZoom:send("greenChrome", (gXRayTime / (MAX_X_RAY_TIME * 0.1)) )
			end
			if math.floor(gLightningTimeLeft * 100) % 2 == 1 then
				gShaderFullScreenPostProcessZoom:send("brightMult", 4) 
			elseif gPlayer ~= nil and gPlayer.negateTimeLeft > 0 then
				gShaderFullScreenPostProcessZoom:send("brightMult", 1) 
			else
				gShaderFullScreenPostProcessZoom:send("brightMult", 0) 
			end
		end
	elseif gXRayTime>0 then
		love.graphics.setShader(gShaderFullScreenPostProcessXRay)
		if gShaderFullScreenPostProcessXRay ~= nil then
			gShaderFullScreenPostProcessXRay:send("colourAdd", gColourAdd)
			if gXRayTime > (MAX_X_RAY_TIME * 0.1) then 	
				gShaderFullScreenPostProcessXRay:send("greenChrome", 1) 
			else 
				gShaderFullScreenPostProcessXRay:send("greenChrome", (gXRayTime / (MAX_X_RAY_TIME * 0.1)) )
			end
			if math.floor(gLightningTimeLeft * 100) % 2 == 1 then
				gShaderFullScreenPostProcessXRay:send("brightMult", 4) 
			elseif gPlayer ~= nil and gPlayer.negateTimeLeft > 0 then
				gShaderFullScreenPostProcessXRay:send("brightMult", 1) 
			else
				gShaderFullScreenPostProcessXRay:send("brightMult", 0) 
			end
		end
	elseif gChromaticAbberation>0 then
		love.graphics.setShader(gShaderFullScreenPostProcessChrom)
		if gShaderFullScreenPostProcessChrom ~= nil then
			gShaderFullScreenPostProcessChrom:send("colourAdd", gColourAdd)
			gShaderFullScreenPostProcessChrom:send("chromaticAbberation", gChromaticAbberation)
			if math.floor(gLightningTimeLeft * 100) % 2 == 1 then
				gShaderFullScreenPostProcessChrom:send("brightMult", 4) 
			elseif gPlayer ~= nil and gPlayer.negateTimeLeft > 0 then
				gShaderFullScreenPostProcessChrom:send("brightMult", 1) 
			else
				gShaderFullScreenPostProcessChrom:send("brightMult", 0) 
			end
		end
	else
		love.graphics.setShader(gShaderFullScreenPostProcess)
		if gShaderFullScreenPostProcess ~= nil then
			gShaderFullScreenPostProcess:send("colourAdd", gColourAdd)
			if math.floor(gLightningTimeLeft * 100) % 2 == 1 then
				gShaderFullScreenPostProcess:send("brightMult", 4) 
			elseif gPlayer ~= nil and gPlayer.negateTimeLeft > 0 then
				gShaderFullScreenPostProcess:send("brightMult", 1) 
			else
				gShaderFullScreenPostProcess:send("brightMult", 0) 
			end
		end
	end
--	lowRezCanvas:setWrap("repeat", "repeat")
--	love.graphics.setShader(gShaderFullScreenPostProcess)
--gScreenRotate = gTime * 1
	love.graphics.translate(gScreenWidth / 2, gScreenHeight / 2)
	love.graphics.rotate(gScreenRotate)
	love.graphics.translate(-gScreenWidth / 2, -gScreenHeight / 2)

  if gTimeUntilGlitchEnds == 0 then	
    love.graphics.draw(lowRezCanvas, gScreenWidth/2, gScreenHeight/2, 0, SCALE, SCALE, gCanvasWidth/2, gCanvasHeight/2) -- MIGHT SCREW UP ON FORCED-PO2 CARDS
  end
	love.graphics.translate(gScreenWidth / 2, gScreenHeight / 2)
	love.graphics.rotate(-gScreenRotate)
	love.graphics.translate(-gScreenWidth / 2, -gScreenHeight / 2)
  
	--------------------------------------------------------------------------------------------
	-- DRAW THE GUI
	love.graphics.setShader()
--	love.graphics.setCanvas(guiCanvas) --This sets the draw target to the canvas
	if gTimeUntilGlitchEnds == 0 then		
		if GameState.current() == gameState then
			gameState.drawGUI()
		else
			gColourAdd = 0
			gShaderZoom = 0
			gChromaticAbberation = 0
		end
		
		if #gDialogArray > 0 then
			for i, v in pairs(gDialogArray) do v:draw() end
		end
	end
	
	-- Display the glitch on the GUI overlay
  love.graphics.setFont(bigFont)
	love.graphics.setColor(255,255,255,255)
	if gTimeUntilGlitchEnds > 0 then
		for i = 1, 3 do
			local l = math.random(#gColourList)
			local x = math.random(math.floor(gScreenWidth / 8)) - 1
			local y = math.random(math.floor(gScreenHeight / 8)) - 1
			
			love.graphics.setColor(0,0,0,255)
			love.graphics.rectangle("fill",  2 * x * 8, 2 * y * 8, 2 * 8, 2 * 8)
			
			local index = gRainbowSetWithWhite[math.random(#gRainbowSetWithWhite)] + 1
			local cols = deepcopy(gColourList[index])
			cols[4] = GLITCH_ALPHA_VALUE
			love.graphics.setColor(cols)
			love.graphics.print(string.char(32+math.random(90)), 2 * x * 8, 2 * y * 8)
		end
	end
	
  love.graphics.setColor(255,255,255,255)
	if gStartedFinalRitual then
		if #gDialogArray == 0 then
			drawEndgamePattern()
		end
	end
  
  --- Draw fake scanlines
  love.graphics.setBlendMode("alpha")
  love.graphics.setColor(255,0,0,128)
  love.graphics.setLineWidth(1)

  --for y = 0, gScreenHeight, 4 do
   -- love.graphics.rectangle("fill", 0,y,gScreenWidth,y, 50, 50)
  --end 
    
	------------------------------------------
	-- Redraw the canvas with with glow/raster lines 
	love.graphics.setColor(255,255,255,255)
	--love.graphics.setCanvas(gStretchableCanvas)
  --love.graphics.setCanvas(gStretchableCanvas)
--	love.graphics.clear()
	
	if gTrailerBloom then
		if gTrailerScanLines then
			love.graphics.setShader(gShaderBloom)--Final)
		else
			love.graphics.setShader(gShaderBloomWithoutScanlines)--Final)
		end
	else
		love.graphics.setShader()--Final)
	end
--	love.graphics.draw(gScreenCanvas, 0, 0) 	

	------------------------------------------
	-- And finally stretch this over the window
	love.graphics.setCanvas()

	local sx, sy = 0,0 --
	
	if gIsFullScreen then 
		sx,sy = love.window.getDesktopDimensions(1)
	else
		sx,sy = love.graphics.getDimensions()
	end
	
	local fracX = sx / gScreenWidth -- love.window.getWidth() / gScreenWidth 
	local fracY = sy / gScreenHeight -- love.window.getHeight() / gScreenHeight 
	
--	fracX = math.floor(fracX)
--	fracY = math.floor(fracY)
	
	local scale = math.max(1, math.min(fracX, fracY))
	
	local offX = math.floor((sx - (gScreenWidth * scale)) / 2)  --  math.floor((sx - gScreenWidth) / 2) * 0.5
	local offY = math.floor((sy - (gScreenHeight * scale)) / 2) -- math.floor((sy - gScreenHeight) / 2) * 0.5
	
--	love.graphics.setShader()
	love.graphics.setColor(255,255,255,255)
--	love.graphics.draw(gStretchableCanvas, offX, offY, 0, scale, scale) 	
	love.graphics.draw(gScreenCanvas, offX, offY, 0, scale, scale) 	
	local bar_height = (sy - gScreenHeight) / 2
  local bar_start = (sy / 2) + ((gScreenHeight * scale) / 2)
	love.graphics.setColor(0,0,0,255)
  love.graphics.rectangle("fill", 0, bar_start, sx, bar_height)
  
	love.graphics.setColor(255,255,255,255)
	love.graphics.setShader()
end



--------------------------------
function love.textinput(t)
	for i, v in pairs(gDialogArray) do if v:textinput(t) then return end end
end

--------------------------------
function love.keypressed(key)	
  if not gSupportsNormalCanvas then
    love.event.push("quit") return  
  end

	if key == "tab" then
		print("Mode: " .. gScreenModes[gMode].width .. ", " .. gScreenModes[gMode].height)

		if gIsFullScreen == false then 
			gIsFullScreen = true 
--			love.window.setMode(gScreenModes[gMode].width, gScreenModes[gMode].height, {fullscreentype = "desktop", fullscreen = true, resizable=false, vsync=true, minwidth=gScreenWidth, minheight=gScreenHeight})
			love.window.setFullscreen(true)
		else 
--					love.window.setMode(gScreenModes[gMode].width, gScreenModes[gMode].height, {fullscreen = false, resizable=false, vsync=true, minwidth=gScreenWidth, minheight=gScreenHeight})
			love.window.setFullscreen(false)
			gIsFullScreen = false 
		end
		return
	end
	if gStartedFinalRitual == false then
		if (key == "return" or key == "enter" or key == "kpenter") and 
    GameState.current() == gameState and gPlayer ~= nil and gPlayer.isActive == true and gPlayer.rezInExplosion == nil and gLeavingLevelCounter == 0 and #gDialogArray == 0 then
			CWindowTerminal("Terminal", gDialogArray)
		end	
	end
	
	for i, v in pairs(gDialogArray) do if v:keypressed(key) then return end end
	
	if key == "f1" then
		takeScreenshot()
	end
	
	if key == "=" then gTrailerScanLines = not gTrailerScanLines end
	if key == "-" then gTrailerBloom = not gTrailerBloom end
	
	GameState.keypressed(key)
end

--------------------------------
function takeScreenshot()
	local screenshot = love.graphics.newScreenshot();
	screenshot:encode("png", "Spellrazor" .. os.time() .. '.png');
	playSound("cameraClick")
end

--------------------------------
function loadData()
	if love.filesystem.exists(SAVE_NAME) then
		local fs = love.filesystem.read(SAVE_NAME)
		print("**** Attempting Load")
		if fs ~= nil then
			gFirstTimePlayer = false
			local fun, err = loadstring(fs)
			if err then error(err) end
			local s = fun() --<-- this is the restored copy of the original table
			if true then -- s["Version"] == VERSION_NUMBER then
				gHighScores = s["gHighScores"] 
--				gPortalsOpened = s["gPortalsOpened"]
--				if gPortalsOpened == nil then gPortalsOpened = {} end
				gConsoleCommandsOpened = s["gConsoleCommandsOpened"]
				if gConsoleCommandsOpened == nil then gConsoleCommandsOpened = {} end
				gFinishedFinalRitual = s["gFinishedFinalRitual"]
				if gFinishedFinalRitual == nil then gFinishedFinalRitual = false end
        gDefaultGameTypeIndex = s["gDefaultGameTypeIndex"]
        if gDefaultGameTypeIndex == nil then gDefaultGameTypeIndex = DEFAULT_GAME_TYPE_INDEX end
        gDefaultSeedTypeIndex = s["gDefaultSeedTypeIndex"]
        if gDefaultSeedTypeIndex == nil then gDefaultSeedTypeIndex = DEFAULT_SEED_TYPE_INDEX end
				return
			else
--				love.errhand("Found save from earlier version. Deleting save! Sorry - you'll have to start your high score table afresh!")
				if love.filesystem.exists(SAVE_NAME) then	love.filesystem.remove(SAVE_NAME) end			
			end
		end
	end
	
	
	resetScoresCorrupted()
end

function resetScoresCorrupted()
	gHighScores = {}
	addHighScore("DMB", 10000000, "@%^e+$%*!",			"LEVEL 25")
	addHighScore("KSC", 1000000,  "Celebrity",		  "LEVEL 20")
	addHighScore("MUW", 750000,  	"Callousness", 		"LEVEL 15")
	addHighScore("ROS", 500000,  	"Overconfidence",	"LEVEL 12")
	addHighScore("OMD", 200000,  	"Incompetence",		"LEVEL 11")
	addHighScore("SJC", 160000,  	"Hubris",					"LEVEL 10")
	addHighScore("KTB", 120000,  	"Scorn",					"LEVEL 9")
	addHighScore("DMB", 80000,  	"Anguish",				"LEVEL 8")
	addHighScore("ABS", 40000,  	"Pride",					"LEVEL 7")
	addHighScore("SIN", 20000,  	"Ignorance",			"LEVEL 6")
	addHighScore("COS", 15000,  	"Intemperance", 	"LEVEL 5")
	addHighScore("ALT", 10000,  	"Foolishness",		"LEVEL 4")
	addHighScore("BRB", 8000,  		"Folly", 					"LEVEL 3")
	addHighScore("BFF", 4000,  		"Sloth", 					"LEVEL 2")
	addHighScore("ZAX", 1000,  		"Indifference", 	"LEVEL 1")
end




function resetScores()
	gHighScores = {}
	addHighScore("DMB", 10000000, "Ascension",			"LEVEL 25")
	addHighScore("KSC", 1000000,  "Celebrity",		  "LEVEL 20")
	addHighScore("MUW", 750000,  	"Callousness", 		"LEVEL 15")
	addHighScore("ROS", 500000,  	"Overconfidence",	"LEVEL 12")
	addHighScore("OMD", 200000,  	"Incompetence",		"LEVEL 11")
	addHighScore("SJC", 160000,  	"Hubris",					"LEVEL 10")
	addHighScore("KTB", 120000,  	"Scorn",					"LEVEL 9")
	addHighScore("DMB", 80000,  	"Anguish",				"LEVEL 8")
	addHighScore("ABS", 40000,  	"Pride",					"LEVEL 7")
	addHighScore("SIN", 20000,  	"Ignorance",			"LEVEL 6")
	addHighScore("COS", 15000,  	"Intemperance", 	"LEVEL 5")
	addHighScore("ALT", 10000,  	"Foolishness",		"LEVEL 4")
	addHighScore("BRB", 8000,  		"Folly", 					"LEVEL 3")
	addHighScore("BFF", 4000,  		"Sloth", 					"LEVEL 2")
	addHighScore("ZAX", 1000,  		"Indifference", 	"LEVEL 1")
end


--------------------------------
function saveData()
	local s = {}
	s["Version"]								= VERSION_NUMBER
	s["gHighScores"] 						= gHighScores
	--s["gPortalsOpened"]					= gPortalsOpened
	s["gConsoleCommandsOpened"]	= gConsoleCommandsOpened
	s["gFinishedFinalRitual"] = gFinishedFinalRitual
  s["gDefaultGameTypeIndex"] = gDefaultGameTypeIndex
  s["gDefaultSeedTypeIndex"] = gDefaultSeedTypeIndex
  
	s = serpent.dump(s)
	deleteSave()
	love.filesystem.write(SAVE_NAME, s)
	print("***** saving: " .. s)
end


function deleteSave(flag)
  if flag then
    gConsoleCommandsOpened	= {}
    DEFAULT_GAME_TYPE_INDEX = 1
    DEFAULT_SEED_TYPE_INDEX = 2
    gDefaultGameTypeIndex = DEFAULT_GAME_TYPE_INDEX
    gDefaultSeedTypeIndex = DEFAULT_SEED_TYPE_INDEX
    gFirstTimeConsoleOpened = true
    gFirstTimePlayed = true
  end
	if love.filesystem.exists(SAVE_NAME) then	love.filesystem.remove(SAVE_NAME) end
end

-------------------------------
function checkForHighScores(score)
	if score == 0 then return false end
	local index = 1
	for i, v in ipairs(gHighScores) do
		if score > v[2] then 
			break
		end
		index = index + 1
	end

	if index <= MAX_HIGHSCORES then return true end
	return false
end

-------------------------------
function addHighScore(name, score, death, level_name)
	if score == 0 then return end
	
	-- Split 'LEVEL X' into two bits of data
	-- Then pad the X part if it's too short
	level_name_bits = splitline(level_name, " ")
	if #level_name_bits > 1 then
		while level_name_bits[2]:len() < 3 do
			level_name_bits[2] = " " .. level_name_bits[2]
		end
	
		level_name = level_name_bits[1] .. " " .. level_name_bits[2]
	else
		
	end
	
	local index = 1
	for i, v in ipairs(gHighScores) do
		if score > v[2] then 
			break
		end
		index = index + 1
	end
	gLastInsertedScore = index
	table.insert(gHighScores, index, {name, score, string.upper(death), level_name})	
	if #gHighScores > MAX_HIGHSCORES then table.remove(gHighScores, #gHighScores) end
	
	saveData()
end

--------------------------------
function drawEndgamePattern()
	love.graphics.setCanvas(gScreenCanvas)
	
	if gRitualBlackScreenFraction > 0 then 
		love.graphics.setColor(0,0,0, math.floor(gRitualBlackScreenFraction * 255))
		love.graphics.rectangle("fill", 0,0,gScreenWidth, gScreenHeight) 
	end
	
	local cycle_time = 60
	local r2, g2, b2 = HSL((((gTime) % cycle_time) / cycle_time * 255) % 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local opacity = math.floor(gRitualOpacityFraction * 255)

	local linecolour = {r2/256 * 255, g2/256*255, b2/256*255,opacity}
	love.graphics.setBlendMode("add")
	love.graphics.setShader(gShaderScrolling)
	if gShaderScrolling ~= nil then
		gShaderScrolling:send("scroll", gRitualScrollFraction)
	end
	
	if gRitualTime < gTotalRitualPeriod - 10 then
		love.graphics.setColor(linecolour)

		for i, j in ipairs(gAround4Diagonal) do
			love.graphics.draw(generativeCanvas, (gScreenWidth / 2), (gScreenHeight / 2), gRitualRotation, 1 * SCALE * j[1], 1* SCALE * j[2]) 
		end
		
		r2, g2, b2 = HSL((((math.floor(cycle_time * 0.1) + gTime) % cycle_time) / cycle_time * 255) % 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
		linecolour = {r2/256 * 255, g2/256*255, b2/256*255,opacity}
		love.graphics.setColor(linecolour)
		
		for i, j in ipairs(gAround4Diagonal) do
			love.graphics.draw(generativeCanvas, (gScreenWidth / 2), (gScreenHeight / 2),gRitualRotation  * 2, 2 * SCALE * j[1], 2* SCALE * j[2]) 
		end

		r2, g2, b2 = HSL((((math.floor(cycle_time * 0.2) + gTime) % cycle_time) / cycle_time * 255) % 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
		linecolour = {r2/256 * 255, g2/256*255, b2/256*255,opacity}
		love.graphics.setColor(linecolour)

		for i, j in ipairs(gAround4Diagonal) do
			love.graphics.draw(generativeCanvas, (gScreenWidth / 2), (gScreenHeight / 2),gRitualRotation * 3, 3 * SCALE * j[1], 3* SCALE * j[2]) 
		end
		
		--- Draw weird face
--		love.graphics.setShader()
		if gShaderScrolling ~= nil then
			gShaderScrolling:send("scroll", 0)
		end
		
		local goddess_alpha_value_sin_range = 110--8
		local goddess_time_start = 20 
		local jitter = 0--8
		if gRitualTime > goddess_time_start then 
			local a = math.max(0, (gRitualTime - goddess_time_start) / (gTotalRitualPeriod - goddess_time_start))
			a = math.sin(a * math.pi/2)
			a = math.floor((a * goddess_alpha_value_sin_range) * math.random())
			if a > 100 then a = 160 
			elseif a < 90 then a = 0 
			else a = 60 end
			linecolour = {r2/128 * 255, g2/128*255, b2/128*255,a}

			love.graphics.setColor(linecolour);

			local scale = gScreenHeight / gGoddessImage:getHeight()
			if TRAILER_MODE_ON ~= true then
				love.graphics.draw(gGoddessImage, math.floor(gScreenWidth * 0.5) - jitter + math.random(jitter), math.ceil(gScreenHeight * 0.5) + math.random(jitter), 0, scale,scale, gGoddessImage:getWidth() / 2, gGoddessImage:getHeight() / 2 )
			end
		end
	else
		if gRitualTime > gTotalRitualPeriod - 9.9 then
			setGlitchingForTime(-0.5)
		end
		if gShaderScrolling ~= nil then
			gShaderScrolling:send("scroll", 0)
		end
		local wid = 1000
		love.graphics.setFont(bigFont)
		love.graphics.setColor(linecolour[1],linecolour[2],linecolour[3],128)
		love.graphics.printf("44 days\n\n41.1, -74.44", -(wid / 2) + (gScreenWidth / 2), (gScreenHeight / 2), wid, 'center')
		love.graphics.setFont(font)
	end
	
	love.graphics.setCanvas(gScreenCanvas)
	love.graphics.setBlendMode("alpha")
	love.graphics.setShader()
end