// A shader to recolour text based on its screen position position
extern vec4 topColour;
extern vec4 midColour;
extern vec4 bottomColour;
extern number scroll;
extern number offsetX;
extern number offsetY;
extern number gridSizeX;
extern number gridSizeY;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{  
  vec4 pixel = Texel(texture, texture_coords);

/*  int sx = int(screen_coords.x + offsetX) / int(gridSizeX);
  int sy = int(screen_coords.y + offsetY) / int(gridSizeY);*/
  int sx = int((screen_coords.x + offsetX) / gridSizeX);
  int sy = int((screen_coords.y + offsetY) / gridSizeY);
  
  int s = int(scroll * 10.0);
  //int colour_types = 6;
  int index;
  int row = int(mod(float(sy), 2.0)); 
  
  s = int(mod(float(s), 6.0/*float(colour_types)*/));

  if(row == 0)
    index = sx + s;
  else
    index = sx + (5/*colour_types*/ - s/* - 1*/);
  
  index = int(mod(float(index), 6.0/*float(colour_types)*/));  
  //  0      1     2     3     4    5
  // top  - top - mid - mid - bot - bot
  // top  - mid - mid - bot - bot - top
  /*if(index == 0)
    pixel *= topColour;
  else if(index == 1)
    pixel *= (topColour + midColour) * 0.5;
  else if(index == 2)
    pixel *= midColour;
  else if(index == 3)
    pixel *= (midColour + bottomColour) * 0.5;
  else if(index == 4)
    pixel *= bottomColour;  
  else
    pixel *= (topColour + bottomColour) * 0.5;*/
  int index2 = (index+1) / 2;
  index = index / 2;
  pixel *= (((index==0)?topColour:(index==1)?midColour:bottomColour)+((index2==0)?topColour:(index2==1)?midColour:bottomColour)) * 0.5;
  
  return pixel;
}