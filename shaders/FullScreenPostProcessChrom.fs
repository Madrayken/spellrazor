// A basic glitch shader with chromatic abberation, brightening and darkening, applied to the whole screen
extern number colourAdd;
extern number chromaticAbberation;
extern number brightMult;
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  vec4 pixel = vec4(0,0,0,1);

  texture_coords.x += chromaticAbberation;
  vec4 pixelR = Texel(texture, texture_coords);
  texture_coords.x -= (chromaticAbberation * 2.0);
  texture_coords.y -=  chromaticAbberation;

  vec4 pixelG = Texel(texture, texture_coords);
  texture_coords.y += (chromaticAbberation * 2.0);

  vec4 pixelB = Texel(texture, texture_coords);
 
  float average = (pixelR.r + pixelG.g + pixelB.b) / 3.0;
  pixel.r = (brightMult * average) + (pixelR.r + colourAdd);
  pixel.g = (brightMult * average) + (pixelG.g + colourAdd); 
  pixel.b = (brightMult * average) + (pixelB.b + colourAdd); 

  return pixel * 1.2;
}