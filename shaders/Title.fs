// A shader to smoothly recolour the world based on your position
extern vec4 topColour;
extern vec4 midColour;
extern vec4 bottomColour;
extern number scroll;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{  
  vec4 pixel = Texel(texture, texture_coords);
  float line_div = 1.0;
  texture_coords.y -= scroll;
  texture_coords.y = (texture_coords.y * line_div) - floor(texture_coords.y * line_div);

  if(texture_coords.y > 0.75)
  {
    float dist = (texture_coords.y - 0.75) / 0.25;
    pixel = pixel * ((bottomColour * dist) + (midColour * (1.0 - dist)));
  }
  else if(texture_coords.y > 0.5)
  {
    float dist = (texture_coords.y - 0.5) / 0.25;
    pixel = pixel * ((midColour * dist) + (topColour * (1.0 - dist)));
  }
  else if(texture_coords.y > 0.25)
  {
    float dist = (texture_coords.y - 0.25) / 0.25;
    pixel = pixel * ((topColour * dist) + (midColour * (1.0 - dist)));
  }
  else
  {
    float dist = texture_coords.y / 0.25;
    pixel = pixel * ((midColour * dist) + (bottomColour * (1.0 - dist)));
  }
  return pixel;
}