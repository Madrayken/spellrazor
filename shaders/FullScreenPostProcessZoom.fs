// A basic glitch shader with chromatic abberation, brightening and darkening, applied to the whole screen
extern number colourAdd;
extern number chromaticAbberation;
extern number greenChrome;
extern number brightMult;
extern number zoom;
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  vec4 pixel = vec4(0,0,0,1);
  float x_off = (800.0 / love_ScreenSize.x) * 0.5; // Note these HAD to be blah.zero style, or it didn't work!
  float y_off = (480.0 / love_ScreenSize.y) * 0.5;
  texture_coords.x = ((texture_coords.x - x_off) * zoom) + x_off;
  texture_coords.y = ((texture_coords.y - y_off) * zoom) + y_off;

  texture_coords.x += chromaticAbberation;
  vec4 pixelR = Texel(texture, texture_coords);
  texture_coords.x -= (chromaticAbberation * 2.0);
  texture_coords.y -=  chromaticAbberation;

  vec4 pixelG = Texel(texture, texture_coords);
  texture_coords.y += (chromaticAbberation * 2.0);

  vec4 pixelB = Texel(texture, texture_coords);
 
  float gc = 1.0 - greenChrome;
  float average = (pixelR.r + pixelG.g + pixelB.b) / 3.0;
  pixel.r = (brightMult * average) + (pixelR.r + colourAdd) * gc;
  pixel.g = (brightMult * average) + ((pixelG.g + colourAdd) * gc) + (average * (1.0 - gc)); 
  pixel.b = (brightMult * average) + (pixelB.b + colourAdd) * gc; 

  return pixel * 1.2;
}