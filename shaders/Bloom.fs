vec4 effect(vec4 colour, Image tex, vec2 tc, vec2 sc)
{  
  vec4 source = Texel(tex, tc);
  
  // Was 1024
  tc.x -= 1.0 / 1024.0;
  vec4 left = Texel(tex, tc) * 0.2;

  // Was 1024

  tc.x += 2.0 / 1024.0;
  vec4 right = Texel(tex, tc) * 0.2;
  float sl = floor(mod(sc.y, 2.0));
  return (source + (left + right)*sl) * colour * (0.8+sl);
}