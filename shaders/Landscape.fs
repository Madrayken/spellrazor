// A shader to smoothly recolour the world based on your position
extern vec4 wallColour1;
extern vec4 wallColour2;
extern vec4 floorColour1;
extern vec4 floorColour2;

extern vec2 focalPos;
#define DEADZONE 0.25
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  float gamut = 32.0 * 5.0; //200;
  vec2 factor =  screen_coords - focalPos;
  float dist = length(factor) / gamut;
  
  dist -= DEADZONE;
  dist = max(dist, 0.0);
  dist *= 1.0 / (1.0 - DEADZONE);
  
  vec4 pixel = Texel(texture, texture_coords);
    
  float wallMult  = pixel.b;
  float floorMult = pixel.r;

  pixel.r = (wallMult * ((wallColour1.r * (1.0-dist)) + (wallColour2.r * (dist)))) + (floorMult * ((floorColour1.r * (1.0-dist)) + (floorColour2.r * (dist))));
  pixel.g = (wallMult * ((wallColour1.g * (1.0-dist)) + (wallColour2.g * (dist)))) + (floorMult * ((floorColour1.g * (1.0-dist)) + (floorColour2.g * (dist))));
  pixel.b = (wallMult * ((wallColour1.b * (1.0-dist)) + (wallColour2.b * (dist)))) + (floorMult * ((floorColour1.b * (1.0-dist)) + (floorColour2.b * (dist))));
  //pixel.xyz = (wallMult * ((wallColour1.xyz * (1.0-dist)) + (wallColour2.xyz * (dist)))) + (floorMult * ((floorColour1.xyz * (1.0-dist)) + (floorColour2.xyz * (dist))));
  pixel = pixel * color;
  return pixel;
}