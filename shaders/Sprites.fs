// A shader to smoothly recolour the world based on your position
extern vec4 colour1;
extern vec4 colour2;
extern vec2 focalPos;
#define DEADZONE 0.25

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  float gamut = 32.0 * 5.0; //200;
  vec2 factor =  screen_coords - focalPos;
  float dist = length(factor) / gamut;
  
  dist -= DEADZONE;
  dist = max(dist, 0.0);
  dist *= 1.0 / (1.0 - DEADZONE);
  
  
  vec4 pixel = Texel(texture, texture_coords);
  
  pixel.r *= ((colour1.r * (1.0-dist)) + (colour2.r * (dist)));
  pixel.g *= ((colour1.g * (1.0-dist)) + (colour2.g * (dist)));
  pixel.b *= ((colour1.b * (1.0-dist)) + (colour2.b * (dist)));
  
  //pixel.xyz *= ((colour1.xyz * (1.0-dist)) + (colour2.xyz * (dist)));
  pixel.a *= color.a;
  return pixel;
}