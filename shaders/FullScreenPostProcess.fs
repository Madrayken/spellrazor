// A basic glitch shader with chromatic abberation, brightening and darkening, applied to the whole screen
extern number colourAdd;
extern number brightMult;
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  vec4 pixel = vec4(0,0,0,1);

  vec4 pix = Texel(texture, texture_coords);
 
  /*float average = (pix.r + pix.g + pix.b) / 3.0;
  pixel.xyz = (brightMult * average) + (pix.xyz + colourAdd);*/
  pixel.r = pix.r*(1.0+brightMult) + colourAdd;
  pixel.g = pix.g*(1.0+brightMult) + colourAdd;
  pixel.b = pix.b*(1.0+brightMult) + colourAdd;

  return pixel * 1.2;
}