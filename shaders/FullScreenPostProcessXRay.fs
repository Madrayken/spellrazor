// A basic glitch shader with chromatic abberation, brightening and darkening, applied to the whole screen
extern number colourAdd;
extern number greenChrome;
extern number brightMult;
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  vec4 pixel = vec4(0,0,0,1);

  vec4 pix = Texel(texture, texture_coords);

  float gc = 1.0 - greenChrome;
  float average = (pix.r + pix.g + pix.b) / 3.0;
  pixel.r = (brightMult * average) + (pix.r + colourAdd) * gc;
  pixel.g = (brightMult * average) + ((pix.g + colourAdd) * gc) + (average * (1.0 - gc)); 
  pixel.b = (brightMult * average) + (pix.b + colourAdd) * gc; 

  return pixel * 1.2;
}