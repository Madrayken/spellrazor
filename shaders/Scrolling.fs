extern number scroll;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{ 
  texture_coords.x -= scroll;
  texture_coords.y -= scroll;
  
  /*if(texture_coords.x > 1.0) texture_coords.x -= 1.0;
  if(texture_coords.y > 1.0) texture_coords.y -= 1.0;

  if(texture_coords.x < 0.0) texture_coords.x += 1.0;
  if(texture_coords.y < 0.0) texture_coords.y += 1.0;*/
  texture_coords.x += (texture_coords.x > 1.0)?-1.0:(texture_coords.x < 0.0)?1.0:0.0;
  texture_coords.y += (texture_coords.y > 1.0)?-1.0:(texture_coords.y < 0.0)?1.0:0.0;

  vec4 pixel = Texel(texture, texture_coords) * color;  
  return pixel;
}