// A basic glitch shader with chromatic abberation, brightening and darkening, applied to the whole screen
//extern number colourAdd = 0;
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  vec4 pixel = Texel(texture, texture_coords) * color;
  
  /*vec2 tex_above = texture_coords;
  vec2 tex_below = texture_coords;
  tex_above.y -= (1.0 / 360.0) * 0.5;
  tex_below.y += (1.0 / 360.0) * 1.5;*/
  
/*  int sy = int(screen_coords.y);
  int row = int(mod(sy, 2)); 
  float mult = 0.75;
  
  vec4 pabove = Texel(texture, tex_above) * color;
  vec4 pbelow = Texel(texture, tex_below) * color; 

  if(row == 0)
  {
    pixel.r *= 0.35; 
    pixel.g *= 0.35; 
    pixel.b *= 0.35; 
    pixel += (pabove + pbelow) * 0.25; 
  }
  else
  {
    pixel.r *= 1.2;
    pixel.g *= 1.2; 
    pixel.b *= 1.2; 
    pixel += (pabove + pbelow) * 0.15; 
  }*/

  return pixel;
}