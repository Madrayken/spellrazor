extern number frame = 0;
extern number alpha = 1;
extern number cutoff = 0.5;
extern number modX = 20;
extern number modY = 17;
extern number frameXMult = 0;
extern number frameYMult = 0;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{ 
  int x = int(screen_coords.x - love_ScreenSize.x / 2);
  int y = int(screen_coords.y - love_ScreenSize.y / 2);
  
  x = int(int(x / 10.0) * 10);
  y = int(int(y / 10.0) * 10);
  
  int dist = int(sqrt(x * x + y * y));
  if(mod(dist, 256) < (cutoff * 256)) return vec4(1,1,1,1);
  return vec4(1,1,1,0);
}