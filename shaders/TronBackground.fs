extern number zoom;
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{  
  float z = zoom;
  z = z * z;
  
  /*texture_coords.x = ((texture_coords.x - 0.5) * z) + 0.5;
  texture_coords.y = ((texture_coords.y - 0.5) * z) + 0.5;*/
  texture_coords = ((texture_coords - 0.5) * z) + 0.5;
  vec4 pixel = Texel(texture, texture_coords) * color;
  
 /* vec2 tex_above = texture_coords;
  vec2 tex_below = texture_coords;
  tex_above.y -= (1.0 / 480.0) * 0.5;
  tex_below.y += (1.0 / 480.0) * 1.5;
  
  vec4 pabove = Texel(texture, tex_above) * color;
  vec4 pbelow = Texel(texture, tex_below) * color; 
  
  pixel *= 1.2;
  pixel += (pabove + pbelow) * 0.15; */
 
  return pixel;
}