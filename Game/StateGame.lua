gameState = {}
gTotalTime = 0
MAX_NODES_WIDE 		= 15-- 3
MAX_NODES_HIGH 		= 15-- 3

NODE_FLOOR_SIZE 				= 3 -- What size are the chunks of floor making up the nodes
NODE_SIZE 		= NODE_FLOOR_SIZE + 1
gMapWidth 		= (NODE_SIZE * MAX_NODES_WIDE) + 1
gMapHeight 	= (NODE_SIZE * MAX_NODES_HIGH) + 1 -- width and height in tiles 

gPlayerSuicide = false
gScore = 0
gScoreMultiplier = 1
gGold = 5
gNextLevelHasPredefinedName = nil
INITIAL_SPELL_COST = 5
gSpellCost = INITIAL_SPELL_COST
gPause = false
gSecurityOff = false
gDefaultPitch = 1.0

gRitualSaidCorrectly = false
gFinishedFinalRitual = false

DEFAULT_SLOWMO_STATE = true
gBoughtSlowmo = false
gSlowmo = DEFAULT_SLOWMO_STATE
gForcedSlowmo = false -- Specifies whether the game starts off as slowed or not

MAX_ROOM_SIZE_X		= 5 -- 4
MAX_ROOM_SIZE_Y		= 4 -- 3
NUMBER_OF_ENEMIES = 20
NUMBER_OF_PICKUPS = 10

IDEAL_FPS = 60
IDEAL_TICK = 1/ 60
FIXED_STEP = 1 / 20
MAX_STEP = 1 / 8

MAX_SPELLS_CARRIED = 10
--------------------------
gSpellsCarried = {}
gSpellsCarriedTimeSinceChanged = {}
gSpellsFound = {}

--------------------------
MAX_INTANGIBLE_TIME = 0.8

MAX_SHIELDS_LOW_SOUND_TIME = 3
MAX_MESSAGE_TIME = 4
MAX_INFO_MESSAGE_TIME = 1
MAX_SPELL_MESSAGE_TIME = 1
LEAVE_LEVEL_TIME = 2

FACTION_NULL 		= 0 -- DO NOT ALTER
FACTION_PLAYER 	= 1 -- DO NOT ALTER
FACTION_ENEMY	 	= 2 -- DO NOT ALTER
FACTION_PICKUP 	= 3 -- DO NOT ALTER

FLOOR_TILE_INDEX = 1
WALL_TILE_INDEX = 16

local gPowerOff = false
gGameOverTimer = 0
GAME_OVER_TIME = 5
GAME_OVER_TEXT_DISPLAY_TIME = GAME_OVER_TIME * 0.75
GAME_OVER_DARKENING_TIME = GAME_OVER_TIME * 0.25

gCurrentLevel = 1
gCurrentLevelName = "LEVEL 1"
gLevelHasBoss = false

local gIdleTimer = 0
local MAX_IDLE_TIMER = 1
local SLOWDOWN_IDLE_TIMER = MAX_IDLE_TIMER * 0.1 --<< Must always be larger 
local EXIT_IDLE_VALUE			= SLOWDOWN_IDLE_TIMER * 0.9 --<< than this
local VELOCITY_LIMIT_FOR_SLOWDOWN = 0.09
local	gNoInputTimer = 0
local SLOWDOWN_NO_INPUT_TIMER = 0.1

local DANGER_CIRCLE_RADIUS = 7
local DANGER_CIRCLE_RADIUS_SQ = DANGER_CIRCLE_RADIUS * DANGER_CIRCLE_RADIUS

local gShowRedCircle = false
local gSlowdownModeKeyReleased = true
local gSlowdownModeActivated = false
local gSlowdownRemovalPeriod = 0
local MAX_SLOWDOWN_REMOVAL_PERIOD = 0.25
local gOldSlowdownModeActivated = nil
gVaultRoomID = 0

gSpiralTable = {}

local MAX_PORTAL_OPEN_TIME = 1
gPortalOpenTime = 0
gPortalIsOpen = false

gTarget = nil
gNearestEnemy = nil

DRAG = 12
IMPULSE = 30
NORMAL_SPEED = 11
FAST_SPEED = 20

gGravity = 36

MAX_SECURITY_TIME = 99
gSecurityTime = 0
MAX_SECURITY_BOTS = 3
gSecurityBotCount = 0

gPlayer = nil
gShieldsLowSoundTimer = 0

MAX_MAP_EFFECT_TIME = 0.5
gMapEffectTimer = 0

MAX_X_RAY_TIME = 10
gXRayTime = 0
local gCreatureCount = 0

gCreatureArray = {}
gPickupArray = {}
gSpellArray = {}
gExplosionArray = {}
gMapFeatureArray = {}
gWorldMessageArray = {}

gActionStack = {}

MAX_LIGHTING_TIME = 0.2
gLightningTimeLeft = 0

gCurrentScreenShakeTime = 0 
gTotalScreenShakeTime = 0
gCurrentScreenShakePower = 0

gUpperCaseList 					= {"A","B","C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N","O","P","Q","R","S","T","U","V","W","X","Y","Z"}
gLimitedUpperCaseList 	= {"A","B","C", "D", "E", "F", "H", "I", "J", "K", "L", "M", "N", "O", "P", "S", "T", "U", "V", "W","X"}

gKeyCodes 			= {"a","b","c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n","o","p","q","r","s","t","u","v","w","x","y","z"}
gShieldCosts = {10, 25, 50}

gScreenX = 0
gScreenY = 0

gSoldMessageTime = 0
gSoldMessage = ""

gMapFeatureTouched = nil
gMapImage = nil

gKilledBoss = false
gPortalPosition = vector(0,0)

local gBottomBarHeight = 18

gHighscoreImage = nil

--------------------------
-- Gamestate specific functions
--------------------------
-------------------------------	
-- The GAME state
-------------------------------
local gGameSeeds = {}
local gCurrentGameSeedIndex = 1
local MAX_GAME_SEED_INDEX = 100
local MAX_SEED_SIZE = 2821109907455
function gameState:enter()	
	-- Unit test randomisation
	-- testRewards()
	-- End of test
  gFirstTimeConsoleOpened = true
	gPlayerSuicide = false
	gHasCheated = false
    	
	gEngineLoop = love.audio.newSource("assets/EngineLoop.wav")
	gEngineLoop:setLooping(true)
	gEngineLoop:setVolume(0)
	gEngineLoop:play()

	gWhisperLoop = love.audio.newSource("assets/WhisperLoop.wav")
	gWhisperLoop:setLooping(true)
	gWhisperLoop:setVolume(0)
	gWhisperLoop:setPitch(0.5)
	gWhisperLoop:play()

	clearConsoleText()
	gPause = false
	gDebug = false
	gGold = 5
	gSpellCost = INITIAL_SPELL_COST
	gBoughtSlowmo = false
	-- gSlowmo = DEFAULT_SLOWMO_STATE
	gSecurityOff = false
	
	gSpellMessageTimer = 0
	gSpellMessage = ""
	gSpellMessageIndex = 0

	gBackgroundMusic = nil
	gBackgroundMusic = love.audio.newSource("assets/MusicLoop1.mp3", 'stream' )
	gBackgroundMusic:setLooping( true ) --so it doesnt stop
	gBackgroundMusic:setVolume(0.1)
	
	local seed = os.time() % 2821109907455
	if gSeedCode ~= "" then
		print("Using user seed: " .. gSeedCode)
		seed = fromBaseN(gSeedCode,36)   
	end
--	print("Seed = " .. seed)
--	local n = toBaseN(seed,36)
--	print("SeedCode = " .. n)
--	local t = fromBaseN(n,36)
--	print("SeedUnCode = " .. t)

	math.randomseed(seed)
	
	for i = 1, MAX_GAME_SEED_INDEX do
		gGameSeeds[i] = math.floor(math.random() * 2821109907455)
	end
	gCurrentGameSeedIndex = 1
	
	gHighscoreImage = love.graphics.newImage("assets/HighscoreBlock.png")
	gScore = 0
	gMapImage = nil
	gMapFeatureTouched = nil
	----------------
	if TRAILER_MODE_ON ~= true then
		gBackgroundMusic:play()
	end
	gDefaultShieldLevel = 1
	gMaxShieldLevel = gDefaultShieldLevel
	gLastCorpsePosition = nil
	gPortalIsOpen = false
	-- Create inventory
	gSpellsCarried = {}
	gSpellsFound = {}
	for i, v in ipairs(gUpperCaseList) do
		gSpellsCarried[v] = 0
		gSpellsFound[v] = false
    gSpellsCarriedTimeSinceChanged[v] = 0
	end
	
	addSpellsWithLetter("A", 3)
	addSpellsWithLetter("S", 3)
	
	if TRAILER_MODE_ON == true then
		addAllSpells()
		gGUIOff = true
		gGold = 94
--		gSlowmo = false
	end
	
	gSpellsAltered = true
	------------------
	gCurrentLevel = 1 
	gCurrentLevelName = "LEVEL 1"
	createGameAtLevel(gCurrentLevel)
end	

------------------------------------
local oldSlowSound = nil
function gameState:update(dt)	
	gTotalTime = gTotalTime + dt
	gSlowdownRemovalPeriod = math.max(0, gSlowdownRemovalPeriod - dt)
			
	if gPlayerSuicide == true and gPlayer ~= nil and gPlayer.isDying == false and gPlayer.jumpTime == 0 and gPlayer.rezInExplosion == nil and gLeavingLevelCounter == 0 then 
		gPlayer:damageShield("suicide", 99999)
		gameState:cleanup()
		GameState.switch(highScoreInput)
		return
	end		
			
			
	local slow_sound = false
	if gPlayer ~= nil and gPlayer.isDying == false and gPlayer.jumpTime == 0 and gPlayer.rezInExplosion == nil and gLeavingLevelCounter == 0 and gSlowmo == true and gSlowdownRemovalPeriod == 0 and gSlowdownModeActivated then 
		dt = dt * 0.1 
		slow_sound = true
	end
		
	if slow_sound ~= oldSlowSound then
		if slow_sound == true then
			setPitchOfAllSounds(0.1)
		else
			setPitchOfAllSounds(1)
		end
		oldSlowSound = slow_sound
	end

	if slow_sound then gDefaultPitch = 0.1 else gDefaultPitch = 1 end	
	if gPause then return end
	
	gNoInputTimer = math.min(gNoInputTimer + dt, SLOWDOWN_NO_INPUT_TIMER)
	
	if gPlayer ~= nil then
		local len = gPlayer.velocity:len() 
		if len <= VELOCITY_LIMIT_FOR_SLOWDOWN then
			gIdleTimer = math.min(gIdleTimer + dt, MAX_IDLE_TIMER)
		else
			gIdleTimer = 0
		end
		
		-- tick down the score multiplier
--[[		if gScoreMultiplier > 1 then 
			gScoreMultiplier = gScoreMultiplier - 0.025 * dt
			if gScoreMultiplier <= 1 then 
				gScoreMultiplier = 1
			end
		end]]
		
		local old_sec_time = gSecurityTime
		if gSecurityOff == false and gFreezeUntilEnemySpotted == false and gPlayer ~= nil and gPlayer.rezInExplosion == nil and gLeavingLevelCounter == 0 and gSecurityTime > 0 then
			if gPlayer ~= nil and gPlayer.currentRoomID ~= 0 and gPlayer.currentRoomID == gVaultRoomID then
				if gPlayer.oldRoomID ~= gPlayer.currentRoomID then
					displayMessage("TRESPASSER!\nSECURITY INCREASED!")
				end
        if gInfoMessageTimer == 0 then
          gSecurityTime = gSecurityTime - dt * 10 -- Make this tick down faster when we're somewhere illegal
        end
			else
        if gInfoMessageTimer == 0 then
          gSecurityTime = gSecurityTime - dt
        end
			end
			if gSecurityTime <= 0 then
				gameState:traceComplete()
			end
		end		
		
		local int_sec_time = math.floor(gSecurityTime)
		if int_sec_time == 5 then
			if int_sec_time ~= math.floor(old_sec_time) then -- We've crossed a second boundary
				displayMessage("SECURITY IMMINENT") 
				playSound("SecurityCountdown", 1)
			end	
		end
		
		if gLightningTimeLeft > 0 then
			gLightningTimeLeft = gLightningTimeLeft - dt
			if gLightningTimeLeft < 0 then
				gLightningTimeLeft = 0
			end
		end	
		
		if gPortalOpenTime > 0 then
			gPortalOpenTime = gPortalOpenTime - dt
			if gPortalOpenTime <= 0 then gPortalOpenTime = 0 end
		end
		
		if gMapEffectTimer > 0 then 
			gMapEffectTimer = gMapEffectTimer - dt
			if gMapEffectTimer <= 0 then gMapEffectTimer = 0 end
		end
		
		if gShieldsLowSoundTimer > 0 then 
			gShieldsLowSoundTimer = gShieldsLowSoundTimer - dt
			if gShieldsLowSoundTimer <= 0 then
				playSound("ShieldsLow")  
				self:showShieldWarning()
				gShieldsLowSoundTimer = MAX_SHIELDS_LOW_SOUND_TIME
			end
		end
	end
	
	
--	if gIdleTimer < MAX_IDLE_TIMER then
		if gMessageTimer > 0 then 
      if gMessageTag == "LOCKED" then
        if gVaultRoomID == 0 then 
          gMessageTimer = 0 
        end
      end
      
			gMessageTimer = gMessageTimer - dt
			if gMessageTimer <= 0 then
				gMessageTimer = 0
			end
		end
    
    if gInfoMessageTimer > 0 then 
			gInfoMessageTimer = gInfoMessageTimer - dt
			if gInfoMessageTimer <= 0 then
				gInfoMessageTimer = 0
			end
		end
    
--	end
	
	if gSpellMessageTimer > 0 then 
		gSpellMessageTimer = gSpellMessageTimer - dt
		if gSpellMessageTimer <= 0 then
			gSpellMessageTimer = 0
		end
	end
	
	if gXRayTime > 0 then 
		gXRayTime = gXRayTime - dt
		if gXRayTime < 0 then gXRayTime = 0 end
	end
	
	if gLeavingLevelCounter > 0 then
		gScreenRotate = math.rad(1000 * (1 - (gLeavingLevelCounter / LEAVE_LEVEL_TIME)))
	end
	
	---------------------------
	-- Watch to see if we've force-skipped a level
	if gTrailerForcedLevelNumber ~= nil then
		gCurrentLevel = gTrailerForcedLevelNumber
		createGameAtLevel(gCurrentLevel)
		gTrailerForcedLevelNumber = nil
	end
	
	---------------------------
	if gLeavingLevelCounter > 0 then
		if gPlayer ~= nil and gPlayer.isDying == false and gPlayer.isActive then
			local dv = gPortalPosition - gPlayer.position 
			dv:normalize_inplace()
			gPlayer.velocity = dv * 2
		end
		
		gLeavingLevelCounter = gLeavingLevelCounter - dt
		if gLeavingLevelCounter <= 0 then
			gLeavingLevelCounter = 0
			
			
      if gNextLevelHasPredefinedName == nil then
				gCurrentLevel = gCurrentLevel + 1
			end
			
			createGameAtLevel(gCurrentLevel)
			return
		end
	end
	
	if gGameOverTimer > 0 then
		gGameOverTimer = gGameOverTimer - dt
		if gGameOverTimer <= 0 then 
			gameState:cleanup()
			GameState.switch(highScoreInput)
			return
		end
	end
	
	local old_player_room_id = 0
	
  dt = math.min(dt, MAX_STEP)
	local accum = dt
	repeat
    local dt2 = math.min(accum, IDEAL_TICK) --  math.min(IDEAL_TICK, accum )   -- use whatever max dt value works best for you
		accum = accum - dt2

    if gPlayer ~= nil and gPlayer.rezInExplosion == nil and gLeavingLevelCounter == 0 then
      processMovementInputs(dt)
      old_player_room_id = gPlayer.currentRoomID
    end

    updatePhysicsForActorArray(gMapFeatureArray, dt2)
    updatePhysicsForActorArray(gCreatureArray, dt2); updateAvoidVoidActorArray(gCreatureArray, dt2)
    
    updatePhysicsForActorArray(gPickupArray, dt2)
    updatePhysicsForActorArray(gSpellArray, dt2); updateAvoidVoidActorArray(gSpellArray, dt2)

    ------------------------------------------
    updatePhysicsForAttachedActorArray(gCreatureArray, dt2)
    updatePhysicsForAttachedActorArray(gSpellArray, dt2)
    updatePickupActorArray(dt2)

    ------------------------------------------
    for i = #gExplosionArray, 1, -1 do
      local ex = gExplosionArray[i]
      if ex:update(dt2) == false then
        for j, v in ipairs(gCreatureArray) do
          if v.rezInExplosion == ex then
            v.rezInExplosion = nil
            break -- 'cos it won't be linked to two creatures!
          end
        end
        
        table.remove(gExplosionArray, i)
      end
    end
  until accum <= 0

	fixedUpdate(dt) -- pass it the dt variable so it can use it to create a fixed time step
	-- Focus the map on the correct bit now
	if gPlayer ~= nil then		
		hideAllRooms()
		
		if old_player_room_id ~= gPlayer.currentRoomID then -- If the player changes room, do this
			playerRoomChange(old_player_room_id, gPlayer.currentRoomID)
		end
		
		getRoomFromID(gPlayer.currentRoomID).isVisible = true -- Force the current room to be visible
		
		if mapAndRevealRoomsInSightLine(gPlayer.mapX, gPlayer.mapY, gPlayer.currentRoomID) == true then
			playSound("RoomDiscovered")
		end
		
		cameraLookAtPlayer(dt)
		
		if isPositionIsNearLockedDoorInRoomWithID(gPlayer.position, gPlayer.currentRoomID) then
			displayMessage("LOCKED!\n\nOpen the Console with RETURN, then type\nin the 4-digit code hidden on this level.", "LOCKED")
		end
	end
	
	updateTilesetBatch()
	updateDebugInfo()
end

------------------------
function gameState:showShieldWarning()
	if gSpellsCarried["S"] > 1 then displayMessage("NO SHIELDS! Press [S] to cast Shield!", "SHIELD") 
	elseif gGold >= gSpellCost then displayMessage("NO SHIELDS! BUY A SHIELD OR TYPE SHIELDUP IN THE CONSOLE!", "SHIELD")
	else displayMessage("SHIELDS DEPLETED: SELL SPELLS FOR GOLD AND BUY A SHIELD IN THE CONSOLE!", "SHIELD") end
end
------------------------
function gameState:removeShieldWarning()
  if gMessageTag == "SHIELD" then
    gMessageTimer = 0 
  end
end

------------------------
function gameState:cleanup()
	gGameOverTimer = 0
	gXRayTime = 0
	gLightningTimeLeft = 0
	gScreenRotate = 0
	gExitTimer = 0
	gBackgroundMusic:stop()
	stopAllSound()
	setPitchOfAllSounds(1)
	gStartedFinalRitual = false
end

------------------------
function gameState:draw()	
	local wall_r1, wall_g1, wall_b1 = HSL((gFixedFrameCount % 1000) / 1000 * 255, 					200, 128)
	local wall_r2, wall_g2, wall_b2 = HSL(((250 + gFixedFrameCount) % 1000) / 1000 * 255, 	200, 128)
	if gPowerOff then
		wall_r1, wall_g1, wall_b1 = 120, 90, 90
		wall_r2, wall_g2, wall_b2 = 120, 90, 90
	end

	-- gLevelIsDark = true
	if gShaderLandscapeRecolour ~= nil and gShaderSprites ~= nil then

		if gLevelIsDark then 
			wall_r1 = 0; wall_g1 = 0; wall_b1 = 0
		end
		gShaderLandscapeRecolour:send("wallColour1", {wall_r2 / 256, wall_g2 / 256, wall_b2 / 256, 1})
		gShaderLandscapeRecolour:send("wallColour2", {wall_r1 / 256, wall_g1 / 256, wall_b1 / 256, 1})
		gShaderSprites:send("colour1", {1,1,1,1})
		
		if gXRayTime ~= 0 or gLevelIsDark == false then
			gShaderSprites:send("colour2", {1,1,1,1})
		elseif gLevelIsDark then
			gShaderSprites:send("colour2", {wall_r1 / 256, wall_g1 / 256, wall_b1 / 256, 1})
		end
			
		if gForcedSlowmo == false and gSlowdownModeActivated and gSlowdownRemovalPeriod == 0 then  
			gShaderLandscapeRecolour:send("floorColour1", {1,0,0, 1})
			gShaderLandscapeRecolour:send("floorColour2", {0,0,0,1})
		elseif gShowRedCircle then
			gShaderLandscapeRecolour:send("floorColour1", {1,0.5,0, 1})
			gShaderLandscapeRecolour:send("floorColour2", {0,0,0,1})
		else
			gShaderLandscapeRecolour:send("floorColour1", {0.7, 0.7, 0.2, 1})
			gShaderLandscapeRecolour:send("floorColour2", {0.4, 0, 0.4,1})
		end
		
		gShaderLandscapeRecolour:send("focalPos", {gScreenX, gScreenY})
		gShaderSprites:send("focalPos", {gScreenX, gScreenY})
	end
	love.graphics.setShader(gShaderLandscapeRecolour)
	drawLevel()
	drawMapFeatures()
	drawWorldMessages()
	
	-- Draw the circle around the player showing the danger radius

	if gPlayer ~= nil then
		if gSlowdownModeActivated == false and gForcedSlowmo == false then
			local mx, my = cellXYToScreenXY(gPlayer.drawX, gPlayer.drawY)
			local vec = vector(0, -DANGER_CIRCLE_RADIUS * gTileSize)
			vec = vec:rotate_inplace(math.rad((gTotalTime * 20) % 360))
			
			local NUMBER_OF_DOTS = 15
			local ang_step = math.rad(360 / NUMBER_OF_DOTS)
			love.graphics.setColor(255, 0, 0, 200)

			for i = 1, NUMBER_OF_DOTS do
				love.graphics.points(mx + vec.x,my+vec.y)
				vec = vec:rotate_inplace(ang_step)
			end
		end
	end
	love.graphics.setColor(255, 255,255,255)

--	love.graphics.setBlendMode("add") --Default blend mode

	love.graphics.setShader(gShaderSprites)

	drawSprites()
	love.graphics.setShader()

	-- Draw the Void spell's circle
	for i, v in ipairs(gCreatureArray) do
		if v.voidTimeLeft > 0 then
			local ind = gRainbowSetBlues[(gFixedFrameCount % #gRainbowSetBlues) + 1]
			local col = deepcopy(gColourList[ind+1])
			love.graphics.setColor(col[1], col[2],col[3], 128)		
			local mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			love.graphics.circle("line", mx,my, MAX_VOID_RADIUS * gTileSize *(gFixedFrameCount % 20) / 20, 20)
			love.graphics.circle("line", mx,my, MAX_VOID_RADIUS * gTileSize * ((gFixedFrameCount + 10) % 20) / 20, 20)
		end
	end

	-- Draw Lines to Warp victims
	love.graphics.setColor(32,128,32, 128)

	love.graphics.setBlendMode("add")
	love.graphics.setLineStyle("smooth")
	for i, v in ipairs(gSpellArray) do
		if v.isWarp and (v.drawX ~= 0 or v.drawY ~= 0) then
			local d = WARP_RADIUS * (1.0 - (v.lifespanInFrames / (WARP_TIME / FIXED_STEP)))
			d = d * d
			
			local mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			for i2, v2 in ipairs(gCreatureArray) do
				if v.faction ~= v2.faction and v2.movementFunction ~= nil then
					if v.position:dist2(v2.position) <= d then
						local mx2, my2 = cellXYToScreenXY(v2.drawX, v2.drawY)
						love.graphics.line(mx, my, mx2, my2)
					end
				end
			end
		end
	end
	love.graphics.setColor(gColourList[WHITE_INDEX + 1])
	love.graphics.setBlendMode("alpha")

	drawExplosions()
	love.graphics.setLineWidth(1)
	love.graphics.setLineStyle("rough")
	
	drawDoorways()
	
	-----------------------------
	-- Draw black bar at either side of the play area to stop the landscape tiles popping into view!
--	love.graphics.setColor(0,0,0,255) 
--	love.graphics.rectangle("fill", -20, 0, 20, gCanvasHeight)
--	love.graphics.rectangle("fill", gCanvasWidth, 0, 20, gCanvasHeight)
	
	---- Draw the effect for when a Map is read
	local ind = gRainbowSetWithWhite[(gFixedFrameCount % #gRainbowSetWithWhite) + 1]
	love.graphics.setColor(gColourList[ind+1])
	
	love.graphics.push()
	if gPortalOpenTime > 0 then
		local frac = (gPortalOpenTime / MAX_PORTAL_OPEN_TIME)
		local sx = gCanvasWidth * frac
		local sy = gCanvasHeight * frac
		local x,y = cellXYToScreenXY(gPortalPosition.x, gPortalPosition.y)
		
		love.graphics.rectangle("line", x - (sx * 0.5), y - (sy * 0.5), sx,sy)
	end
	love.graphics.pop()

	if gMapEffectTimer > 0 then
		local frac = 1.0 - (gMapEffectTimer / MAX_MAP_EFFECT_TIME)
		local sx = gCanvasWidth * frac
		local sy = gCanvasHeight * frac
		love.graphics.rectangle("line", (gCanvasWidth * 0.5) - (sx * 0.5), (gCanvasHeight * 0.5) - (sy * 0.5), sx,sy)
	end
	love.graphics.setColor(gColourList[WHITE_INDEX + 1])

-- Debug
	if gDebug then
		for i, v in ipairs(gCreatureArray) do
			love.graphics.setColor(gColourList[WHITE_INDEX + 1])
			local mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			love.graphics.circle("line", mx,my, v.physicsRadius * gTileSize, 10)
			love.graphics.setColor(gColourList[RED_INDEX + 1])

			if v.goalPosition ~= nil then
				mx, my = cellXYToScreenXY(v.goalPosition.x, v.goalPosition.y)
				love.graphics.circle("line", mx,my, v.physicsRadius * gTileSize, 10)
			end
		end
		
		for i, v in ipairs(gSpellArray) do
			local mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			love.graphics.circle("line", mx,my, v.physicsRadius * gTileSize, 10)
		end
	end
	
--		love.graphics.setBlendMode("alpha") --Default blend mode

	
end

-----------------------
function gameState:exit()
	gameState:cleanup()
	GameState.switch(menuState)
end
-----------------------
function gameState:keypressed(key)
	gNoInputTimer = 0 -- Register that we pressed something
	
	local can_act = isPlayerAbleToPerformActions()
	
	if gStartedFinalRitual == true then can_act = false end

	if can_act then
		if key == "space" then
			if gMapFeatureTouched ~= nil and gMapFeatureTouched.mapFeatureType == "BuyTile" then
				if gMapFeatureTouched.data == 27 then -- Buy shieldup 
					if not isShieldupAtMax() and goldAfterShieldup() >= 0 then
						buyShieldup()
					end
				else
					local cost = 5
					if gGold >= cost then
						if self:buySpell(1, gUpperCaseList[gMapFeatureTouched.data]) then gGold = gGold - cost end
					end
				end
			elseif processCooldownForActorAction(gPlayer, "CastSlash") then
				addActionToStack({"CastSlash", gPlayer, gTarget})
				gIdleTimer = 0
				gSlowdownRemovalPeriod = MAX_SLOWDOWN_REMOVAL_PERIOD
	
				gNoInputTimer = 0
			end
			return
		end
	end

	if key == "backspace" or key == "delete" then
		gPause = not gPause
	end
	
  -- Force a quit, no matter what
	if key == "escape" and love.keyboard.isDown("lctrl") then love.event.push("quit") return end

  
	if key == "escape" then
			CWindowDialog("Quit", gDialogArray, "Exit to menu?", {{"Yes!", "y", gameState.exit}, {"Nope!", "n", nil}})	
	elseif key == "1" then
		for i, v in ipairs(gCreatureArray) do
			print(string.format("Creature: %d at %f, %f", i, v.position.x, v.position.y))
			if getMapCell(math.floor(v.position.x), math.floor(v.position.y)).isSolid then print ("At illegal position!") playSound("Error", 1) end
		end
	--elseif key == "2" then
	--	for i = 1, 100 do
	--		print(getAlphanumericPasswordOfLength(4))
	--	end
	elseif can_act then
--	if key == 't' then gPlayer:damageShield() end
		for i, v in ipairs(gKeyCodes) do
			if key == v then	
				local spell = gSpellsCarried[gUpperCaseList[i]]
				if gMapFeatureTouched ~= nil and gMapFeatureTouched.mapFeatureType == "SellTile" then
					if spell > 0 then
						gGold = gGold + 1
						self:sellSpell(1, v)
					end
				elseif gPlayer.negateTimeLeft == 0 then
					if v == "s" and gPlayer.shieldLevel >= gPlayer.maxShieldLevel then 
							displayMessage("SHIELD AT MAX!") playSound("SpellAtZero", 1)
					elseif processCooldownForActorAction(gPlayer, v) == true then
						if spell > 0 then gSpellsCarried[gUpperCaseList[i]] = spell - 1
							local str = "[" .. gUpperCaseList[i] .. "] DEPLETED TO ZERO!"
							if gSpellsCarried[gUpperCaseList[i]] == 0 then displayMessage(str) playSound("SpellAtZero", 1) end
							gSpellsAltered = true
							addActionToStack({v, gPlayer, gTarget})
							local fulltxt = spellDescriptions[gUpperCaseList[i]][1]
							local txt = fulltxt:sub(2,2) .. fulltxt:sub(4)
							displaySpellMessage(txt, i)
							gIdleTimer = 0
							gSlowdownRemovalPeriod = MAX_SLOWDOWN_REMOVAL_PERIOD
							
							gNoInputTimer = 0
						else
							local str = "[" .. gUpperCaseList[i] .. "] UNAVAILABLE!"
							displayMessage(str) playSound("SpellAtZero", 1)
						end
					end
				end
			end
		end
	end
	
	if gStartedFinalRitual == false then
	--	if gSlowmo then
			if love.keyboard.isDown("lctrl") or love.keyboard.isDown("rctrl") then 
        if gForcedSlowmo == true then 
          gForcedSlowmo = false; gSlowmo = false
          playSound("AlwaysSlowmoOff", 1)
          displayMessage( "SLOWMO: Always off  - CTRL to cycle") 
        else
          if gSlowmo == true then 
            gForcedSlowmo = true; gSlowmo = true
            playSound("AlwaysSlowmoOn", 1)
            displayMessage("SLOWMO: Always on   - CTRL to cycle") 
          else
            gForcedSlowmo = false; gSlowmo = true
            playSound("AlwaysSlowmoOn", 1.2)
            displayMessage("SLOWMO: Danger only - CTRL to cycle") 
          end
        end
      end
	--	end
	end
end
--------------------------
-- General functions
--------------------------
function gameState:traceComplete()
	gSecurityTime = MAX_SECURITY_TIME
	if gSecurityBotCount < MAX_SECURITY_BOTS then
		local sec = createSecurityBot()
		if sec ~= nil then gSecurityBotCount = gSecurityBotCount + 1 end
	end
end
-------------------------------
function gameState:suicide()
	gPlayerSuicide = true
end	
-------------------------------
function gameState:resetTrace()
	gSecurityTime = MAX_SECURITY_TIME
end
-------------------------------
function gameState:sellSpell(n, spell)
	spell = spell:upper()
	if gSpellsCarried[spell] >= n then gSpellsCarried[spell] = gSpellsCarried[spell] - n return true end
	return false
end
-------------------------------
function gameState:buySpell(n, spell)
	if gSpellsCarried[spell] + n <= MAX_SPELLS_CARRIED then addSpellsWithLetter(spell, n) return true end
	return false
end

-------------------------------
function gameState:reduceTraceTimeBy(v)
	gSecurityTime = gSecurityTime - v
	if gSecurityTime <= 0 then 
		gameState:traceComplete()
	end
end
-------------------------------
function gameState:drawLineWithIdleInformation()
	
	if gForcedSlowmo then
		love.graphics.setColor(128, 128, 0)
	else
		love.graphics.setColor(128, 0, 0)
	end
	local y = gCanvasHeight - gBottomBarHeight
	local x1 = 0
	love.graphics.rectangle("fill", 0, 2 * y, 2 * gCanvasWidth, 2)
	love.graphics.setColor(255, 128, 0)

--[[	if gSlowmo then
		local frac =  math.floor((gIdleTimer / SLOWDOWN_IDLE_TIMER) * (gCanvasWidth / 2))
		love.graphics.line(x1,y, x1 + frac, y)
		love.graphics.line(gCanvasWidth - frac, y, gCanvasWidth, y)
	end]]
end

-------------------------------
function gameState:drawScore()
	local x1 = 0
	local ind = gRainbowSetDark[(math.floor(gFixedFrameCount * 0.2) % #gRainbowSetDark) + 1]
	love.graphics.setColor(gColourList[ind+1])
--	love.graphics.draw(gHighscoreImage, x1, 0, 0, 1, 1,0,0)
	love.graphics.draw(gHighscoreImage, x1 * 2, 0, 0, 2, 2,0,0)
	if gDebug and gDebugText ~= nil and gDebugText ~= "" then
--		love.graphics.print(gDebugText, 2, 10)
		love.graphics.print(gDebugText, 2 * 2, 10 * 2)

	end
	ind = gRainbowSetWithWhite[(math.floor(gFixedFrameCount * 1.5) % #gRainbowSetWithWhite) + 1]
	love.graphics.setColor(gColourList[ind+1])
	
	x1 = x1 + 8
--	love.graphics.print(gScore, x1, 2)
	love.graphics.print(gScore, x1 * 2, 2 * 2)
	
	local sm = math.floor(gScoreMultiplier * 10) / 10
	if sm > 1 then
--		love.graphics.print("x" .. sm,  x1, 14)
		love.graphics.print("x" .. sm,  x1 * 2, 14 * 2)

	end
end

-------------------------------
function gameState:drawAlertStatus()
	local x1 = -4
	local ind = gRainbowSetDark[(math.floor(gFixedFrameCount * 0.2) % #gRainbowSetDark) + 1]
	love.graphics.setColor(gColourList[ind+1])
--	love.graphics.draw(gHighscoreImage, gCanvasWidth, 0, 0, -1, 1, 0, 0)
	love.graphics.draw(gHighscoreImage, gScreenWidth, 0, 0, -2, 2, 0, 0)

	ind = gRainbowSetWithWhite[(math.floor(gFixedFrameCount * 1.5) % #gRainbowSetWithWhite) + 1]
	love.graphics.setColor(gColourList[ind+1])
	local wid = 500

--	love.graphics.printf("Time: " ..  math.floor(gSecurityTime), -8-wid + gCanvasWidth, 2, wid, 'right')
  love.graphics.printf("Time: " ..  math.floor(gSecurityTime), 2 * (-8-wid + gCanvasWidth), 2 * 2, wid * 2, 'right')

end

-------------------------------
function gameState:drawGold()
	local x1 = gCanvasWidth
	local wid = 500

	ind = gRainbowSetWithWhite[(math.floor(gFixedFrameCount * 1.5) % #gRainbowSetWithWhite) + 1]
	love.graphics.setColor(gColourList[ind+1])
--	love.graphics.printf("Gold: " .. gGold, -8-wid + gCanvasWidth, 14, wid, 'right')
	love.graphics.printf("Gold: " .. gGold, 2 * (-8-wid + gCanvasWidth), (2 * 14), (2 * wid), 'right')

end

-------------------------------
function gameState:drawGUI()
	if gGUIOff == false then
--		love.graphics.setFont(beebTileFont)
    love.graphics.setFont(bigFont)

		gameState:drawScore()
		gameState:drawAlertStatus()
		gameState:drawGold()
		local x1 = 0
		love.graphics.setColor(0, 0, 0, 255)
--		love.graphics.rectangle("fill", x1,gCanvasHeight - gBottomBarHeight, gCanvasWidth, gBottomBarHeight * 2) -- We draw it extra long in the y to fix the screenscale vs fullscreen drawing issue
		love.graphics.rectangle("fill", x1 * 2,(gCanvasHeight - gBottomBarHeight) * 2, 2 * gCanvasWidth, gBottomBarHeight * 2) -- We draw it extra long in the y to fix the screenscale vs fullscreen 
		local ind = gRainbowSetDark[(math.floor(gFixedFrameCount * 0.5) % #gRainbowSetDark) + 1]
		love.graphics.setColor(gColourList[ind+1])
		
		local letter_width = 8 -- Due to padding from print
		local letter_set_width = 3 * letter_width
		
		if gSpellMessageTimer > 0 then
			local wid = 1000
			local frac = math.floor(3 * (1.0 - (gSpellMessageTimer / MAX_SPELL_MESSAGE_TIME)))
			local col = gColourList[CYAN_INDEX+1+frac];
			love.graphics.setColor(col)		
--			love.graphics.printf(gSpellMessage, -(wid / 2) + (gCanvasWidth / 2), gCanvasHeight - gBottomBarHeight - 10, wid, 'center')
			love.graphics.printf(gSpellMessage, 2 * (-(wid / 2) + (gCanvasWidth / 2)), 2 * (gCanvasHeight - gBottomBarHeight - 10), 2 * wid, 'center')
		end
		
		gameState:drawLineWithIdleInformation()
		
		if gPlayer ~= nil and gPlayer.negateTimeLeft > 0 then
			love.graphics.setColor(64, 64, 64, 64)
		else love.graphics.setColor(255, 255, 255) end
		
		local t = gFixedFrameCount * 10
		local r1, g1, b1 = HSL(((500 + 	t) % 1000) / 1000 * 255, 	255, 255)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
		local r2, g2, b2 = HSL(((200 + 	t) % 1000) / 1000 * 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
		local r3, g3, b3 = HSL(((0 + 	t) % 1000) / 1000 * 255, 	255, 32)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)

		local str = "" -- The numbers for anything under ten
     local str2 = "" -- The numbers for anything maxed out
		for i, v in ipairs(gUpperCaseList) do
      local c = gSpellsCarried[v]
			if c > 0 then
        if c >= 10 then
				str2 = str2 .. v .. "  "
				str = str .. "   "
        else
         str = str .. v .. "  "
         str2 = str2 .. "   "
        end
			else
				str = str .. "   "
        str2 = str2 .. "   "
			end
		end
	
		local text_x = (gCanvasWidth / 2) - ((25 * letter_set_width) + letter_width) / 2
		local text_y = gCanvasHeight - (gBottomBarHeight) + 2
		-------------------------------------
		if gShaderGridColour ~= nil then
			gShaderGridColour:send("offsetX", text_x * 2)
			gShaderGridColour:send("offsetY", 1 + text_y * 2)
			gShaderGridColour:send("gridSizeX", letter_set_width * 2)
			gShaderGridColour:send("gridSizeY", 16) -- was 9 
			gShaderGridColour:send("topColour", 	{1, 1, 1, 1})
			gShaderGridColour:send("midColour", 	{0.3, 1, 1, 1})
			gShaderGridColour:send("bottomColour",{0.3, 0.3, 0.3, 1})
  
			gShaderGridColour:send("scroll", gFixedFrameCount / 20)
		end
		love.graphics.setShader(gShaderGridColour)
		love.graphics.print(str, text_x * 2, text_y * 2)
    
    
     ------------------------------------------
    local plusstr = "" -- The numbers for anything under ten
    for i, v in ipairs(gUpperCaseList) do
      local c = gSpellsCarriedTimeSinceChanged[v]
			if c > 0 then
        plusstr = plusstr .. " + "
			else
        plusstr = plusstr .. "   "
			end
		end
    love.graphics.print(plusstr, text_x * 2, text_y * 2)  
    ------------------------------------------
    
    if gShaderGridColour ~= nil then
			gShaderGridColour:send("topColour", 	{1, 0, 0, 1})
			gShaderGridColour:send("midColour", 	{1, 0.5, 0, 1})
			gShaderGridColour:send("bottomColour",{0.2, 0, 0, 1})
      gShaderGridColour:send("scroll", gFixedFrameCount / 10)
		end
    
    love.graphics.print(str2, text_x * 2, text_y * 2)  
    ------------------------------------------------
    
    love.graphics.setShader()
		love.graphics.setColor(128, 128, 255)
		local mini_number_y_offset_from_letters = 9
		local x = 3
		local y = text_y + mini_number_y_offset_from_letters
		if gSpellsAltered then
			for i, v in ipairs(gPickupArray) do
				v:updateGraphic()
			end
			
			tinyFiguresBatch:clear()
--			tinyFiguresBatch:bind()
			
			for i, v in ipairs(gUpperCaseList) do
				local val = gSpellsCarried[v]
				if val > 0 then
					local low = (val % 10)
					local high = math.floor(val / 10)
					tinyFiguresBatch:add( tinyFiguresAtlas.quads[""..high], 2 * (text_x + x), 	2 * y, 0, 1* 2, 1 * 2, 2 * 2, 0)
					tinyFiguresBatch:add( tinyFiguresAtlas.quads[""..low], 	2 * (text_x + (x + 5)) , 	2 * y, 0, 1 * 2, 1 * 2, 2 * 2, 0)
				end
				x = x + letter_set_width 
			end
			love.graphics.draw(tinyFiguresBatch)
		end
		love.graphics.setColor(255, 255, 255)
		
		drawMap()
	
		if gGameOverTimer > 0 then
			if gGameOverTimer < GAME_OVER_TEXT_DISPLAY_TIME then 
				local wid = 1000
				local ind = gRainbowSetWithWhite[(gFixedFrameCount % #gRainbowSetWithWhite) + 1]
				love.graphics.setColor(gColourList[ind+1])
				local str = "ADVENTURER KILLED ON " .. gCurrentLevelName
				if gDeathMessage ~= "" then str = str .. " BY " .. string.upper(gDeathMessage) end
				love.graphics.printf(str, 2*(-(wid / 2) + gCanvasWidth / 2), (2 * gCanvasHeight / 2), 2 * wid, 'center')
			end
			love.graphics.setColor(255, 255, 255)

			if gGameOverTimer < GAME_OVER_DARKENING_TIME then
				gColourAdd = -(1.0 - (gGameOverTimer / GAME_OVER_DARKENING_TIME))
				gShaderZoom = 2 * (1.0 - (gGameOverTimer / GAME_OVER_DARKENING_TIME))
			end
		elseif gMessageTimer > 0 then
			if gStartedFinalRitual == false then
				local wid = 1000
				love.graphics.setColor(0,0,0,255)
         local y = (gCanvasHeight * 0.2) + 12
				love.graphics.printf(gMessage, 2 * (-(wid / 2) + (gCanvasWidth / 2) + 2), 2 * y, wid * 2, 'center')
				love.graphics.printf(gMessage, 2 * (-(wid / 2) + (gCanvasWidth / 2) - 2), 2 * y, wid * 2, 'center')
         love.graphics.printf(gMessage, 2 * (-(wid / 2) + (gCanvasWidth / 2)), (2 * y - 2), wid * 2, 'center')
				love.graphics.printf(gMessage, 2 * (-(wid / 2) + (gCanvasWidth / 2)), (2 * y + 2), wid * 2, 'center')
				
        	local r1, g1, b1 = HSL((gTotalTime / 10.0) * 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)

        love.graphics.setColor(r1,g1,b1,255)
				love.graphics.printf(gMessage, 2 * (-(wid / 2) + (gCanvasWidth / 2)), 2 * y, wid * 2, 'center')
			end 
    end
    
    if gInfoMessageTimer > 0 then
			if gStartedFinalRitual == false then
				local wid = 1000
        local y = gCanvasHeight * 0.08
        local width = 1280
        love.graphics.setColor(0,0,0,210)
        love.graphics.rectangle("fill", gCanvasWidth - width/2, y+25, width, 90)
        	local r1, g1, b1 = HSL(((gTotalTime + 50)/ 10.0) * 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)

        love.graphics.setColor(r1,g1,b1,255)

				--love.graphics.setColor(gColourList[ind+1])
				love.graphics.printf(gInfoMessage, 2 * (-(wid / 2) + (gCanvasWidth / 2)), 2 * y, wid * 2, 'center')
			end
		end
	end
	
	if gLeavingLevelCounter > 0 then
		gColourAdd = -(1.0 - (gLeavingLevelCounter / LEAVE_LEVEL_TIME))
		gShaderZoom = 2 * (1.0 - (gLeavingLevelCounter / LEAVE_LEVEL_TIME))
	end
	love.graphics.setColor(255, 255, 255, 255)
end
----------------------------
function drawMap()
	local base_alpha = 100
	local top_alpha = 250 - base_alpha
	if gMapImage ~= nil then
		local alpha = math.floor(80 + ((top_alpha - base_alpha) * gIdleTimer / MAX_IDLE_TIMER))
		local flicker = math.floor(gFixedFrameCount / 2)
		
--		if gFixedFrameCount % 2 == 0 then alpha = math.floor(alpha * 0.8) end

		local mx, my = math.floor(gCanvasWidth / 2 - gMapImage:getWidth() / 2), 3
		love.graphics.setColor(255, 255, 255, alpha)
		love.graphics.draw(gMapImage, 2 * mx, 2 * my, 0,2,2, 0, 0)
		alpha = alpha * 2
		alpha = math.floor( math.min(alpha, 255))
				
		local ind = 0
		local col = {255,255,255,255}
		if gPlayer ~= nil then
			ind = gRainbowSetWithWhite[(flicker % #gRainbowSetWithWhite) + 1]
			col = deepcopy(gColourList[ind+1]); col[4] = alpha
			love.graphics.setColor(col)
			
			love.graphics.rectangle("fill", 2 * math.floor(mx-1 + gPlayer.mapX), 2 * math.floor(my-1+gPlayer.mapY), 3, 3)
		end
		
		ind = gRainbowSetReds[(flicker % #gRainbowSetReds) + 1]
		col = deepcopy(gColourList[ind+1]); col[4] = alpha
		love.graphics.setColor(col)
		for i, v in ipairs(gCreatureArray) do
			if not v.isPlayer and v.isVisible and v.isChest == false then
				love.graphics.rectangle("fill", 2 * math.floor(mx + v.mapX), 2 * math.floor(my+v.mapY), 1, 1)
			end
		end
				
		col = deepcopy(gColourList[YELLOW_INDEX+1]); col[4] = alpha
		love.graphics.setColor(col)
		for i, v in ipairs(gCreatureArray) do
			if not v.isPlayer and v.isVisible and v.isChest == true then
				love.graphics.rectangle("fill", 2 * math.floor(mx + v.mapX), 2 * math.floor(my+v.mapY), 1, 1)
			end
		end
		
--[[		col = deepcopy(gColourList[GREEN_INDEX+1]); col[4] = alpha
		love.graphics.setColor(col)
		for i, v in ipairs(gMapFeatureArray) do
			if v.isVisible then
				love.graphics.rectangle("fill", math.floor(mx-1 + v.mapX), math.floor(my-1+v.mapY), 3, 3)
			end
		end]]
		
		if gPortalIsOpen then
			ind = gRainbowSetWithWhite[(math.floor(flicker / 2) % #gRainbowSetWithWhite) + 1]
			col = deepcopy(gColourList[ind+1]); col[4] = alpha
			love.graphics.setColor(col)
			love.graphics.rectangle("fill", 2 * math.floor(mx-1 + gPortalPosition.x), 2 * math.floor(my-1+gPortalPosition.y), 3, 3)
		end
	end
	love.graphics.setColor(255, 255, 255, 255)
end

--------------------------------
function drawMapFeatures()
	if gShowRedCircle then love.graphics.setColor(255,0,0,255) end
	
	spriteBatch:clear()
--  spriteBatch:bind()
	local mx, my
 	for i, v in ipairs(gMapFeatureArray) do
		if v.isActive and v.isVisible and v.quadName ~= nil then
			mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			spriteBatch:add( spriteAtlas.quads[v.quadName], mx, my, v.drawAngleInRads, 1, 1, v.hw * gTileSize, v.hh * gTileSize)
		end
	end
	
	love.graphics.setFont(beebTileFont)
	for i, v in ipairs(gMapFeatureArray) do
		if v.isActive and v.isVisible and v.mapFeatureType == "BuyTile" then
			local spell_letter = gUpperCaseList[v.data]
			mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			spriteBatch:add( spriteAtlas.quads["Pickup" .. v.data], mx, my, v.drawAngleInRads, 2, 2, 4, 4)
		end
	end
	
--	spriteBatch:unbind()
  love.graphics.draw(spriteBatch)
	
	love.graphics.setFont(bigFont)
end

--------------------------------
function drawWorldMessages()
	love.graphics.setFont(beebTileFont)
	love.graphics.setColor(0, 64, 128,255)

	for i, v in ipairs(gWorldMessageArray) do
		v:draw()
	end

	love.graphics.setColor(255,255,255)
	love.graphics.setFont(bigFont)
end

--------------------------------
local questionMarks = {"QuestionMark", "QuestionMark1", "QuestionMark2"}
function drawSprites()
	spriteBatch:clear()
--  spriteBatch:bind()
	local mx, my
 
	-------------------
	for i, v in ipairs(gMapFeatureArray) do
		if v.isActive and v.isVisible and v.quadName ~= nil then
			mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			spriteBatch:add( spriteAtlas.quads[v.quadName], mx, my, v.drawAngleInRads, 1, 1, v.hw * gTileSize, v.hh * gTileSize)
		end
	end
 
	--- Draw the portal
	mx = math.floor(gPortalPosition.x * gTileSize) / gTileSize
	my = math.floor(gPortalPosition.y * gTileSize) / gTileSize	
	local c = getMapCell(math.floor(gPortalPosition.x), math.floor(gPortalPosition.y))
	
	local str = "Portal"
	if gNextLevelHasPredefinedName then str = "BlockedPortal" end
	
	if c.roomID ~= 0 and getRoomFromID(c.roomID).isMapped then
		mx, my = cellXYToScreenXY(mx, my)
		if gPortalIsOpen then
			spriteBatch:add(spriteAtlas.quads[str .. (1 + gFixedFrameCount % 3)], mx, my, math.rad(gFixedFrameCount * 4), 1,1, gTileSize/2, gTileSize/2) 
		end
	end

	for i, v in ipairs(gPickupArray) do
		if v.isActive and v.rezInExplosion == nil and v.isVisible and v.quadName ~= nil then
			mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			spriteBatch:add( spriteAtlas.quads[v.quadName], mx, my, v.drawAngleInRads,1,1,v.hw * gTileSize, v.hh * gTileSize)
		end
	end 
	
	-- Draw shadow under creatures who are jumping
	for i, v in ipairs(gCreatureArray) do
		if v.jumpTime > 0 then	
			mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			spriteBatch:add(spriteAtlas.quads["Shadow0"], mx, my, 0, 1, 1, 8, 8)
		end
	end
	
	-- Draw facing marker for player
	if gPlayer ~= nil then
		if gPlayer.invisibleTimeLeft == 0 then
			mx, my = cellXYToScreenXY(gPlayer.drawX, gPlayer.drawY)
			local x_moving = true
			local y_moving = true
			-- But use the velocity to decide if we're actually NOT moving
			local len = gPlayer.velocity:len() 
			my = my - 20 * math.sin(math.rad(180 * gPlayer.jumpTime / MAX_INTANGIBLE_TIME))
			if gPlayer.rezInExplosion == nil then
				local vec = vector(0, 8)
				vec:rotate_inplace(gPlayer.facingAngleInRads + math.rad(180))
				spriteBatch:add(spriteAtlas.quads["DirectionalArrow"], mx+vec.x, my+vec.y, gPlayer.facingAngleInRads, 1, 1, 3, 2)
			end
		end
	end
	
	-- Draw creatures
	for i, v in ipairs(gCreatureArray) do
		if (v.unhittableTimeInFrames == 0 or (gFixedFrameCount % 2) == 1) then
			if v.isActive and v.rezInExplosion == nil and v.quadName ~= nil then
				mx, my = cellXYToScreenXY(v.drawX, v.drawY)
				my = my - 20 * math.sin(math.rad(180 * v.jumpTime / MAX_INTANGIBLE_TIME))

				if v.isVisible then
					if v.invisibleTimeLeft == 0 then
						if v.flipHorizontalForMovement == true and v.velocity.x > 0 then
							spriteBatch:add( spriteAtlas.quads[v.quadName], mx, my, v.drawAngleInRads,-1,1,v.hw * gTileSize, v.hh * gTileSize)
						else
							spriteBatch:add( spriteAtlas.quads[v.quadName], mx, my, v.drawAngleInRads,1,1,v.hw * gTileSize, v.hh * gTileSize)
						end
					end
					
					--- Draw negate effect
					if v.negateTimeLeft > 0 then
						local frame = math.floor(v.negateTimeLeft * 20) % 3
						spriteBatch:add( spriteAtlas.quads["Negate" .. frame], mx, my, math.random(math.rad(360)), 1, 1, 0.5 * gTileSize, 0.5 * gTileSize)
					end	
					
					--- Draw freeze effect
					if v.freezeTimeLeft > 0 then
						local frame = math.floor(v.freezeTimeLeft * 20) % 4
						local scale_x = 1
						if math.random() > 0.5 then scale_x = -1 end
						spriteBatch:add( spriteAtlas.quads["Freeze" .. frame], mx, my, 0, scale_x, 1, 0.5 * gTileSize, 0.5 * gTileSize)
					end	
					
					--- The player shows his shields when invisible
					if v.isPlayer or v.invisibleTimeLeft == 0 then
						if v.shieldLevel > 0 then
							spriteBatch:add(spriteAtlas.quads["Shield" .. v.shieldLevel], mx, my, -math.rad(gFixedFrameCount * 8), 1,1, 12, 12) 
						end
					end
				
					if v.isPlayer == false and v.faction == FACTION_PLAYER and v.attachedToActor == nil then
						if v.turncoatTimeLeft == 0 or v.turncoatTimeLeft > 5 or gFixedFrameCount % 2 == 1 then
							spriteBatch:add(spriteAtlas.quads["Heart"], mx, my - 12, 0, 1,1, 3, 3) 
						end
					end
				else
					local l = (v.uniqueID + math.floor(gFixedFrameCount / 4)) % 3
					spriteBatch:add(spriteAtlas.quads[questionMarks[l+1]], mx, my, 0, 1,1, 3, 3) 
				end
			end
		end
	end
	
	if gPlayer ~= nil and gPlayer.isActive and gPlayer.rezInExplosion == nil then
		mx, my = cellXYToScreenXY(gPlayer.drawX, gPlayer.drawY)
		my = my - 20 * math.sin(math.rad(180 * gPlayer.jumpTime / MAX_INTANGIBLE_TIME))

		if gPlayer.ultraswordTimeLeft > 0 then
			if gPlayer.ultraswordTimeLeft > 5 or gFixedFrameCount % 2 == 1 then
				spriteBatch:add(spriteAtlas.quads["PlayerUpgraded"], mx,my, 0, 1, 1, gPlayer.hw * gTileSize, gPlayer.hh * gTileSize)
			end
		end
		
		if gPlayer.unhittableTimeInFrames == 0 and gPlayer.shieldLevel == 0 and gFixedFrameCount % 3 == 1 then
			spriteBatch:add(spriteAtlas.quads["PlayerInDanger"], mx,my, 0, 1, 1, gPlayer.hw * gTileSize, gPlayer.hh * gTileSize)
		end
	end
	
 
	for i, v in ipairs(gSpellArray) do
		if v.isActive and v.isVisible and v.quadName ~= nil then
			mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			if v.resizeToReachTarget == false then
				spriteBatch:add( spriteAtlas.quads[v.quadName], mx, my, v.drawAngleInRads,1,1,v.hw * gTileSize, v.hh * gTileSize)
			else
				local dist = -v.targetPosition:dist(v.position) / 2
				--dist = (dist / 32) 
				spriteBatch:add( spriteAtlas.quads[v.quadName], mx, my, v.drawAngleInRads,1,dist,v.hw * gTileSize, v.h * gTileSize)
			end
		end
	end  
	
	--- Draw targeting cursor
	local x_border = 16
	local y_border =  16

	if gPlayer ~= nil then
		if gTarget ~= nil then 
			mx, my = cellXYToScreenXY(gTarget.drawX, gTarget.drawY)
			spriteBatch:add(spriteAtlas.quads["TargetCursor" .. ((gFixedFrameCount % 3) + 1)], mx, my, math.rad(gFixedFrameCount * 4), 1,1, 11, 11) 
		end
		--- Draw nearest enemy cursor
		if gNearestEnemy ~= nil then
			mx, my = cellXYToScreenXY(gNearestEnemy.drawX, gNearestEnemy.drawY)
			local nmx = math.clamp(mx, x_border, gCanvasWidth-x_border)
			local nmy = math.clamp(my, y_border, gCanvasHeight-gBottomBarHeight-y_border)
			
			if nmx ~= mx or nmy ~= my then  
				spriteBatch:add(spriteAtlas.quads["NearestCreatureIcon"], nmx, nmy, 0, 1,1, 5, 5) 
			end
		elseif gPortalIsOpen then
			mx = math.floor(gPortalPosition.x * gTileSize) / gTileSize
			my = math.floor(gPortalPosition.y * gTileSize) / gTileSize	
			mx, my = cellXYToScreenXY(mx, my)

			local nmx = math.clamp(mx, x_border, gCanvasWidth-x_border)
			local nmy = math.clamp(my, y_border, gCanvasHeight-gBottomBarHeight-y_border)
						
			if nmx ~= mx or nmy ~= my then  
				spriteBatch:add(spriteAtlas.quads["NearestPortalIcon"], nmx, nmy, 0, 1,1, 5, 5) 
			end
		end
	end
	
--  spriteBatch:unbind()
  love.graphics.draw(spriteBatch)
	
	------ And now draw weird sprites over the top
	love.graphics.setColor(255,255,255,64)
	spriteBatch:clear()
--  spriteBatch:bind()
	
	for i, v in ipairs(gCreatureArray) do
		if v.isActive and v.rezInExplosion == nil and v.invisibleTimeLeft > 0 then
			mx, my = cellXYToScreenXY(v.drawX, v.drawY)
			
			if v.flipHorizontalForMovement == true and v.velocity.x > 0 then
				spriteBatch:add( spriteAtlas.quads[v.quadName], mx, my, v.drawAngleInRads,-1,1,v.hw * gTileSize, v.hh * gTileSize)
			else
				spriteBatch:add( spriteAtlas.quads[v.quadName], mx, my, v.drawAngleInRads,1,1,v.hw * gTileSize, v.hh * gTileSize)
			end
		end
	end
	
--	spriteBatch:unbind()
	love.graphics.draw(spriteBatch)
	love.graphics.setColor(255,255,255,255)
end


--------------------
function addExplosionWithDefinition(r, pos)
	local ex = gExplosionDefinitions[r]
	
-------------------#points life   speed  palette movementFunction                velocityFromHitFrac  expandsOutwards circleRadius
	return addExplosion(pos, ex[1], ex[2], ex[3], ex[4],  ex[5], 						vector(0,0), ex[6], 							ex[7], 					ex[8]) 
end


function addExplosion(position, number_of_points, life, max_speed, colour_array, velocity_function, velocity_bias, velocity_fraction, expands_outwards, circle_radius)
	local ex = CExplosion(number_of_points, life, max_speed, colour_array, velocity_function, velocity_bias, velocity_fraction, expands_outwards, circle_radius)
	table.insert(gExplosionArray, ex)
	ex:start(position)
	return ex
end

--------------------
function drawExplosions()
	love.graphics.setBlendMode("add") --Default blend mode

	love.graphics.setPointSize(1)
	for i, v in ipairs(gExplosionArray) do
		v:draw()
	end
	
	love.graphics.setBlendMode("alpha") --Default blend mode
end

--------------------
function startScreenShakeTimeAndPower(time, power)
	gCurrentScreenShakeTime = time
	gTotalScreenShakeTime = gCurrentScreenShakeTime
	gCurrentScreenShakePower = power or 0.25
end

--------------------------------------
function displayMessage(text, tag)
	gMessageTimer = MAX_MESSAGE_TIME
	gMessage = text
  gMessageTag = tag
end

function displayInfoMessage(text)
	gInfoMessageTimer = MAX_INFO_MESSAGE_TIME
	gInfoMessage = text
end

function displaySpellMessage(text, index)
	gSpellMessageTimer = MAX_SPELL_MESSAGE_TIME
	gSpellMessage = text
	gSpellMessageIndex = index
end

--------------------------------------
function isPlayerAbleToPerformActions()
	if gPlayer ~= nil and gLeavingLevelCounter == 0 and gPlayer.rezInExplosion == nil then return true end
	return false
end

--------------------------------------
function processCooldownForActorAction(actor, act_name)	
	if gSpellCooldown[act_name] ~= nil then -- If there's a cooldown
		if actor.spellCooldown[act_name] == nil or actor.spellCooldown[act_name] == 0 then
			actor.spellCooldown[act_name] = gSpellCooldown[act_name]
			return true
		end
	else
		return true
	end
	return false-- skip this, as there's a timeout waiting to occur
end

	
------------------------------- 
gTimeSinceLastDirectionReleased = 0
gTimeOfLastDirectionPressed = 0
gTimePressingSameDirection = 0

TIME_UNTIL_DOUBLE_TAP_NEGATED = 10 / IDEAL_FPS
TIME_UNTIL_SPEED_BOOST = 60 / IDEAL_FPS
gPreviousDirectionPressed = ""
gPreviousDirectionReleased= ""

function processMovementInputs(dt)
	local s = NORMAL_SPEED
	local acc = vector(0,0)
	
	gPlayer.moveInputX = false
	gPlayer.moveInputY = false

	local old_dir = gPreviousDirectionPressed
  if love.keyboard.isDown("up") or love.keyboard.isDown("kp8")  then acc.y = -IMPULSE; 	gPlayer.moveInputY = true; gPreviousDirectionPressed = "up" end
  if love.keyboard.isDown("down") or love.keyboard.isDown("kp2") then acc.y = IMPULSE; 	gPlayer.moveInputY = true; gPreviousDirectionPressed = "down" end
  if love.keyboard.isDown("left") or love.keyboard.isDown("kp4") then acc.x = -IMPULSE; 	gPlayer.moveInputX = true; gPreviousDirectionPressed = "left" end
  if love.keyboard.isDown("right") or love.keyboard.isDown("kp6") then acc.x = IMPULSE; 	gPlayer.moveInputX = true; gPreviousDirectionPressed = "right" end
	 
	-- If we're pressing nothing
	if acc.x == 0 and acc.y == 0 then
		gPlayer.maxSpeed = NORMAL_SPEED
		gTimeOfLastDirectionPressed = 0
		if gPreviousDirectionPressed ~= "" and gTimeSinceLastDirectionReleased < TIME_UNTIL_DOUBLE_TAP_NEGATED then
			gTimeSinceLastDirectionReleased = gTimeSinceLastDirectionReleased + dt
			gPreviousDirectionReleased = gPreviousDirectionPressed
		else
			gPreviousDirectionReleased = ""
		end
		gTimePressingSameDirection = 0
	else
		gSlowdownRemovalPeriod = MAX_SLOWDOWN_REMOVAL_PERIOD
		gNoInputTimer = 0
	-- If we're pressing something
		gTimeOfLastDirectionPressed = gTimeOfLastDirectionPressed + dt
		-- And we've been pushing something for a while
		if gTimeOfLastDirectionPressed >=  TIME_UNTIL_DOUBLE_TAP_NEGATED then
			gPreviousDirectionPressed = ""				
--			gPlayer.maxSpeed = NORMAL_SPEED
		elseif gPreviousDirectionPressed == gPreviousDirectionReleased and gPreviousDirectionPressed ~= "" then
			gPreviousDirectionPressed = ""
			gPreviousDirectionReleased = ""
			-- acc = acc * 100
			gPlayer.maxSpeed = NORMAL_SPEED -- FAST_SPEED
		end
				
		-- Limit the press-time counter
		gTimeOfLastDirectionPressed = math.min(math.max(TIME_UNTIL_DOUBLE_TAP_NEGATED, TIME_UNTIL_SPEED_BOOST), gTimeOfLastDirectionPressed)
		
		gTimeSinceLastDirectionReleased = 0
	end
	
	
	gPlayer:setAcceleration(acc)
	
	local ind = 0
	
	if acc.x > 0 then ind = 2
	elseif acc.x == 0 then ind = 1 end
	
	if acc.y == 0 then ind = ind + 3 
	elseif acc.y > 0 then ind = ind + 6 end

	gPlayer:setQuad("Player" .. ind)
	
	-- Force slowdown to deactivate
	if love.keyboard.isDown("lshift") or love.keyboard.isDown("rshift") then 
		gSlowdownRemovalPeriod = math.max(0.1, gSlowdownRemovalPeriod)
	end
	
	if gPortalIsOpen then
		local vec = gPortalPosition - gPlayer.position
		local dist = vec:len()
		if dist < 1 then
			gNoInputTimer = 0 -- Register that we pressed something
			dist = math.min(dist, 0.1)
			vec:normalize_inplace()
			vec = vec * dist
			gPlayer.position = gPlayer.position + vec
		end
	end
	
	if gMapFeatureTouched ~= nil and gPlayer ~= nil and gPlayer.jumpTime == 0 then
		local vec = gMapFeatureTouched.position - gPlayer.position
		local dist = vec:len()
		dist = math.min(dist, 0.02)
		vec:normalize_inplace()
		vec = vec * dist
		gPlayer.position = gPlayer.position + vec
		
		local str = nil
		if gMapFeatureTouched.mapFeatureType == "SellTile" then
			str = "Cast any spell to sell it for 1 Gold"
			displayMessage(str)
		elseif gMapFeatureTouched.mapFeatureType == "BuyTile" then
			local cost = 5
			if gMapFeatureTouched.data == 27 then
				if isShieldupAtMax() then 
					str = "Shield capacity already at max" 
				else
					local gold_left = goldAfterShieldup()
					if gold_left < 0 then 
						str = "You need " .. -gold_left .. " Gold to buy Shieldup!"
					else 
						str = "Buy Shieldup for " .. cost .. " Gold?" 
					end
				end
			else
				local spell_letter = gUpperCaseList[gMapFeatureTouched.data]
				if gSpellsCarried[spell_letter] < 10 then 
					str = "Press Space to buy [" .. gUpperCaseList[gMapFeatureTouched.data] .. "] for " .. cost .. " Gold?"
				else
					str = "You cannot carry more [" .. gUpperCaseList[gMapFeatureTouched.data] .. "]!"
				end
			end	
		elseif gMapFeatureTouched.mapFeatureType == "InfoTile" then
			str = gMapFeatureTouched.data
		end
		if str ~= nil then
			displayInfoMessage(str)
		end
	else
		if gInfoMessage == str then
			gInfoMessageTimer = 0
		end
	end
end
-------------------------------------
function isShieldupAtMax()
	if gMaxShieldLevel >= 4 then return true else return false end
end
-------------------------------------
function goldAfterShieldup()
	local cost = gShieldCosts[gMaxShieldLevel]
	return gGold - cost
end
-------------------------------------
function buyShieldup()
	local cost = gShieldCosts[gMaxShieldLevel]
	gGold = gGold - cost
	gMaxShieldLevel = gMaxShieldLevel + 1
	gShieldsLowSoundTimer = 0
	gPlayer:setMaxShieldLevel(gMaxShieldLevel)
	playSound("Shield",1)
end
-------------------------------------
function countEnemies()
  local c = 0
  for i, v in ipairs(gCreatureArray) do
    if v.isCreature and v.isChest == false and v.isSecurityBot == false and  v.faction ~= FACTION_PLAYER then c = c + 1 end -- Count creatures
  end
  return c
end

-------------------------------------
function playerToMapFeatureCollision()
	local old_feature_type_touched = nil 
	if gMapFeatureTouched ~= nil then old_feature_type_touched = gMapFeatureTouched.mapFeatureType end
	gMapFeatureTouched = nil

	if true then -- gShowRedCircle == false then 
		-- Deal with collisions: Player to map feature
		for i, v in ipairs(gMapFeatureArray) do
			if v:isWithinInteractionRangeOf(gPlayer) then
	--			if v.mapFeatureType == "Terminal" then
					gMapFeatureTouched = v
					break
	--			end
			end
		end
	end
	
	if gMapFeatureTouched ~= nil then -- We're touching something
		if gMapFeatureTouched.mapFeatureType ~= old_feature_type_touched then
			playSound("MapFeatureLock")
		end
	elseif old_feature_type_touched ~= nil then
--		playSound("MapFeatureUnlock")
	end
end

-------------------------------------
function playerToPickupCollision()
	if gPlayer.jumpTime > 0 then return end
  -- Deal with collisions: Player to pickup
  for i, v in ipairs(gPickupArray) do
    if v:isWithinPickupRangeOf(gPlayer) then
      v.hitByThingWithVelocity = gPlayer.velocity:clone()
--      if v.pickupType ~= "Key" then
        pickupSpellOfType(v.pickupType)
--      else
--        gGotKey = true
--        playSound("GotKey")
--      end
      v:kill()
    end
  end
end

-------------------------------------
function  spellsIgnoringTheirOwnCasterUpkeep()
	-- This code is to clear up the complexities of things being hit by their own bullets
	for i, v in ipairs(gSpellArray) do
		if v.casterToIgnoreUntilOutOfHisRange ~= nil then
			if v.casterToIgnoreUntilOutOfHisRange.isAlive == false or v.casterToIgnoreUntilOutOfHisRange.isDying then 
				v.casterToIgnoreUntilOutOfHisRange = nil 
				break 
			elseif v.casterToIgnoreUntilOutOfHisRange.position:dist2(v.position) > v.physicsRadiusSq then
				v.casterToIgnoreUntilOutOfHisRange = nil 
			end
		end
	end
end

-------------------------------------
function playerToCreatureCollision()
	if gPlayer.rezInExplosion ~= nil then return end
	if gLeavingLevelCounter > 0 then return end
	if gPlayer.jumpTime > 0 then return end
	
  for i, v in ipairs(gCreatureArray) do
    if v ~= gPlayer and v.faction ~= FACTION_PLAYER then
      if v:isTouching(gPlayer) then
        local vec = gPlayer.position - v.position
				vec:normalize_inplace()

        local dist_to_move = 0
        if v.isDangerousToTouch then
          if gPlayer.unhittableTimeInFrames == 0 then
						if gPlayer:damageShield(v:getNameForDeathMessage()) == false then
							gPlayer:kill() -- and kill the player
							break -- Only deal with one enemy collision!
						end
					end
					dist_to_move = (v.physicsRadius + gPlayer.physicsRadius) * 10 
        elseif v.isChest then
					dist_to_move = 1
				else
					dist_to_move = (v.physicsRadius + gPlayer.physicsRadius) * 10 
				end
        -- Player responds to the collision appropriately
				vec = vec * dist_to_move
        gPlayer.velocity = vec
        gPlayer.hitByThingWithVelocity = vec:clone()
	    end
    end
  end
end

-------------------------------------
function spellToCreatureCollision()
	if gLeavingLevelCounter > 0 then return end

  -- Deal with collisions: Spell to enemy
  for i = #gSpellArray, 1, -1 do
    local spell = gSpellArray[i]
    for i2, creature in ipairs(gCreatureArray) do
      if creature.rezInExplosion == nil and creature.jumpTime == 0 and (spell.faction ~= creature.faction or spell.faction == FACTION_NULL) and creature.attachedToActor ~= gPlayer and spell:isTouching(creature) then
        creature.hitByThingWithVelocity = spell.velocity:clone()
        
				if creature.unhittableTimeInFrames == 0 then
					if creature.shieldLevel == 0 and spell.contactWithActorFunction ~= nil then -- Don't kill the creature - do something else!
						spell:contactWithActorFunction(creature)
					elseif creature:damageShield(spell:getNameForDeathMessage(), spell.damage) == false then
						creature:kill() -- and kill the enemy
						gLastCorpsePosition = creature.position:clone()
					end	
				end
				
				if creature.isDying == false then
					if creature.isPlayer == false then -- If the spell is friendly to the player, do a knock-back
						local vec = spell.velocity
						
						if spell.isSwordSlash then vec = creature.position - spell.attachedToActor.position end -- If a sword, the vector is from the caster, not the sword
						
						vec = vec:normalize_inplace() * 2
						creature.velocity = vec
--						creature:basicCollision(0.5)
--						creature.velocity = vector.zero
					end
					
--[[					if spell.isSwordSlash then
						local slash = creature:getAttachedSwordSlash() 
						if slash ~= nil then
							playSound()
							slash:kill()
						end
					end]]
        end
				
        if spell.dieOnThingCollision then -- Make the spell disappear, too
					spell:kill()
--          table.remove(gSpellArray, i)
--          spell = nil
--          break
        end
      end
    end
  end
end

-------------------------------------
function allyToCreatureCollision()
  for i, ally in ipairs(gCreatureArray) do
		if ally ~= gPlayer and ally.faction == FACTION_PLAYER then
			for i2, creature in ipairs(gCreatureArray) do
				if creature.faction ~= ally.faction and creature.unhittableTimeInFrames == 0 and creature.jumpTime == 0 and ally:isTouching(creature) then
					creature.hitByThingWithVelocity = ally.velocity:clone()
					if creature:damageShield(ally:getNameForDeathMessage()) == false then
						creature:kill() -- and kill the enemy
						gLastCorpsePosition = creature.position:clone()
					end	

					-- If the ally is an attached spell, then consider killing it
					if ally.attachedToActor == gPlayer and ally.dieOnThingCollision and creature.isChest == false then
						if ally:damageShield(creature:getNameForDeathMessage()) == false then
							ally:kill() -- and kill the enemy
						end
					end
					-- I decided to end the turncoat period of a creature bumping into another creature. The former code causing mutual annihilation is below
	--        if ally.turncoatTimeLeft > 0 then ally:endTurncoat() end -- But remember, there might be NON turncoated allies
	--        break
	--					ally.hitByThingWithVelocity = creature.velocity:clone()
	--					if ally:damageShield() == false then
	--						ally:kill() -- and kill the enemy
	--						gLastCorpsePosition = ally.position:clone()
	--					end
        end
      end
    end
  end
end

-------------------------------------
local tick = FIXED_STEP
local lastTotalTime = 0
gIsOverExit = false
function fixedUpdate(dt)	
	tick = tick - dt
	while tick <= 0 do
		tick = tick + FIXED_STEP
		
		gFixedFrameCount = gFixedFrameCount + 1
--		print("Fixed Tick Time: " .. gTotalTime - lastTotalTime)
		lastTotalTime = gTotalTime

		gChromaticAbberation = gChromaticAbberation * 0.75
		if gChromaticAbberation < 0.00001 then gChromaticAbberation = 0 end
		
    if gPlayer ~= nil then
      cameraFixedPointUpdate()
    end
    
		spellToCreatureCollision()
		spellsIgnoringTheirOwnCasterUpkeep()
		fixedUpdateForActorArray(gSpellArray)

		-- Ticks down life and deals with deaths
		fixedUpdateForActorArray(gCreatureArray)
		fixedUpdateForActorArray(gPickupArray)
    fixedUpdateForActorArray(gMapFeatureArray)

		prepareAllDoorsForClosing()
		setAllDoorsNearCreaturesToOpening()
		roomFixedUpdate()
		doorFixedUpdate()
		
    --- Count down time for the "+" to appear by spells as they are collected
    for k, v in pairs(gSpellsCarriedTimeSinceChanged) do
      if v > 0 then gSpellsCarriedTimeSinceChanged[k] = gSpellsCarriedTimeSinceChanged[k] - 1 end
    end
    
    
		gIsOverExit = false
		if gPlayer ~= nil then
			if gWhisperNodePosition ~= nil then
				local pos = vector(gPlayer.mapX - (gWhisperNodePosition.x * NODE_SIZE) - (NODE_SIZE * 0.5), gPlayer.mapY - (gWhisperNodePosition.y * NODE_SIZE) - (NODE_SIZE * 0.5))
				gDebugText = pos.x .. ", " .. pos.y .. " / " .. gWhisperNodePosition.x .. ", " .. gWhisperNodePosition.y

				local dist = pos:len() 
				local max_dist = NODE_SIZE
				dist = math.max(0, 1 - (dist / max_dist))
				gWhisperLoop:setVolume(dist * 0.2)
			end
			
			
			local speed_frac = gPlayer.velocity:len() / gPlayer.maxSpeed
			local vol = math.min(0.5, speed_frac) * 0.3
			local pitch = 0.1 + speed_frac * 0.1
			gEngineLoop:setVolume(vol)
			gEngineLoop:setPitch(pitch)

			gScreenX, gScreenY = cellXYToScreenXY(gPlayer.position.x, gPlayer.position.y)
		
			if gLeavingLevelCounter == 0 and gPortalIsOpen then
				local dv = gPlayer.position - gPortalPosition
				if dv:len2() < 1 then
					gIsOverExit = true
					if gIdleTimer >= EXIT_IDLE_VALUE then
						startLeavingLevel()
					end
				end
			end
		else
			gEngineLoop:setVolume(0)
		end
		
		if gLeavingLevelCounter > 0 then
			return
		end

    ------------------------------------------
    -- Count enemies
    local old_creature_count =  countEnemies()

		------------------------------------------
		
		if gPlayer ~= nil then			
			playerToCreatureCollision()
 		
			if gPlayer.isDying == true then return end
      
      --Deal with player picking up pickups
			playerToPickupCollision()
			playerToMapFeatureCollision()
		end
		
    allyToCreatureCollision()
    
		local new_creature_count = countEnemies()

		if gPlayer ~= nil and gPlayer.isDying == false then
			if gPortalIsOpen == false and gCreatureCount > new_creature_count and new_creature_count == 0 then	
				openPortal()
			end
			
			processActionStack()
		end
		gCreatureCount = new_creature_count
	end
end
--------------------------------
function openPortal()
	gPortalIsOpen = true
	playSound("PowerOff")
	gPowerOff = true
	wedgeAllDoors()
	gPortalOpenTime = MAX_PORTAL_OPEN_TIME
	gPortalPosition = getFurthestCornerOfRoomWithIDFromPosition(gPlayer.currentRoomID, gPlayer.position)
end

--------------------------------
function startLeavingLevel()
  if gCurrentLevel == 1 and gSeedCode == TUTORIAL_SEED_CODE then
    if gDefaultGameTypeIndex == nil or gDefaultGameTypeIndex == 1 then gDefaultGameTypeIndex = 3 end -- Move to the next logical game type 
  end
  
	gLeavingLevelCounter = LEAVE_LEVEL_TIME
	-- Store any attributes that need to roll over to the next level
	gDefaultShieldLevel = gPlayer.shieldLevel
	--
	playSound("LeaveLevel")
end
--------------------
function pickupSpellOfType(t)
	local s = gSpellsCarried[t]
	if s < MAX_SPELLS_CARRIED then
		addSpellsWithLetter(t, 1)
		gSpellsAltered = true
		playSound("Pickup")
	else
		gGold = gGold + 1
		playSound("PickupGold")
	end
end
--------------------
function updateAvoidVoidActorArray(array, dt)
	for i2, v2 in ipairs(gCreatureArray) do
		if v2.voidTimeLeft > 0 then
			for i, v in ipairs(array) do
				if v.isActive and v.faction ~= v2.faction then 
					local vec = v.position - v2.position
					local dist_sq = vec:len2()
					if dist_sq < (MAX_VOID_RADIUS * MAX_VOID_RADIUS) + v.physicsRadiusSq then
						vec:normalize_inplace()
						vec = vec * 10 * dt
						local end_pos = v.position + vec
						if v:getIsPositionLegal(end_pos) == true then
							v.position = end_pos:clone()
						end
					end
				end
			end
		end
	end
end	

function updatePhysicsForActorArray(array, dt)
	for i, v in ipairs(array) do
		if v.isActive then 
			v:updatePhysics(dt)
		end
	end
end	

function updatePickupActorArray(dt)
	if gPlayer ~= nil and gPlayer.jumpTime == 0 then
		for i, v in ipairs(gPickupArray) do
			if v.isDying == false and v.isAlive then
				v:moveTowardPlayerIfClose(dt)
			end
		end
	end
end
--------------------
function updatePhysicsForAttachedActorArray(array, dt)
	for i, v in ipairs(array) do -- Attached actors need to be moved last
		if v.isActive then 
			if v.attachedToActor ~= nil and (v.attachedToActor.isDying or v.attachedToActor.isAlive == false) then
				v.attachedToActor = nil
				v:kill()
			else
				v:updateAttachedActor(dt)
			end
		end
	end
end

--------------------
function fixedUpdateForActorArray(array)
	
	for i = #array, 1, -1 do
		local v = array[i]
		if v.isActive and v:fixedUpdate() == false then
			if v.isPlayer then 
				gPlayer = nil
			end

			v = nil
			table.remove(array, i)
		end
	end
		
	if gPlayer ~= nil then
		updateNearestTargetForPlayer()
	end
end

--------------------
function updateNearestTargetForPlayer()
	-- Find nearest target for player
	local MAX_FIRING_RANGE = 20
	local old_target = gTarget
	
	gTarget = nil
	gNearestEnemy = nil
	local distance_sq = 99999999
	local non_vis_distance_sq = 99999999
	local MAX_FIRING_RANGE_SQ = MAX_FIRING_RANGE * MAX_FIRING_RANGE

	gShowRedCircle = false
	local do_slowdown = false

	for i, v in ipairs(gCreatureArray) do
		if v.faction ~= FACTION_PLAYER and v.isChest == false and v.invisibleTimeLeft == 0 then
			local dist = gPlayer.position:dist2(v.position)
			if v.isVisible == false then
				if dist < non_vis_distance_sq then 
					gNearestEnemy = v
					non_vis_distance_sq = dist
				end
			elseif v.isInLineOfSight then
				if dist < DANGER_CIRCLE_RADIUS_SQ then do_slowdown = true end -- Danger due to proximity
				if dist < distance_sq and dist < MAX_FIRING_RANGE_SQ then
					gTarget = v
					distance_sq = dist
				end
			end
		end
	end
		
	for i, v in ipairs(gSpellArray) do
		if v.faction ~= FACTION_PLAYER and v.isVisible and v.position:dist2(gPlayer.position) < DANGER_CIRCLE_RADIUS_SQ then
			do_slowdown = true
			break
		end
	end	
		
	if gIsOverExit == false and (do_slowdown or gForcedSlowmo) then
		if gForcedSlowmo == false then
			gShowRedCircle = true
		end
		gSlowdownModeActivated = true
	else
		gSlowdownModeActivated = false
	end	
end

--------------------------------------------------------------------------------------------------------------------------------------------
function createGameAtLevel(level)	
	-- Force the level to a specific one if the console has been used to do so
	if gTrailerForcedLevelNumber ~= nil then
		level = gTrailerForcedLevelNumber
	end
	
	math.randomseed(gGameSeeds[gCurrentGameSeedIndex])
	gCurrentGameSeedIndex = gCurrentGameSeedIndex + 1 
	if gCurrentGameSeedIndex > MAX_GAME_SEED_INDEX then
		gCurrentGameSeedIndex = 1
	end
	
  gMessageTimer = 0
  gInfoMessageTimer = 0
  
	playSound("StartLevel")
	gPowerOff = false
	gShaderZoom = 0
	gScreenRotate = 0
	
	gShieldsLowSoundTimer = 0
	gMapEffectTimer = 0
	gXRayTime = 0
	gNegateTime = 0
	gFreezeUntilEnemySpotted = true
	
	gColourAdd = 0
	gChromaticAbberation = 0
	gCurrentScreenShakeTime = 0
	gPlayer = nil
	
	gMapFeatureArray = {}
	gCreatureArray = {}
	gPickupArray = {}
	gSpellArray = {}
	gExplosionArray = {}
	gWorldMessageArray = {}
	gTotalScreenShakeTime = 0
	gCurrentScreenShakeTime = 0

	gSlowdownModeKeyReleased = true
	gSlowdownModeActivated = false
	gSlowdownRemovalPeriod = 0
	MAX_SLOWDOWN_REMOVAL_PERIOD = 0.25

	gMapFeatureTouched = nil
	gMapImage = nil
		
	-- DEBUG
	local special_level = false
	local level_def = nil
	
	if gNextLevelHasPredefinedName ~= nil then 		
		level_def = gSpecialLevelDefinitions[gNextLevelHasPredefinedName] 
		gCurrentLevelName = gNextLevelHasPredefinedName:upper()
	else
		if level > 15 then level = 16 + ((level - 16) % 5) end -- loop the last 5 level types
	
--		level = math.min(level, #gLevelDefinitions) -- STOPS LEVEL GETTING TOO BIG!
		level_def = gLevelDefinitions[level]
		gCurrentLevelName = "LEVEL " .. gCurrentLevel
	end
	
	------------
	MAX_NODES_WIDE 		= level_def[1]
	MAX_NODES_HIGH 		= level_def[2]
	MAX_ROOM_SIZE_X		=  level_def[3]
	MAX_ROOM_SIZE_Y		=  level_def[4]
	NUMBER_OF_ENEMIES = level_def[5]
	NUMBER_OF_CHESTS 	=  level_def[6]
	NUMBER_OF_SPECIAL_CHESTS 	=  level_def[7]
	
	NUMBER_OF_PICKUPS = level_def[8]
	------------
	
	gMapWidth 	= ((NODE_FLOOR_SIZE + 1) * MAX_NODES_WIDE) + 1
	gMapHeight 	= ((NODE_FLOOR_SIZE + 1) * MAX_NODES_HIGH) + 1 -- width and height in tiles 
	
	createMapCells() -- gets the map cells ready
	setupTileset()
	
	---------------
	-- Create Player
	gFirstUpdate = true
	gPlayer = CActorMobile("Player")
	gPlayer:setAnimation("Player0")
	gPlayer.isPlayer = true
	gPlayer.faction = FACTION_PLAYER
	gPlayer.collisionFunction = CActorMobile.basicCollision
	gPlayer.faceMovementDirection = false
	gPlayer:setMaxShieldLevel(gMaxShieldLevel)
	gPlayer.shieldLevel = gDefaultShieldLevel
	gPlayer.explosionType = "PlayerExplosion"
	table.insert(gCreatureArray, gPlayer)
	
	---------------
	gSecurityTime = MAX_SECURITY_TIME
	gSecurityBotCount = 0
	gScoreMultiplier = 1
	gScoreMultiplier = 1
	clearActionStack()
	createLevel()

	gRitualSaidCorrectly = false

	-- Darkness
	gLevelIsDark = false
	if gNextLevelHasPredefinedName ~= nil then 
		gLevelIsDark = true 
	else
		local f = math.max(0, gCurrentLevel - 5) -- Gives us a 'levels past 5' value
		local rand = math.random(100)
		local comp = math.min(math.max(0, f) * 20, 50)
		if rand < comp then
			gLevelIsDark = true 
			print("Level is dark")
		end
	end
	
--	if gCurrentLevel > 1 and gCurrentLevel % 3 == 0 then
--		addShop()
--	end
	
  local vec = nil
  if gSeedCode == TUTORIAL_SEED_CODE and level == 1 then
    vec = vector(14.5, 26.5)
  end
    
	local room = setupPlayerPositionAndReturnRoom(vec) -- Usually passed a nil
	room:map(); 
	
	local password = nil
	local treasure_room = nil
	gVaultRoomID = 0
	if gCurrentLevel > 1 and gCurrentLevel % 2 == 1 and gNextLevelHasPredefinedName == nil then
		password, treasure_room = addVaultRoomWithPasswordAvoidingRoom(room)

--	elseif gCurrentLevel > 1 and gCurrentLevel % 3 == 0 then
		-- Add boss code
--		print("SHOULD ADD BOSS LEVEL HERE")
	end
		
	gMapImage = getPixelMapOfLevel()
  gPlayer.currentRoomID = room.roomID
	gPlayer:setRezInExplosion("PlayerRezIn")
	playSound("PlayerRezIn")

	---------------
	if treasure_room ~= nil then 
		gVaultRoomID = treasure_room.roomID 
	end 

	-- Create enemies
  if gSeedCode == TUTORIAL_SEED_CODE and level == 1 then
    createChestAt("TutChest", vector(6.5, 15.5))
    createEnemyWithName("StillFrog", vector(22.5, 24.5))
    createEnemyWithName("StillFrog", vector(4.5, 3.5))
    createEnemyWithName("StillFrog", vector(8.5, 6.5))
    createEnemyWithName("StillFrog", vector(4.5, 9.5))
    gVaultRoomID = 999
    local d = getDoorBetweenRoomIDandRoomID(4,5)
    if d ~= nil then 
      d:lockWithPassword("1234") 
    end

  else
    createEnemies(level_def[9], level_def[6], level_def[7], gVaultRoomID)
  end
  gLevelHasBoss = level_def[10]
	
	---------------
	-- Create World messages
	---------------
	local message_set = level_def[11]

	
	local room_list = getRoomIDArray()
	local useful_wall_list = {}
	for i, v in ipairs(room_list) do
		local wall_list = getRoomFromID(v):getUnbrokenWallList()
		for i2, v2 in ipairs(wall_list) do
			table.insert(useful_wall_list, v2)
		end
	end
	-- We now have a list of walls that can be used for messages
		
	useful_wall_list = getRandomisedArray(useful_wall_list) -- A list of {x,y,size,is_horizontal}
	message_set = getRandomisedArray(message_set)
	if password then table.insert(message_set, 1, "CODE: " .. password) end 
  
  if gSeedCode == TUTORIAL_SEED_CODE and level == 1 then message_set = {}; table.insert(message_set, 1, "CODE: 1234"); table.insert(message_set, 1, "CODE: 1234"); table.insert(message_set, 1, "CODE: 1234"); end 

	for i, msg in ipairs(message_set) do
		local len = msg:len() 
		for i2, wall in ipairs(useful_wall_list) do
			if wall[3] * 2 >= len then -- Because a wall segment is 2 letters, not 1
				if wall[4] then 
					addWorldMessage(msg, wall[1] - math.floor(len/4), math.floor(wall[2]), true, gWorldMessageArray)
				else
					addWorldMessage(msg, wall[1], wall[2] - math.floor(len/4), false, gWorldMessageArray)
				end
				table.remove(useful_wall_list, i2)
				break
			end
		end
	end

	---------------
	gCreatureCount = countEnemies()
	if gNextLevelHasPredefinedName then
		local str = "ENTERING " .. gNextLevelHasPredefinedName:upper()
		
		if gNextLevelHasPredefinedName == "palace" then str = "WELCOME TO THE PALACE OF WORMS" end

		setGlitchingForTime(3)
		displayMessage(str)
	elseif gLevelHasBoss == false then
		local str = "ENTERING " .. gCurrentLevelName		
		if gVaultRoomID ~= 0 and (gSeedCode ~= TUTORIAL_SEED_CODE or level ~= 1) then 
      createInfoPointAtPos(gPlayer.position, "TREASURE VAULT ON THIS LEVEL!\nSeek the 4-character code hidden in the wall glitches\nand type it into the Console to unlock the door.")  
    end 
		displayMessage(str)
	else
		displayMessage("WARNING!! BOSS " .. gCurrentLevelName)
	end
--	gGotKey = false
	gKilledBoss = false
	gPortalOpenTime = 0
	gPortalIsOpen = false
	gPortalPosition = setupExitPosition()
	
	createMapFeatures(level)
	---------------
	-- Create pickups
	createPickupsFromRewardSet("PickupsLevel" .. level, NUMBER_OF_PICKUPS)
	gWhisperNodePosition = nil
	if gCurrentLevelName == "LEVEL 7" then
		gWhisperNodePosition = vector(math.random(MAX_NODES_WIDE) - 1, math.random(MAX_NODES_HIGH) - 1)
	end
	
	
	gNextLevelHasPredefinedName = nil
end

-----------------------------------
function addVaultRoomWithPasswordAvoidingRoom(avoid_room)
	
	local room = getViableTreasureRoomAvoidingRoom(avoid_room) -- Okay - we now have a room that is at least 2 nodes wide in one dimension
	
	if room ~= nil then
		local password = getAlphanumericPasswordOfLength(4)

		local valid_coords = {}
		room:lockDoorsWithPassword(password)
		
		local half_step = math.floor(NODE_SIZE / 2)
		for x = room.x1 + half_step, room.x2 - half_step, NODE_SIZE do
			for y = room.y1 + half_step, room.y2 - half_step, NODE_SIZE do
				table.insert(valid_coords, {x+.5, y+.5})
			end
		end 
		
		valid_coords = getRandomisedArray(valid_coords)
		
		local max_treasures = 999 --math.min(10, math.floor((((room.x2-room.x1) / NODE_SIZE) * ((room.y2 - room.y1)) / NODE_SIZE) * 0.75))
		local rewards =  deepcopy(gProbabilitySet["TREASUREROOM"]) -- Create the mutable list used to make all these pickups
		for i, v in ipairs(valid_coords) do
			if createPickupFromRewardSetAtSpecificLocation(rewards, vector(v[1],v[2])) == false then break end
			if max_treasures > 0 then max_treasures = max_treasures - 1 else break end
		end
		
		return password, room
	end
end

-----------------------------------
function addShop()
	local room = getViableShopRoom() -- Okay - we now have a room that is at least 2 nodes wide in one dimension
	local prizes = deepcopy(gProbabilitySet["SHOPPRZ"])
	
	if room ~= nil then
		-- How many tiles do we have?
		local count = 1
		local half_step = math.floor(NODE_SIZE / 2)
		for x = room.x1 + half_step, room.x2 - half_step, NODE_SIZE do
			for y = room.y1 + half_step, room.y2 - half_step, NODE_SIZE do
				if count == 1 then 
					createSellPointAtPos(vector(x+0.5,y+0.5))
				else
					local reward = getRandomThingFromProbabilitySet(prizes)
					print(reward)
					createBuyPointAtPos(vector(x+0.5,y+0.5), convertLetterToPickupValue(reward))
				end
				count = count + 1
			end
		end
	end
end

-----------------------------------
function createBuyPointAtPos(pos, number_of_buyable)
	local a = CActorMapFeature("BuyTile")
	a.position = pos:clone()
	a.data = number_of_buyable
	getMapCell(math.floor(a.position.x), math.floor(a.position.y)).isMapFeature = true
	a:setInteractionRadius(1.5)
	a:setBasicAndActivatedQuads("BuyTile", "BuyTileLock")
	table.insert(gMapFeatureArray, a)
end
-----------------------------------
function createSellPointAtPos(pos)
	local a = CActorMapFeature("SellTile")
	a.position = pos:clone()
	getMapCell(math.floor(a.position.x), math.floor(a.position.y)).isMapFeature = true
	a:setInteractionRadius(1.5)
	a:setBasicAndActivatedQuads("SellTile", "SellTileLock")
	table.insert(gMapFeatureArray, a)
	return a
end
-----------------------------------
function createInfoPointAtPos(pos, message)
	local a = CActorMapFeature("InfoTile")
	a.position = pos:clone()
  a.data = "Hint:\n---------\n" .. message

	getMapCell(math.floor(a.position.x), math.floor(a.position.y)).isMapFeature = true
	a:setInteractionRadius(1.5)
  a:setAnimation("InfoTile")
	table.insert(gMapFeatureArray, a)
	return a
end
-----------------------------------
function createMapFeatures(level)
	local arr = getRoomIDArray()
  local room_id = gPlayer.currentRoomID
  local room = nil
  local pos = nil
  ---------------------
  if gSeedCode == TUTORIAL_SEED_CODE and level == 1 then
    removeFirstMemberWithValue_inplace(arr, room_id)
    createInfoPointAtPos(gPlayer.position, "Use CURSOR/ARROWS to move.\nUse A-Z to cast spells.\nUse SPACE for Sword.")  
    
    createInfoPointAtPos(vector(6.5, 22.5), "Spells are limited.\nThe letters at the bottom show how many of each spell you own.\nPick up spell-letters you see lying around.")   
     
    createInfoPointAtPos(vector(6.5, 18.5), "Use SPACE to swing the Sword\nand smash the chest to get its spells.\nThe Sword is weak but never runs out.")   
 
    createInfoPointAtPos(vector(18.5, 18.5), "The game will go into SLOWMO when enemies are near.\nMoving or casting spells breaks SLOWMO for a second.")   

    createInfoPointAtPos(vector(22.5, 18.5), "The A to Z keys cast spells.\nFor example 'A' is an Arrow spell.\nTargeting automatically picks the nearest enemy.")   
 
    createInfoPointAtPos(vector(22.5, 26.5), "You can change the way SLOWMO works\nby pressing CTRL at any time.\nYou can even turn it off.")   

    createInfoPointAtPos(vector(18.5, 10.5), "The door here is locked.\nLook around the level for a clue.\nPay special attention to glitches in the walls.")
    
    createInfoPointAtPos(vector(18.5, 6.5), "Hidden text gets more or less glitchy\ndepending on where you are in the level.\nMove around until you find the right spot to read it.")   

    createInfoPointAtPos(vector(10.5, 6.5), "Remember: SLOWMO is your friend!\nIf in doubt, do NOTHING.\nThe game will slow down and give you time to think.")   

    createInfoPointAtPos(vector(6.5, 6.5), "You can use the Console for lots of things.\nPress RETURN to open it and\ntry 'buy 1 s' to buy a spare Shield spell.")   

    createInfoPointAtPos(vector(2.5, 6.5), "Once everything in the level is dead\nmove over the exit to enter the next level.")   
  else
     if level == 1 then
      removeFirstMemberWithValue_inplace(arr, room_id)
      room = getRoomFromID(getRandomMember(arr))
      room_id = room.roomID
      local pos = getNearestCornerOfRoomWithIDFromPosition(room_id, gPlayer.position)
      pos.x, pos.y = snapXYToNodeCellCenterXY(pos.x, pos.y)
      createInfoPointAtPos(pos, "Remember: you can change how SLOWMO\nworks by pressing CTRL.")   
    elseif level == 2 then      
      removeFirstMemberWithValue_inplace(arr, room_id)
      room = getRoomFromID(getRandomMember(arr))
      room_id = room.roomID
      pos = getNearestCornerOfRoomWithIDFromPosition(room_id, gPlayer.position)
      pos.x, pos.y = snapXYToNodeCellCenterXY(pos.x, pos.y)
      createInfoPointAtPos(pos, "You cannot exceed 10 of any spell.\nAny drops that would give you more than 10\nautomatically turn into Gold.") 
      
      removeFirstMemberWithValue_inplace(arr, room_id)
      room = getRoomFromID(getRandomMember(arr))
      room_id = room.roomID
      local pos = getNearestCornerOfRoomWithIDFromPosition(room_id, gPlayer.position)
      pos.x, pos.y = snapXYToNodeCellCenterXY(pos.x, pos.y)
      createInfoPointAtPos(pos, "Always keep some spare 'Shield' spells.\nIf you can't afford to buy them you can\nuse the Console to sell other spells for gold.")   
    elseif level == 3 then
      removeFirstMemberWithValue_inplace(arr, room_id)
      room = getRoomFromID(getRandomMember(arr))
      room_id = room.roomID
      local pos = getNearestCornerOfRoomWithIDFromPosition(room_id, gPlayer.position)
      pos.x, pos.y = snapXYToNodeCellCenterXY(pos.x, pos.y)
      createInfoPointAtPos(pos, "Remember: open the Console with RETURN and type 'shieldup'\nto increase your maximum number of shields.") 
            
      removeFirstMemberWithValue_inplace(arr, room_id)
      room = getRoomFromID(getRandomMember(arr))
      room_id = room.roomID
      local pos = getNearestCornerOfRoomWithIDFromPosition(room_id, gPlayer.position)
      pos.x, pos.y = snapXYToNodeCellCenterXY(pos.x, pos.y)
      createInfoPointAtPos(pos, "The Console has information about\nevery spell and every creature.")   
    elseif level == 4 then
      removeFirstMemberWithValue_inplace(arr, room_id)
      room = getRoomFromID(getRandomMember(arr))
      room_id = room.roomID
      local pos = getNearestCornerOfRoomWithIDFromPosition(room_id, gPlayer.position)
      pos.x, pos.y = snapXYToNodeCellCenterXY(pos.x, pos.y)
      createInfoPointAtPos(pos, "Spellrazor is filled with mysteries.\nType 'secrets' into the Console to start exploring them.")   
    end
  end
end
-----------------------------------
function addAllSpells()
	for i, v in ipairs(gUpperCaseList) do
		gSpellsCarried[v] = 10
    gSpellsCarriedTimeSinceChanged[v] = 0
	end
	gSpellsAltered = true
end

-----------------------------------
function getRandomisedValueWithFeederValueAndWrapSize(value, wrap)
	return gGameSeeds[1 + (value % MAX_GAME_SEED_INDEX)] % wrap
end

------------------------------------
function updateDebugInfo()
	if gPlayer == nil then return end
	if gDebug == false then return end
--	gDebugText = ""
	
	local names = {"(TL", "(T", "(TR", "(R", "(BR", "(B", "(BL", "(L"}
	local x, y = math.floor(gPlayer.mapX / NODE_SIZE), math.floor(gPlayer.mapY / NODE_SIZE)
	--local x, y = gPlayer.mapX, gPlayer.mapY
		
--	gDebugText = gDebugText .. x .. ", " .. y .. ": "
--	for i, v in ipairs(gAround8) do
--		gDebugText = gDebugText .. names[i] .. ":"
		--if getMapCellConnectionsXY(x,y)[i] == true then  gDebugText = gDebugText .. "Y), " else gDebugText = gDebugText .."_), " end 
--		if getNodeConnectionsXY(x,y)[i] == true then  gDebugText = gDebugText .. "Y), " else gDebugText = gDebugText .."_), " end 
--	end
end
------------------------------------
function addSpellsWithLetter(letter, count)
	if count == nil then count = 1 end
	if gSpellsCarried[letter] == nil then gSpellsCarried[letter] = 0 end
	gSpellsCarried[letter] = gSpellsCarried[letter] + count
  gSpellsCarriedTimeSinceChanged[letter] = 40
	gSpellsFound[letter] = true
	if gSpellsCarried[letter]  > MAX_SPELLS_CARRIED then gSpellsCarried[letter] = MAX_SPELLS_CARRIED end
end

------------------------------------
function gameState:startEndgame()
	setGlitchingForTime(-2)
	setupEndgameBackground()
	gStartedFinalRitual = true 	
	gBackgroundMusic:stop()
	gBackgroundMusic = nil
	gBackgroundMusic = love.audio.newSource("assets/VocalChant.mp3", 'stream' )
	gBackgroundMusic:setLooping( false ) --so it doesnt stop
	gBackgroundMusic:setVolume(0.5)
	gBackgroundMusic:play()	
end

------------------------------------
function gameState:endEndgame()
	gBackgroundMusic:stop()
	gBackgroundMusic = nil	
	gBackgroundMusic = love.audio.newSource("assets/MusicLoop1.mp3", 'stream' )
	gBackgroundMusic:setLooping( true ) --so it doesnt stop
	gBackgroundMusic:setVolume(0.1)
	gBackgroundMusic:play()
	gFinishedFinalRitual = true
	
	setGlitchingForTime(-5)
	
	local list = {"iamharbinger"}
	local new_commands = ""
	local new_command_count = 0
	for i, v in ipairs(list) do
		local text = v:lower()
		if gConsoleCommandsOpened[text] == nil then
			gConsoleCommandsOpened[text] = true
			if new_command_count ~= 0 then new_commands = new_commands .. ", " end
			new_commands = new_commands ..  "[" .. v .. "]"
			new_command_count = new_command_count + 1
		end
	end
end

