local class = require 'library/middleclass'
CDoorway = class('game/CDoorway')
IS_HORIZONTAL = true
IS_VERTICAL = false
local DOOR_OPEN_SPEED = 1 / 20 * 8

function CDoorway:initialize(x, y, is_horiz, room_1, room_2)
  self.x = x
  self.y = y
	self.cx = x
	self.cy = y
	self.isVisible = false
  self.isHorizontal = is_horiz
	self.isWedged = false
	self.isLocked = false
	self.password = nil
	self.isCreatureNear = false
	self.openFraction = 0
	self.roomID1 = room_1
	self.roomID2 = room_2
  		
	if self.isHorizontal then
		self.cx = self.x + NODE_FLOOR_SIZE / 2
		self.cy = self.cy + 0.5
		for i = 1, NODE_FLOOR_SIZE do 
			setMapCellAsDoor(self.x + i - 1,self.y)
		end
	else
		self.cx = self.cx + 0.5
		self.cy = self.y + NODE_FLOOR_SIZE / 2
		for i = 1, NODE_FLOOR_SIZE do 
			setMapCellAsDoor(self.x,self.y + i - 1)
		end
	end
	
	self:setMapSolidity()

end

function CDoorway:setMapSolidity()
	local solidity = true
	if self.openFraction > 0 then
		solidity = false
	end
	
	if self.isHorizontal then
		for i = 1, NODE_FLOOR_SIZE do 
			setMapCellSolidity(self.x + i - 1,self.y, solidity)
			setMapCellAsDoor(self.x + i - 1,self.y)
		end
	else
		for i = 1, NODE_FLOOR_SIZE do 
			setMapCellSolidity(self.x,self.y + i - 1, solidity)
			setMapCellAsDoor(self.x,self.y + i - 1)
		end
	end
end

function CDoorway:isSupportedByWalls()
	local x = math.floor(self.x)
	local y = math.floor(self.y)
	if self.isHorizontal then
		if getMapCell(x-1, y).isSolid and getMapCell(x+NODE_FLOOR_SIZE, y).isSolid then return true end
	else
		if getMapCell(x, y-1).isSolid and getMapCell(x, y+NODE_FLOOR_SIZE).isSolid then return true end
	end
	return false
end

--------------------------
function CDoorway:wedge()
	self.isWedged = true
	self.openFraction = 1
	
	-- Go through nearby tiles and nodes and do the appropriate update
	self:updateMapConnections()
	self:setMapSolidity()
end

function CDoorway:removeWedge()
	self.isWedged = false
	self.openFraction = 0
	self:setMapSolidity()
end

function CDoorway:setMapCellsToUnlocked()
	if self.isHorizontal then
		for i = 1, NODE_FLOOR_SIZE do 
			setMapCellAsUnlocked(self.x + i - 1,self.y)
		end
	else
		for i = 1, NODE_FLOOR_SIZE do 
			setMapCellAsUnlocked(self.x,self.y + i - 1)
		end
	end
end

function CDoorway:setMapCellsToLocked()
	if self.isHorizontal then
		for i = 1, NODE_FLOOR_SIZE do 
			setMapCellAsLocked(self.x + i - 1,self.y)
		end
	else
		for i = 1, NODE_FLOOR_SIZE do 
			setMapCellAsLocked(self.x,self.y + i - 1)
		end
	end
end
--------------------------
function CDoorway:lock()
	self.isLocked = true
	self.isWedged = false
	self.openFraction = 0
	self:setMapSolidity()
	self:setMapCellsToLocked()
	
	-- Go through nearby tiles and nodes and do the appropriate update
	self:updateMapConnections()
end
--------------------------
function CDoorway:unlock()
	self.isLocked = false
	self:setMapCellsToUnlocked()
	self.isWedged = false
	self.openFraction = 0
	self:setMapSolidity()

	-- Go through nearby tiles and nodes and do the appropriate update
	self:updateMapConnections()
end

-------------------------
function CDoorway:updateMapConnections()
		-- Go through nearby tiles and nodes and do the appropriate update
	local nx, ny = getNodeXYFromCellXY(math.floor(self.cx), math.floor(self.cy))
	for y = ny - 1, ny + 1 do
		for x = nx - 1, nx + 1 do
			if x >= 0 and x < MAX_NODES_WIDE and y > 0 and y < MAX_NODES_HIGH then
				updateNodeConnectionsXY(x,y)
			end
		end
	end
	
	for y = self.cy - 1, self.cy + 1 do
		for x = self.cx - 1, self.cx + 1 do
			updateCellConnectionsXY(math.floor(x),math.floor(y))
		end
	end
end


--------------------------
function CDoorway:fixedUpdate()
	if self.isWedged then return end
	if self.isLocked then return end
	
	if self.isCreatureNear then	
		if self.openFraction < 1 then 
			self.openFraction = self.openFraction + DOOR_OPEN_SPEED
			if self.openFraction >= 1 then 
				self.openFraction = 1			
				self:setMapSolidity()
			end
		end
	else
		if self.openFraction > 0 then 
			self.openFraction = self.openFraction - DOOR_OPEN_SPEED
			if self.openFraction <= 0 then 
				self.openFraction = 0 
				self:setMapSolidity()
			end
		end
	end
end

---Get a good position on the far side of a doorway
function CDoorway:getNodePositionOnFarSideOfDoorBasedOnXY(rx, ry)
	local look_ahead = NODE_SIZE / 2
	local x = self.cx
	local y = self.cy
	if self.isHorizontal then
		if ry > y then y = y - look_ahead
		elseif ry < y then y = y + look_ahead end
	else
		if rx > x then x = x - look_ahead
		elseif rx < x then x = x + look_ahead end
	end
	x = math.floor(x) + 0.5
	y = math.floor(y) + 0.5

	return vector(x,y)
end

function CDoorway:getNodePositionOnNearSideOfDoorBasedOnXY(rx, ry)
	local look_ahead = NODE_SIZE / 2
	local x = self.cx
	local y = self.cy
	if self.isHorizontal then
		if ry > y then y = y + look_ahead
		elseif ry < y then y = y - look_ahead end
	else
		if rx > x then x = x + look_ahead
		elseif rx < x then x = x - look_ahead end
	end
	x = math.floor(x) + 0.5
	y = math.floor(y) + 0.5

	return vector(x,y)
end

function CDoorway:lockWithPassword(password)
	self:lock()
	self.password = password
end

function CDoorway:unlockWithPassword(password)
	local result = false
	if self.isLocked and self.password == password then
		result = true
		self:unlock()
	end
	return result
end