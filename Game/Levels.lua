local BOSS = true
local NO_BOSS = false

-------------------------------------------------W---H---MAX_ROOM_X---MAX_ROOM_Y-#NME--#CHST-#SCHST--#PICKUPS--ENEMY_SET-----------------------------------
gSpecialLevelDefinitions = {}
gSpecialLevelDefinitions["ascension"] 			= {	12, 12, 	  4, 						8,		10,		3,	3,			0,			{false, {"Ogret", 	50}}, 	  			NO_BOSS, {"Alastar"}}
gSpecialLevelDefinitions["abaddon"] 				= {	12, 12, 	  4, 						5,		6,		3,	3,			0,			{false, {"Magus", 	50}}, 	  			NO_BOSS, {"Arcesso"}}
gSpecialLevelDefinitions["palace"] 				= {	8, 	8, 	  	4, 						4,		20,		0,	0,			0,			{false, {"Skull", 	50}}, 	  			NO_BOSS, {"Ritual"}}

gLevelDefinitions = {
----W---H---MAX_ROOM_X---MAX_ROOM_Y----#NME--#CHST-#SCHST--#PICKUPS--ENEMY_SET-----------------------------------  
  { 7, 7, 	  	4, 						3,		0,			3,	0,			0,				{{"Bug", 5}, 			{"Frog", 		3}},																					NO_BOSS, {"SPACE = SWORD",  "Press RETURN for console", "Arrows to move", "A-Z = cast spells", "Open chests with SWORD"}},
	{ 15, 12, 		5, 						3,		0,			4,	0,			0,				{{"Bug", 5}, 			{"Frog", 		5}, 		{"Fly",  		5}},											NO_BOSS, {"Tesla = genius", "Type SECRETS in the console", "Type SHIELDUP in the console", "Press RETURN for console"}},
	{	15, 15, 	  5, 						3,		0,			5,  0,			0,				{{"Fly", 7}, 			{"Frog", 		5}, 		{"Grunt",  	3}}, 	  									NO_BOSS, {"Always keep spare Shields", "Press RETURN for console"}},
	{	16,  7, 	  3, 						5,		0,			4,	0,			0,				{{"Fly", 5},  		{"Grunt", 	4},  		{"Swordsman", 	2}}, 	  							NO_BOSS, {"CTRL = cycle Slowmo modes", "Press RETURN for console", "Flotilla"}},
	{	16, 12, 	  4, 						3,		0,			5,	0,			0,				{{"Fly", 7}, 			{"Grunt", 	6}, 		{"Popper", 	2}, {"Swordsman", 	2}}, 	NO_BOSS, {"Later levels are dark", "Eraserhead", "Press RETURN for console", "Coffee"}},
	
	{	22, 7, 	  	2, 						2,		0,			4,	0,			0,				{{"Fly", 7}, 			{"Swordsman", 4}, 	{"Corner", 	5}, {"Grunt", 	4}}, 	 	  NO_BOSS, {"?","?","?","?"}},
	{	16, 12, 	  7, 						7,		0,			5,	0,			0,				{{"Grunt", 16}, 	{"Popper", 	4}}, 	  																		NO_BOSS, {"What happened to DMB?", "Nevermore"}},
	{	12, 12, 	  3, 						3,		0,			4,	0,			0,				{{"Grunt", 8},		{"Wraith", 	4}, 		{"Corner", 	4}}, 	  								NO_BOSS, {"Euclidean", "Proust", "Angels"}},
	{	16, 12, 	  5, 						4,		0,			5,	0,			0,				{{"Fly", 5}, 			{"Spider", 5},			{"Popper", 	2}}, 	  								NO_BOSS, {"Summoning", "Effect > Cause"}},
	{	12, 12, 	  3, 						3,		0,			4,	0,			0,				{{"Grunt", 12}, 	{"Spider", 5}, 			{"Siren", 	2}}, 	  									NO_BOSS, {"Palace", "Visage"}},
	
	{	12, 12, 	  3, 						3,		0,			4,	0,			0,				{{"Grunt", 12}, 	{"Wraith", 4}, 			{"Ogret", 	2}}, 	  									NO_BOSS, {"Palace", "Abaddon", "Angels", "Flotilla"}},
	{	12, 12, 	  3, 						3,		0,			4,	0,			0,				{{"Grunt", 12}, 	{"Wraith", 4}, 			{"Beamer", 	2}}, 	  								NO_BOSS, {"Palace", "Abaddon", "Angels", "Flotilla"}},
	{	12, 12, 	  3, 						3,		0,			4,	0,			0,				{{"Grunt", 12}, 	{"Ogret", 5}, 			{"Popper", 	2}, {"Beamer", 	2}}, 	  	NO_BOSS, {"Palace", "Abaddon", "Angels", "Flotilla"}},
	{	12, 12, 	  3, 						3,		0,			4,	0,			0,				{{"Grunt", 12}, 	{"Ogret", 5}, 			{"Siren", 	2}, {"Beamer", 	2}}, 	  	  NO_BOSS, {"Palace", "Abaddon", "Angels", "Flotilla"}},
	{	12, 7, 	  	3, 						3,		0,			4,	0,			0,				{{"Ogret", 8}, 		{"Corner", 5}, 			{"Siren", 	2}, {"Beamer", 	2}}, 	  NO_BOSS, {"Palace", "Abaddon", "Angels", "Flotilla"}},

-- These last are looped 
	{	12, 12, 	  3, 						5,		20,			4,	0,			0,				{false, {"Wraith", 30},		{"Grunt", 	50}, 	{"Spider", 	50},	{"Beamer", 		40}, 	{"Fly", 	10}, 	{"Popper", 	10}, {"Ogret", 10}, {"Corner", 	10}, {"Siren", 10}}, 	  NO_BOSS, {"Palace", "Abaddon", "Angels", "Flotilla"}			},
	{	12, 12, 	  3, 						5,		20,			4,	0,			0,				{false, {"Wraith", 30},		{"Grunt", 	50}, 	{"Spider", 	50},	{"Beamer", 		40}, 	{"Fly", 	10}, 	{"Popper", 	10}, {"Ogret", 10}, {"Corner", 	10}, {"Siren", 10}}, 	  NO_BOSS, {"Palace", "Abaddon", "Demons", "Flotilla"}			},
	{	12, 12, 	  3, 						5,		20,			4,	0,			0,				{false, {"Wraith", 30},		{"Grunt", 	50}, 	{"Spider", 	50},	{"Beamer", 		40}, 	{"Fly", 	10}, 	{"Popper", 	10}, {"Ogret", 10}, {"Corner", 	10}, {"Siren", 10}}, 	  NO_BOSS, {"Palace", "Abaddon", "Angels", "Flotilla"}		},
	{	12, 12, 	  3, 						5,		20,			4,	0,			0,				{false, {"Wraith", 30},		{"Grunt", 	50}, 	{"Spider", 	50},	{"Beamer", 		40}, 	{"Fly", 	10}, 	{"Popper", 	10}, {"Ogret", 10}, {"Corner", 	10}, {"Siren", 10}}, 	  NO_BOSS, {"Palace", "Abaddon", "Demons", "Flotilla"}			},
	{	12, 12, 	  3, 						5,		20,			4,	0,			0,				{false, {"Wraith", 30},		{"Grunt", 	50}, 	{"Spider", 	50},	{"Beamer", 		40}, 	{"Fly", 	10}, 	{"Popper", 	10}, {"Ogret", 10}, {"Corner", 	10}, {"Siren", 10}}, 	  NO_BOSS, {"Palace", "Abaddon", "Death", "Flotilla"}			},
}