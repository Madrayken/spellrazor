require "game.CSpecialEffectsWalker"
require "library.Tools"
-------------------------------
local GRID_FADE_TIME = 8 -- seconds before one layer fades
local GRID_WIBBLE_X_TIME = 12
function drawTronBackground(colour, time, offset_frac, center_lines_on)
	if center_lines_on == nil then 
		center_lines_on = true 
	end
	local old_canvas = love.graphics.getCanvas()
	local old_shader = love.graphics.getShader()
	love.graphics.setCanvas(tronBackgroundCanvas)
	love.graphics.clear(0,0,0,0)
	
	local wibble = math.sin(math.rad(360 * (time % GRID_WIBBLE_X_TIME) / GRID_WIBBLE_X_TIME))
	
	love.graphics.setLineWidth(1)
	love.graphics.setLineStyle("rough")

	local lines = 40
	local x_step = TRON_BACKGROUND_SIZE / 40
	local y_step = TRON_BACKGROUND_SIZE / 40
	local col = deepcopy(colour)
--	love.graphics.setColor(col[1] * 2, col[2] * 2, col[3] * 2, 255)
	love.graphics.setColor(col)
	
	local x, y
	x = (TRON_BACKGROUND_SIZE / 2) - (lines / 2) * x_step
	y = (TRON_BACKGROUND_SIZE / 2) - (lines / 2) * y_step
	for i = 1, lines do
		love.graphics.line(x + i * x_step, 0, x + i * x_step, TRON_BACKGROUND_SIZE)
		love.graphics.line(0, y + i * y_step, TRON_BACKGROUND_SIZE, y + i * y_step)

		x = x + x_step
		y = y + y_step
	end
	love.graphics.setColor(col[1], col[2], col[3], 255)

	love.graphics.setCanvas(old_canvas)
	
	local scale = 2
	love.graphics.push()
	
	love.graphics.setBlendMode("add") --Default blend mode

	local ang = wibble + time / 5
	love.graphics.setShader(gShaderTronBackground)
	love.graphics.translate(gCanvasWidth/2, gCanvasHeight/2)
	love.graphics.rotate(ang)
	love.graphics.translate(-gCanvasWidth/2, -gCanvasHeight/2)
	local time_to_fade = 3
	local frac = (time % time_to_fade) / time_to_fade
	frac = 1 - frac
	local rgb = 255 * (1.0 - frac)
	love.graphics.setColor(rgb,rgb,rgb, rgb)
	if gShaderTronBackground ~= nil then
		gShaderTronBackground:send("zoom", 0.1 + frac * 2.2)
	end
	love.graphics.draw(tronBackgroundCanvas, gCanvasWidth/2 - (TRON_BACKGROUND_SIZE*scale) /2, gCanvasHeight/2-(TRON_BACKGROUND_SIZE*scale)/2, 0, scale, scale)
		
	ang = ang + math.rad(30)
	love.graphics.translate(gCanvasWidth/2, gCanvasHeight/2)
	love.graphics.rotate(ang)
	love.graphics.translate(-gCanvasWidth/2, -gCanvasHeight/2)
	time = time + (time_to_fade / 3)
	frac = ((time % time_to_fade) / time_to_fade) % 1
	frac = 1 - frac
	rgb = 255 * (1.0 - frac)
	love.graphics.setColor(rgb,rgb,rgb, rgb)	
	if gShaderTronBackground ~= nil then
		gShaderTronBackground:send("zoom", 0.1 + frac * 2.2)
	end
	love.graphics.draw(tronBackgroundCanvas, gCanvasWidth/2 - (TRON_BACKGROUND_SIZE*scale) /2, gCanvasHeight/2-(TRON_BACKGROUND_SIZE*scale)/2, 0, scale, scale)
	
	ang = ang + math.rad(30)
	love.graphics.translate(gCanvasWidth/2, gCanvasHeight/2)
	love.graphics.rotate(ang)
	love.graphics.translate(-gCanvasWidth/2, -gCanvasHeight/2)
	time = time + (time_to_fade / 3)
	frac = ((time % time_to_fade) / time_to_fade) % 1
	frac = 1 - frac
	rgb = 255 * (1.0 - frac)
	love.graphics.setColor(rgb,rgb,rgb, rgb)	
	if gShaderTronBackground ~= nil then
		gShaderTronBackground:send("zoom", 0.1 + frac * 2.2)
	end
	love.graphics.draw(tronBackgroundCanvas, gCanvasWidth/2 - (TRON_BACKGROUND_SIZE*scale) /2, gCanvasHeight/2-(TRON_BACKGROUND_SIZE*scale)/2, 0, scale, scale)
			
	love.graphics.setColor(col[1], col[2], col[3], 255)
	love.graphics.setShader(old_shader)
	love.graphics.pop()
	love.graphics.setBlendMode("add") --Default blend mode
	love.graphics.setLineStyle("smooth")

	if center_lines_on == true then
		for i = 1, 100 do
			x = gCanvasWidth/2 
			y = gCanvasHeight * 0.15
			
			x2 = math.random(gCanvasWidth)
			y2 = math.random(gCanvasHeight)
			
	--		love.graphics.line(x, y, TRON_BACKGROUND_SIZE/2, TRON_BACKGROUND_SIZE/2)
			love.graphics.line(x, y, x2, y2)
		end
	end

	love.graphics.setBlendMode("alpha") --Default blend mode
end

-------------------------------------------------------------
local walkers = {}
local stepTime = 0
local totalTime = 0
local MAX_STEP_TIME = 0.01

gRitualTime = 0
gRitualBlackScreenFraction = 0
gRitualRotation = 0
gRitualScrollFraction = 0
gRitualOpacityFraction = 1

function setupEndgameBackground()
	gRitualTime = 0
	gRitualBlackScreenFraction = 0
	gRitualRotation = 0
	gRitualScrollFraction = 0
	gRitualOpacityFraction = 1
	
	math.randomseed(os.clock()*100000000000)
	walkers = {}
	walkers[1] = CSpecialEffectsWalker(0,0, {30, 1, 50, 7, math.random(7)}, 300)

--[[  for i = 1, 50 do
    walkers[i] = CSpecialEffectsWalker(math.random(gGenerativeCanvasSize),math.random(gGenerativeCanvasSize), {math.random(8) * 7, math.random(7), math.random(50) * 50, math.random(7)}, 300)
  end]]
end
-------------------------------------------------------------
gTotalRitualPeriod = 60*3
function updateEndgameBackground(dt)
	local ritual_period = gTotalRitualPeriod
	local ritual_message_start = ritual_period - 2
	local fade_down_period = 10
	
	local rotate_start = 60
	local rotate_period = ritual_period - rotate_start - 20
	
	local scroll_start = 25
	local scroll_period = ritual_period - scroll_start - 30
	
	local undo_fade_period = 10
	local undo_fade_start = ritual_period - undo_fade_period
	
	local text_list = {"thou", "art", "harbinger"}
	
  love.graphics.setLineStyle("rough")
	
	local old_time = gRitualTime
	gRitualTime = gRitualTime + dt
	
	if gRitualTime > fade_down_period then
		if math.floor(gRitualTime * 2) > math.floor(old_time * 2) and #walkers < 50 then
			table.insert(walkers, CSpecialEffectsWalker(math.random(gGenerativeCanvasSize), math.random(gGenerativeCanvasSize), {math.random(8) * 7, math.random(7), math.random(50) * 50, math.random(7)}, 300))
		end
	end
	
	if gRitualTime > ritual_period then
		gRitualTime = 0
		gStartedFinalRitual = false
		gameState:endEndgame()
	elseif gRitualTime < fade_down_period then
		gRitualBlackScreenFraction = gRitualTime / fade_down_period
	else
	end
	
	-- Deal with scrolling
	frac = math.clamp((gRitualTime - scroll_start) / scroll_period, 0, 1)
	frac = math.sin(math.rad(180) * frac * dt) -- Get smoothed fraction
	gRitualScrollFraction = (gRitualScrollFraction + frac * dt * 6) % 1
	
	-- Deal with rotation
	local frac = math.clamp((gRitualTime - rotate_start) / rotate_period, 0, 1)
	frac = math.sin(math.rad(360) * frac)
	gRitualRotation = gRitualRotation + frac * math.pi * 0.0005
	love.graphics.setCanvas(generativeCanvas)
	
  for t = 1, 5 do
    stepTime = stepTime + dt
    totalTime = totalTime + dt
    if stepTime > MAX_STEP_TIME then 
      stepTime = stepTime - MAX_STEP_TIME      
      
      for i, v in ipairs(walkers) do
        v:update()
      end
      
      love.graphics.setColor(0,0,0, 255)
      for i, v in ipairs(walkers) do
        for i2, v2 in ipairs(gAround4Diagonal) do
          local draw_x, draw_y = v:getEraseXY()
          local x2 = draw_x * v2[1]
          local y2 = draw_y * v2[2]
          love.graphics.line(x2+0.5, y2+0.5, x2+0.6, y2+0.5)
        end
      end
      
      love.graphics.setColor(255,255,255, 128)
      for i, v in ipairs(walkers) do        
        local draw_x, draw_y = v:getDrawXY()
        local x2 = draw_x-- * v2[1]
        local y2 = draw_y-- * v2[2]
        love.graphics.line(x2+0.5, y2+0.5, x2+0.6, y2+0.5)
      end
    end
  end
	love.graphics.setCanvas()
end