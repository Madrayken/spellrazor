require "library.Tools"
require "game.Map"
require "game.CExplosion" 
vector = require "library.hump.vector"
local class = require 'library.middleclass'
local bresenham = require 'library.bresenham'

CActor = class('CActor')

-- Animation frames
gAnimationSet = {}
gAnimationSet["Chest"] 				= {2, "Chest01", "Chest02", "Chest03"}
gAnimationSet["SpecialChest"] = {2, "SpecialChest01", "SpecialChest02", "SpecialChest03"}
gAnimationSet["Skull"] 				= {2, "Skull"}

gAnimationSet["Key"] 					= {2, "Key1", "Key2", "Key3"}
gAnimationSet["Gold"] 				= {2, "Gold1", "Gold2", "Gold3", "Gold4", "Gold5"}

gAnimationSet["PickupFade"] 	= {-2, "PickupFade1", "PickupFade2", "PickupFade3", "PickupFade4"}
gAnimationSet["Flame"] 				= {1, "Flame0", 		"Flame1"}
gAnimationSet["Beam"] 				= {1, "Beam", 		"Beam2"}
gAnimationSet["Bomb"] 				= {1, "Bomb1", 		"Bomb2", "Bomb3", "Bomb4", "Bomb5"}
gAnimationSet["Warp"] 				= {1, "Warp1", 		"Warp3", "Warp3", "Warp4"}

--gAnimationSet["PlayerIdle"] 	= {4, "PlayerIdle0", "PlayerIdle1", "PlayerIdle2", "PlayerIdle3", "PlayerIdle4", "PlayerIdle5", "PlayerIdle6", "PlayerIdle7", "PlayerIdle8"}
gAnimationSet["Grunt"] 				= {2, "Grunt1", "Grunt2", "Grunt3", "Grunt4", "Grunt5", "Grunt6", "Grunt7", "Grunt8"}
gAnimationSet["Beamer"] 			= {2, "Beamer1", "Beamer2", "Beamer3"}

gAnimationSet["EnemyBug"] 		= {3, "EnemyBug1", "EnemyBug2"}
gAnimationSet["EnemyZombie"] 	= {3, "Zombie1", "Zombie2", "Zombie3","Zombie4","Zombie5","Zombie6","Zombie7","Zombie8"}
gAnimationSet["SecurityBot"] 	= {2, "EnemyWizard1", "EnemyWizard2", "EnemyWizard3","EnemyWizard4","EnemyWizard5", "EnemyWizard6"}
gAnimationSet["Ogre"] 				= {2, "Ogre1", "Ogre2", "Ogre3", "Ogre4", "Ogre5", "Ogre6"}


gAnimationSet["EnemyFly"] 		= {1, "EnemyFly1", "EnemyFly2", "EnemyFly3"}
gAnimationSet["EnemyFlea"] 		= {-4, "Flea1", "Flea2"}
gAnimationSet["Swordsman"] 		= {2, "Swordsman1", "Swordsman2", "Swordsman3", "Swordsman4"}
gAnimationSet["Wraith"] 			= {2, "Wraith", "Wraith2", "Wraith3", "Wraith4"}
gAnimationSet["Siren"] 			= {2, "Siren", "Siren2", "Siren3", "Siren4"}

gAnimationSet["Popper"] 			= {1, "Popper1", "Popper2","Popper3"}

gAnimationSet["EnemyFrog"] 		= {-4, "EnemyFrogSit", "EnemyFrog"}
gAnimationSet["Spider"] 			= {4, "Spider", 			"Spider2"}

gAnimationSet["Corner"] 			= {2, "Corner1", "Corner2"}


gAnimationSet["Lightning"] 		= {1, "Lightning1", "Lightning2", "Lightning3"}
gAnimationSet["Genocide"] 		= {1, "Genocide1", "Genocide2", "Genocide3"}

gAnimationSet["Bullet"] 			= {2, "EnemyBullet1", "EnemyBullet2"}
gAnimationSet["Bouncer"] 			= {2, "Bouncer1", 		"Bouncer2"}
gAnimationSet["Homer"] 				= {1, "Homer1", 		"Homer2","Homer3","Homer4","Homer5"}

gAnimationSet["InfoTile"] 		= {1, "InfoTile", 		"InfoTile2","InfoTile3"}

local DEFAULT_MAX_SPEED = 30

CActor.static.nextUniqueID = 1
function CActor:initialize(name, creator)
	self.uniqueID = CActor.static.nextUniqueID
	CActor.static.nextUniqueID = CActor.static.nextUniqueID + 1
	self.name = name
	self.rezInExplosion = nil
	self.creator = creator

	self.deathTime = 1
	self.isChest = false
	
	self.isActive = true
	self.isAlive = true
	self.isVisible = false
	self.faction = FACTION_NULL
	self.isCreature = false
	self.isDying = false
	
	self.isBoss = false
	self.isSecurityBot = false
	
	self.animationName = nil
  self.animation = nil -- Note that all animations assume that every frame in that animation is the same size
  self.position = vector(0,0)
	self.oldPosition = nil
	self.mapX = 0 -- Used to get a pixel perfect position
	self.mapY = 0 -- Used to get a pixel perfect position
  self.drawX = 0
  self.drawY = 0
	self.score = 0
	
  self.h = 1
  self.w = 1
  self.hh = 0.5
  self.hw = 0.5
	self.lifespanInFrames = 0 -- This is in FIXED_STEP units, so 5 / FIXED_STEP = 5 seconds
  self.totalAnimationFrameTime = 0
  self.currentAnimationFrameTimeLeft = 0
	self.loopAnimation = true
  self.animationFrame = 1
  self.numberOfFrames = 1
		
	self.unhittableTimeInFrames = 0	
	
	self.physicsRadius = 0
	self.physicsRadiusSq = 0
	self:setPhysicsRadius(0.45)

	self.hitByThingWithVelocity = vector(0,0)
	
	self.facingAngleInRads = 0--math.rad(90)
	self.drawAngleInRads = 0
		
	self.dieOnThingCollision = false
	
	self.explosionType = nil
	
	self.currentRoomID = 0
	self.oldRoomID = 0
	self:setQuad("PlayerBox8x8")
	
	self.performActionsOnDeath = {}
end

function CActor:setRezInExplosion(r)
	-- Also starts the effect
	if self.rezInExplosion ~= nil then
		for i, v in ipairs(gExplosionArray) do
			if v == self.rezInExplosion then
				table.remove(gExplosionArray, i)
				break
			end
		end
	end
	local ex = gExplosionDefinitions[r]
	self.rezInExplosion = addExplosion(self.position, ex[1], ex[2], ex[3], ex[4], ex[5], ex[6], vector(0,0), false, 0) -- ALWAYS make this a rez-in effect
end

-----------------------
function CActor:getNameForDeathMessage()
	if self.name == "Player" then return "carelessness" end
	if self.creator ~= nil and self.creator.name ~= nil and self.creator.name ~= "" then 
		if self.creator.name == "Player" then return "carelessness" end
		return self.creator.name 
	end
	return self.name
end

-----------------------
function CActor:setSpriteDimensions(quadname)
  w, h = spriteAtlas:getDimensions(quadname)    
	self.w = w / gTileSize
  self.h = h / gTileSize
	
  self.hw = math.floor((self.w / 2) * gTileSize) / gTileSize
  self.hh = math.floor((self.h / 2) * gTileSize) / gTileSize
end

-----------------------
function CActor:setQuad(q)
	self.quadName = q
	if q ~= nil then
		self:setSpriteDimensions(q)
	end
end

----------------------
function CActor:setPhysicsRadius(rng)
	self.physicsRadius = rng
	self.physicsRadiusSq = self.physicsRadius * self.physicsRadius
end

-----------------------
function CActor:setAnimation(a)
	self.animationName = a
  if a ~= nil and gAnimationSet[a] ~= nil then
		if self.animation ~= gAnimationSet[a] then
			self.animation = gAnimationSet[a]
			
			local t = self.animation[1]
			if t < 0 then 
				t = -t
				self.loopAnimation = false
			end
			self.totalAnimationFrameTime = t
			self.currentAnimationFrameTimeLeft = self.totalAnimationFrameTime
			self.numberOfFrames = #self.animation - 1 -- One of the data elements is the counter for the animation
			self:setQuad(self.animation[2]) -- Set us to the first frame of the animation
		end
	else
		self.animation = nil
		self:setQuad(a)
		self.numberOfFrames = 1
		self.animationFrame = 1
	end
end
-----------------------
local TINY_MOVEMENT = 0.001
function CActor:isStill()
	if self.oldPosition == nil then return false end
	if math.abs(self.oldPosition.x - self.position.x) < TINY_MOVEMENT and  math.abs(self.oldPosition.y - self.position.y) < TINY_MOVEMENT then return true end
	return false
end

-----------------------
function CActor:updatePhysics(dt)
	self.oldPosition = self.position:clone()

  if self.attachedToActor == nil then	
		self.mapX = math.floor(self.position.x)
		self.mapY = math.floor(self.position.y)

    self.drawX = math.floor(self.position.x * gTileSize) / gTileSize
    self.drawY = math.floor(self.position.y * gTileSize) / gTileSize
		
		local cell = getMapCell(self.mapX, self.mapY)
		if cell.roomID ~= 0 then
			self.currentRoomID = cell.roomID
			if getRoomFromID(self.currentRoomID).isMapped or gXRayTime > 0 then self.isVisible = true else self.isVisible = false end
		end
  end
end

----------------------------
function CActor:getIsPositionLegal(pos)
	local tlx = math.floor(pos.x - self.physicsRadius)
	local tly = math.floor(pos.y - self.physicsRadius)
	local brx = math.floor(pos.x + self.physicsRadius)
	local bry = math.floor(pos.y + self.physicsRadius)

	if isPositionWithinMapBounds(tlx,tly) == false then return false end
	if isPositionWithinMapBounds(brx,bry) == false then return false end
	
	local array_of_points = {{tlx, tly}, {brx, tly}, {tlx, bry}, {brx,bry}}
	for i, v in ipairs(array_of_points) do
		if getMapCell(v[1], v[2]).isSolid then
			return false
		end
	end
	return true
end

--------
function CActor:fixedUpdate()
	if self.isAlive == false then
		return false
	end
	
	if self.unhittableTimeInFrames > 0 then self.unhittableTimeInFrames = self.unhittableTimeInFrames - 1 end
	
	-- Deal with dying things
	
	if self.lifespanInFrames < 0 then self.lifespanInFrames = 2 end -- Things given -ve life die instantly on the NEXT update
	
  if self.lifespanInFrames > 0 then
    self.lifespanInFrames = self.lifespanInFrames - 1
		
		if self.lifespanInFrames == 0 then 
			self.isAlive = false; 
			self.isDying = false; 
			
			if #self.performActionsOnDeath > 0 then
				for i, v in ipairs(self.performActionsOnDeath) do
--					print("Stuff to do on death... " .. v)
					local name = nil
					if self.creator ~= nil then name = self.creator.name else name = self.name end
					addActionToStack({v, self, self.target, name})
				end
			end
			
			return false; 
		end
  end
 
	-- Animate
  if self.animation ~= nil then		
    self.currentAnimationFrameTimeLeft = self.currentAnimationFrameTimeLeft - 1
    if self.currentAnimationFrameTimeLeft <= 0 then

      self.currentAnimationFrameTimeLeft = self.totalAnimationFrameTime
      self.animationFrame = self.animationFrame + 1
      if self.animationFrame > self.numberOfFrames then 
				if self.loopAnimation then
					self.animationFrame = 1 
				else
					self.animationFrame = self.animationFrame - 1
					self.quadName = self.animation[self.animationFrame + 1] -- Again - that first data element is the number of frames to wait between animations			
					self.animation = nil
					return -- And leave this function
				end
			end
			
      self.quadName = self.animation[self.animationFrame + 1] -- Again - that first data element is the number of frames to wait between animations			
		end
  end
end

-------------------------
function CActor:isTouching(actor)
	if self == actor then return false end
	
	if self.casterToIgnoreUntilOutOfHisRange == actor then
		return
	end
		
	if actor.physicsRadius == 0 or self.isDying == true or self.isAlive == false then
		return false
	elseif self.lineEnd == nil then
		local v = actor.position - self.position
		local dist = v:len()
		if dist < (self.physicsRadius + actor.physicsRadius) then 
			if self.requiresLOSToTouch and not bresenham.los(math.floor(self.mapX), math.floor(self.mapY), math.floor(actor.mapX), math.floor(actor.mapY), isWalkable) then
				return false
			end
			return true 
		end
		return false
	else
		local p = getClosestPointToLine(self.position, self.lineEnd, actor.position)
		
		local v = actor.position - p
		local dist = v:len()
		if dist < (self.physicsRadius + actor.physicsRadius) then 
			return true 
		end
		return false
	end
end

-------------------------
function CActor:kill()
	if self.isDying or self.isAlive == false then print("KILLING A DEAD THING!") return end
	
	self.lifespanInFrames = self.deathTime
	self.isDying = true
	
	if self.explosionType ~= nil then
		local ex = gExplosionDefinitions[self.explosionType]
		addExplosion(self.position, ex[1], ex[2], ex[3], ex[4], ex[5], self.hitByThingWithVelocity, ex[6], ex[7], ex[8])
	end
	
	if self.prizeSetOnDeath ~= nil then
    if gPlayer ~= nil then
      local vec = self.position - gPlayer.position
      vec:normalize_inplace()            
      createPickupsFromRewardSetNearLocation(self.prizeSetOnDeath, self.numberOfPrizes,self.position, vec)
    end
	end
	
	if self.isCreature and self.isChest == false then
		gScore = gScore + math.floor(self.score * math.floor(gScoreMultiplier * 10) / 10)
		gScoreMultiplier = gScoreMultiplier * 1.2
		gScoreMultiplier = math.min(5, gScoreMultiplier)
	end

	if self.isBoss == true then
		gKilledBoss = true
--		createPickupsFromRewardSetNearLocation("Key", 1, self.position)
	end

	if self.isSecurityBot == true then
		gSecurityBotCount = gSecurityBotCount - 1
	end
	
	if self.dieFunction then self.dieFunction(self) end
end

---------------------------
function CActor:diePop()	
  local mx = 1
  local my = 1
  if math.random() > 0.5 then mx = -1 end
  if math.random() > 0.5 then my = -1 end
  
	local number_of_flies = 5
	if self.currentRoomID == 0 then return end
	
	addActionToStack({"JumpLand", self, nil})
	
	local new_pos = self.position:clone()
	
	local index = 1
	for j = 1, 5 do
		local not_found = true
		while not_found and index < gSpiralTableSize do
      local v = gSpiralTable[index] 
      new_pos.x = self.mapX + v[1] * mx
      new_pos.y = self.mapY + v[2] * my
      if isPositionWithinMapBounds(new_pos.x, new_pos.y) then
        local cell = getMapCell(new_pos.x, new_pos.y)
        if cell.roomID == self.currentRoomID and cell.isSolid == false then
          not_found = false
        end
      end
      index = index + 1
    end
		
    if not_found == false then
      new_pos.x = new_pos.x + 0.5
      new_pos.y = new_pos.y + 0.5
      local a = createEnemyWithName("Fly",  new_pos)
    end
	end
end

