local class 		= require 'library.middleclass'
local bresenham = require 'library.bresenham'

local UNHITTABLE_TIME_AFTER_DAMAGE = 30
local ENEMY_UNHITTABLE_TIME_AFTER_DAMAGE = 10
local Y_SHOT_DELAY_TIME = 0.15

CActorMobile = class('CActorMobile', CActor)

function CActorMobile:initialize(name, creator)
  CActor.initialize(self, name, creator) -- initialize the parent    	
	self.randomValue = math.random()
	
  self.bounceFrac = 0.5
	self.maxSpeed = 15
	self.faceMovementDirection = false
	self.flipHorizontalForMovement = false
	self.totalTimeBetweenActions = 5
	self.currentTimeSinceLastAction = math.random(self.totalTimeBetweenActions)
	self.attachedToActor = nil
	self.initialOffsetFromActor = nil
	self.offsetFromActor = nil
	self.angleOffsetFromParent = 0
	self.angularRotationAroundParent = 0
	self.shieldLevel = 0
	self.maxShieldLevel = 0
	self.shieldRegainTimer = 0
	self.maxShieldRegainTimer = 5
	self.isInLineOfSight = false
	self.target = nil
	self.targetPosition = nil
	self.casterToIgnoreUntilOutOfHisRange = nil
	self.spellCooldown = {}
	self.turncoatTimeLeft = 0
	self.ultraswordTimeLeft = 0
	self.isWarp = false
	self.damage = 1
	self.isSwordSlash = false
	self.lineEnd = nil -- Used for non-bounding box collision
	self.isDangerousToTouch = false 
	self.resizeToReachTarget = false
	self.drag = DRAG

	------- AI stuff
	self.goalRoomID = 0
	self.goalPosition = nil
	self.state = ""
	self.pendingState = ""
	self.distanceFromGoalSq = 0
	self.brainTime = 0
	--------------------
	self.isPlayer = false
	
	-- For enemies objects
	self.enemyType = nil
	self.prizeSetOnDeath = nil -- What do you drop on death

	-- For spell-bullets
	self.ignoresWalls = false
	self.invisibleTimeLeft = 0
	self.voidTimeLeft = 0

	self.requiresLOSToTouch = false
	
	self.jumpTime = 0
	self.yShotTime = 0
	self.yShotDelayTime = 0
	
	self.freezeTimeLeft = 0
	self.negateTimeLeft = 0

  self.velocity = vector(0,0)
  self.acceleration = vector(0,0)
  self.lastAcceleration = vector(0,0)
	self.placementFunction = nil
	self.movementFunction = nil
	self.castingFunction = nil
	self.spellProbabilityList = nil
	self.collisionFunction = CActorMobile.basicCollision
	self.contactWithActorFunction = nil
	
end


-----------------------
function CActorMobile:setAcceleration(vec)
  self.acceleration = vec;
end
-----------------------
function CActorMobile:attachToActor(actor)
  if actor == self then return end
  
  self.attachedToActor = actor
	
	if actor ~= nil then
		self.initialOffsetFromActor = self.position - actor.position; -- Now use position as an offset
		self.offsetFromActor = self.initialOffsetFromActor:clone()
	end
end
-----------------------
function CActorMobile:setMaxShieldLevel(s)
	if s ~= nil then
		self.shieldLevel = s
		self.maxShieldLevel = s
	else
		self.shieldLevel = 0
	end
	
	if self.isPlayer and self.shieldLevel == 0 then
		gameState:showShieldWarning()  
		playSound("ShieldsLow") 
		gShieldsLowSoundTimer = MAX_SHIELDS_LOW_SOUND_TIME
	end
end
-----------------------
function CActorMobile:damageShield(thing_name, damage)
	if damage == nil then damage = 1 end
	
	if self.invisibleTimeLeft > 0 then 
		self.invisibleTimeLeft = 0
	end
	
	if self.shieldLevel < damage then 
		self.shieldLevel = 0
		self:kill() -- and kill the player
	end
	
	if self.isPlayer then 
		gScoreMultiplier = 1

		gChromaticAbberation = MAX_CHROMATIC_ABBERATION_AMOUNT 
		startScreenShakeTimeAndPower(0.5, 0.2)
		if self.shieldLevel >= 1 then
			playSound("PlayerHit")
		end
		if self.shieldLevel == 1 then
			gameState:showShieldWarning()
			playSound("ShieldsLow") 
			gShieldsLowSoundTimer = MAX_SHIELDS_LOW_SOUND_TIME
		end
	end
	
	if self.shieldLevel >= damage then 
		if self.isPlayer then
			self.unhittableTimeInFrames = UNHITTABLE_TIME_AFTER_DAMAGE
		else
			self.unhittableTimeInFrames = ENEMY_UNHITTABLE_TIME_AFTER_DAMAGE
		end
		self.shieldLevel = self.shieldLevel - damage
		
		if self.isPlayer then gDefaultShieldLevel = self.shieldLevel else playSound("EnemyHit") end -- Record this for when we go down a level!
		
		return true
	elseif self.isPlayer then
		playSound("PlayerDie")
		if thing_name ~= nil then
			gDeathMessage = string.upper(thing_name)
		end
		gGameOverTimer = GAME_OVER_TIME
		return false
	else
		playSound("EnemyDie")
		return false
	end
end

-------------------------
function CActorMobile:updatePhysics(dt)
	self.oldRoomID = self.currentRoomID
	if self.isActive == false then return end
	if self.rezInExplosion ~= nil then return end
	
	self.oldPosition = self.position:clone()

	if self.target ~= nil and (self.target.isAlive == false or self.target.isDying) then self.target = nil end
	
  if self.attachedToActor == nil then
		self:updateVelocity(dt)
		if self.collisionFunction ~= nil then
			self.collisionFunction(self, dt)
		end
		
		self.mapX = math.floor(self.position.x)
		self.mapY = math.floor(self.position.y)

		if math.abs(self.acceleration.x) > 0001 then self.lastAcceleration.x = self.acceleration.x end
		if math.abs(self.acceleration.y) > 0001 then self.lastAcceleration.y = self.acceleration.y end
	
    -- and clear so that there's an impulse next time
    self.acceleration.x = 0;
    self.acceleration.y = 0;
		
    local cell = getMapCell(self.mapX, self.mapY)
    
    if cell.roomID ~= 0 then
      self.currentRoomID = cell.roomID
      local room = getRoomFromID(self.currentRoomID)
      if (self.isChest and room.isMapped) or room.isVisible or gXRayTime > 0 or self.lineOfSight then 
        self.isVisible = true 
      else 
        self.isVisible = false 
      end
    end
 
		self.drawX = math.floor(self.position.x * gTileSize) / gTileSize
		self.drawY = math.floor(self.position.y * gTileSize) / gTileSize		
	end
end

--------------------------------
function CActorMobile:updateAttachedActor(dt)
  if self.attachedToActor ~= nil then
		if self.angleOffsetFromParent ~= nil then
			if self.angularRotationAroundParent ~= 0 then
				self.angleOffsetFromParent = self.angleOffsetFromParent + self.angularRotationAroundParent * dt
			end
			-- If something was created at an angle from the player, we 
			local vec = self.initialOffsetFromActor:clone() -- 
			vec:rotate_inplace(self.attachedToActor.facingAngleInRads + self.angleOffsetFromParent)		
			self.offsetFromActor = vec:clone()
			if self.faceMovementDirection == true then
				self.drawAngleInRads = self.attachedToActor.facingAngleInRads + self.angleOffsetFromParent
			end
		else
			self.offsetFromActor:rotate_inplace(self.angularRotationAroundParent * dt)		
			if self.faceMovementDirection == true then
				self.drawAngleInRads = self.offsetFromActor:angleTo() + math.rad(90)
			end
		end
		
		if self.resizeToReachTarget then
			self.drawAngleInRads = self.position:angleTo(self.targetPosition) - math.rad(90)
		end
		
		self.position = self.attachedToActor.position:clone()
		self.position = self.position + self.offsetFromActor
		
		self.mapX = math.floor(self.position.x)
		self.mapY = math.floor(self.position.y)
		
		self.currentRoomID = self.attachedToActor.currentRoomID
		self.isVisible = self.attachedToActor.isVisible
		self.invisibleTimeLeft = self.attachedToActor.invisibleTimeLeft
		self.drawX = math.floor(self.position.x * gTileSize) / gTileSize
		self.drawY = math.floor(self.position.y * gTileSize) / gTileSize
  end
end


--------------------------------
function CActorMobile:updateVelocity(dt)
	gravity = 0
--		if self.enemyType ~= nil then
--			gravity = gGravity * dt
--		end

	self.velocity.x = self.velocity.x + self.acceleration.x * dt
--    self.velocity.y = self.velocity.y + gravity + self.acceleration.y * dt
	self.velocity.y = self.velocity.y + self.acceleration.y * dt
			
	-- Clamping speed
	local d = self.velocity:len2()
	if d > self.maxSpeed * self.maxSpeed then
	  self.velocity:normalize_inplace()
	  self.velocity = self.velocity * self.maxSpeed
	end
	
	if self.isPlayer then
		if self.velocity.x > 0 and self.acceleration.x <= 0 then
			self.velocity.x = self.velocity.x - self.drag * dt
			self.velocity.x = math.max(0, self.velocity.x)
		elseif self.velocity.x < 0 and self.acceleration.x >= 0 then
			self.velocity.x = self.velocity.x + self.drag * dt
			self.velocity.x = math.min(0, self.velocity.x)
		end

		if self.velocity.y > 0 and self.acceleration.y <= 0 then
			self.velocity.y = self.velocity.y - self.drag * dt
			self.velocity.y = math.max(0, self.velocity.y)
		elseif self.velocity.y < 0 and self.acceleration.y >= 0 then
			self.velocity.y = self.velocity.y + self.drag * dt
			self.velocity.y = math.min(0, self.velocity.y)
		end
	end

	if math.abs(self.velocity.x) < 0.001 then self.velocity.x= 0 end
	if math.abs(self.velocity.y) < 0.001 then self.velocity.y = 0 end
	
	if self.velocity.y ~= 0 or self.velocity.x ~= 0 then
		self.facingAngleInRads = self.velocity:angleTo() + math.rad(90)
		if self.faceMovementDirection then
			self.drawAngleInRads = self.facingAngleInRads
		end
	end

  -- Added to help Lightning face the right direction
  if self.resizeToReachTarget and self.targetPosition ~= nil then
		self.drawAngleInRads = self.position:angleTo(self.targetPosition) - math.rad(90)
  end
end
--------------------------------------------------------------
function CActorMobile:setToBestLegalPosition(end_pos)
	if end_pos == nil then return end
	end_pos.x = getClampInclusive(end_pos.x, 0 + self.physicsRadius, gMapWidth - self.physicsRadius)
	end_pos.y = getClampInclusive(end_pos.y, 0 + self.physicsRadius, gMapHeight - self.physicsRadius)
	local move_vector = end_pos - self.position
	if self:getIsPositionLegal(end_pos) == true then
		self.position = end_pos:clone()
		return
	end

	end_pos = vector(self.position.x, self.position.y + move_vector.y)
	if self:getIsPositionLegal(end_pos) == true then
		self.position = end_pos:clone()
		self.velocity.x = 0
		return
	end

	end_pos = vector(self.position.x+move_vector.x, self.position.y)
	if self:getIsPositionLegal(end_pos) == true then
		self.position = end_pos:clone()
		self.velocity.y = 0
		return
	end
	
	self.velocity = vector(0,0)
end

--------------------------------------------------------------
-- Cheap and cheerful
function CActorMobile:bouncyCollision(dt)
	local move_vector = vector(self.velocity.x * dt, self.velocity.y * dt)
	local end_pos = vector(self.position.x + move_vector.x, self.position.y + move_vector.y)
	if self.jumpTime > 0 then
		end_pos.x = getClampInclusive(end_pos.x, 0 + self.physicsRadius, gMapWidth - self.physicsRadius)
		end_pos.y = getClampInclusive(end_pos.y, 0 + self.physicsRadius, gMapHeight - self.physicsRadius)
		self.position = end_pos:clone()
		return 
	end
	
	if self:getIsPositionLegal(end_pos) == true then
		self.position = end_pos:clone()
		return
	end

	end_pos = vector(self.position.x, self.position.y + move_vector.y)
	if self:getIsPositionLegal(end_pos) == true then
		self.position = end_pos:clone()
		self.velocity.x = -self.velocity.x * self.bounceFrac
		return
	end

	end_pos = vector(self.position.x+move_vector.x, self.position.y)
	if self:getIsPositionLegal(end_pos) == true then
		self.position = end_pos:clone()
		self.velocity.y = -self.velocity.y * self.bounceFrac
		return
	end

	self.velocity = self.velocity * -self.bounceFrac
end

--------------------------------------------------------------
function CActorMobile:basicCollision(dt)
	local move_vector = vector(self.velocity.x * dt, self.velocity.y * dt)
	local end_pos = vector(self.position.x + move_vector.x, self.position.y + move_vector.y)

	if self.jumpTime > 0 then
		end_pos.x = getClampInclusive(end_pos.x, 0 + self.physicsRadius, gMapWidth - self.physicsRadius)
		end_pos.y = getClampInclusive(end_pos.y, 0 + self.physicsRadius, gMapHeight - self.physicsRadius)
		self.position = end_pos:clone()
		return 
	end
	
	if self:getIsPositionLegal(end_pos) == true then
		self.position = end_pos:clone()
		return
	end

	end_pos = vector(self.position.x, self.position.y + move_vector.y)
	if self:getIsPositionLegal(end_pos) == true then
		self.position = end_pos:clone()
		self.velocity.x = 0
		return
	end

	end_pos = vector(self.position.x+move_vector.x, self.position.y)
	if self:getIsPositionLegal(end_pos) == true then
		self.position = end_pos:clone()
		self.velocity.y = 0
		return
	end

	self.velocity = vector(0,0)
end

----------------------------
function CActorMobile:dieOnHitWallsCollision(dt)
	local move_vector = vector(self.velocity.x * dt, self.velocity.y * dt)
	local end_pos = vector(self.position.x + move_vector.x, self.position.y + move_vector.y)
	if self:getIsPositionLegal(end_pos) == false then
		self:kill()
	else
		self.position = end_pos:clone()
	end
end

-----------------
function CActorMobile:endTurncoat()
	self.turncoatTimeLeft = 0
	self.target = nil
	if self.faction == FACTION_PLAYER then self.faction = FACTION_ENEMY 
	elseif self.faction == FACTION_ENEMY then self.faction = FACTION_PLAYER end
	playSound("TurncoatEnd")
end

-----------------
function CActorMobile:teleportTo(pos)
	self.position = pos:clone()
	self.oldPosition = self.position:clone()
end

-----------------
function CActorMobile:fixedUpdate(fs)
	self.brainTime = self.brainTime + 1
	if self.turncoatTimeLeft > 0 then 
		self.turncoatTimeLeft = self.turncoatTimeLeft - FIXED_STEP
		if self.turncoatTimeLeft <= 0 then
			self:endTurncoat()
		end
	end
	
	if self.ultraswordTimeLeft > 0 then
		self.ultraswordTimeLeft = self.ultraswordTimeLeft - FIXED_STEP
		if self.ultraswordTimeLeft < 0 then self.ultraswordTimeLeft = 0 end
	end
	
	
	if self.freezeTimeLeft > 0 then 
		self.freezeTimeLeft = self.freezeTimeLeft - FIXED_STEP
		if self.freezeTimeLeft < 0 then self.freezeTimeLeft = 0 end
	end
	
	if self.negateTimeLeft > 0 then 
		self.negateTimeLeft = self.negateTimeLeft - FIXED_STEP
		if self.negateTimeLeft < 0 then self.negateTimeLeft = 0 end
	end
	
	if self.invisibleTimeLeft > 0 then 
		self.invisibleTimeLeft = self.invisibleTimeLeft - FIXED_STEP
		if self.invisibleTimeLeft <= 0 then 
			self.invisibleTimeLeft = 0
		end
	end
	
	if self.voidTimeLeft > 0 then 
		self.voidTimeLeft = self.voidTimeLeft - FIXED_STEP
		if self.voidTimeLeft <= 0 then 
			self.voidTimeLeft = 0
		end
	end
	
	if self.yShotTime > 0 then
		self.yShotTime = math.max(self.yShotTime - FIXED_STEP, 0)
		self.yShotDelayTime = self.yShotDelayTime - FIXED_STEP
		if self.yShotDelayTime <= 0 then
			self.yShotDelayTime = Y_SHOT_DELAY_TIME
			addActionToStack({"yShots", self, self.target, name})
		end
	end
	
	
	if self.jumpTime > 0 then 
		self.jumpTime = self.jumpTime - FIXED_STEP
		
		if self.jumpTime - FIXED_STEP <= 0 then addActionToStack({"JumpLand", self, nil})  end -- Make it create a bomb when you are about to land, not when you actually land
		if self.jumpTime <= 0 then
			if self:getIsPositionLegal(self.position) == false then	self:damageShield("clumsiness", 9) end
			self.jumpTime = 0
		end
	end	
	
	for k,v in pairs(self.spellCooldown) do
		if v > 0 then 
			v = v - FIXED_STEP 
			if v < 0 then v = 0 end
				self.spellCooldown[k] = v
		end
	end
	
	if gPlayer ~= nil and gPlayer.rezInExplosion == nil and self.rezInExplosion == nil then
		self:chooseNewTarget()
		self:updateLineOfSightToPlayer()
	end
	
	if CActor.fixedUpdate(self) == false then return false end
	
	if gPlayer ~= nil and gPlayer.rezInExplosion == nil and self.rezInExplosion == nil then
		if self.isCreature == false or self.freezeTimeLeft == 0 then
			if self.target ~= nil and self.target.invisibleTimeLeft == 0 and self.resizeToReachTarget ~= true then 
        self.targetPosition = self.target.position:clone() 
      end
      
			if self.unhittableTimeInFrames > math.floor(ENEMY_UNHITTABLE_TIME_AFTER_DAMAGE * 0.6) then 
        -- Stagger enemies
			else
				if self.movementFunction ~= nil then self.movementFunction(self) end
			end
		else
			self.velocity.x = 0; self.velocity.y = 0
		end
    
		-- Make enemies cast with relation to their target's position
		if self.faction == FACTION_PLAYER and self.target == gPlayer then 
			-- Do nothing!
		elseif self.castingFunction ~= nil and self.negateTimeLeft == 0 then 
			self.castingFunction(self) 
		end
	end
	
	self.target = nil -- Force the target to nullify every turn?
	
  return true
end

----------------
function CActorMobile:chooseNewTarget()
	if self.target == nil and self.isChest == false and self.isCreature then 
		if self.faction == FACTION_PLAYER then
			local v = self:chooseTarget()
			if v ~= nil then
				self.target = v
			else
				self.target = gPlayer
			end
		elseif gPlayer.invisibleTimeLeft == 0 then
			self.target = gPlayer
		end
	end
end
----------------
function CActorMobile:updateLineOfSightToPlayer()
	if self.isPlayer then self.isInLineOfSight = true return end
	--- Check LOS for creatures
	if self.attachedToActor == nil then
		if bresenham.los(math.floor(self.position.x), math.floor(self.position.y), math.floor(gPlayer.position.x), math.floor(gPlayer.position.y), isWalkable) then
			self.isInLineOfSight = true
			if self.isChest == false and self.isCreature then
				gFreezeUntilEnemySpotted = false
			end
		else
			self.isInLineOfSight = false
		end
	end

end	
----------------
function CActorMobile:chooseTarget()	
	local target = nil
	local smallest_dist_sq = 99999999
	for i, v in pairs(gCreatureArray) do
		local d_sq = v.position:dist2(self.position)
		if v.isChest == false and v.invisibleTimeLeft == 0 and d_sq < smallest_dist_sq and v.faction ~= self.faction and v ~= self and v.isInLineOfSight then 
			target = v 
			smallest_dist_sq = d_sq
		end
	end
	return target
end

-----------------
function CActorMobile:resetShield()
	self.shieldLevel = self.maxShieldLevel
	if self.isPlayer and self.shieldLevel > 0 then gShieldsLowSoundTimer = 0 end
end

-----------------
function CActorMobile:increaseShield()
	if self.shieldLevel < self.maxShieldLevel then 
		self.shieldLevel = self.shieldLevel + 1 
--	elseif (self.isPlayer and self.maxShieldLevel < gMaxShieldLevel) or (self.maxShieldLevel < MAX_POSSIBLE_SHIELD) then 
--		self.maxShieldLevel = self.maxShieldLevel + 1 self.shieldLevel = self.maxShieldLevel 
	end
	
	if self.isPlayer and self.shieldLevel > 0 then gShieldsLowSoundTimer = 0; gameState:removeShieldWarning() end
end
----------------
-- Placement Types
----------------
function CActorMobile:plCorner()
	local find = true
	local iter = 0
	while find == true do
		find = false
		iter = iter + 1
		local room_id = getMapCell(math.floor(self.position.x), math.floor(self.position.y)).roomID
		if room_id == 0 then return end
		local position = getNearestCornerOfRoomWithIDFromPosition(room_id, self.position)
		-- Check no other Corner creatures are here
		for i, v in ipairs(gCreatureArray) do
			if v ~= self and v.position:dist2(self.position) <= 1 then
				find = true
				break
			end
		end
		if find == false or iter == 10 then
			self.position = position
		end
	end
end

----------------
-- Movement Types
----------------
function CActorMobile:getTileGoalPositionCardinal(target_position, target_in_scope)
	local c = getMapCell(self.mapX, self.mapY)
	local dir_positions = {}
	local valids = {}
	for i, v in ipairs(gAround8) do
		if (i % 2) == 0 and c.connections[i] == true then dir_positions[i] = {self.mapX + v[1] + 0.5, self.mapY + v[2] + 0.5} end
		if dir_positions[i] ~= nil then table.insert(valids, dir_positions[i]) end
	end

	local dir = 0
	if target_in_scope then
		local diff = target_position - self.position
		if math.abs(diff.x) > math.abs(diff.y) then
			if diff.x > 0 then dir = 4 else dir = 8 end
		elseif diff.y > 0 then 
			dir = 6 else dir = 2
		end
	end
	
	local valid = nil

	if dir == 0 or dir_positions[dir] == nil then
		valid = getRandomMember(valids)
	else
		valid = dir_positions[dir]
	end

	if valid ~= nil then return vector(valid[1], valid[2]) end
	
	return nil
end

---------------------------
function getNodeDifferenceFromTo(from, to)
	local diff = vector(0,0)
	diff.x = math.floor(to.x / NODE_SIZE) - math.floor(from.x / NODE_SIZE)
	diff.y = math.floor(to.y / NODE_SIZE) - math.floor(from.y / NODE_SIZE)
	return diff
end
---------------------------
function CActorMobile:getNodeGoalPositionCardinal(target_position, target_in_scope)
	local dir_positions = {}
	local valids = {}

	local npos = vector(snapXYToNodeCellCenterXY(self.mapX, self.mapY))
	local x, y = math.floor(self.mapX / NODE_SIZE), math.floor(self.mapY / NODE_SIZE)
	for i, v in ipairs(gAround8) do
		if (i % 2) == 0 and getNodeConnectionsXY(x,y)[i] == true then dir_positions[i] = {npos.x + v[1] * NODE_SIZE, npos.y + v[2] * NODE_SIZE} end
		if dir_positions[i]  ~= nil then table.insert(valids, dir_positions[i]) end
	end

	local dir = 0
	if target_in_scope then
		local diff = getNodeDifferenceFromTo(self.position, target_position)
		
--		if diff.x == 0 and diff.y == 0 then return target_position end -- Uncomment and the unit will head toward the player if in the same space, but will also occasionally slide against walls
		if math.abs(diff.x) > math.abs(diff.y) then
			if diff.x > 0 then dir = 4 else dir = 8 end
		elseif diff.y > 0 then dir = 6 else dir = 2 end
	end
	
	local valid = dir_positions[dir] -- Did moving in a cardinal direction work?
	if valid ~= nil then 
		self.wallHugDirection = nil
	else -- Okay, even moving in a cardinal direction didn't work, we've hit a blockage
		if self.wallHugDirection and dir_positions[self.wallHugDirection] ~= nil then valid = dir_positions[self.wallHugDirection] else self.wallHugDirection = nil end
		
		if self.wallHugDirection == nil then
			local rdir = dir + 2; if rdir > 8 then rdir = rdir - 8 end
			local ldir = dir - 2; if ldir < 1 then ldir = ldir + 8 end
			local ldist = 99999; rdist = 99999
			local lpos = vector(dir_positions[ldir][1], dir_positions[ldir][2]) 
			local rpos = vector(dir_positions[rdir][1], dir_positions[rdir][2]) 

			if lpos then ldist = (target_position - lpos):len2() end
			if rpos then rdist = (target_position - rpos):len2() end
								
			if ldist < rdist then self.wallHugDirection = ldir else self.wallHugDirection = rdir end
		end
		valid = dir_positions[self.wallHugDirection]
	end

	if valid ~= nil then return vector(valid[1], valid[2]) end
	return nil
end
----------------------------------------
function CActorMobile:getNodeGoalPositionAllAngles(target_position, target_in_scope)
	local dir_positions = {}
	local valids = {}

	local npos = vector(snapXYToNodeCellCenterXY(self.mapX, self.mapY))
	local x, y = math.floor(self.mapX / NODE_SIZE), math.floor(self.mapY / NODE_SIZE)
	for i, v in ipairs(gAround8) do
		if getNodeConnectionsXY(x,y)[i] == true then dir_positions[i] = {npos.x + v[1] * NODE_SIZE, npos.y + v[2] * NODE_SIZE} end
		if dir_positions[i] ~= nil then table.insert(valids, dir_positions[i]) end
	end
		
	local dir = 0
	if target_in_scope then
		local diff = getNodeDifferenceFromTo(self.position, target_position)
		
--		if diff.x == 0 and diff.y == 0 then return target_position end -- Uncomment and the unit will head toward the player if in the same space, but will also occasionally slide against walls
		if diff.y > 0 then 
			if diff.x < 0 then dir = 7 elseif diff.x > 0 then dir = 5 else dir = 6 end
		elseif diff.y < 0 then 
			if diff.x < 0 then dir = 1 elseif diff.x > 0 then dir = 3 else dir = 2 end
		else
			if diff.x < 0 then dir = 8 elseif diff.x > 0 then dir = 4 else dir = 0 end
		end
	end
	
	-- At this point, dir is an idealised value, possibly even a diagonal into a solid square
	local valid = nil
	if dir == 0 then
		valid = getRandomMember(valids)
		self.wallHugDirection = nil
	else
		valid = dir_positions[dir]
		if valid ~= nil then -- Yay! We can now go the way we want!
			self.wallHugDirection = nil
		else
			-- Okay - this whole diagonal thing has failed. Move in cardinals only
			local diff = getNodeDifferenceFromTo(self.position, target_position)
			if math.abs(diff.x) > math.abs(diff.y) then
			if diff.x > 0 then dir = 4 else dir = 8 end
			elseif diff.y > 0 then dir = 6 else dir = 2 end
			
			valid = dir_positions[dir] -- Did moving in a cardinal direction work?
			if valid ~= nil then 
				self.wallHugDirection = nil
			else -- Okay, even moving in a cardinal direction didn't work, we've hit a blockage
				if self.wallHugDirection and self.wallHugDirection ~= 0 and dir_positions[self.wallHugDirection] ~= nil then valid = dir_positions[self.wallHugDirection] else self.wallHugDirection = nil end
				
				if self.wallHugDirection == nil and dir ~= 0 then
					local rdir = dir + 2; if rdir > 8 then rdir = rdir - 8 end
					local ldir = dir - 2; if ldir < 1 then ldir = ldir + 8 end
					local ldist = 99999; rdist = 99999
					local lpos = nil
					local rpos = nil
					if dir_positions[ldir] ~= nil then vector(dir_positions[ldir][1], dir_positions[ldir][2]) end
					if dir_positions[rdir] ~= nil then vector(dir_positions[rdir][1], dir_positions[rdir][2]) end

					if lpos then ldist = (target_position - lpos):len2() end
					if rpos then rdist = (target_position - rpos):len2() end
										
					if ldist < rdist then self.wallHugDirection = ldir else self.wallHugDirection = rdir end
				end
				valid = dir_positions[self.wallHugDirection]
			end
		end
	end			

	if valid ~= nil then return vector(valid[1], valid[2]) end
	return nil
end

---------------------------
function CActorMobile:mvHome()
	local v = self:chooseTarget()
	if v ~= nil then
		self.target = v
		self.targetPosition = v.position:clone()
	end
	
	local acc = self.maxSpeed * 5
	if self.target == nil then return end
	
	local t = self.target
		-- Enemy movement
		if self.position.x < t.position.x then 
			self.acceleration.x = acc 
		elseif self.position.x > t.position.x then 
			self.acceleration.x = -acc 
		end 
	
		if self.position.y < t.position.y then 
			self.acceleration.y = acc 
		elseif self.position.y > t.position.y then 
			self.acceleration.y = -acc 
		end
end
----------------
function CActorMobile:mvHomeFast()
	local v = self:chooseTarget()
	if v ~= nil then
		self.target = v
		self.targetPosition = v.position:clone()
	end
	local acc = self.maxSpeed * 40
	if self.target == nil then return end
	local t = self.target
		-- Enemy movement
		if self.position.x < t.position.x then 
			self.acceleration.x = acc 
		elseif self.position.x > t.position.x then 
			self.acceleration.x = -acc 
		end 
	
		if self.position.y < t.position.y then 
			self.acceleration.y = acc 
		elseif self.position.y > t.position.y then 
			self.acceleration.y = -acc 
		end
end

----------------
function CActorMobile:mvHomeDirect()
	local v = self:chooseTarget()
	if v ~= nil then
		self.target = v
		self.targetPosition = v.position:clone()
	end
	
	local f = self.brainTime % self.pauseTime

	if f == 0 then 
		self.brainTime = 0
		local acc = self.maxSpeed * 5
		if self.target == nil then return end
		local t = self.target
		-- Enemy movement
		if self.position.x < t.position.x then 
			self.velocity.x = self.maxSpeed 
		elseif self.position.x > t.position.x then 
			self.velocity.x = -self.maxSpeed 
		end 
	
		if self.position.y < t.position.y then 
			self.velocity.y = self.maxSpeed 
		elseif self.position.y > t.position.y then 
			self.velocity.y = -self.maxSpeed 
		end
	end
end

----------------
function CActorMobile:mvFly()
	local CIRCLE_TIME = 10
	
	local acc = self.maxSpeed * 5
	if self.target == nil or self.targetPosition == nil then return end
	local p = self.targetPosition:clone()
	
	--And now offset !
	local offset = vector(0,2)
	offset:rotate_inplace(((gFixedFrameCount + self.randomValue * 100) / CIRCLE_TIME) * math.rad(360))
	
	p = p + offset
	-- Enemy movement
	if self.position.x < p.x then 
		self.acceleration.x = acc 
	elseif self.position.x > p.x then 
		self.acceleration.x = -acc 
	end 

	if self.position.y < p.y then 
		self.acceleration.y = acc 
	elseif self.position.y > p.y then 
		self.acceleration.y = -acc 
	end
end

--------------------------------------
local jitterSpeedMults = {{1,1},{-1,1},{-1,-1},{1,-1}}
function CActorMobile:mvJitter()
	local JITTER_TIME = self.pauseTime
	
	if self.target == nil or self.targetPosition == nil then return end
	local f = gFixedFrameCount % (JITTER_TIME + math.floor(math.random(JITTER_TIME)))
	
	local dx = self.targetPosition.x - self.position.x
	local dy = self.targetPosition.y - self.position.y
		
	local vx = 0
	local vy = 0
	
	if f == 0 then 
		self.velocity.x = 0
		self.velocity.y = 0

		local jit = jitterSpeedMults[math.random(#jitterSpeedMults)]
		self.velocity.x = jit[1] * self.maxSpeed
		self.velocity.y = jit[2] * self.maxSpeed
	elseif f == 20 then
		local jit = jitterSpeedMults[math.random(#jitterSpeedMults)]
		vx = jit[1] * self.maxSpeed
		vy = jit[2] * self.maxSpeed
		self.velocity.x = 0
		self.velocity.y = 0

		if math.abs(dx) > math.abs(dy) then 
			if self.position.x < self.targetPosition.x then 
				self.velocity.x = self.maxSpeed
			elseif self.position.x > self.targetPosition.x then 
				self.velocity.x = -self.maxSpeed
			end
			
			self.velocity.y = vy
		else
			if self.position.y < self.targetPosition.y then 
				self.velocity.y = self.maxSpeed
			elseif self.position.y > self.targetPosition.y then 
				self.velocity.y = -self.maxSpeed
			end
			
			self.velocity.x = vx
		end
	end
end

	
--------------------------------------
function getNodePositionOnFarSideOfDoorBetween(id1, id2)
	local room = getRoomFromID(id1)
	local door = room:getDoorwayToRoomID(id2)
	return door:getNodePositionOnFarSideOfDoorBasedOnXY(room.cx, room.cy)
end

function getNodePositionOnNearSideOfDoorBetween(id1, id2)
	local room = getRoomFromID(id1)
	local door = room:getDoorwayToRoomID(id2)
	return door:getNodePositionOnNearSideOfDoorBasedOnXY(room.cx, room.cy)
end
--------------------------------------
function CActorMobile:mvIntelligent()
	if self.target == nil or self.targetPosition == nil then return end
	if self.currentRoomID == 0 then return end
	self.drag = 1
	
	local f = self.brainTime % self.pauseTime
	if f == 0 or self.goalPosition == nil then 		
		self.brainTime = 0	
		if self.goalPosition == nil then 					
			if self.currentRoomID == self.target.currentRoomID or self.isInLineOfSight then
				self.goalPosition = self:getNodeGoalPositionAllAngles(self.targetPosition, true) --  self:getNodeGoalPositionCardinal(self.targetPosition, true)  
			else
				local route = getRoomRouteFromTo(self.currentRoomID, self.target.currentRoomID)
				if #route > 1 then -- [1] is the current room 
					if route[1] == self.currentRoomID then
						local target_pos = getNodePositionOnNearSideOfDoorBetween(route[1], route[2])
						local diff = target_pos - self.position
						if math.abs(diff.x) < 0.2 and math.abs(diff.y) < 0.2 then
							target_pos = getNodePositionOnFarSideOfDoorBetween(route[1], route[2])
						end
						self.goalPosition = self:getNodeGoalPositionAllAngles(target_pos, true) -- self:getNodeGoalPositionCardinal(target_pos, true) 
					end
				end
			end
		end
	end
	
	gDebugText = self.state
	
	----- Do the movement
	self:doGoalMovement(0.4)
end

----------------
function CActorMobile:mvTileWalker()
	if self.target == nil or self.targetPosition == nil then return end

	self.drag = 1

	local f = self.brainTime % self.pauseTime
	if f == 0 then --  or self.goalPosition == nil then
 		self.brainTime = 0	
	
		if self.goalPosition == nil then 	
			self.goalPosition = self:getTileGoalPositionCardinal(self.targetPosition, self.isInLineOfSight)
		end
	end
	
		----- Do the movement
	if self.goalPosition ~= nil then
		local vec = self.goalPosition - self.position
		local dist = math.min(vec:len(), self.maxSpeed)
		vec:normalize_inplace() 
		vec = vec * self.maxSpeed		
		self.velocity = self:clampVelocityToStopJittering(vec)
	end

	---- Check the goal -----
	local old_dist_sq = self.distanceFromGoalSq
	if self.goalPosition ~= nil then
		self.distanceFromGoalSq = self.position:dist2(self.goalPosition)
		if self.distanceFromGoalSq < 0.25 then --or self.distanceFromGoalSq >= old_dist_sq then
			self.goalPosition = nil
--			self.velocity:zero()
		end
	end

end
----------------
local lastThinkTime = 0
function CActorMobile:mvTileHopper()
	if self.target == nil or self.targetPosition == nil then return end

	self.drag = 1

	if self.goalPosition ~= nil then self.brainTime = 0 end

	local f = self.brainTime % self.pauseTime
	if f == 0 then
 		self.brainTime = 0	
--		print("Think took: " .. gTotalTime - lastThinkTime)
		lastThinkTime = gTotalTime
		if self.goalPosition == nil then 	
			self.goalPosition = self:getTileGoalPositionCardinal(self.targetPosition, self.isInLineOfSight)
		else
--			print("Didn't reach goal")
		end
	end
	
		----- Do the movement
	if self.goalPosition ~= nil then
		local vec = self.goalPosition - self.position
		local dist = math.min(vec:len(), self.maxSpeed)
		vec:normalize_inplace() 
		vec = vec * self.maxSpeed		
		self.velocity = vec:clone() -- self:clampVelocityToStopJittering(vec)
	end

	---- Check the goal -----
	local old_dist_sq = self.distanceFromGoalSq
	if self.goalPosition ~= nil then
		self.distanceFromGoalSq = self.position:dist2(self.goalPosition)
		if self.distanceFromGoalSq < 0.1 then --or self.distanceFromGoalSq >= old_dist_sq then
			self.position.x = math.floor(self.position.x) + 0.5
			self.position.y = math.floor(self.position.y) + 0.5
			self.goalPosition = nil
			self.velocity:zero()
		end
	end
end
----------------
function CActorMobile:mvNodeWalker()
	if self.target == nil or self.targetPosition == nil then return end

	self.drag = 1
	
	local f = self.brainTime % self.pauseTime
	if f == 0 or self.goalPosition == nil then 		
		self.brainTime = 0	
		if self.goalPosition == nil then 					
			self.goalPosition = self:getNodeGoalPositionAllAngles(self.targetPosition, self.isInLineOfSight) -- self:getNodeGoalPositionCardinal()
		end
	end
	
	self:doGoalMovement(0.4)
end

------------------------------------------------
function CActorMobile:doGoalMovement(success_distance)
	----- Do the movement
	if self.goalPosition ~= nil then
		local vec = self.goalPosition - self.position
		local original_dist = vec:len()
		local dist = math.min(original_dist, self.maxSpeed)
			
		vec:normalize_inplace() 
		vec = vec * self.maxSpeed		
		self.velocity = self:clampVelocityToStopJittering(vec)
	end

	---- Check the goal -----
	local old_dist_sq = self.distanceFromGoalSq
	if self.goalPosition ~= nil then
		self.distanceFromGoalSq = self.position:dist2(self.goalPosition)
		if self.distanceFromGoalSq < success_distance * success_distance then
			self.goalPosition = nil
		elseif self:isStill() then -- If I get stuck, move me toward the centre of the room
			self.goalPosition = nil
		end
	end
end

----------------
-- Casting types
----------------
function CActorMobile:ctBasic()
	if self.target == nil or self.targetPosition == nil then return end

	local FIRING_RANGE_X = 22
	local FIRING_RANGE_Y = 12

	if self.currentTimeSinceLastAction > 0 then
		self.currentTimeSinceLastAction = self.currentTimeSinceLastAction - FIXED_STEP
		if math.random() > 0.8 then self.currentTimeSinceLastAction = self.currentTimeSinceLastAction - FIXED_STEP end
	
		if self.currentTimeSinceLastAction <= 0 then 
			if math.abs(self.position.x - self.targetPosition.x) < FIRING_RANGE_X and  math.abs(self.position.y - self.targetPosition.y) < FIRING_RANGE_Y and (self.faction == FACTION_PLAYER or self.isInLineOfSight) then
				self.currentTimeSinceLastAction = self.currentTimeSinceLastAction + self.totalTimeBetweenActions
				
				if self.spellProbabilityList ~= nil then
					spell = getRandomThingFromProbabilitySet(self.spellProbabilityList)
					if processCooldownForActorAction(self, spell) then
						addActionToStack({spell, self, self.target})
					end				end
			else -- The player's not nearby, so wait a moment
				self.currentTimeSinceLastAction = self.currentTimeSinceLastAction + 1
			end
		end
	end
end
----------------
function CActorMobile:ctBasicPlusSword()
	if self.target == nil or self.targetPosition == nil then return end

	local FIRING_RANGE_X = 22
	local FIRING_RANGE_Y = 12

	if self.unhittableTimeInFrames == 0 then
		local SWORD_RANGE = 1.6
		local dist_sq = (self.position - self.targetPosition):len2()
		if dist_sq < SWORD_RANGE * SWORD_RANGE then
			if processCooldownForActorAction(self, "CreatureCastSlash") == true then
				addActionToStack({"CastSlash", self, self.target})
				return
			end
		end
	end
	
	if #self.spellProbabilityList < 2 then return end
	
	if self.currentTimeSinceLastAction > 0 then
		self.currentTimeSinceLastAction = self.currentTimeSinceLastAction - FIXED_STEP
		if math.random() > 0.8 then self.currentTimeSinceLastAction = self.currentTimeSinceLastAction - FIXED_STEP end
	
		if self.currentTimeSinceLastAction <= 0 then 
			if math.abs(self.position.x - self.targetPosition.x) < FIRING_RANGE_X and  math.abs(self.position.y - self.targetPosition.y) < FIRING_RANGE_Y and (self.faction == FACTION_PLAYER or self.isInLineOfSight) then
				self.currentTimeSinceLastAction = self.currentTimeSinceLastAction + self.totalTimeBetweenActions
				
				if self.spellProbabilityList ~= nil then
					spell = getRandomThingFromProbabilitySet(self.spellProbabilityList)
					if processCooldownForActorAction(self, spell) then
						addActionToStack({spell, self, self.target})
					end				end
			else -- The player's not nearby, so wait a moment
				self.currentTimeSinceLastAction = self.currentTimeSinceLastAction + 1
			end
		end
	end
end
----------------
function CActorMobile:ctBasicPlusCornerBeam()
	if self.target == nil or self.targetPosition == nil then return end

	if processCooldownForActorAction(self, "CornerBeam") == true then
		addActionToStack({"CornerBeam", self, self.target})
	end

	if #self.spellProbabilityList < 2 then return end
	
	if self.currentTimeSinceLastAction > 0 then
		self.currentTimeSinceLastAction = self.currentTimeSinceLastAction - FIXED_STEP
		if math.random() > 0.8 then self.currentTimeSinceLastAction = self.currentTimeSinceLastAction - FIXED_STEP end
	
		if self.currentTimeSinceLastAction <= 0 then 
			if math.abs(self.position.x - self.targetPosition.x) < FIRING_RANGE_X and  math.abs(self.position.y - self.targetPosition.y) < FIRING_RANGE_Y and (self.faction == FACTION_PLAYER or self.isInLineOfSight) then
				self.currentTimeSinceLastAction = self.currentTimeSinceLastAction + self.totalTimeBetweenActions
				
				if self.spellProbabilityList ~= nil then
					spell = getRandomThingFromProbabilitySet(self.spellProbabilityList)
					if processCooldownForActorAction(self, spell) then
						addActionToStack({spell, self, self.target})
					end
				end
			else -- The player's not nearby, so wait a moment
				self.currentTimeSinceLastAction = self.currentTimeSinceLastAction + 1
			end
		end
	end
end

----------------

--- Function to look at the desired movement and temper it accordingly
function CActorMobile:clampVelocityToStopJittering(vec)
	if self.target ~= nil and self.targetPosition == nil and self.target.currentRoomID == self.currentRoomID then
		local room = getRoomFromID(self.currentRoomID)
		local shove = self.position:clone()
		shove.x = getClampInclusive(shove.x, room.x1+1.5, room.x2-1.5)
		shove.y = getClampInclusive(shove.y, room.y1+1.5, room.y2-1.5)
		
		shove = shove - self.position
		vec = vec + shove
	end
	return vec
end



---------------------------
-- Spell effect on hit
---------------------------
function CActorMobile:spSwitchFaction(creature)
	-- NOTE that if we killed the creature here we have to alter the gLastCorpsePosition
	if self.faction == creature.faction then print("BUT WE'RE FRIENDS!") return
	elseif self.faction ~= FACTION_PLAYER then print("AN ENEMY CAST THIS???") end
	
	creature.target = nil
	
	creature.faction = FACTION_PLAYER
--	creature.turncoatTimeLeft = MAX_TURNCOAT_TIME
	playSound("Turncoat")
end

---
function CActorMobile:getAttachedSwordSlash()
	local slash = nil
	for i, v in ipairs(gSpellArray) do
		if v.attachedToActor == self and v.isSwordSlash then return v end
	end
	return nil
end



