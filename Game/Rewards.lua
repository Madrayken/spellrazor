require 'game.CRoom'

DOOR_COLOUR = 0

-- Rewards with a zero are guaranteed, all others are part of the 'ball pool' of randomness
gProbabilitySet = {}

-- The first boolean is 'Do rewards taken reduce the size of the list?'        
-- Go through all the physical items and create a finite list of them
gProbabilitySet["PickupsLevel1"]   	= {false, {"A", 20}, {"B", 15}, {"S", 10}, {"X", 10}}
gProbabilitySet["PickupsLevel2"]   	= {false, {"A", 15}, {"B", 15}, {"D", 5}, {"H", 10}, {"X", 10}, {"I", 3}, {"K", 3}, {"F", 2}, {"N", 2}}
gProbabilitySet["PickupsLevel3"]   	= {false, {"A", 15}, {"B", 15}, {"D", 5}, {"H", 10}, {"X", 10}, {"I", 3}, {"K", 3}, {"F", 2}, {"N", 2}, {"L", 2}}

gProbabilitySet["PRZT"]  						= {false, {"A", 30}}

gProbabilitySet["PRZ0"]  						= {false, {"A", 30}, {"B", 30}, 					{"H", 10}}
gProbabilitySet["PRZ1"]  						= {false, {"A", 20}, {"B", 30}, {"C", 5}, {"H", 10}, {"J", 10}, {"X", 10}}
gProbabilitySet["PRZ2"]  						= {false, {"A", 20}, {"B", 50}, {"C", 15}, {"H", 15}, {"J", 10}, {"X", 10}, {"D", 10}}

gProbabilitySet["BOSS1"]  					= {false, {"S", 20}, {"Z", 2}, {"K", 2}}

gProbabilitySet["TUTCHEST"]  			  = {false, {"A", 20}}
gProbabilitySet["STARTCHEST"]  			= {false, {"A", 20}, {"E", 5}, {"H", 5}, {"S", 5}, {"T", 5}, {"M", 5}, {"Q", 5}}
gProbabilitySet["CHESTPRZ"]  				= {false, {"C", 5},  {"D", 5}, {"E", 7}, {"F", 5},  {"G", 4}, {"I", 5}, {"J", 7}, {"K", 5}, {"L", 5}, {"M", 7}, {"N", 5}, {"O", 4}, {"Q", 5}, {"S", 7}, {"T", 8}, {"U", 4}, {"V", 5}}
gProbabilitySet["SPECIALCHESTPRZ"]  = {false, {"S", 10}, {"L", 10}, {"Y", 10}, {"Z", 10}}

gProbabilitySet["TREASUREROOM"]  		= {true, {"G", 4}, {"L", 5}, {"O", 5}, {"P", 4}, {"Q", 5}, {"R", 5}, {"U", 5}, {"V", 5}, {"Y", 5}, {"W", 5}}

gProbabilitySet["SHOPPRZ"]  				= {true, {"G", 4}, {"L", 5}, {"P", 4}, {"Q", 5}, {"R", 5}, {"V", 5}, {"Y", 5}, {"Z", 5}, {"SHIELDUP", 5}}
gProbabilitySet["Key"]  						= {false, {"Key", 1}}

----------------------
function convertLetterToPickupValue(letter)
	if letter == "SHIELDUP" then return 27 end
	return (string.byte(letter) - 64)
end

----------------------
function createPickupsFromRewardSet(reward_set, count)
  return createPickupsFromRewardSetNearLocation(reward_set, count, nil)
end
----------------------
function createPickupsFromRewardSetNearLocation(reward_set, count, pos, ideal_direction)
  local ret = false
  -- Allows us to flip the spiral table around a bit
  local mx = 1
  local my = 1
  if math.random() > 0.5 then mx = -1 end
  if math.random() > 0.5 then my = -1 end

  local spiral_index = 1
  for i = 1, count do
		local a = CActorPickup("Pickup")
		a.pickupType = getRandomThingFromProbabilitySet(gProbabilitySet[reward_set])
		if a.pickupType == nil then return false end
		if a.pickupType == "Key" then
--			a:setQuad("Key1")
			a:setAnimation("Key")
		else
			local quad = "Pickup" .. (string.byte(a.pickupType) - 64)
			a:setQuad(quad) -- A special 'set quad' thing that records the initial quad setup
			a.baseQuad = quad
		end
--		a.explosionType = "Pickup"
    if a.pickupType ~= nil then
      if pos == nil then
				local rx = math.random(gMapWidth-1)
				local ry = math.random(gMapHeight-1)
				local AVOID_SPECIAL_ROOMS = true
        a.position = findValidThingPositionInAnyRoomNear(vector(rx,ry), AVOID_SPECIAL_ROOMS)
				if a ~= nil then
					if getMapCell(math.floor(a.position.x), math.floor(a.position.y)).isSolid == true then
						error("Pickup without initial pos produced in illegal position: " .. a.position.x .. ", " .. a.position.y)		
						a = nil
					end
				end
      else
        local cell = getMapCell(math.floor(pos.x), math.floor(pos.y))
        a.position.x = pos.x
        a.position.y = pos.y
        -- Look ahead a bit if necessary
        if ideal_direction ~= nil then 
          local new_pos = pos + ideal_direction 
          new_pos.x = math.floor(new_pos.x)
          new_pos.y = math.floor(new_pos.y)
          if isPositionWithinMapBounds(new_pos.x, new_pos.y) and getMapCell(new_pos.x, new_pos.y).isSolid == false then
            a.position.x = new_pos.x -- Great - we'll move these drops a little bit away from the player
            a.position.y = new_pos.y
          end
        end

        if cell.roomID ~= 0 then
          local room = getRoomFromID(cell.roomID)
          if room ~= nil then
            a.position.x = math.floor(a.position.x)
            a.position.y = math.floor(a.position.y)
            
            local new_pos = vector(0,0)
            local not_found = true
            while not_found and spiral_index < gSpiralTableSize do
              local v = gSpiralTable[spiral_index] 
              new_pos.x = a.position.x + v[1] * mx
              new_pos.y = a.position.y + v[2] * my
              if isPositionWithinMapBounds(math.floor(new_pos.x), math.floor(new_pos.y)) then
                local cell2 = getMapCell(new_pos.x, new_pos.y)
                if cell2.roomID == cell.roomID and cell2.isSolid == false then
                  not_found = false
                end
              end
              spiral_index = spiral_index + 1
            end
            
            if not_found == false then
              a.position.x = new_pos.x + 0.5
              a.position.y = new_pos.y + 0.5
            else
              print("Could not find room!")
              return false
            end
--            a.position = findValidThingPositionInSpecificRoomNear(room, a.position)
	--					print("Finding position in specific room")
          else
            print("Could not find room!")
            return false
          end
				else
	--				print("No Room - finding new one")
					a.position = findValidThingPositionInAnyRoomNear(a.position)					
				end
				
				if a ~= nil then
					if getMapCell(math.floor(a.position.x), math.floor(a.position.y)).isSolid == true then
						error("Pickup with initial pos produced in illegal position: " .. a.position.x .. ", " .. a.position.y)		
						a = nil
					end
				end
      end
			
			if a ~= nil then
				table.insert(gPickupArray, a)
				ret = true
			end
    else
      print("Pickup illegal")
    end
  end
  
	return ret
end

----------------------
function createPickupFromRewardSetAtSpecificLocation(reward_set, pos)
	local cell = getMapCell(math.floor(pos.x), math.floor(pos.y))
	if cell.isSolid == false and cell.roomID ~= 0 then
		local a = CActorPickup("Pickup")
		a.pickupType = getRandomThingFromProbabilitySet(reward_set)
		if a.pickupType == nil then return false end
		if a.pickupType == "Key" then
			a:setAnimation("Key")
		else
			local quad = "Pickup" .. (string.byte(a.pickupType) - 64)
			a:setQuad(quad) -- A special 'set quad' thing that records the initial quad setup
			a.baseQuad = quad
		end
		a.position = pos		
		table.insert(gPickupArray, a)
	end
	return true
end

----------------------------------------------------------------------------------------------------------------------------
function testRewards()
	local rs = deepcopy(gProbabilitySet["TREASUREROOM"])
	for i = 1, 100 do
		print(getRandomThingFromProbabilitySet(rs))
	end
end
