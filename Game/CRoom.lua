require 'library/Tools'
require 'game/CDoorway'
vector = require 'library/hump.vector'
local class = require 'library/middleclass'


WALL_COLOUR = 10
CURRENTLY_VISIBLE_FLOOR_COLOUR = 3
CURRENTLY_OUT_OF_SIGHT_FLOOR_COLOUR = 4

CRoom = class('CRoom')

function CRoom:initialize(room_id, x1, y1, x2, y2)
  -- This rectangle is inclusive. The point at x2, y2 will be the bottom-right corner of the wall!
  self.roomID = room_id
  self.isMapped = false
  self.isVisible = false
  self.oldIsVisible = false
  self.x1 = x1
  self.y1 = y1
  self.x2 = x2 
  self.y2 = y2
  self.cx = math.floor((x1 + x2) / 2)
  self.cy = math.floor((y1 + y2) / 2)
  self.w = self.x2 - self.x1
  self.h = self.y2 - self.y1
  self.roomType = nil
  
  self.zapTimer = 0
    
  --print("Room init: " .. self.x1 .. ", ".. self.y1 .. ", " .. self.x2 .. ", " .. self.y2)
  -- Ensure the cells know what room this is
  for y = self.y1, self.y2 do
    for x = self.x1, self.x2 do
      setMapCellSolidity(x,y, true)
    end
  end
  
  -- clear the floor's solidity
  for y = self.y1+1, self.y2-1 do
    for x = self.x1+1, self.x2-1 do
      setMapCellRoomID(x, y, room_id)
      setMapCellSolidity(x,y, false)
   --   print(x .. ", " .. y)
    end
  end
  
  self.neighbourTable = {}
  self.numberOfNeighbours = 0
  
  self.doorArray = {}
end

-------------------------
function CRoom:addNeighbour(r)
  if self.neighbourTable[r.roomID] ~= true then
    self.neighbourTable[r.roomID] = true
    self.numberOfNeighbours = self.numberOfNeighbours + 1
  end
end

function CRoom:isNeighbourOfRoomWithID(id)
  if self.neighbourTable[id] == true then return true end
  return false
end


function CRoom:isNeighbourOfRoom(r)
  return self:isNeighbourOfRoomWithID(r.roomID)
end
-------------------------
function CRoom:getCenter()
  return self.cx, self.cy
end

-------------------------
function CRoom:zap(zapper)
  self.zapTimer = 10
  for i, v in ipairs(gCreatureArray) do
    if v.currentRoomID == self.roomID and v.faction ~= zapper.faction then
      if v:damageShield(nil, 9) == false then v:kill() end
    end
  end
end

-------------------------
function CRoom:fixedUpdate()
  if self.oldIsVisible ~= self.isVisible then 
    self.oldIsVisible = self.isVisible
    self:draw() 
  end
  
  if self.zapTimer > 0 then
    self.zapTimer = self.zapTimer - 1
  
    local col = CURRENTLY_VISIBLE_FLOOR_COLOUR

    if gPlayer == nil or self.isVisible == false then
      col = CURRENTLY_OUT_OF_SIGHT_FLOOR_COLOUR
    end
    
 --   local tile = 1
    if self.zapTimer % 2 ~= 0 then
      col = 0
 --     tile = 17
    end
    
    for y = self.y1+1, self.y2-1 do
      for x = self.x1+1, self.x2-1 do
        local c = getMapCell(x,y)
        if c.isSolid == false then
          c.colourIndex = col
        end
      end
    end
  end
end

-------------------------
function CRoom:draw()
  for y = self.y1, self.y2 do
    for x = self.x1, self.x2 do
      setMapTileAndColourBasedOnSolidityAndSeenStatus(x,y, self.isVisible)
    end
  end
end

-------------------------
function CRoom:map()
  if self.isMapped == false then
    self.hasChangedVisibility = true
    for y = self.y1, self.y2 do
      for x = self.x1, self.x2 do
        setMapCellMapped(x,y, true)
      end
    end  
      
    self.isMapped = true
    return true
  end
  return false
end
-------------------------
function CRoom:addDoorway(d)
  table.insert(self.doorArray, d)
end


-------------------------
function CRoom:isAdjacent(room)
  if room.roomID == self.roomID then return false end -- NOT ME!
  if room.x2 == self.x1-1 
  or room.x1 == self.x2+1 
  or room.y2 == self.y1-1 
  or room.y1 == self.y2+1 then return 1 end
  return false
end

-------------------------
function CRoom:getRandomValidNodeCenterPositionWithinRoom()
  -- we use 2 as the step-in value because it's better
  local x = math.random(self.x1+2, self.x2-2)
  local y = math.random(self.y1+2, self.y2-2)
  
  x,y = snapXYToNodeCellXY(x,y) -- gets us a top-left position in a node
  x = x + math.floor(NODE_SIZE / 2)
  y = y + math.floor(NODE_SIZE / 2)
  
  if getMapCell(x, y).isSolid == false then
    return vector(x + 0.5,y + 0.5) -- Yup - we're somewhere safe in this room
  else
    local new_pos = vector(0,0)
    for i, v in ipairs(gSpiralTable) do
      new_pos.x = x + v[1] * NODE_SIZE
      new_pos.y = y + v[2] * NODE_SIZE
      
      if new_pos.x > self.x1 and new_pos.x < self.x2 and new_pos.y > self.y1 and new_pos.y < self.y2 then
        local cell = getMapCell(new_pos.x, new_pos.y)
        if cell.isSolid == false and cell.isMapFeature == false then 
          new_pos.x = new_pos.x + 0.5
          new_pos.y = new_pos.y + 0.5
          return new_pos 
        end
      end
    end
  end
  return nil
end

-------------------------
function CRoom:getRandomValidThingPositionWithinRoom()
  -- we use 2 as the step-in value because it's better
  local x = math.random(self.x1+2, self.x2-2)
  local y = math.random(self.y1+2, self.y2-2)
  
  local pos = self:getNearestValidMapCellPositionToPosition(vector(x,y))
  pos.x = pos.x + 0.5
  pos.y = pos.y + 0.5
  return pos
end
-------------------------
function CRoom:overlaps(x1, y1, x2, y2)
  if x2 <= self.x1-1 
  or x1 >= self.x2+1 
  or y2 <= self.y1-1 
  or y1 >= self.y2+1 then return false end
  return true
end

----------------
function CRoom:getNearestValidMapCellPositionToPosition(room_pos)
  -- Are we inside this room?
  local pos = room_pos:clone()
  pos.x = math.floor(pos.x)
  pos.y = math.floor(pos.y)

  if pos.x > self.x1 and pos.x < self.x2 and pos.y > self.y1 and pos.y < self.y2 then
    if getMapCell(pos.x, pos.y).isSolid == false then
 --     print("spiralling position")
      return pos -- Yup - we're somewhere safe in this room
    else
 --     print("spiralling position")
      return self:spiralOutToFindValidPointWithinRoom(pos) -- We ended up somewhere dodgy. Spiral out to find somewhere safe
    end
  end
  
  -- Okay, check which corner we're nearest
  pos.x = getClampInclusive(pos.x, self.x1+1, self.x2-1)
  pos.y = getClampInclusive(pos.y, self.y1+1, self.y2-1)
  pos.x = math.floor(pos.x)
  pos.y = math.floor(pos.y)
  return pos
end
----------------
function CRoom:getNearestValidNodeCenterPositionToPosition(room_pos)
  -- Are we inside this room?
  local pos = room_pos:clone()
  pos.x = math.floor(pos.x)
  pos.y = math.floor(pos.y)

  if pos.x > self.x1 and pos.x < self.x2 and pos.y > self.y1 and pos.y < self.y2 then
    if getMapCell(pos.x, pos.y).isSolid == false then
 --     print("spiralling position")
      return pos -- Yup - we're somewhere safe in this room
    else
 --     print("spiralling position")
      return self:spiralOutToFindValidPointWithinRoom(pos) -- We ended up somewhere dodgy. Spiral out to find somewhere safe
    end
  end
  
  -- Okay, check which corner we're nearest
  print("clamped position")
  pos.x = getClampInclusive(pos.x, self.x1+1, self.x2-1)
  pos.y = getClampInclusive(pos.y, self.y1+1, self.y2-1)
  pos.x = math.floor(pos.x)
  pos.y = math.floor(pos.y)
  return pos
end
----------------
function CRoom:clampPositionToRoom(room_pos)
  -- Are we inside this room?
  local pos = room_pos:clone()
  pos.x = math.floor(pos.x)
  pos.y = math.floor(pos.y)
  if pos.x > self.x1 and pos.x < self.x2 and pos.y > self.y1 and pos.y < self.y2 then
    return pos -- Yup - we're somewhere safe in this room
  end
  
  -- Okay, check which corner we're nearest
  pos.x = getClampInclusive(pos.x, self.x1+1, self.x2-1)
  pos.y = getClampInclusive(pos.y, self.y1+1, self.y2-1)
  return pos
end

------------------------------------
function CRoom:spiralOutToFindValidPointWithinRoom(pos)
  local new_pos = pos:clone()
  local x = math.floor(pos.x)
  local y = math.floor(pos.y)
  for i, v in ipairs(gSpiralTable) do
    new_pos.x = x + v[1]
    new_pos.y = y + v[2]
    if new_pos.x > self.x1 and new_pos.x < self.x2 and new_pos.y > self.y1 and new_pos.y < self.y2 then
      local cell = getMapCell(new_pos.x, new_pos.y)
      if cell.roomID == self.roomID and cell.isSolid == false then
        return new_pos 
      end
    end
  end
  
  return nil
end

function CRoom:getRandomNeighbourID()
  local n = math.random(self.numberOfNeighbours)
  for k, v in pairs(self.neighbourTable) do
    n = n - 1 
    if n == 0 then return k end
  end
  
  return 0
end

function CRoom:getRandomNeighbourCenter()
  local id = self:getRandomNeighbourID()
  if id ~= 0 then 
    local r = getRoomFromID(id) 
    return vector(r.cx, t.cy)
  end
end

function CRoom:getNearestDoorWithinRange(pos, range)
  local dist_sq = 99999999
  local r_sq = range * range
  local nearest = nil
  for i, v in ipairs(self.doorArray) do
    local vec = vector(v.cx, v.cy)
    local d2 = pos:dist2(vec)
    if d2 < dist_sq and d2 < r_sq then
      dist_sq = d2
      nearest = v
    end
  end
  return nearest
end

function CRoom:getDoorwayToRoomID(id2)
  for i, v in ipairs(self.doorArray) do
    if (v.roomID1 == self.roomID and v.roomID2 == id2) or (v.roomID1 == id2 and v.roomID2 == self.roomID) then
      return v
    end
  end
  return nil
end

MIN_WORLD_MESSAGE_SIZE = 3
function CRoom:getUnbrokenWallList() -- Return a list of {x1, y1, size, horizontal}
  local array = {}
  if self.x2 - self.x1 > MIN_WORLD_MESSAGE_SIZE then
    local unbroken = true
    for x = self.x1 + 1, self.x2-1 do
      c = getMapCell(x,self.y1)
      if c.isSolid == false or c.isDoor then unbroken = false break end
    end
    if unbroken then table.insert(array, {(self.x1 + self.x2) / 2, self.y1, self.x2 - self.x1 - 2, true}) end
  end
  
  if self.y2 - self.y1 > MIN_WORLD_MESSAGE_SIZE then
    local unbroken = true
    for y = self.y1 + 1, self.y2-1 do
      c = getMapCell(self.x1, y)
      if c.isSolid == false or c.isDoor then unbroken = false break end
    end
    if unbroken then table.insert(array, {self.x1, (self.y1 + self.y2) / 2, self.y2 - self.y1 - 2, false}) end
  end
  
  return array
end

function CRoom:lockDoorsWithPassword(password)
  for i, v in ipairs(self.doorArray) do
    v:lockWithPassword(password)
  end
end

function CRoom:unlockDoorsWithPassword(password)
  local result = false
  for i, v in ipairs(self.doorArray) do
    if v:unlockWithPassword(password) == true then result = true end
  end
  return result
end

