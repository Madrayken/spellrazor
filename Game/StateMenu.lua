menuState = {}
-------------------------------	
-- The MENU state
-------------------------------
local NUMBER_OF_SEED_LETTERS = 8
local PRESS_START_TEXT_Y = 0
local MAX_HIGH_SCORE_TIMER = 20
local HIGH_SCORE_START_TIME = math.floor(MAX_HIGH_SCORE_TIMER * 1.5)

local gTitleImage = nil
local menuStateTimeInSeconds = 0
--local gGridTextYSize = 12

local gSeedCodeLetters = {"","","","","","","",""}
local gCursor = 1
local gLineHeight = 14

local gTrailerShowGridOnly = false
local gTrailerShowTitleOnly = false

function menuState:enter()
	gFrontEndMusic = nil
	gFrontEndMusic = love.audio.newSource("assets/SpellrazorMusic.mp3", 'stream')
	gFrontEndMusic:setLooping(true)
	gFrontEndMusic:play()

	PRESS_START_TEXT_Y = math.floor(gCanvasHeight * 0.92)

	menuStateTimeInSeconds = 0
  if gJustFinishedGame == true then 
    menuStateTimeInSeconds = HIGH_SCORE_START_TIME 
    gJustFinishedGame = false
  end
	
  gTitleImage = love.graphics.newImage("assets/Title.png")

	if gShaderGridColour ~= nil then
		gShaderGridColour:send("offsetX", 0)
		gShaderGridColour:send("offsetY", 8)
		gShaderGridColour:send("gridSizeX", 8)
		gShaderGridColour:send("gridSizeY", gLineHeight)
	end
end
----------------------------------
local menuBodyTextColour = {255,255,255,255}
function menuState:update(dt)
	menuStateTimeInSeconds = menuStateTimeInSeconds + dt
	local t = menuStateTimeInSeconds * 100
	local r1, g1, b1 = HSL(((500 + 	t) % 1000) / 1000 * 255, 	255, 255)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local r2, g2, b2 = HSL(((200 + 	t) % 1000) / 1000 * 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local r3, g3, b3 = HSL(((0 + 	t) % 1000) / 1000 * 255, 	255, 32)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)

	menuBodyTextColour = {r2/256 * 255, g2/256*255, b2/256*255,255}
	if gShaderTitleRecolour ~= nil then
		gShaderTitleRecolour:send("topColour", 		{r1 / 256, g1 / 256, b1 / 256, 1})
		gShaderTitleRecolour:send("midColour", 		{r2 / 256, g2 / 256, b2 / 256, 1})
		gShaderTitleRecolour:send("bottomColour", {r3 / 256, g3 / 256, b3 / 256, 1})
		gShaderTitleRecolour:send("scroll", ((menuStateTimeInSeconds * 50) % 50) / 50)
	end
  
	t = menuStateTimeInSeconds * 100
  
	r1, g1, b1 = HSL(((200 + 	t) % 1000) / 1000 * 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	r2, g2, b2 = HSL(((150 + 	t) % 1000) / 1000 * 255, 	255, 100)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	r3, g3, b3 = HSL(((100 + 	t) % 1000) / 1000 * 255, 	255, 86)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)

	if gShaderGridColour ~= nil then
		gShaderGridColour:send("topColour", 		{r1 / 256, g1 / 256, b1 / 256, 1})
		gShaderGridColour:send("midColour", 		{r2 / 256, g2 / 256, b2 / 256, 1})
		gShaderGridColour:send("bottomColour", {r3 / 256, g3 / 256, b3 / 256, 1})
		gShaderGridColour:send("scroll", menuStateTimeInSeconds)
	end
end
-------------------------
function menuState:draw()
	
	gColourAdd = 0
	gShaderZoom = 0
	
	love.graphics.setColor(255,255,255,255)
	love.graphics.setFont(beebTileFont)
	love.graphics.setBackgroundColor(0, 0, 0)
	
	local t = gTime * 100
	local r2, g2, b2 = HSL((((200 + 	t) % 1000) / 1000 * 255 + 160) % 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local linecolour = {r2/256 * 255, g2/256*255, b2/256*255,255}

	local lo = 0.2
	local mid = 0.3
	local hi = 0.4
	
	if gTrailerShowTitleOnly == false or gTrailerShowGridOnly == true then 
		drawTronBackground({linecolour[1] * lo, linecolour[2] * lo, linecolour[3] * lo, 255}, gTime, 0.6, not gTrailerShowGridOnly)
	end
	
	-------------------------------
	if gTrailerShowGridOnly and gTrailerShowTitleOnly == false then return end
	-------------------------------
	if gTrailerShowTitleOnly == false then
		love.graphics.setColor(128,256,256,255) 
		love.graphics.print("'Like' us on @Fluttermind, www.spellrazor.com or www.facebook.com/Spellrazor", 6,2)
		love.graphics.print("v" .. VERSION_NUMBER, 2, gCanvasHeight-10)
	end
	
	local offset_y = 0

	if gShaderLandscapeRecolour ~= nil then
		gShaderLandscapeRecolour:send("focalPos", {(gCanvasWidth / 2), offset_y + (gCanvasHeight * 0.35) + 25 * math.sin(math.rad((gDrawCount / 2) % 360))})
	end
	love.graphics.setShader(gShaderTitleRecolour)		
	love.graphics.draw(gTitleImage, math.floor(gCanvasWidth * 0.5), math.floor(offset_y + math.ceil(gCanvasHeight * 0.15)), 0, 1,1,gTitleImage:getWidth() / 2, gTitleImage:getHeight() / 2 )
	love.graphics.setShader()
	
	if gTrailerShowTitleOnly == true then
		return
	end
	
	self:drawSeed()
  local menu_type_time = math.floor(menuStateTimeInSeconds) % (MAX_HIGH_SCORE_TIMER * 2)
	if #gHighScores > 0 and menu_type_time >= HIGH_SCORE_START_TIME then 
		self:drawHighScores()
	else
		self:drawMenu()
	end
	
	-- Draw a lump at the bottom and right of the screen to hide the weird, ugly 'canvas isn't the size you think it is' issue!
	--love.graphics.setColor(0,0,0,255) 
	--love.graphics.rectangle("fill", gCanvasWidth, 0, 80, gCanvasHeight)
	--love.graphics.rectangle("fill", 0, gCanvasHeight, gCanvasWidth, 80)
	love.graphics.setColor(255,255,255, 255)
end
-----------------------------
function menuState:drawMenu()
	local wid = 1000
	local h_wid = math.floor(wid / 2)
	local h_canv_wid = math.floor(gCanvasWidth / 2)
--		local ind = gRainbowSetWithWhite[(gFixedFrameCount % #gRainbowSetWithWhite) + 1]
	local y = math.floor((gCanvasHeight / 2) - 90)
  
  local rectwidth = gCanvasWidth * 0.7
  love.graphics.setColor(0,0,0,190)
  love.graphics.rectangle("fill", gCanvasWidth/2 - rectwidth / 2, y-8, rectwidth, gCanvasHeight * 0.62)
  
  
	love.graphics.setColor(gColourList[WHITE_INDEX + 1])
	love.graphics.printf("(c)2015 Fluttermind Ltd. (original 1981)", -h_wid + h_canv_wid, y, wid, 'center')
	y = y + gLineHeight * 1.5
	local x = math.floor((gCanvasWidth / 3) - 24)
	love.graphics.setColor(menuBodyTextColour)
	love.graphics.printf("    The infinite electric dungeon awaits." , -h_wid + h_canv_wid, y, wid, 'center')
	y = y + gLineHeight
	love.graphics.printf("    Find spells, kill monsters, use the exit portal." , -h_wid + h_canv_wid, y, wid, 'center')
	y = y + gLineHeight * 2
	love.graphics.print("    - CURSORS/ARROWS to Move", 						x, y)
	y = y + gLineHeight	
  love.graphics.print("    - A to Z keys for Spells", 	x, y)
	y = y + gLineHeight	
	love.graphics.print("    - SPACE for Sword", 				x, y)
  y = y + gLineHeight	
  love.graphics.print("    - RETURN to Open Console", 			x, y)	
	y = y + gLineHeight
  love.graphics.print("    - BACKSPACE for Pause", 				x, y)
  y = y + gLineHeight	
	love.graphics.print("    - TAB for Fullscreen", 				x, y)
  y = y + gLineHeight	
	love.graphics.print("    - CTRL to Cycle SLOWMO Type", x, y)
  y = y + gLineHeight	
	love.graphics.print("    - ESC to Quit", 								x, y)

	y = y + gLineHeight	* 1.5
	love.graphics.print("Hint 1: Spells auto-target the nearest enemy!    ", 	x- 60, y)
  y = y + gLineHeight	* 1
	love.graphics.print("Hint 2: Use the console! Buy shields/shieldup!   ", 	x - 60, y)

	love.graphics.setColor(255,255,255, 255)
	love.graphics.printf("Press Cursors to see the high scores", math.floor(-h_wid + h_canv_wid), PRESS_START_TEXT_Y - 16, wid, 'center')
  love.graphics.printf("Press Space/Return/Enter to start", math.floor(-h_wid + h_canv_wid), PRESS_START_TEXT_Y, wid, 'center')
end
-----------------------------
function menuState:drawHighScores()
	local wid = 1000
	local h_wid = math.floor(wid / 2)
	local h_canv_wid = math.floor(gCanvasWidth / 2)

	local y = math.floor(gCanvasHeight / 2) - 102

  local rectwidth = gCanvasWidth * 0.8
  love.graphics.setColor(0,0,0,190)
  love.graphics.rectangle("fill", gCanvasWidth/2 - rectwidth / 2, y+2, rectwidth, gCanvasHeight * 0.68)

	love.graphics.setColor(gColourList[WHITE_INDEX + 1])

	love.graphics.printf("High Scores", -h_wid + h_canv_wid, y + 8, wid, 'center')
  y = y + gLineHeight * 1

	local x = h_canv_wid - (36 * 8)

	love.graphics.setShader(gShaderGridColour)
	for i, v in ipairs(gHighScores) do
		local str = "" 
		if gLastInsertedScore ~= 0 and i == gLastInsertedScore then
			str = str .. "      > "
		else
			str = str .. "        "
		end
		str = str .. string.format("%2d - %9d", i, v[2]) .. " - " .. v[1]
		
		y = y + gLineHeight
		love.graphics.print(str, x, y)
		str = string.format("- KILLED ON %-9s BY %s", v[4], v[3])
		love.graphics.print(str, x + (28 * 8), y)
		
	end
	
	love.graphics.setShader()
	love.graphics.setColor(255,255,255, 255)

	y = y + gLineHeight * 2
	love.graphics.printf("Press Space/Return/Enter to start", -h_wid + h_canv_wid, PRESS_START_TEXT_Y, wid, 'center')
end
------------------------------
function menuState:drawSeed()
	local wid = 1000

	love.graphics.setColor(0,0,255, 255)
	local str = ""
	for i = 1, NUMBER_OF_SEED_LETTERS do
		str = str .. gSeedCodeLetters[i]
	end
	
	if gSeedCodeLetters[1] == "" then
		str = "Seed: Random (Type A-Z/0-9)" 
	else
		str = "Seed: " .. str
	end
	love.graphics.print(str, 250, gCanvasHeight - gLineHeight + 4)
  
  
  if math.floor(gTime * 5) % 2 == 1 then
    local last_letter = gCursor
    love.graphics.setColor(128,128,255, 255)
    str = "      "
    for i = 1, NUMBER_OF_SEED_LETTERS do
      if i ~= gCursor then
        str = str .. " " 
      elseif gSeedCodeLetters[i] ~= "" then 
        str = str .. gSeedCodeLetters[i] 
      else
        str = str .. "_"
      end
    end
    love.graphics.print(str, 250, gCanvasHeight - gLineHeight + 4)
  end
end

------------------------------
function findLastSeedLetterPosition()
  for i = 1, NUMBER_OF_SEED_LETTERS do
    if gSeedCodeLetters[i] == "" then 
      return i
    end
  end
  
  return NUMBER_OF_SEED_LETTERS
end
------------------------------
function menuState:keypressed(key)
	if key == "escape" then saveData(); love.event.push("quit") return end
	
  if key == "left" or key == "right" or key == "up" or key == "down" or key == "kp4" or key == "kp6" or key == "kp2"or key == "kp8" then
    if math.floor(menuStateTimeInSeconds) % (MAX_HIGH_SCORE_TIMER * 2) < HIGH_SCORE_START_TIME then 
      menuStateTimeInSeconds = HIGH_SCORE_START_TIME 
    end
  end
  
	if key == "'" then gTrailerShowGridOnly = not gTrailerShowGridOnly end
	if key == ";" then gTrailerShowTitleOnly = not gTrailerShowTitleOnly end
	
	if key == "backspace" then

    for i = gCursor, NUMBER_OF_SEED_LETTERS-1 do
      gSeedCodeLetters[i] = gSeedCodeLetters[i+1]
    end
    gSeedCodeLetters[NUMBER_OF_SEED_LETTERS] = ""

    
    gCursor = math.min(gCursor, findLastSeedLetterPosition()-1)

--			gSeedCodeLetters[gCursor] = ""
--		else
	--		gSeedCodeLetters = {"","","","","","","",""}
--		end
		playSound("Typing")
	end
	
	if key == "space" or key == "return" or key == "enter" or key == "kpenter" then
    self:createStartMenu()
	end
	
	if string.len(key) == 1 then
		local byte = string.byte(key)	
		if (byte >= 48 and byte <= 57) or (byte >= 97 and byte <= 122) then
			gSeedCodeLetters[gCursor] = string.upper(string.char(byte))
			gCursor = gCursor + 1
			playSound("Typing")
		end

    gCursor = math.min(gCursor, findLastSeedLetterPosition())

--		if gCursor > NUMBER_OF_SEED_LETTERS then
--			gCursor = 1
--		end
	end
end	

function menuState:createStartMenu()
    CWindowDialog("Start", gDialogArray, "Select game type:", {
      {"TUTORIAL", nil, menuState.startGameFromMenuTutorial, "Play the tutorial world\nwith NORMAL Slowmo style."}, 
      {"EASY",   nil, menuState.startGameWithSlowmoAlwaysOn,   "Tactical play style\n--------------------\nGo into 'Slowmo' any time\nthere are no keypresses"}, 
      {"NORMAL", nil, menuState.startGameWithSlowmoDangerOnly, "Balanced play style\n--------------------\nGo into 'Slowmo' only when\ndanger is near and there\nare no keypresses"}, 
      {"INSANE", nil, menuState.startGameWithSlowmoAlwaysOff,  "Arcade play style\n--------------------\nNo 'Slowmo' regardless of how\nconfusing things get!"}, 
      {"CANCEL", nil, nil}
  },   gDefaultGameTypeIndex)
end

function menuState:startGameWithSlowmoAlwaysOn(index)
  gForcedSlowmo = true
  gSlowmo = true
  gDefaultGameTypeIndex = index
  menuState:chooseSeed()
end

function menuState:startGameWithSlowmoDangerOnly(index)
  gForcedSlowmo = false
  gSlowmo = true
  gDefaultGameTypeIndex = index
  menuState:chooseSeed()
end

function menuState:startGameWithSlowmoAlwaysOff(index)
  gForcedSlowmo = false
  gSlowmo = false
  gDefaultGameTypeIndex = index
  menuState:chooseSeed()
end

function menuState:chooseSeed()
  CWindowDialog("Seed", gDialogArray, "Choose World:", {
        {"FIXED",           nil, menuState.startGameFromMenuFixed,    "Play a world based on the SEED.\nYou can change this by typing\nletters/numbers on the title screen."}, 
        {"RANDOM",          nil, menuState.startGameFromMenuRandom,   "Play a random world"}, 
        {"DAILY CHALLENGE", nil, menuState.startGameFromMenuDaily,   "Play a random world"}, 
        {"CANCEL", nil, menuState.createStartMenu}
    },   gDefaultSeedTypeIndex)
end

TUTORIAL_SEED_CODE = "tutorial1"
function menuState:startGameFromMenuTutorial()
  gForcedSlowmo = false
  gSlowmo = true
  gSeedCode = TUTORIAL_SEED_CODE -- tutorial1/9/12
  menuState:startGameFromMenu()
end

function menuState:startGameFromMenuFixed(index)
  gSeedCode = ""
--  gCursor = 1
  for i = 1, NUMBER_OF_SEED_LETTERS do
		if gSeedCodeLetters[i] ~= "" then
      gSeedCode = gSeedCode .. gSeedCodeLetters[i]
--      gSeedCodeLetters[i] = ""
		end
  end
  gCursor = 1
  if gSeedCode == "" then gSeedCode = "fixed" end
  gDefaultSeedTypeIndex = index
  menuState:startGameFromMenu()
end


function menuState:startGameFromMenuRandom(index)
  menuState:createRandomSeed()
  gDefaultSeedTypeIndex = index
  menuState:startGameFromMenu()
end

function menuState:startGameFromMenuDaily(index)
  gCursor = 1  
  local date_table = os.date("*t")
  local wrap = fromBaseN("ZZZZZZZZ", 36)
  local value = (999999000000 + date_table.year * 10000077714 + date_table.month * 320990131 + 70071301317 * date_table.day) % wrap
  gSeedCode = toBaseN(value,36)
--  gSeedCode = gSeedCode .. date_table.year .. date_table.month .. date_table.day
--  gSeedCode = getAlphanumericPasswordOfLength(8)
  for i = 1, NUMBER_OF_SEED_LETTERS do
    gSeedCodeLetters[i] = gSeedCode:sub(i,i)
  end
  
  gDefaultSeedTypeIndex = index
  menuState:startGameFromMenu()
end

function menuState:startGameFromMenu()
  gCursor = 1
  GameState.switch(gameState)
  gFrontEndMusic:stop()
  stopAllSound()
end


function menuState:createRandomSeed()
  gCursor = 1  
  gSeedCode = getAlphanumericPasswordOfLength(8)
  for i = 1, NUMBER_OF_SEED_LETTERS do
    gSeedCodeLetters[i] = gSeedCode:sub(i,i)
  end
end

