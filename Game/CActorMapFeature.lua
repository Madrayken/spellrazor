local class = require 'library.middleclass'

CActorMapFeature = class('CActorMapFeature', CActor)

function CActorMapFeature:initialize(name)
  CActor.initialize(self, name)

	self.mapFeatureType = name
	self.data = nil -- This holds information like the spell type to buy
	self.interactionRadius = 0.75
	self.interactionRadiusSq = self.interactionRadius * self.interactionRadius
--	self:setQuad("Pickup1")
	self.basicQuad = "Pickup1"
	self.activatedQuad = "Pickup1"
	self:setQuad("InfoTile")
end

-----------------------
function CActorMapFeature:setInteractionRadius(r)
	self.interactionRadius = r
	self.interactionRadiusSq = r * r
end
-----------------------
function CActorMapFeature:setBasicAndActivatedQuads(q1, q2)
	self.basicQuad = q1
	self.activatedQuad = q2
	self:setQuad(q1)
end
-----------------------
function CActorMapFeature:isWithinInteractionRangeOf(actor)		
  if self.interactionRadius == 0 then
		if mapFeatureType ~= "InfoTile" then 
			self:setQuad(self.basicQuad)
		end
		return false
	else
		if self.position:dist2(actor.position) < self.interactionRadiusSq then 
			if self.animation == nil and mapFeatureType ~= "InfoTile" then 
				self:setQuad(self.activatedQuad)
			end
			return true 
		end
		if self.animation == nil and mapFeatureType ~= "InfoTile" then 
			self:setQuad(self.basicQuad)
		end
		return false
	end
end

