require 'game.Map'
require 'game.Rewards'
MAX_POSSIBLE_SHIELD = 4
FIXED_ACTOR_PUSH_RADIUS = 0.7 -- Since physicsRadius is used for nav, it has to be < 0.5, as a result we fake a larger body size for other interactions


-----------------------------Anim------------DmgOnTch-Movement---------------------PlaceFn------DieFn--Spells--Prize-------------#przes----MvSpd--PausTme--ActSpd-Shield---------------
--explosion-------RezIn--------------faceMv---flipMv--score
local enemyDefs = {}
enemyDefs["TutChest"]  	  = {"Chest",  			false, 	nil,                  			nil, 					nil, nil,   "TUTCHEST",	    5,				0,     0,				0,     0,       						"ChestExplosion", 	nil,						false,	false,	0}
enemyDefs["StartChest"]  	= {"Chest",  			false, 	nil,                  			nil, 					nil, nil,   "STARTCHEST",	    5,				0,     0,				0,     0,       						"ChestExplosion", 	nil,						false,	false,	0}
enemyDefs["Chest"]  			= {"Chest",   		  false, 	nil,                  			nil,					nil, nil,   "CHESTPRZ",		  3,				0,     0,				0,     0,       						"ChestExplosion", 	nil,						false,	false,	0}
enemyDefs["SpecialChest"] = {"SpecialChest",  false, 	nil,                  			nil,					nil, nil,   "SPECIALCHESTPRZ",3,		    0,     0,				0,     0,       						"ChestExplosion", 	nil,						false,	false,	0}

enemyDefs["Bug"]  				= {"EnemyBug", 		true, 	CActorMobile.mvFly,  				nil,					nil, nil,   "PRZ0",  			  2,				4,     0,				0,     0,       						"GruntExplosion", 	nil,						true,		false,	200}
enemyDefs["Frog"]  			= {"EnemyFrog", 	  true, 	  CActorMobile.mvTileHopper,	nil,					nil, nil,   "PRZ0",  			  2,				7,     2,			  0,     0,       						"GruntExplosion", 	nil,						true,		false,	200}
enemyDefs["StillFrog"]  	= {"EnemyFrog", 	  true, 	  nil,	                      nil,					nil, nil,   "PRZT",  			  2,				7,     2,			  0,     0,       						"GruntExplosion", 	nil,						true,		false,	200}

enemyDefs["Fly"]  				= {"EnemyFly", 		true,		CActorMobile.mvFly,  				nil,					nil, nil,   "PRZ0",  			  3,				10,    0,				0,     0,       						"GruntExplosion", 	nil,						true,		false,	200}
enemyDefs["Swordsman"]  	= {"Swordsman", 	  false,	  CActorMobile.mvTileWalker,	nil,					nil, "SW0", "PRZ1",  	  	  3,				7,     1,			  2,     1,       						"GruntExplosion", 	nil,						false,	true,	  200}
enemyDefs["Grunt"]  			= {"Grunt", 			  false,	  CActorMobile.mvJitter,			nil,					nil, "GRU", "PRZ1",  			  3,				8,     20,			5,     0,       						"GruntExplosion", 	nil,						false,	false,	1000}
enemyDefs["Popper"]  			= {"Popper", 			false,	CActorMobile.mvTileWalker,	nil,CActor.diePop, "POP", "PRZ2",				    3,     		2,		 5,			  6,     0,       						"PopExplosion", 		nil,						false,	false,	1600}
enemyDefs["Spider"]  			= {"Spider", 			false, 	CActorMobile.mvTileWalker,  nil,					nil, "SPI", "PRZ2",  			  3,				5,    2,				3,     0,       						"GruntExplosion", 	nil,						true,		false,	1000}
enemyDefs["Beamer"]  			= {"Beamer", 			false,	CActorMobile.mvNodeWalker,	nil,					nil, "BEA", "PRZ2",  			  3,				6,     20,			4,     0,       						"GruntExplosion", 	nil,						false,	false,	1200}
enemyDefs["Corner"]  			= {"Corner", 			false,	nil, 			CActorMobile.plCorner, 					nil, "COR" ,"PRZ2",  			  3,				0,     20,			2,     0,       						"GruntExplosion", 	nil,						false,	false,	1500}
enemyDefs["Wraith"]  			= {"Wraith", 			false,	CActorMobile.mvNodeWalker,	nil,					nil, "WRA", "PRZ2",  	  	  3,				6,     5,			  2,     1,       						"GruntExplosion", 	nil,						false,	true,	  1600}
enemyDefs["Siren"]  			= {"Siren", 			  false,	  CActorMobile.mvJitter,			nil,					nil, "SIR", "PRZ2",  	  	  3,				6,     5,			  2,     1,       						"GruntExplosion", 	nil,						false,	true,	  1600}
enemyDefs["Ogret"] 			= {"Ogre", 				false,	CActorMobile.mvIntelligent, nil,					nil, "OGR", "PRZ2",				  3,				5,     20,			1,  	 2,										"PlayerExplosion", 	nil, 						false,	true,	2000}
enemyDefs["Magus"] 			= {"SecurityBot",   false,  CActorMobile.mvIntelligent, nil,					nil, "MAG", "PRZ2",				  0,				5,     20,			0.25,  2,										"PlayerExplosion", 	nil, 						false,	false, 2000}
enemyDefs["Skull"] 			= {"Skull", 			  false,  nil, 											nil,					nil, nil, 	nil,					  0,				0,     0,				0,  	 0,										"GruntExplosion", 	nil, 						false,	false,	0}

enemyDefs["Ogre"] 				= {"Ogre", 				  false,	CActorMobile.mvIntelligent, nil,					nil, "OGR", "PRZ2",				  3,				5,     20,			1,  	 2,										"PlayerExplosion", 	"PlayerRezIn", 	false,	true,	0}
enemyDefs["SecurityBot"] 	= {"SecurityBot",   false,	CActorMobile.mvIntelligent, nil,					nil, "SEC", nil,					0,				5,     20,			0.25,  2,										"PlayerExplosion", 	"PlayerRezIn", 	false,	false,	0}

--------------------------------------------------------------------------------------------------------------------------------------------
local enemyCastingDefs = {}
enemyCastingDefs["SW0"] = {false, {"CastSlash", 0}}

enemyCastingDefs["MAG"] = {false, {"Bullet", 50},	{"b", 5}}
enemyCastingDefs["SEC"] = {false, {"Bullet", 50},	{"b", 5}}
enemyCastingDefs["GRU"] = {false, {"Bullet", 50}, {"h", 5}}
enemyCastingDefs["SP2"] = {false, {"Bullet", 50},	{"e", 500}}
enemyCastingDefs["SPI"] = {false, {"j", 30}, {"v", 20}}
enemyCastingDefs["WRA"] = {false, {"EInvis", 20}, {"CastSlash", 20}}
enemyCastingDefs["SIR"] = {false, {"EInvis", 20}, {"Bullet", 20}, 	 {"n", 10}}

enemyCastingDefs["OGR"] = {false, {"a", 30}}

enemyCastingDefs["BEA"] = {false, {"CornerBeam", 100}}
enemyCastingDefs["COR"] = {false, {"e", 500}}
enemyCastingDefs["POP"] = {false, {"h", 50}}

--------------------------------------------------------------------------------------------------------------------------------------------

function createEnemies(enemy_set_def, number_of_chests, number_of_special_chests, avoiding_room)
  local player_room_id = nil
  if gPlayer ~= nil then player_room_id = gPlayer.currentRoomID end
	
	if number_of_chests > 0 and gCurrentLevel == 1 then
		createChestInCornerOfPlayerRoom("StartChest")
	end
	
	local chest_count = number_of_chests
	
	if number_of_chests > 0 then
		number_of_chests = math.max(3, math.random(number_of_chests))
	end
	
	local chest_room_array = getRoomIDArray()
	removeFirstMemberWithValue_inplace(chest_room_array, player_room_id) -- Remove the player's room
	
	-- Also remove all rooms which have a special purpose
	for i = #chest_room_array, 1, -1 do
		if getRoomFromID(chest_room_array[i]).roomType ~= nil then
			table.remove(chest_room_array, i)
		end
	end
	
	chest_room_array = getRandomisedArray(chest_room_array)
	for i = 1, chest_count do
		createChestInRoomWithID("Chest", chest_room_array[i])
	end
	
	for i = 1, number_of_special_chests do
		createChestInRoomWithID("SpecialChest", chest_room_array[i])
	end

	--------------------
	local ids_to_avoid = {player_room_id}
	if avoiding_room ~= nil then table.insert(ids_to_avoid, avoiding_room) end
		
	-- This is a specific list of enemies
	if enemy_set_def[1] ~= true and enemy_set_def[1] ~= false then 
		for i, v in pairs(enemy_set_def) do
			for j = 1, v[2] do
				local enemy_type = v[1]
				local room = getRandomRoomAvoidingRoomIDs(ids_to_avoid)			
				if room.roomID == gPlayer.currentRoomID then print("Placed player in same room as enemy!!! " .. player_room_id) end
				if room == nil then love.event.push("quit") end
				local pos = room:getRandomValidThingPositionWithinRoom()
				local enemy = createEnemyWithName(enemy_type, pos)
				if enemy.placementFunction ~= nil then enemy.placementFunction(enemy) end
			end
		end
	else	
		for i = 1, NUMBER_OF_ENEMIES do
			local enemy_type = getRandomThingFromProbabilitySet(enemy_set_def)
			
			local room = getRandomRoomAvoidingRoomIDs(ids_to_avoid)
			
			if room.roomID == gPlayer.currentRoomID then print("Placed player in same room as enemy!!! " .. player_room_id) end
			if room == nil then love.event.push("quit") end
			local pos = room:getRandomValidThingPositionWithinRoom()
			
			local enemy = createEnemyWithName(enemy_type, pos)
			if enemy.placementFunction ~= nil then enemy.placementFunction(enemy) end
		end
	end
end
------------------------------------
function createEnemyWithName(name, pos, avoiding_room)
  if enemyDefs[name] == nil then return false end
	if pos == nil then
		local player_room_id = nil
		if gPlayer ~= nil then player_room_id = gPlayer.currentRoomID else return end
		local ids_to_avoid = {player_room_id}
		if avoiding_room ~= nil then table.insert(ids_to_avoid, avoiding_room) end
		
		
		local room = getRandomRoomAvoidingRoomIDs(ids_to_avoid)
    if room.roomID == gPlayer.currentRoomID then print("Placed player in same room as enemy!!! " .. player_room_id) end
    if room == nil then print("Cannot place enemy") love.event.push("quit") return nil end
		pos = room:getRandomValidThingPositionWithinRoom(pos)
		if pos == nil then print("Enemy placed in illegal position") return nil end
	end
	
  local def = enemyDefs[name]
  local a = CActorMobile(name)
  a.enemyType = name
  a.faction = FACTION_ENEMY
  
	a.isCreature					= true
  a:setAnimation(def[1])
	a.isDangerousToTouch 			= def[2]
		
  a.movementFunction        = def[3]
	a.placementFunction 			= def[4]
	a.dieFunction 						= def[5]

	if def[6] ~= nil then
		a.spellProbabilityList    = deepcopy(enemyCastingDefs[def[6]])
		a.castingFunction = CActorMobile.ctBasic

		for i = #a.spellProbabilityList, 2, -1 do
			local sp = a.spellProbabilityList[i]
			if sp[1] == "CastSlash" then
				table.remove(a.spellProbabilityList, i)
				a.castingFunction = CActorMobile.ctBasicPlusSword
			elseif sp[1] == "CornerBeam" then
				table.remove(a.spellProbabilityList, i)
				a.castingFunction = CActorMobile.ctBasicPlusCornerBeam
			end
		end
  end
		
	a.prizeSetOnDeath         = def[7]
	a.numberOfPrizes 					= def[8]
  a.maxSpeed                = def[9]
	a.pauseTime								= def[10]
	
	
  a.totalTimeBetweenActions = def[11]
  if a.totalTimeBetweenActions > 0 then
    a.currentTimeSinceLastAction = math.random(a.totalTimeBetweenActions)  
  else
    a.currentTimeSinceLastAction = 0
  end
  a:setMaxShieldLevel(def[12])
  a.explosionType           = def[13]
	a.target 									= gPlayer
	a.position = pos:clone()

  if def[14] then
    a:setRezInExplosion(def[14])
  end
	
	a.faceMovementDirection = def[15]
	a.flipHorizontalForMovement = def[16]
	a.score = def[17]
  
  table.insert(gCreatureArray, a)
  
  return a
end

-----------------------------------------
function createSecurityBot()
--	local room = pickRandomNeighbouringRoom(gPlayer.currentRoomID)
--	if room ~= nil then
--		local pos = getFurthestCornerOfRoomWithIDFromPosition(room.roomID, gPlayer.position)
		local pos = getFurthestCornerOfRoomWithIDFromPosition(gPlayer.currentRoomID, gPlayer.position)
		if pos ~= nil then 
			local enemy = createEnemyWithName("SecurityBot", pos)
			if enemy == nil then playSound("Error")
				print("Could not create security bot")
				return nil
			else
				playSound("MagusSummoned")
				enemy.isSecurityBot = true
			end
			return enemy
		end
		print("Cannot create SecurityBot")
end
-----------------------------------------
function createChestInCornerOfPlayerRoom(name)
  local pos = getFurthestCornerOfRoomWithIDFromPosition(gPlayer.currentRoomID, gPlayer.position)
	if pos ~= nil then
		local chest = createEnemyWithName(name, pos)
		chest.isDangerousToTouch = false
		chest.isChest = true
	end
end

-----------------------------------------
function createChestAt(name, pos)
	if pos ~= nil then
		local chest = createEnemyWithName(name, pos)
		chest.isDangerousToTouch = false
		chest.isChest = true
	end
end

-----------------------------------------
function createChestInCenterOfRoom(name, room)
	local pos = findValidThingPositionInSpecificRoomNear(vector(room.cx, room.cy))
	if pos ~= nil then
		local chest = createEnemyWithName(name, pos)
		chest.isDangerousToTouch = false
		chest.isChest = true
	end
end

-----------------------------------------
function createChestInRoomWithID(name, id)
  local room = getRoomFromID(id)
	if id == nil then return end
	
  local pos = room:getRandomValidThingPositionWithinRoom()
	if pos ~= nil then
		local chest = createEnemyWithName(name, pos)
		chest.isDangerousToTouch = false
		chest.isChest = true
	end
end