local class = require 'library.middleclass'

CActorPickup = class('CActorPickup', CActor)

function CActorPickup:initialize(name)
  CActor.initialize(self, name)

	self.pickupType = nil
	self.pickupRadius = 0.75
	self.pickupRadiusSq = self.pickupRadius * self.pickupRadius
	self:setQuad("Pickup1")
	self.baseQuad = nil
	self.faction = PICKUP_FACTION
	self.moveTowardPlayerRadius = 2
	self.moveTowardPlayerRadius = self.moveTowardPlayerRadius *	self.moveTowardPlayerRadius 
  self.timeUntilPickupable = 0.002
	self.deathTime = 10
end

function CActorPickup:updateGraphic()
	if self.isDying ~= true and self.baseQuad ~= nil then
		local s = gSpellsCarried[self.pickupType]
		if s < MAX_SPELLS_CARRIED then
			self:setAnimation(self.baseQuad)
		else
			self:setAnimation("Gold")
		end
	end
end

-----------------------
function CActorPickup:setPickupRadius(r)
	self.pickupRadius = r
	self.pickupRadiusSq = r * r
end

-----------------------
function CActorPickup:isWithinPickupRangeOf(actor)
  if self.timeUntilPickupable > 0 then return false end
	if self.pickupRadius == 0 or self.isDying == true or self.isAlive == false then
		return false
	else
		
	if self.position:dist2(actor.position) < self.pickupRadiusSq then 
		return true 
	end
		return false
	end
end

-------------------------
local MOVE_SPEED = 4
function CActorPickup:moveTowardPlayerIfClose(dt)
  if self.timeUntilPickupable > 0 then self.timeUntilPickupable = self.timeUntilPickupable - dt
    if self.timeUntilPickupable < 0 then self.timeUntilPickupable = 0 end
    return 
  end
  
	if self.position:dist2(gPlayer.position) < self.moveTowardPlayerRadius then 
		local v = gPlayer.position - self.position
		v:normalize_inplace()
		v = (v * MOVE_SPEED * dt) + self.position
		if isWalkable(math.floor(v.x), math.floor(v.y)) then
			self.position = v
		end
	end
end

function CActorPickup:kill()
	CActor.kill(self)
	self:setAnimation("PickupFade")
end