introState = {}
local introStateTimeInSeconds = 0
local lineNumber = 1 
local charNumber = 0
local lengthOfString = 0
local timePerChar = 0.0
local CHAR_DELAY = 0.02
local PRESS_START_TEXT_Y = 0 --gCanvasHeight * 0.92
local doneInitialGlitch = false

local bigString = "----====(((({{{{ SPELLRAZOR }}}}))))====----\n            \n\
In 1981 Duncan Michael Bower completed work on a game named Spellrazor.\n                        \
His company fired him, citing 'increasing mental instability' as the reason.\n                        \
Suprisingly, they also ordered the game's hardware and printouts destroyed.\n                        \n\n\
Duncan (known as DMB) was never heard from again, and several rumours began.                        \n\n\
Some believe he was silenced to stop him talking about Spellrazor's effects\n\non the second test audience.\n                        \n\
Others say he was secretly recruited by the C.I.A. to create 'Polybius'.\n                        \n\
In 2014 I received a blank brown envelope containing a 67 page printout.\n                        \
The listing's title was 'Spellrazor'. No other details were present.\n                        \n\n\
It was written in an obscure variant of 6502 assembler and translating it\n\nwas a slow, painful process. I finished the work in October of 2015.\n                        \n\n\
I don't understand 50% of the code, as it self-modifies and glitches.\n                        \n\
But I am happy. Spellrazor deserves to be free. To spread. To inculcate.\n                        \n\
                                                        - Dene Carter 2015"
local bigStringBlank = ""

function introState:enter()	
	bigStringBlank = ""
  if gFirstTimePlayer then  
    CWindowDialog("Start", gDialogArray, "New version: " .. VERSION_NUMBER .. "\n \nThis game will always be 100% free.\nTo help support us, please 'Like' us on Facebook:\n \nwww.facebook.com/Spellrazor\n \nor say 'Hi!' on Twitter: @Fluttermind", {{"Maybe I will!", "n", nil}})	
  end
  lengthOfString = bigString:len()
  gColourAdd = 0
	gShaderZoom = 0
  
  gTextIntroMusic = nil
	gTextIntroMusic = love.audio.newSource("assets/TextIntroTrackLoop.mp3", 'stream')
	gTextIntroMusic:setLooping(true)
	gTextIntroMusic:play()
	gTextIntroMusic:setVolume(0.2)
end

function introState:update(dt)
	if charNumber < lengthOfString then
		timePerChar = timePerChar + dt
		if timePerChar > CHAR_DELAY then
			timePerChar = timePerChar - CHAR_DELAY
			charNumber = charNumber + 1
			bigStringBlank = bigStringBlank .. bigString:sub(charNumber, charNumber)
		end
	end
	
  if doneInitialGlitch == false then setGlitchingForTime(1); doneInitialGlitch = true end
	local old_time = introStateTimeInSeconds
  local seconds_per_message = 8
  introStateTimeInSeconds = introStateTimeInSeconds + dt
  
  if math.floor(introStateTimeInSeconds / seconds_per_message) ~= math.floor(old_time / seconds_per_message) then
		setGlitchingForTime(0.5)
  end
end 

function introState:draw()
  local t = introStateTimeInSeconds * 100
	local r2, g2, b2 = HSL((((200 + 	t) % 1000) / 1000 * 255 + 160) % 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local linecolour = {r2/256 * 255, g2/256*255, b2/256*255,255}

	local lo = 0.2
	local mid = 0.3
	local hi = 0.4
  love.graphics.setBackgroundColor(0, 0, 0)

--	drawTronBackground({linecolour[1] * lo, linecolour[2] * lo, linecolour[3] * lo, 255}, introStateTimeInSeconds, 0.6, false)
  
  local wid = 1000

	love.graphics.setColor(255,255,128,255)
	love.graphics.setFont(beebTileFont)
  love.graphics.printf(bigStringBlank, -(wid / 2) + (gCanvasWidth / 2), 8, wid, 'center')

	--love.graphics.print(bigStringBlank, 8, 8)

	love.graphics.setColor(255,0,0,255)

	if charNumber >= lengthOfString then
		love.graphics.printf("Press Return/Enter to boot Spellrazor", -(wid / 2) + (gCanvasWidth / 2), math.floor(gCanvasHeight * 0.96), wid, 'center')
	end
end

function introState:keypressed(key)
	if key == "escape" then love.event.push("quit") return 
	elseif (key == "return" or key == "enter" or key == "kpenter") and charNumber >= lengthOfString then
    gTextIntroMusic:stop()
    gTextIntroMusic = nil
    stopAllSound()
    
    menuState:createRandomSeed()
    GameState.switch(menuState)
  else 
		charNumber = lengthOfString
		bigStringBlank = bigString
	end
end
