require "library.Tools"
local class = require 'library.middleclass'
local MAX_GLITCH_LEVELS = 4
local MAX_GLITCH_VARIANTS_PER_LEVEL = 4 

CWorldMessage = class('CWorldMessage')
function CWorldMessage:initialize(string, x, y, is_horizontal)
  self.x = x
  self.y = y
	self.angleInRads = 0
  self.strings = {}
	
	if is_horizontal == false then
		self.angleInRads = math.rad(90)
			self.x = self.x + 0.5
	end
	
  for i = 1, MAX_GLITCH_LEVELS do
    self.strings[i] = {}
    for k = 1, MAX_GLITCH_VARIANTS_PER_LEVEL do
      self.strings[i][k] = ""..string
    end
  end
  
  for i = 2, MAX_GLITCH_LEVELS do
    self:createGlitchVariants(self.strings[i], i)
  end
end

-----------------------
function CWorldMessage:createGlitchVariants(array, depth)
  local len = array[1]:len()
  local number_of_glitches = 1 + math.ceil(math.floor(len * depth / MAX_GLITCH_VARIANTS_PER_LEVEL) / 2)
  local random_pos_array = {} 
  
  for i = 1, number_of_glitches do 
    random_pos_array[i] = math.random(len)
  end

  for i = 1, MAX_GLITCH_VARIANTS_PER_LEVEL do
    for n = 1, number_of_glitches do -- math.random(math.floor(string:len() * 0.5)) do -- Glitch it several times
      array[i] = replaceChar(array[i] , random_pos_array[n], string.char(32+math.random(90)))
    end
  end
end

-------------------------------
function CWorldMessage:draw()
  local mx, my = cellXYToScreenXY(self.x,self.y)
  local level, variant = 1, 1
  if gPlayer ~= nil then 
    level = 1 + (math.floor(((self.x + gPlayer.position.x) / NODE_FLOOR_SIZE) + ((self.y + gPlayer.position.y) / NODE_FLOOR_SIZE)) % MAX_GLITCH_LEVELS)
    variant = 1 + getRandomisedValueWithFeederValueAndWrapSize(gFixedFrameCount, MAX_GLITCH_VARIANTS_PER_LEVEL)
  end
  local str = self.strings[level][variant]

	love.graphics.print(str, mx + 4, my + 4, self.angleInRads)
end

-------------------------------
function addWorldMessage(str, x, y, is_horizontal, array)
  local msg = CWorldMessage(str, x, y, is_horizontal)
  table.insert(array, msg)
end