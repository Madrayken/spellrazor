-- Defender style explosions
local class = require 'library/middleclass'
vector = require 'library/hump.vector'

CExplosion = class('CExplosion')

POINTS_PER_EXPLOSION = 16

DOT_SIZE = 1
function CExplosion:initialize(number_of_points, lifespanInFrames, max_speed, colour_array, velocity_function, velocity_bias, velocity_frac, expands_outwards, max_circle_radius)
  self.numberOfPoints = number_of_points or POINTS_PER_EXPLOSION
  self.position = vector(0,0)
  self.totalLife = lifespanInFrames or 1
  self.currentLife = 0
  self.expandsOutwards = expands_outwards 

  self.circleDrawRadius = max_circle_radius or 0
  
  self.x = {}
  self.y = {}
  self.drawX = {}
  self.drawY = {}
  self.xScale = 2
  self.yScale = 1
  self.hitByThingWithVelocity = vector(0,0)
  self.mesh = {}
  self.colourArray = colour_array or {{255, 0, 0}, {255, 255, 255}, {255, 0, 255}}
  self.velocities = {}
  self.velocityBias = velocity_bias or vector(0,0)
  self.velocityBias = self.velocityBias * velocity_frac
  self.velocityFunction = velocity_function or self.getVelocityBasedOnSpiral
  self.maxSpeed = max_speed
  local vertices = {}
  for i = 1, self.numberOfPoints do
    self.x[i] = 0
    self.y[i] = 0
    self.drawX[i] = 0
    self.drawY[i] = 0
    
    if colour_array == nil then colour_array = self.colourArray end
    index = math.random(1, #colour_array)
    cols = colour_array[index]
    
    vertices[i] = {}    
    vertices[i] = 
    {
      {-DOT_SIZE,-DOT_SIZE, 0, 0,         cols[1], cols[2], cols[3]},
      {DOT_SIZE, -DOT_SIZE, 1, 0,         cols[1], cols[2], cols[3]},
      {DOT_SIZE,  DOT_SIZE, 1, 1,         cols[1], cols[2], cols[3]},
      {-DOT_SIZE, DOT_SIZE, 0, 1,         cols[1], cols[2], cols[3]},
    }
  
    -- the Mesh DrawMode "fan" works well for 4-vertex Meshes.
    self.mesh[i] = love.graphics.newMesh(vertices[i],  "fan", nil)
  end
end

local BIAS_SIZE = 6

function CExplosion:start(pos)
  -- The 'bias' forces the explosion to be non-
  for i = 1, self.numberOfPoints do
    self.x[i] = pos.x
    self.y[i] = pos.y
    self.velocities[i] = self.velocityFunction(self, i)
  end

  if self.expandsOutwards == false then
    for i = 1, self.numberOfPoints do
      self.x[i] = self.x[i] + self.velocities[i].x * self.totalLife
      self.y[i] = self.y[i] + self.velocities[i].y * self.totalLife
      self.velocities[i] = self.velocities[i] * -1
    end
  end
  self.currentLife = self.totalLife
end

--------------------
function CExplosion:update(dt)
  if self.currentLife <= 0 then 
    return false
  end

  self.currentLife = self.currentLife - dt
  if self.currentLife < 0 then
    self.currentLife = 0
    return false
  end
  
  for i = 1, self.numberOfPoints do
    self.x[i] = self.x[i] + (self.velocities[i].x + self.velocityBias.x) * dt
    self.y[i] = self.y[i] + (self.velocities[i].y + self.velocityBias.y) * dt

    self.drawX[i], self.drawY[i] = cellXYToScreenXY(self.x[i], self.y[i])
    self.drawX[i] = math.floor(self.drawX[i])
    self.drawY[i] = math.floor(self.drawY[i])
  end
  return true
end

function CExplosion:draw()  
  if self.currentLife > 0 then
    for i = 1, self.numberOfPoints do -- usually just 1 for a circle thing
      love.graphics.draw(self.mesh[i], self.drawX[i], self.drawY[i])
      if self.circleDrawRadius > 0 then
        local frac = self.currentLife / self.totalLife
        if self.expandsOutwards then frac = 1.0 - frac end
                
        love.graphics.setColor(self.colourArray[1 + math.floor(#self.colourArray * frac)])
        love.graphics.circle("line", self.drawX[i],self.drawY[i], self.circleDrawRadius * gTileSize * frac , 20)
      end
    end
  end
end

------------------------
function CExplosion:getVelocityBasedOnSpiral(i)
  local v = gSpiralTable[i+1]
  return vector(v[1] * self.maxSpeed * (1 + (i / 10)), v[2] * self.maxSpeed * (2 + (i / 10)))
end

function CExplosion:getVelocityBasedOnRandom(i)
  local v = vector((math.random() * self.maxSpeed) - (self.maxSpeed / 4), 0)
  v:rotate_inplace(math.random() * math.rad(360))
  return v
end

function CExplosion:getVelocityBasedOnCircle(i)
  local ang_step = math.rad(360) / self.numberOfPoints
  local v = vector(self.maxSpeed, 0)
  v:rotate_inplace(i * ang_step)
  return v
end

-------------------------------
-------------------------------------------------#points life   speed palette                                               movementFunction                          velocityFromHitFrac  expandsOutwards circleRadius
gExplosionDefinitions = {}
gExplosionDefinitions["GruntExplosion"]         =   {16,  1,    4.5,  {{0, 255, 0},     {255, 255, 255}, {0, 255, 255}},    CExplosion.getVelocityBasedOnSpiral,      1,               true}
gExplosionDefinitions["ChestExplosion"]         =   {16,  1,    20,   {{255, 0, 0},     {255, 255, 0},   {255, 255, 255}},  CExplosion.getVelocityBasedOnRandom,      0.25,               true}
gExplosionDefinitions["PopExplosion"]           =   {32,  1,    10,   {{255, 0, 0},     {255, 255, 0},   {255, 255, 255}},  CExplosion.getVelocityBasedOnRandom,      0.0,               true}

gExplosionDefinitions["BombExplosion"]          =   {40,  0.25,  30,   {{255, 0, 0},     {255, 255, 0},   {255, 255, 255}},  CExplosion.getVelocityBasedOnRandom,     0.0,                true}

gExplosionDefinitions["PlayerExplosion"]        =   {100, 2,    20,   {{255, 255, 255}, {128, 128, 255}, {64, 64, 255}},    CExplosion.getVelocityBasedOnRandom,      0.25,               true}
gExplosionDefinitions["Pickup"]                 =   {10,  0.25, 5,    {{255, 255, 255}, {255, 200, 255}, {255, 0, 255}},    CExplosion.getVelocityBasedOnCircle,      0,                  false}
gExplosionDefinitions["PlayerRezIn"]            =   {60,  1,    20,   {{255, 255, 255}, {128, 128, 255}, {64, 64, 255}},    CExplosion.getVelocityBasedOnSpiral,      0.25,               false}
gExplosionDefinitions["OgreRezIn"]              =   {60,  0.5,  20,   {{255, 255, 255}, {255, 128, 128}, {255, 64, 0}},     CExplosion.getVelocityBasedOnSpiral,      0,                  false}
gExplosionDefinitions["Circle"]                 =   {1,   0.1,    0,  {{255, 255, 255}, {255, 0, 0}, {128, 0, 0}},    nil,                                      0,                  true,             4}
gExplosionDefinitions["WarpCircle"]             =   {1,   0.1,    0,  {{0, 200, 0, 128}, {0, 128, 0, 128}, {0, 64, 0,128}},    nil,                                      0,                  true,             4}
