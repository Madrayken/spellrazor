require 'library.Tools'
local class = require 'library.middleclass'

CSpecialEffectsWalker = class('CSpecialEffectsWalker')

function CSpecialEffectsWalker:initialize(x, y, code, delay)
  self.origX = x
  self.origY = y
  
  self.currentStatSet = {}
  self.currentStatSet.x = x
  self.currentStatSet.y = y
  self.currentStatSet.step1MaxTime = code[1]
  self.currentStatSet.step1Time = code[1]
  self.currentStatSet.step1Rotate = code[2]
  self.currentStatSet.step2MaxTime = code[3]  
  self.currentStatSet.step2Time = code[3]
  self.currentStatSet.step2Rotate = code[4]
  self.currentStatSet.dir = math.random(8)-1

  self.tailStatSet = deepcopy(self.currentStatSet)
  self.delay = delay
  self.colour = colour
end

function CSpecialEffectsWalker:getDrawXY()
  return self.currentStatSet.x, self.currentStatSet.y
end

function CSpecialEffectsWalker:getEraseXY()
  return self.tailStatSet.x, self.tailStatSet.y
end


function CSpecialEffectsWalker:update()
  if self.delay > 0 then
    self.delay = self.delay - 1
  else
    self:updateStatSet(self.tailStatSet)
  end
  
  self:updateStatSet(self.currentStatSet)
end

function CSpecialEffectsWalker:updateStatSet(set)
  if set.step1Time ~= 0 then
    set.step1Time = set.step1Time - 1
    if set.step1Time == 0 then 
      set.step1Time = set.step1MaxTime
      set.dir = (set.dir + set.step1Rotate) % 8
    end
  end
  
  if set.step2Time ~= 0 then
    set.step2Time = set.step2Time - 1
    if set.step2Time == 0 then 
      set.step2Time = set.step2MaxTime
      set.dir = (set.dir + set.step2Rotate) % 8
    end
  end

  local dirxy = gAround8[set.dir + 1]

  set.x = set.x + dirxy[1]
  set.y = set.y + dirxy[2]
  
  set.x = set.x % math.floor(gGenerativeCanvasSize)
  set.y = set.y % math.floor(gGenerativeCanvasSize)
--  set.y = set.y % math.floor(gCanvasHeight)
  
  set.x = math.abs(set.x)
  set.y = math.abs(set.y)
end
