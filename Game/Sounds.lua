-----------------------------
MAX_SOUND_PRIORITY = 10
gCurrentSoundEffectPriority = 0
gSoundEffectPriorities = {}
gSoundEffectPriorities["Pickup"] = 8
gSoundEffectPriorities["PlayerArrow"] = 8
gSoundEffectPriorities["Homer"] = 8
gSoundEffectPriorities["Bouncer"] = 8
gSoundEffectPriorities["EnemyBullet"] = 4
gSoundEffectPriorities["Map"] = 3
gSoundEffectPriorities["XRay"] = 3
gSoundEffectPriorities["SwordSlash"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["EnemyDie"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["PlayerHit"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["PlayerDie"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["Shield"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["PlayerRezIn"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["GotKey"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["RoomDiscovered"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["PowerOff"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["SpellAtZero"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["ShieldsLow"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["Lightning"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["Freeze"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["Negate"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["Inviso"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["LeaveLevel"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["DeathTouch"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["Kiss"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["Turncoat"] = MAX_SOUND_PRIORITY
gSoundEffectPriorities["TurncoatEnd"] = MAX_SOUND_PRIORITY


function playSound(name, pitch)
	if pitch == nil then pitch = gDefaultPitch end
	name = name or "TestSound"
	-- Use the sound engine like Defender, and only allow one channel at a time
--	local priority = gSoundEffectPriorities[name]
	
--	if priority == MAX_SOUND_PRIORITY then
		TEsound.play("assets/" .. name .. ".wav", 1, 1, pitch) -- Too important to ever be ignored
--		return 
--	end
	
--[[	if priority == nil then priority = MAX_SOUND_PRIORITY end
	
	if priority >= gCurrentSoundEffectPriority then 
--		TEsound.stop("sfx")	
		gCurrentSoundEffectPriority = priority
		TEsound.play("assets/" .. name .. ".wav", "sfx", 1, 1, autoStopSound)
	end]]
end

function autoStopSound(list)
	gCurrentSoundEffectPriority = 0
end

function setPitchOfAllSounds(pitch)
	gEngineLoop:setPitch(pitch)
	gBackgroundMusic:setPitch(pitch)
	local tags = TEsound.findTag("all")
	for i, v in ipairs(tags) do
		TEsound.pitch(v, pitch)	
	end
end

function stopAllSound()
	if gEngineLoop ~= nil then 
		gEngineLoop:setVolume(0)
	end
	
	if gWhisperLoop ~= nil then 
		gWhisperLoop:setVolume(0)
	end
	
	
	local tags = TEsound.findTag("all")
	for i, v in ipairs(tags) do
		TEsound.stop(v, true)	
	end
	TEsound.cleanup()
end
