local bresenham = require 'library.bresenham'

MAX_FREEZE_TIME = 5
MAX_NEGATE_TIME = 5

MAX_INVISIBLE_TIME = 10
MAX_ENEMY_INVISIBLE_TIME = 3
MAX_TURNCOAT_TIME = 60
QUAKE_DAMAGE_RADIUS = 4
WARP_RADIUS = 12
NEGATE_RADIUS = 14
WARP_TIME = 1.5
MAX_VOID_TIME = 8
MAX_VOID_RADIUS = 3
MAX_Y_SHOT_TIME = 4
MAX_ULTRASWORD_TIME = 25

local spellDefs = {} --------art--------------sfx------Lfe--Bnce--Spd---DieOnColl-collfnc---------------------------		dmgRng--- faceAng-MvFn
spellDefs["Bullet"] 			= {"Bullet", 				"Bullet", 2,		1,		8,  	true,			CActorMobile.dieOnHitWallsCollision, 	0.2,   		false, nil}
spellDefs["YShot"] 				= {"Bullet", 				nil, 2,		1,		8,  	true,			CActorMobile.dieOnHitWallsCollision, 	0.2,   		false, nil}

spellDefs["FriendBullet"] = {"Bullet", 	"FriendBullet", 2,		1,		8,  	true,			CActorMobile.dieOnHitWallsCollision, 	0.2,   		false, nil}

spellDefs["Arrow"] 				= {"Arrow1", 				"Arrow", 	2,		1,		15,  	true,			CActorMobile.dieOnHitWallsCollision, 	0.3,   		true,  nil}
spellDefs["Bouncer"] 			= {"Bouncer", 			"Bouncer",5,		1,		20,  	true,			CActorMobile.bouncyCollision, 				0.4,   		false, nil}
spellDefs["Homer"] 				= {"Homer", 				"Homer", 	5,		1,		8,  	true,			CActorMobile.basicCollision, 					0.4,   		false, CActorMobile.mvHomeFast}
spellDefs["Kiss"] 				  = {"Kiss", 					"Kiss", 	5,		0,		6,  	true,			CActorMobile.basicCollision, 					0.4,   		false, CActorMobile.mvHomeFast}
spellDefs["Quake"] 				= {"Bomb", 					"Bomb", 	0.8,		0,		0,  	false,		nil, 					0,   			false, nil}
spellDefs["Warp"] 				  = {"Warp", 					"WarpStart", 	WARP_TIME,	0,		0,  	false,		nil, 					0,   			false, nil}
spellDefs["QuakeBlast"] 	  = {nil, 				"Lightning", 	-1,		0,		0,  	false,		nil, 								QUAKE_DAMAGE_RADIUS,   			false, nil}

spellDefs["WarpBlast"] 		  = {nil, 						"WarpEnd", 			-1,		0,		0,  	false,		nil, 																		0,   		false, nil}

spellDefs["JumpLand"] 	    = {nil, 				  "Lightning", 	-1,		0,		0,  	false,		nil, 								QUAKE_DAMAGE_RADIUS,   			false, nil}
spellDefs["Rocket"] 			  = {"Rocket", 				"Rocket", 	50,		0,		6,  	true,			CActorMobile.dieOnHitWallsCollision, 	0.4,   	true, nil}

gSpellCooldown = {}
gSpellCooldown["CastSlash"] = 0.3
gSpellCooldown["CreatureCastSlash"] = 1.5
gSpellCooldown["d"] = 0.5
gSpellCooldown["l"] = 0.5
gSpellCooldown["e"] = 0.5
gSpellCooldown["o"] = 0.5
gSpellCooldown["n"] = 5
gSpellCooldown["CornerBeam"] = 0.5
gSpellCooldown["EInvis"] = 4

--[[	
-- Perma powerups
- Sword power
- Shields stacking
- 

* SPACE - Sword
* A - Arrow - straight-line attack to creature
* B - Bouncer - bouncing shot (HoTT lightning)
* C - Companion - raises friendly zomb-bot who follows you around and attacks all within range
* D - Deathtouch
* E - E-Beam - 
* F - Freeze - stops creatures moving
* G - Genocide - allows you to move through walls for 3 seconds
* H - Homer - HoTT fireball
* I - Inviso - Makes you invisible for 'n' turns (creatures can't see you)
* J - Jump - jump over walls?
* K - Kiss - small circle radiates out from you, turning an enemy into a friend
* L - Lightning - forking attack on one enemy. Spreads to one other if they die.
* M - Map - reveals a proportion of the map
* N - Negate - removes spell effects and stops creatures using magic for 'n' turns
* O - Ogre - creates a monster on a leash who walks ahead of you
* P - Portal
* Q - Quake - creates a bomb that blows up walls and doors (and enemies) after 5 secs
* R - Rocket
* S - Shield - create one layer of shield
* T - Teleport
* U - Ultrasword - larger sword that can even strike enemies through walls
* V - Void - slow, randomly moving, gravitational effect that sucks monsters into it  
* W - Warp - moves all enemy creatures to this point after a delay
* X - X-Ray - shows creatures in ajoining rooms
* Y - Y-Shot - spinning set of shots useful for clearing a room
* Z - Z-bomb - basically a huge smartbomb thing]]


---------------------------------------------------------------------------------
function createSpellFromDefinition(name, creator, target, extra_velocity)
	local a = CActorMobile(name, creator)
	local def = spellDefs[name]
	a:setAnimation(def[1])
	
	if def[2] ~= nil then	playSound(def[2]) end
		
	a.lifespanInFrames = def[3] / FIXED_STEP -- lifespanInFrames in FIXED_STEP units
	a.bounceFrac = def[4]
	a.maxSpeed = def[5]
	
	if extra_velocity ~= nil then
		a.maxSpeed = a.maxSpeed + extra_velocity
	end
	
	a.dieOnThingCollision = def[6]
	a.collisionFunction = def[7]
	a:setPhysicsRadius(def[8])
	a.faceMovementDirection = def[9]
	
	if def[10] ~= nil then a.movementFunction = def[10] end

	a.faction = creator.faction
	a.position = creator.position:clone()

	local ang = creator.facingAngleInRads + math.rad(-90) -- Need to convert from 'Dene' angles to 'math' angles
	if target ~= nil then
		a.target = target
		
		local tpos = a.target.position:clone()
		if a.target.invisibleTimeLeft > 0 then 
      local z = tpos.x
      if math.random() > 0.5 then
        local z = tpos.x
        tpos.x = tpos.y
        tpos.y = z
      else
        local z = tpos.x
        tpos.x = -tpos.y
        tpos.y = -z
      end
		end
		ang = creator.position:angleTo(tpos) -- got in radians
	end

	local acc = vector(1, 0) 
	acc:rotate_inplace(ang)
	
	a.velocity = acc * a.maxSpeed
	
	table.insert(gSpellArray, a)

	return a
end


function createTestHomer()
	local a = CActorMobile(name, nil)
	local def = spellDefs["Homer"]
	a:setAnimation(def[1])
	
	if def[2] ~= nil then	playSound(def[2]) end
		
	a.lifespanInFrames = def[3] / FIXED_STEP -- lifespanInFrames in FIXED_STEP units
	a.bounceFrac = def[4]
	a.maxSpeed = def[5]
		
	a.dieOnThingCollision = def[6]
	a.collisionFunction = def[7]
	a:setPhysicsRadius(def[8])
	a.faceMovementDirection = def[9]
	
	if def[10] ~= nil then a.movementFunction = def[10] end

	a.faction = FACTION_ENEMY
	a.position = gPlayer.position + vector(3, 0)

	local ang = 0
	a.target = gPlayer		
	
	table.insert(gSpellArray, a)

	return a
end


--------------------------------
function addActionToStack(data)
	table.insert(gActionStack, data)
end

function clearActionStack()
	gActionStack = {}
end

function processActionStack()
	for i, v in ipairs(gActionStack) do
		processAction(v)
	end
	clearActionStack()
end
--------------------------------
-- Main code
--------------------------------
function processAction(act)
		--e.g.		addActionToStack({"CastArrow", caster, target})
	local act_name = act[1]
	local creator = act[2]
	local target = act[3]
	local name = act[4]
	if creator == nil then return end
		
	--- Turn off invisibility after move
	if creator.invisibleTimeLeft > 0 then 
		creator.invisibleTimeLeft = 0
	end
	
	---------------------------------
	if act[1] == "CastSlash" then
		local a = CActorMobile("Slash", creator)
		a.faction = creator.faction
		a.lifespanInFrames = 5
		a.position = creator.position:clone()
		a.faceMovementDirection = true
		local vec = vector(0, -0.8)

		if creator.ultraswordTimeLeft > 0 then
			vec = vector(0, -1.2)
			playSound("UpgradedSwordSlash")
			a:setPhysicsRadius(1)
			a:setQuad("SwordSlashUpgrade0")
			a.dieOnThingCollision = false
			a.damage = 2
			a.requiresLOSToTouch = false
		else
			a.dieOnThingCollision = true
			playSound("SwordSlash")
			a:setPhysicsRadius(0.75)
			a:setQuad("SwordSlash0")
			a.requiresLOSToTouch = true
		end
		a.isSwordSlash = true
		local ang = creator.facingAngleInRads - math.rad(90) -- Need to convert from 'Dene' angles to 'math' angles
		if target ~= nil then
			local tpos = target.position:clone()
			if target.invisibleTimeLeft > 0 then 
				tpos.x = tpos.x + math.random(-4, 4)
				tpos.y = tpos.y + math.random(-4, 4)
			end
			ang = creator.position:angleTo(tpos)-- + math.rad(90) -- got in radians
		end
		vec:rotate_inplace(ang)
		a.position = a.position + vec
		a.angleOffsetFromParent = nil
		a.angularRotationAroundParent = 25
		a.movementFunction = nil
		
		a:attachToActor(creator)
		table.insert(gSpellArray, a)
	-----------------------------------
	elseif act[1] == "Bullet" then
		local MAX_RANGE = 15
		local extra_speed = creator.position:dist2(target.position) / (MAX_RANGE * MAX_RANGE) * 5
		createSpellFromDefinition("Bullet", creator, target, extra_speed)
	---------------------------------
	elseif act[1] == "FriendBullet" then
		local MAX_RANGE = 15
		local extra_speed = creator.position:dist2(target.position) / (MAX_RANGE * MAX_RANGE) * 5
		createSpellFromDefinition("FriendBullet", creator, target, extra_speed)
	--------------------------------- 
	elseif act[1] == "a" then 
		createSpellFromDefinition("Arrow", 		creator, target)
	--------------------------------- 
	elseif act[1] == "b" then 
		local a = createSpellFromDefinition("Bouncer", 	creator, target) 
		a.faction = FACTION_NULL
		a.casterToIgnoreUntilOutOfHisRange = creator
	--------------------------------- 
	elseif act[1] == "c" then
		playSound("Companion")

		local a = CActorMobile("Ally", creator)
		a:setAnimation("Ally")
		a.isPlayer = false
		a.faction = FACTION_PLAYER
		a.collisionFunction = nil
		a.spellProbabilityList    = {false, {"FriendBullet", 50}}
		a.castingFunction = CActorMobile.ctBasic
  	a.isCreature = true
		a.position = creator.position:clone()
		local vec = vector(0, -1)
		a.faceMovementDirection = false
		a.position = a.position + vec
		a.angleOffsetFromParent = nil -- math.rad(-90)
		a.angularRotationAroundParent = 5
		a.movementFunction = nil
		a:attachToActor(creator)
		a:setPhysicsRadius(0.25)
		a.dieOnThingCollision = true

		a.totalTimeBetweenActions = 2
		a.currentTimeSinceLastAction = math.random(a.totalTimeBetweenActions)  		

		a.lifespanInFrames = 5000

		--a.explosionType = "PlayerExplosion"
		table.insert(gCreatureArray, a)
	---------------------------------
	elseif act[1] == "d" then			
		playSound("DeathTouch")

		-- Check if there's already one attached
		for i, v in pairs(gSpellArray) do
			if v.attachedToActor == creator and v.name == "Deathtouch" then 
				v.lifespanInFrames = 100
				print("respinning")
				return
			end
		end
		-- Nope: create a new one
		local a = CActorMobile("Deathtouch", creator)
				
		a.faction = creator.faction
		a.position = creator.position:clone()
		a.lifespanInFrames = 100

		a.faceMovementDirection = true
		a.angularRotationAroundParent = 25
		a.movementFunction = nil
		a:attachToActor(creator)
		a:setPhysicsRadius(1.5)
		a:setQuad("SpinningBlade")
		a.dieOnThingCollision = false
		table.insert(gSpellArray, a)
	--------------------------------
	elseif act[1] == "e" then
		if creator.isPlayer == false and creator.isVisible == false then return end -- Don't allow monsters to shoot from another room

		local a = CActorMobile("EBeam", creator)

		playSound("Beam")
		a.faction = creator.faction
		a.lifespanInFrames = 5
		a.position = creator.position:clone()
		local size = math.max(gMapWidth, gMapHeight)
		local vec = vector(0, -size) -- Face UP by default
					
		local ang = creator.facingAngleInRads

		if creator.isPlayer and gPlayer ~= nil and target ~= nil then
			ang = gPlayer.position:angleTo(target.position) + math.rad(90)-- got in radians
		end
		
		a.faceMovementDirection = false
		a.angleOffsetFromParent = 0
		a.movementFunction = nil
		a.target = nil
		
		-- If this is an enemy, only let it fire this orthogonally
		if creator.faction ~= FACTION_PLAYER and target ~= nil then
			local diff = target.position - creator.position
			if math.abs(diff.x) > math.abs(diff.y) then
				if diff.x > 0 then vec = vector(size,0) else vec = vector(-size,0) end
			else
				if diff.y > 0 then vec = vector(0,size) else vec = vector(0,-size) end
			end
		else
			vec:rotate_inplace(ang)
		end
		
		local end_point_x = math.floor(creator.position.x + vec.x)
		local end_point_y = math.floor(creator.position.y + vec.y)

		local points, los = bresenham.line(math.floor(creator.position.x), math.floor(creator.position.y), end_point_x, end_point_y, isWalkable)
		local end_point = points[#points] -- This gives us a blocky endpoint, but it doesn't line up with the original firing position
		local vec2 = vector(end_point[1], end_point[2]) - vector(points[1][1], points[1][2]) -- find the vector of this line
		local dist = vec2:len() + 1-- and measure it
		vec = vec:normalize_inplace() * dist -- then make the original firing line this long
		
		a.targetPosition = creator.position + vec
		a.lineEnd = a.targetPosition:clone()

		a.resizeToReachTarget = true

		a:attachToActor(creator)
		a:setAnimation("Beam")
		table.insert(gSpellArray, a)
	--------------------------------
	elseif act[1] == "CornerBeam" then
		if creator.isPlayer == false and creator.isVisible == false then return end -- Don't allow monsters to shoot from another room

		local a = CActorMobile("CornerBeam", creator)

		playSound("Beam")
		a.faction = creator.faction
		a.lifespanInFrames = 5
		a.position = creator.position:clone()
		local size = math.max(gMapWidth, gMapHeight)
		local vec = vector(0, -size) -- Face UP by default
					
		local ang = creator.facingAngleInRads

		if creator.isPlayer and gPlayer ~= nil and target ~= nil then
			ang = gPlayer.position:angleTo(target.position) + math.rad(90)-- got in radians
		end
		
		a.faceMovementDirection = false
		a.angleOffsetFromParent = 0
		a.movementFunction = nil
		a.target = nil
		
		-- If this is an enemy, only let it fire this orthogonally
		if creator.faction ~= FACTION_PLAYER and target ~= nil then
			local diff = target.position - creator.position
			if math.abs(diff.x) > math.abs(diff.y) then
				if diff.x > 0 then vec = vector(size,0) else vec = vector(-size,0) end
			else
				if diff.y > 0 then vec = vector(0,size) else vec = vector(0,-size) end
			end
		else
			vec:rotate_inplace(ang)
		end
		
		local end_point_x = math.floor(creator.position.x + vec.x)
		local end_point_y = math.floor(creator.position.y + vec.y)

		local points, los = bresenham.line(math.floor(creator.position.x), math.floor(creator.position.y), end_point_x, end_point_y, isWalkable)
		local end_point = points[#points] -- This gives us a blocky endpoint, but it doesn't line up with the original firing position
		local vec2 = vector(end_point[1], end_point[2]) - vector(points[1][1], points[1][2]) -- find the vector of this line
		local dist = vec2:len() + 1-- and measure it
		vec = vec:normalize_inplace() * dist -- then make the original firing line this long
		
		a.targetPosition = creator.position + vec
		a.lineEnd = a.targetPosition:clone()

		a.resizeToReachTarget = true

		a:attachToActor(creator)
		a:setAnimation("Beam")
		table.insert(gSpellArray, a)
	---------------------------------
	elseif act[1] == "f" then
		playSound("Freeze")		
		for i, v in pairs(gCreatureArray) do
			if v.faction ~= creator.faction and v.isVisible and v.isChest == false then
				v.freezeTimeLeft = MAX_FREEZE_TIME
			end
		end			
	---------------------------------
	elseif act[1] == "g" then 
		if creator.isPlayer == false and target ~= nil and target.currentRoomID ~= creator.currentRoomID then return end -- Don't allow monsters to shoot from another room
		local a = CActorMobile("Genocide", creator)
		local default_length = 2
		playSound("Genocide")
		a.faction = creator.faction
		a.lifespanInFrames = 3
		a.position = creator.position:clone()
		
		local vec = vector(0, -default_length) -- Face UP by default
		
		a.faceMovementDirection = false
		a.angleOffsetFromParent = 0
		a.movementFunction = nil
		local got_target_in_range = false
		if target ~= nil then
			a.target = target		
			local diff_vec = (target.position - creator.position)
			if diff_vec:len2() > default_length * default_length then
				diff_vec:normalize_inplace()
				diff_vec = diff_vec * default_length
			else
				got_target_in_range = true
			end
			a.targetPosition = a.position:clone() + diff_vec
		else
			a.target = nil		
			vec:rotate_inplace(creator.facingAngleInRads)
			a.targetPosition = a.position:clone() + vec;
		end
			
		a.resizeToReachTarget = true

		a:attachToActor(creator)
		a:setAnimation("Genocide")
		table.insert(gSpellArray, a)
				
		if a.target ~= nil and got_target_in_range then
			gLastCorpsePosition = a.target.position:clone()
			genocideCreaturesBasedOnThis(a.target)
			startScreenShakeTimeAndPower(0.25, 1)
--			gLightningTimeLeft = MAX_LIGHTING_TIME	
		end
	---------------------------------
	elseif act[1] == "h" then 
		createSpellFromDefinition("Homer", 				creator, target)
	---------------------------------
	elseif act[1] == "i" then
		playSound("Inviso")
		creator.invisibleTimeLeft = MAX_INVISIBLE_TIME
	---------------------------------
	elseif act[1] == "EInvis" then
		playSound("EnemyInviso")
		creator.invisibleTimeLeft = MAX_ENEMY_INVISIBLE_TIME		
	---------------------------------
	elseif act[1] == "j" then
		if creator.isPlayer then playSound("Jump") end
		creator.jumpTime = MAX_INTANGIBLE_TIME
	---------------------------------
	elseif act[1] == "k" then 
		local spell = createSpellFromDefinition("Kiss", 				creator, target) 
		spell.contactWithActorFunction = CActorMobile.spSwitchFaction
	---------------------------------
	elseif act[1] == "l" then 
--		if creator.isPlayer == false and target ~= nil and target.currentRoomID ~= creator.currentRoomID then return end -- Don't allow monsters to shoot from another room
--    if creator.isPlayer == false and target ~= nil and not target.isVisible then return end -- Don't allow monsters to shoot from another room
    playSound("Lightning")

    local chain_of_targets = {target}
    createLightningSpark(creator, creator, target)

    getChainOfTargetsForLightning(creator.faction, creator, chain_of_targets)
    for i, v in ipairs(chain_of_targets) do
      if i ~= 1 then 
        createLightningSpark(creator, chain_of_targets[i-1], v)
      end
    end
    
    -- Do the actual damage bit
    for i, v in ipairs(chain_of_targets) do
      if v:damageShield(creator:getNameForDeathMessage()) == false then
        v:kill()     
        gLastCorpsePosition = v.position:clone()
      end
    end
		
		startScreenShakeTimeAndPower(0.25, 0.5)
		gLightningTimeLeft = MAX_LIGHTING_TIME	
	---------------------------------
	elseif act[1] == "m" then
		playSound("Map")
		mapNearbyRooms(false)
		gMapEffectTimer = MAX_MAP_EFFECT_TIME
		gMapImage = getPixelMapOfLevel()
	---------------------------------
	elseif act[1] == "n" then
		playSound("Negate")

		local e = addExplosionWithDefinition("WarpCircle", creator.position)
		e.circleDrawRadius = NEGATE_RADIUS
		e.currentLife = 4 * FIXED_STEP
		e.totalLife = 4 * FIXED_STEP
		local neg2 = NEGATE_RADIUS * NEGATE_RADIUS
		for i, v in pairs(gCreatureArray) do
			if v.faction ~= creator.faction and v.isVisible and v.isChest == false then
				if creator.position:dist2(v.position) < neg2 then
					if creator.faction == FACTION_PLAYER then
						v.negateTimeLeft = MAX_NEGATE_TIME
					else
						v.negateTimeLeft = MAX_NEGATE_TIME / 2
					end
				end
			end
		end
	--------------------------------- 
	elseif act[1] == "o" then
		playSound("SummonOgre")
		local pos = getNearestCornerOfRoomWithIDFromPosition(creator.currentRoomID, creator.position)
		local a = createEnemyWithName("Ogre", pos)
		a.faction = creator.faction
		a.turncoatTimeLeft = MAX_TURNCOAT_TIME
	---------------------------------
	elseif act[1] == "p" then
		openPortal()
---------------------------------
	elseif act[1] == "r" then
		local a = createSpellFromDefinition("Rocket", creator, target)
		a.name = name
		a.performActionsOnDeath = {"QuakeBlast"}
	---------------------------------
	elseif act[1] == "q" then
		local a = createSpellFromDefinition("Quake", creator, nil)
		a.name = name
		a.performActionsOnDeath = {"QuakeBlast"}
		local e = addExplosionWithDefinition("Circle", a.position)
		e.circleDrawRadius = QUAKE_DAMAGE_RADIUS
		e.currentLife = a.lifespanInFrames * FIXED_STEP
		e.totalLife = a.lifespanInFrames * FIXED_STEP
	---------------------------------
	elseif act[1] == "s" then
		playSound("Shield")
--		creator:resetShield()
		creator:increaseShield()
	-----------------------	
	elseif act[1] == "t" then
		local room = nil
		local position = nil

		-- Teleport to auto-map if you're a player
		if creator.isPlayer then
			local unseen_rooms = {}
			for i, v in pairs(getRoomIDArray()) do
				if getRoomFromID(v).isMapped == false then table.insert(unseen_rooms, v) end
			end
			
			if #unseen_rooms > 0 then
				print("Unseen rooms at teleport moment")
				local id = getRandomMember(unseen_rooms)
				room = getRoomFromID(id)
				position = room:getRandomValidThingPositionWithinRoom()
			end
		else
			print("Non player trying to teleport")
		end
			
		if room == nil then
			print("NO unseen rooms at teleport moment")
			position = vector(math.random(gMapWidth-2) + 1, math.random(gMapHeight-2) + 1)
			position, room = findValidThingPositionInAnyRoomNear(position)
		end
		 
		if position == nil then 
			print("Illegal position at teleport moment") 
			return 
		end 
		if room == nil then print("Illegal room at teleport moment") 
			return 
		end 

		creator.position = position:clone()
		creator.mapX = math.floor(creator.position.x)
		creator.mapY = math.floor(creator.position.y)
		creator.drawX = math.floor(creator.position.x * gTileSize) / gTileSize
		creator.drawY = math.floor(creator.position.y * gTileSize) / gTileSize
		creator.velocity.x = 0
		creator.velocity.y = 0
		
		room:map(); 
		room:draw()
		gMapImage = getPixelMapOfLevel()

		creator:setRezInExplosion("PlayerRezIn")
		playSound("PlayerRezIn")
	-------------------------------
	elseif act[1] == "u" then 
		if creator.isPlayer then playSound("UpgradeSword") end
		creator.ultraswordTimeLeft = MAX_ULTRASWORD_TIME
	-------------------------------
	elseif act[1] == "v" then
		playSound("Void")
		creator.voidTimeLeft = MAX_VOID_TIME	
	-------------------------------
	elseif act[1] == "w" then
		local a = createSpellFromDefinition("Warp", creator, nil)
		a.name = name
		a.performActionsOnDeath = {"WarpBlast"}
		a.isWarp = true
		local e = addExplosionWithDefinition("WarpCircle", a.position)
		e.circleDrawRadius = WARP_RADIUS
		e.currentLife = a.lifespanInFrames * FIXED_STEP
		e.totalLife = a.lifespanInFrames * FIXED_STEP
	---------------------------------
	elseif act[1] == "x" then
		playSound("XRay")
		gXRayTime = MAX_X_RAY_TIME
	---------------------------------
	elseif act[1] == "y" then
		creator.yShotTime = MAX_Y_SHOT_TIME
	-----------------------	
	elseif act[1] == "z" then 
		local room = getRoomFromID(creator.currentRoomID)
		room:zap(creator)
		playSound("PlayerDie")
		startScreenShakeTimeAndPower(0.5,1)
	-----------------------
	elseif act[1] == "QuakeBlast" then
		local a = createSpellFromDefinition("QuakeBlast", 				creator, nil)
		a.faction = FACTION_NULL
		a.name = name
		destroyMapCellsWithinRange(math.floor(a.position.x), math.floor(a.position.y), a.physicsRadius)
		destroyDoorsWithoutWalls()
		gMapImage = getPixelMapOfLevel()
		startScreenShakeTimeAndPower(0.25, 0.5)
		gLightningTimeLeft = MAX_LIGHTING_TIME
		local e = addExplosionWithDefinition("Circle", a.position)
		e.circleDrawRadius = a.physicsRadius
		e.currentLife = gLightningTimeLeft
		e.totalLife = gLightningTimeLeft
		
	------------------------------
	elseif act[1] == "WarpBlast" then
		local a = createSpellFromDefinition("WarpBlast", 				creator, nil)
		startScreenShakeTimeAndPower(0.25, 0.5)
		gLightningTimeLeft = MAX_LIGHTING_TIME
		local pos, room = findValidThingPositionInAnyRoomNear(a.position:clone())
		
		local wr2 = WARP_RADIUS * WARP_RADIUS
		for i, v in ipairs(gCreatureArray) do
			if v.isActive and v.rezInExplosion == nil and v.quadName ~= nil and v.movementFunction ~= nil then
				if v.position:dist2(a.position) < wr2 then
					local vec = vector(math.random(-3, 3), math.random(-3, 3))
					v:teleportTo(findValidThingPositionInSpecificRoomNear(room, pos + vec))
				end
			end
		end
		
	-----------------------
	elseif act[1] == "JumpLand" then
		local a = createSpellFromDefinition("JumpLand", 				creator, nil)
		a.requiresLOSToTouch = true
		startScreenShakeTimeAndPower(0.25, 0.5)
		addExplosionWithDefinition("Circle", a.position)
	-----------------------
	elseif act[1] == "yShots" then
		playSound("YShot")
		local MAX_RANGE = 15
		local deg_offset = math.rad(gFixedFrameCount * 10)
		local deg_step = math.rad(360 / 3)
		for i = 1, 3 do
			local a = createSpellFromDefinition("YShot", creator, nil, nil)
			a.velocity = vector(0, -a.maxSpeed)
			a.velocity:rotate_inplace(deg_offset)
			deg_offset = deg_offset + deg_step
		end
	end
end

------------------------------------------------------
function createLightningSpark(original_creator, creator, target) -- Who shot it, who is it being chained by, who is it being chained to?
  local a = CActorMobile("Lightning", creator)

  a.faction = creator.faction
  a.lifespanInFrames = 5
  a.position = creator.position:clone()
  
  local vec = vector(0, -0.4) -- Face UP by default
  
  a.faceMovementDirection = false
  a.angleOffsetFromParent = 0
  a.movementFunction = nil
  if target ~= nil then
    a.target = target		
    a.targetPosition = target.position:clone()
  else
    a.target = nil		
    vec:rotate_inplace(creator.facingAngleInRads)
    a.targetPosition = a.position:clone() + vec * 4;
  end
    
  a.resizeToReachTarget = true

  -- Only attach the spell to the caster, as the targets are going to die this turn
  if creator == original_creator and target ~= nil then
    a:attachToActor(creator)
  end
  
  a:setAnimation("Lightning")
  table.insert(gSpellArray, a)
end

------------------------------------------------------
function getChainOfTargetsForLightning(faction, creator, chain_list)
	local target_list = {}
	local max_dist_sq = 10 * 10
  
  -- Get all valid targets creatures that aren't in the ignore list
  for i, v in ipairs(gCreatureArray) do
    if v.isVisible and v.isAlive and v.isDying == false and v.faction ~= faction and v.isChest == false then
      local ignore = false
      for i2, v2 in ipairs(chain_list) do 
        if v == v2 then 
          ignore = true 
        end 
      end
      
      if not ignore then
        local dist2 = (creator.position - v.position):len2()
        if dist2 < max_dist_sq then
          table.insert(target_list, v)
        end  
      end
    end
  end
  -- create a chain of targets
  local nearest_dist = 99999
  local new_target = nil
  for i, v in ipairs(target_list) do
    local dist2 = (creator.position - v.position):len2()
    if bresenham.los(math.floor(creator.position.x), math.floor(creator.position.y), math.floor(v.position.x), math.floor(v.position.y), isWalkable) then
      if dist2 < nearest_dist then
        nearest_dist = dist2
        new_target = v
      end  
    end
  end
  
  if new_target ~= nil then 
    table.insert(chain_list, new_target)
    getChainOfTargetsForLightning(faction, new_target, chain_list)
  end
end

-------------------------------
function genocideCreaturesBasedOnThis(creature)
	local last_pos = nil
	if creature:damageShield("") == false then 
		last_pos = creature.position:clone()
		creature:kill() 
	
		for i, v in ipairs(gCreatureArray) do
			if v.name == creature.name and v.isDying == false then 
				v:damageShield("", 9) 
				v:kill() 
			end						
		end
	end
	return last_pos
end
