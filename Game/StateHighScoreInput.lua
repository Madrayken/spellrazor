highScoreInput = {}

local gCursor = 1
local gName = {"_","_","_"}
local gHighScoreMusic = nil
function highScoreInput:enter()
	gScore = gScore + gGold -- Add this to the score
	
  gTitleImage = love.graphics.newImage("assets/Title.png")
	
	gBackgroundMusic:stop()
	stopAllSound()
		
	if checkForHighScores(gScore) == false then 
    gJustFinishedGame = true
		GameState.switch(menuState)
		return
	end
	gHighScoreMusic = love.audio.newSource("assets/SpellrazorMusicDeath.mp3", 'stream')
	gHighScoreMusic:setLooping(false)
	gHighScoreMusic:play()
	
	gName = {"_","_","_"}
	gCursor = 1
end

------------------------
local gExitTimer = 0
local menuStateTimeInSeconds = 0
function highScoreInput:update(dt)
	menuStateTimeInSeconds = menuStateTimeInSeconds + dt
	
	local t = menuStateTimeInSeconds * 100
	local r1, g1, b1 = HSL(((500 + 	t) % 1000) / 1000 * 255, 	255, 255)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local r2, g2, b2 = HSL(((200 + 	t) % 1000) / 1000 * 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local r3, g3, b3 = HSL(((0 + 	t) % 1000) / 1000 * 255, 	255, 32)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)

	menuBodyTextColour = {r2/256 * 255, g2/256*255, b2/256*255,255}
	if gShaderTitleRecolour ~= nil then
		gShaderTitleRecolour:send("topColour", 		{r1 / 256, g1 / 256, b1 / 256, 1})
		gShaderTitleRecolour:send("midColour", 		{r2 / 256, g2 / 256, b2 / 256, 1})
		gShaderTitleRecolour:send("bottomColour", {r3 / 256, g3 / 256, b3 / 256, 1})

		gShaderTitleRecolour:send("scroll", ((menuStateTimeInSeconds * 50) % 50) / 50)
	end
--[[	local t = menuStateTimeInSeconds * 100
	local r1, g1, b1 = HSL(((500 + 	t) % 1000) / 1000 * 255, 	255, 255)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local r2, g2, b2 = HSL(((200 + 	t) % 1000) / 1000 * 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local r3, g3, b3 = HSL(((0 + 	t) % 1000) / 1000 * 255, 	255, 32)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
]]--
	if gShaderGridColour ~= nil then

		gShaderGridColour:send("topColour", 	{r2/256, b2/256, g2/256, 1})
		gShaderGridColour:send("midColour", 	{r2/256*0.75, b2/256*0.75, g2/256, 1})
		gShaderGridColour:send("bottomColour",{r2/256*0.5, b2/256*0.5, g2/256, 1})
		gShaderGridColour:send("scroll", menuStateTimeInSeconds)
	end
	if gExitTimer > 0 then 
		gExitTimer = gExitTimer - dt
		if gExitTimer <= 0 then
			gExitTimer = 0
			gHighScoreMusic:stop()
			gHighScoreMusic = nil
      gJustFinishedGame = true
			GameState.switch(menuState)
		end
	end
end
-------------------------
local MAX_EXIT_TIME = 1
local MAX_HIGH_SCORE_TIMER = 8
local gGridTextYSize = 12

function highScoreInput:draw()
	love.graphics.setBackgroundColor(0, 0, 0)
	love.graphics.setColor(255,255,255,255)
	love.graphics.setFont(font)
	love.graphics.setShader()
	
	local t = menuStateTimeInSeconds * 100
	local r2, g2, b2 = HSL((((200 + 	t) % 1000) / 1000 * 255 + 160) % 255, 	255, 128)-- + (150 + (gDrawCount * 10) % 1000) / 1000 * 128)
	local linecolour = {r2/256 * 255, g2/256*255, b2/256*255,255}

	
	local lo = 0.2
	local mid = 0.3
	local hi = 0.4
	
	drawTronBackground({linecolour[1] * lo, linecolour[2] * lo, linecolour[3] * lo, 255}, menuStateTimeInSeconds, 0.6)
	love.graphics.setShader(gShaderTitleRecolour)		
	love.graphics.draw(gTitleImage, math.floor(gCanvasWidth * 0.5), math.ceil(gCanvasHeight * 0.15), 0, 1,1,gTitleImage:getWidth() / 2, gTitleImage:getHeight() / 2 )
	love.graphics.setShader()


	local str = "" .. gName[1].. " " .. gName[2] .. " " ..gName[3]
	wid = 1000
	
	local y = (gCanvasHeight * 0.35)
		
	local ind = gRainbowSetDark[(math.floor(menuStateTimeInSeconds * 10) % #gRainbowSetDark) + 1]
	love.graphics.setColor(gColourList[ind+1])
		
  love.graphics.printf("NEW HIGH SCORE!\n\n" .. gScore .. " ON " .. gCurrentLevelName .. "\n\nENTER YOUR INITIALS:", -(wid / 2) + (gCanvasWidth / 2), y, wid, 'center')
	
	ind = gRainbowSetWithWhite[(math.floor(menuStateTimeInSeconds * 15) % #gRainbowSetWithWhite) + 1]
	love.graphics.setColor(gColourList[ind+1])
  love.graphics.printf(str, -(wid / 2) + (gCanvasWidth / 2), y + 80, wid, 'center')
		
	
--	love.graphics.setColor(0,0,0,255) 
--	love.graphics.rectangle("fill", gCanvasWidth, 0, 80, gCanvasHeight)
--	love.graphics.rectangle("fill", 0, gCanvasHeight, gCanvasWidth, 80)
	love.graphics.setColor(255,255,255, 255)
end
------------------------------
function highScoreInput:keypressed(key)
	if key == "escape" then GameState.switch(menuState) return end
	
	if string.len(key) == 1 then
		local byte = string.byte(key)	
		if byte < 90 then byte = byte + 32 end
		-- ignore non-printable characters (see http://www.ascii-code.com/)
		if byte >= 97 and byte <= 122 then
			gName[gCursor] = string.char(byte-32)
			gCursor = gCursor + 1
			playSound("Arrow")
		end

		if gCursor > 3 then
			if gHasCheated then 
				gDeathMessage = "CHEATING"
			end
			gExitTimer = MAX_EXIT_TIME
			addHighScore(""..gName[1]..gName[2]..gName[3], gScore,  gDeathMessage, gCurrentLevelName)
		end
	end
end	