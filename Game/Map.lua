local class = require 'library.middleclass'

require 'game.CMapCell'
require 'game.CRoom'
require 'game.CDoorway'
require 'library.Tools'


local DOOR_THICKNESS = 4

local map = {}-- stores tiledata
local nodeMap = {} -- stores node data (a larger-tile sized navigation unit)
local lookAheadAmount = NODE_SIZE / 2

local roomTable = {}
local doorways = {}


local tileQuads = {} -- parts of the tileset used for different tiles
local tilesetImage

gMapX = 0
gMapY = 0 -- view x,y in tiles. can be a fractional value like 3.25.
gNumberOfRooms = 0
 
gDebugRoomID = 0 
 
function clearMap()
	roomTable = {}
	doorways = {}
end
 
-------------------- 
function createMapCells()
	map = {}
	nodeMap = {}
  for x=1, gMapWidth do
    map[x] = {}
    for y=1,gMapHeight do
			map[x][y] = CMapCell() -- fill everywhere with black, and solid
    end
  end
	
	for x=1, MAX_NODES_WIDE do
    nodeMap[x] = {}
    for y=1,MAX_NODES_HIGH do
			nodeMap[x][y] = {false,false,false,false,false,false,false,false} -- NESW passability
    end
  end
end

-------------------- 
-- We start with the basic idea of 'nodes' equally-sized spaces surrounded by a ring of 'wall'
-- In this case, our 'node' spaces are 2 blocks wide, surrounded by a ring of wall 1 thick
--      |--|--|
--      |  |  |
--      |--|--| 
gBSPSpaces = {}

local nextRoomID = 0
CBSPSpace = class('CBSPSpace')

function CBSPSpace:initialize(room_id, x1,y1,x2,y2)
	-- These BSP rooms are totally inclusive. 
	-- both x1, y1 and x2, y2 are INSIDE the bsp
	self.roomID = room_id
	self.x1 = x1; self.y1 = y1; self.x2 = x2; self.y2 = y2;
	--self.cx = math.floor((x1 + x2) / 2)
	--self.cy = math.floor((y1 + y2) / 2)
	self.dividerX1 = nil
	self.dividerX2 = nil
	self.dividerY1 = nil
	self.dividerY2 = nil

	self.branch1 = nil
	self.branch2 = nil
end

function CBSPSpace:getMapCoords()
	local x1 = self.x1 * NODE_SIZE
	local y1 = self.y1 * NODE_SIZE
	local x2 = self.x2 * NODE_SIZE + NODE_SIZE
	local y2 = self.y2 * NODE_SIZE + NODE_SIZE
	return x1,y1,x2,y2
end
----------------------------
function createLevel()
	clearMap()
	-- Our generation algorithm will tell us which walls to tear down
	-- To do this, We create a separate, notional BSPTree of subdivided spaces 
	nextRoomID = 1	
		
	-- Now we know this, create some notional BSP spaces. We store them by subdivision size for easy access later
	gBSPSpaces = {}
	
	local first_space = CBSPSpace(nextRoomID, 0,0,MAX_NODES_WIDE-1, MAX_NODES_HIGH-1)

	table.insert(gBSPSpaces, first_space)
	
	local subdivide = true
	while subdivide == true do
		subdivide = false
		for i, v in ipairs(gBSPSpaces) do
			if v.branch1 == nil then
				if (v.x2 - v.x1) + 1 > MAX_ROOM_SIZE_X or (v.y2 - v.y1) + 1 > MAX_ROOM_SIZE_Y then
					subdivideTree(v)
					subdivide = true
				end
			end
		end
	end
		
	-- Okay, we now have a set of BSP spaces, all linked together. Transform them into actual CRoom classes
	for i, v in ipairs(gBSPSpaces) do
		if v.branch1 == nil then
			print(string.format("BSP ".. v.roomID .." at: %f %f %f %f of size %f %f ", v.x1, v.y1, v.x2, v.y2, v.x2 - v.x1, v.y2 - v.y1))

			local x1, y1, x2, y2 = v:getMapCoords()
			roomTable[v.roomID] = CRoom(v.roomID, x1, y1, x2, y2)

			print(string.format("RoomID: " .. v.roomID .. " at: %f %f %f %f", x1, y1, x2, y2))
			
			gNumberOfRooms = gNumberOfRooms + 1
		end
	end
	
	for i, v in ipairs(gBSPSpaces) do
		if v.branch1 ~= nil then
			createDoorAlongBSPSplit(v)
		end
	end
	
	-- Try adding one random room-door to each room
	for k, v in pairs(roomTable) do
		createDoorAlongWallInRoom(v)
		
		if v.w >= NODE_SIZE * 3 and v.h >= NODE_SIZE * 3 and math.random() > 0.85 then
			createPillarsInRoom(v)
		end
	end
	
	for i, v in ipairs(doorways) do
		if math.random() < 0.5 then
			v:wedge()
		end
	end
	
	for y = 0, gMapHeight-1 do
		for x = 0, gMapWidth-1 do
			setMapTileAndColourBasedOnSolidityAndSeenStatus(x, y, false, false)
		end
  end	
	
	for y = 1, gMapHeight-2 do
		for x = 1, gMapWidth-2 do
			updateCellConnectionsXY(x, y)
		end
  end	
	
	-- Create a map of valid links from node to node
	for x = 0, MAX_NODES_WIDE-1 do
		for y = 0, MAX_NODES_HIGH-1 do			
			updateNodeConnectionsXY(x,y)
		end
	end
end
-------------------------
function updateCellConnectionsXY(x,y)
	local cell = getMapCell(x,y)
	local conn = cell.connections
	for i, v in ipairs(gAround8) do
		local c = getMapCell(x + v[1],y + v[2])
		if c.isSolid == false then 
			cell.connections[i] = true
		elseif c.isDoor == true and c.isLocked == false then 
			cell.connections[i] = true  -- Make links to doorways valid
		else 	
			cell.connections[i] = false 
		end
	end
	
	-- Ensure diagonals are clear by looking at neighbours of a diagonal. This stops us trying to move through the corner of a pillar, for example
	for i = 1, #gAround8, 2 do
		local i_left = i - 1; if i_left == 0 then i_left = 8 end
		local i_right = i + 1; if i_right == 9 then i_right = 1 end
		if conn[i] == true and (conn[i_left] == false or conn[i_right] == false) then 
			conn[i] = false 
		end
	end
end
-------------------------
function cellIsNavigable(c)
	if c.isSolid == false or (c.isDoor == true and c.isLocked == false) then  return true end
	return false
end
-------------------------
function nodeCenterIsNavigable(x,y)
	local navigable = true
	local cx = x * NODE_SIZE + 1 -- get the central cell of this node
	local cy = y * NODE_SIZE + 1 
	for y = cy, cy+NODE_SIZE-2 do
		for x = cx, cx+NODE_SIZE-2 do
			if cellIsNavigable(getMapCell(cx,cy)) == false then 
				navigable = false; 
				break 
			end
		end
	end
	
	return navigable
end

function updateNodeConnectionsXY(x,y)
	local node = getNodeConnectionsXY(x,y)
	local cx = x * NODE_SIZE + NODE_SIZE / 2 -- get the central cell of this node
	local cy = y * NODE_SIZE + NODE_SIZE / 2
	
	for i, v in ipairs(gAround8) do
		node[i] = false
		
		local c = getMapCell( math.floor(cx + v[1] * lookAheadAmount), math.floor(cy + v[2] * lookAheadAmount))
		if cellIsNavigable(c) and nodeCenterIsNavigable(x + v[1], y + v[2]) then   
			node[i] = true 
		end
	end
	-- Ensure diagonals are clear by looking at neighbours of a diagonal. This stops us trying to move through the corner of a pillar, for example
	for i = 1, #gAround8, 2 do
		local i_left = i - 1; if i_left == 0 then i_left = 8 end
		local i_right = i + 1; if i_right == 9 then i_right = 1 end
		if node[i] == true and (node[i_left] == false or node[i_right] == false) then node[i] = false end
	end
end
-------------------------
function subdivideTree(room, level_of_subdivision)
-- Rooms are stored in the BSP as x1,y1 (top left) to x2,y2 (bottom right)
	local x1 = room.x1 
	local y1 = room.y1
	local x2 = room.x2
	local y2 = room.y2
	
	if x2-x1 > y2-y1 then
		local variance = math.floor((x2 - x1) * 0.5)
		local x_split_pos = math.floor((x1 + x2) / 2) - math.floor(variance * (0.5)) + randomIntNormalBetween(0, variance)
		if x_split_pos == x1 or x_split_pos == x2 then  -- Ensure the room's not too small
			print("Room too small") 
			return 
		end 
		
		room.dividerX1 = x_split_pos+1
		room.dividerY1 = y1
		room.dividerX2 = x_split_pos+1
		room.dividerY2 = y2 
		room.dividerIsHorizontal = false
		
		room.branch1 = CBSPSpace(nextRoomID + 1, x1,						y1,	x_split_pos,	y2)
		room.branch2 = CBSPSpace(nextRoomID + 2, x_split_pos+1, y1, x2, 					y2)
	else
		local variance = math.floor((y2 - y1) * 0.5)
		local y_split_pos = math.floor((y1 + y2) / 2)  - math.floor(variance * (0.5)) + randomIntNormalBetween(0, variance)-- randomIntNormalBetween(y1+1, y2-1))
		if y_split_pos == y1 or y_split_pos == y2 then -- Ensure the room's not too small
			print("Room too small")
			return 
		end
		
		room.dividerX1 = x1
		room.dividerY1 = y_split_pos+1
		room.dividerX2 = x2
		room.dividerY2 = y_split_pos+1
		room.dividerIsHorizontal = true

		room.branch1 = CBSPSpace(nextRoomID + 1, x1, y1, 						x2, y_split_pos)
		room.branch2 = CBSPSpace(nextRoomID + 2, x1, y_split_pos+1, x2, y2)
	end
	-- Insert these nodes into the BSP at the correct level 
	table.insert(gBSPSpaces, room.branch1)
	table.insert(gBSPSpaces, room.branch2)
	
	nextRoomID = nextRoomID + 2 -- 2 because we made 2 branches
end

-------------------------
function getNodeXYFromCellXY(x,y)
	return math.floor(x / NODE_SIZE), math.floor(y/NODE_SIZE)
end

-------------------------
function snapXYToNodeCellXY(x,y)
	return math.floor(x / NODE_SIZE) * NODE_SIZE, math.floor(y / NODE_SIZE) * NODE_SIZE
end

function snapXYToNodeCellCenterXY(x,y)
	local half = NODE_SIZE / 2
	x, y = snapXYToNodeCellXY(x-0.5, y-0.5)
	x = x + half + 0.5
	y = y + half + 0.5
	return x,y
end
-------------------------
function createDoorAlongBSPSplit(bsp)
	local old_room_id = nil
	local current_room_id = nil
	
	if bsp.dividerIsHorizontal then
		local x = math.random(bsp.dividerX1, bsp.dividerX2-1)
		local y = bsp.dividerY1
		x = x * NODE_SIZE
		y = y * NODE_SIZE
		x = x + 1 -- Step into the square by one node
		old_room_id 		= getMapCell(x, y-1).roomID
		current_room_id = getMapCell(x, y+1).roomID
		addDoorway(x, y, IS_HORIZONTAL, old_room_id, current_room_id)
	else
		local x = bsp.dividerX1 
		local y = math.random(bsp.dividerY1, bsp.dividerY2-1)
		x = x * NODE_SIZE
		y = y * NODE_SIZE
		y = y + 1 -- Step into the square by one node

		old_room_id 		= getMapCell(x-1, y).roomID
		current_room_id = getMapCell(x+1, y).roomID
		addDoorway(x, y, IS_VERTICAL, old_room_id, current_room_id)
	end
		
	if roomTable[current_room_id]:isNeighbourOfRoom(roomTable[old_room_id]) == false then
		roomTable[current_room_id]:addNeighbour(roomTable[old_room_id]) 
		roomTable[old_room_id]:addNeighbour(roomTable[current_room_id]) 
	end
end

--------------------------------------------
function createPillarsInRoom(r)
	local x1 = math.random(r.x1 + (NODE_FLOOR_SIZE + 1), r.x2-(NODE_FLOOR_SIZE + 2))
	local y1 = math.random(r.y1 + (NODE_FLOOR_SIZE + 1), r.y2-(NODE_FLOOR_SIZE + 2))
	x1, y1 = snapXYToNodeCellXY(x1,y1)
	x1 = x1 - 1
	y1 = y1 - 1
	
	for i = 1, NODE_FLOOR_SIZE + 2 do
		for j = 1, NODE_FLOOR_SIZE + 2 do
			setMapCellSolidity(x1+i, y1+j, true)
		end
	end
end

--------------------------------------------
-- We can use this to create a door in an existing room
function createDoorAlongWallInRoom(room)
	local other_room_id = nil
	local current_room_id = room.roomID
	
	local index = math.random(4)
	local wall_offsets = {
		{room.x1, room.x2 - NODE_SIZE, room.y1, room.y1}, -- Door along top
		{room.x1, room.x2 - NODE_SIZE, room.y2, room.y2}, -- Door along bottom
		{room.x2, room.x2, room.y1, room.y2 - NODE_SIZE}, -- Door along right
		{room.x1, room.x1, room.y1, room.y2 - NODE_SIZE}, -- Door along left
	}
	local neighbour_offsets = {{0,-1}, {0,1}, {1,0}, {-1,0}}
	
	if index == 1 then
		if room.y1 == 0 then return end
	elseif index == 2 then 
		if room.y2 == gMapHeight-1 then return end
	elseif index == 3 then 
		if room.x2 == gMapWidth-1 then return end
	else
		if room.x1 == 0 then return end
	end
	
	local x = math.random(wall_offsets[index][1], wall_offsets[index][2])
	local y = math.random(wall_offsets[index][3], wall_offsets[index][4])

	x = math.floor(x / NODE_SIZE) * NODE_SIZE
	y = math.floor(y / NODE_SIZE) * NODE_SIZE

	local nx = x + neighbour_offsets[index][1]
	local ny = y + neighbour_offsets[index][2]

	other_room_id = getMapCell(nx, ny).roomID
--	print("With index " .. index .. " At cell " .. nx .. ", " .. ny .. ", Other room ID " .. other_room_id)
	
	if other_room_id ~= nil and other_room_id ~= 0 then
		if room:isNeighbourOfRoomWithID(other_room_id) then return false end
			
		if index <= 2 then
			addDoorway(x+1, y, IS_HORIZONTAL, other_room_id, current_room_id)
		else
			addDoorway(x, y+1, IS_VERTICAL, other_room_id, current_room_id)
		end
			
		if roomTable[current_room_id]:isNeighbourOfRoom(roomTable[other_room_id]) == false then
			roomTable[current_room_id]:addNeighbour(roomTable[other_room_id]) 
			roomTable[other_room_id]:addNeighbour(roomTable[current_room_id]) 
			
			return true
		end
	end	
	
	return false
end
 
------------------------- 
function addDoorway(x,y, is_horiz, from_room_id, to_room_id) 
	local d = CDoorway(x,y, is_horiz, from_room_id, to_room_id)
	table.insert(doorways, d)
	roomTable[from_room_id]:addDoorway(d)
	roomTable[to_room_id]:addDoorway(d)
end

function isPositionIsNearLockedDoorInRoomWithID(pos, id)
	if id == nil then return nil end
	local room = getRoomFromID(id)
	local range = 2
	if room ~= nil then
		local door = room:getNearestDoorWithinRange(pos, range)
		if door ~= nil and door.isLocked == true then return true end
	end
	return false
end

-------------------- 
function setupTileset()
  tilesetImage = love.graphics.newImage( "assets/ColouredTileset.png" )
  tilesetImage:setFilter(gCanvasType, gCanvasType) -- this "linear filter" removes some artifacts if we were to scale the tiles
 	
	for y = 0, gNumberOfColourTypes-1 do
		for x = 0, gNumberOfTileTypes-1 do
			quad_label = (y * gNumberOfTileTypes + x) + 1
			tileQuads[quad_label] = love.graphics.newQuad(x * gTileSize, y * gTileSize, gTileSize, gTileSize, tilesetImage:getWidth(), tilesetImage:getHeight())
		end
	end
 
  tilesetBatch = love.graphics.newSpriteBatch(tilesetImage, gTilesDisplayWidth * gTilesDisplayHeight)
  updateTilesetBatch()
end

--------------------
function setMapTile(x,y, tile)
	-- The game deals with a map 0->Mapsize-1
	-- The data deals with the map 1-Mapsize
	local i = map[x+1][y+1] 
	i.tileIndex = tile
end

function setMapTileAndColour(x,y, tile, col_index)
	-- The game deals with a map 0->Mapsize-1
	-- The data deals with the map 1-Mapsize
	local i = map[x+1][y+1] 
	i.tileIndex = tile
	i.colourIndex = col_index
end


function setMapCellColour(x,y, col_index)
	-- The game deals with a map 0->Mapsize-1
	-- The data deals with the map 1-Mapsize
	local i = map[x+1][y+1] 
	i.colourIndex = col_index
end

function setMapCellAsDoor(x,y)
	local i = map[x+1][y+1] 
	i.isDoor = true
end

function setMapCellAsLocked(x,y)
	local i = map[x+1][y+1] 
	i.isLocked = true
end

function setMapCellAsUnlocked(x,y)
	local i = map[x+1][y+1] 
	i.isLocked = false
end

--------------------
function setMapTileAndColourBasedOnSolidityAndSeenStatus(x,y, is_visible, ignore_floors)
	ignore_floors = ignore_floors == nil and true or ignore_floors
	
	x=x+1
	y=y+1
	local i = map[x][y] 
	local tile = 0
	local col_index = 1 -- black!
	-- Look for blank tiles around this one	
	if i.isSolid == false or i.isDoor then 
		local x2 = (x - 1) % NODE_SIZE
		local y2 = (y - 1) % NODE_SIZE
		if x2 == 0 then
			tile = 0
		elseif y2 == 0 then 
			tile = 0
		elseif x2 == 2 and y2 == 2 then
			tile = 0
		else
			tile = 17 + ((y2-1) * 3) + (x2-1)
		end
--		tile = 0
		if i.isMapped then
			if is_visible then
				col_index = CURRENTLY_VISIBLE_FLOOR_COLOUR
			else
				col_index = CURRENTLY_OUT_OF_SIGHT_FLOOR_COLOUR
			end
		end
	else
		if y - 1 >=1  					and (map[x][y-1].isDoor or map[x][y-1].isSolid == false) and map[x][y-1].isMapped == true then tile = tile + 1 end
		if y + 1 <= gMapHeight 	and (map[x][y+1].isDoor or map[x][y+1].isSolid == false) and map[x][y+1].isMapped == true then tile = tile + 2 end
		if x - 1 >=1  					and (map[x-1][y].isDoor or map[x-1][y].isSolid == false) and map[x-1][y].isMapped == true then tile = tile + 4 end
		if x + 1 <= gMapWidth 	and (map[x+1][y].isDoor or map[x+1][y].isSolid == false) and map[x+1][y].isMapped == true then tile = tile + 8 end
		
		if tile == 0 then tile = 15 end
		
		if i.isMapped then
			col_index = WALL_COLOUR
		end
	end
	if not ignore_floors or i.isSolid then
		i.tileIndex = tile + 1 -- convert to lua
	end
	i.colourIndex = col_index
end

----------------------------------
function setMapCellMapped(x,y, is_seen)
	local i = map[x+1][y+1] 
	i.isMapped = is_seen
end
----------------------------------
function setMapCellRoomID(x,y, room_id)
	-- The game deals with a map 0->Mapsize-1
	-- The data deals with the map 1-Mapsize
	local i = map[x+1][y+1] 
	i.roomID = room_id
end

----------------------------------
function setMapCellSolidity(x,y, flag)
	local i = map[x+1][y+1] 
	i.isSolid = flag
end

function isPositionWithinMapBounds(x,y)
	if x < 0 or y < 0 then return false end
	if x > gMapWidth-1 or y > gMapHeight-1 then return false end
  return true
end


function getMapCell(x,y)
	return map[x+1][y+1] 
end
 
function getMapCellConnectionsXY(x,y)
	return map[x+1][y+1].connections
end
 
 
function getNodeConnectionsXY(x,y)
	return nodeMap[x+1][y+1] 
end 
 
-------------------- 
function getRandomRoom()
	return getRandomMember(roomTable)
end

-------------------- 
function setupPlayerPositionAndReturnRoom(vec)
  if vec ~= nil then
    local cell = getMapCell(math.floor(vec.x), math.floor(vec.y))
    room = getRoomFromID(cell.roomID)
    gPlayer.position.x = math.floor(vec.x)
    gPlayer.position.y = math.floor(vec.y)
  else
    room = getRandomRoom()
    gPlayer.position.x = (room.x1 + room.x2) / 2
    gPlayer.position.y = (room.y1 + room.y2) / 2
    gPlayer.position = findValidThingPositionInAnyRoomNear(gPlayer.position)
	end

	gPlayer.mapX = math.floor(gPlayer.position.x)
	gPlayer.mapY = math.floor(gPlayer.position.y)
	gPlayer.drawX = math.floor(gPlayer.position.x * gTileSize) / gTileSize
  gPlayer.drawY = math.floor(gPlayer.position.y * gTileSize) / gTileSize

	gFirstUpdate = true
	
	-- Ensure the start room is always safe and the doors are closed
	for i, v in pairs(room.doorArray) do
		v:removeWedge()
	end
	
	return room
end

--------------------------------
function setupExitPosition()
	room = getRandomMember(roomTable)
	
	gPortalPosition.x = (room.x1 + room.x2) / 2
	gPortalPosition.y = (room.y1 + room.y2) / 2
	
	return findValidThingPositionInAnyRoomNear(gPortalPosition)
end

--------------------------------
function drawLevel()
  love.graphics.draw(tilesetBatch, math.floor(-(gMapX%1)*gTileSize), math.floor(-(gMapY%1)*gTileSize))
end

--------------------------------
function playerRoomChange(old_id, new_id)
	if old_id ~= nil and old_id ~= 0 then
		roomTable[old_id].isVisible = false
	end
	
	roomTable[new_id].isVisible = true
end

--------------------------------
function hideRoomsByDoors(room_id)
	if room_id == nil or room_id == 0 then return end
	
	local room = roomTable[room_id]
	for i, v in ipairs(room.doorArray) do
		if v.roomID1 ~= room_id then roomTable[v.roomID1].isVisible = false end
		if v.roomID2 ~= room_id then roomTable[v.roomID2].isVisible = false end
	end
end
--------------------------------
local DOOR_OPEN_FRACTION_ALLOWING_VIEW = 0.25
function mapAndRevealRoomsByDoors(room_id)
	local mapped_a_room = false
	if room_id == 0 then return end
	local room = roomTable[room_id]
	for i, v in ipairs(room.doorArray) do
		local dx = math.abs(gPlayer.mapX - v.cx)
		local dy = math.abs(gPlayer.mapY - v.cy)
		local DIST = 8
		if (dx < DIST and dy < DIST and v.openFraction > DOOR_OPEN_FRACTION_ALLOWING_VIEW) or v.isWedged then
			if v.roomID1 ~= room_id then
				if roomTable[v.roomID1]:map() then mapped_a_room = true end
				roomTable[v.roomID1].isVisible = true
			else
				if roomTable[v.roomID2]:map() then mapped_a_room = true end
				roomTable[v.roomID2].isVisible = true
			end
		end
	end
	
	gMapImage = getPixelMapOfLevel()
	return mapped_a_room
end

--------------------------------
function mapAndRevealRoomsInSightLine(x,y, current_room_id)
	local mapped_a_room = false
	local VIEW_DIST = 22
	
	for i, v in ipairs(gAround4Cardinal) do
		local dx = x
		local dy = y
		for z = 1, VIEW_DIST do
			dx = dx + v[1]
			dy = dy + v[2]
			
			if dx > gMapWidth-1 or dx < 1 or dy < 1 or dy > gMapHeight-1 then break end
	
			local c = getMapCell(dx, dy)
			if c.isSolid then 
				break 
			elseif c.roomID ~= current_room_id and c.roomID ~= 0 then
				if roomTable[c.roomID]:map() then 
					mapped_a_room = true
				end
				roomTable[c.roomID].isVisible = true
			end
		end
	end
	
	if mapped_a_room then gMapImage = getPixelMapOfLevel() end
	return mapped_a_room
end

--------------------------------
function cellXYToScreenXY(x, y)
	return  math.floor((x - gMapX) * gTileSize), math.floor((y - gMapY) * gTileSize)
end

function screenXYToCellXY(x, y)
	return (x / gTileSize) + gMapX,  (y / gTileSize) + gMapY
end

function screenDistanceXYToCellDistanceXY(x, y)
	return (x / gTileSize),  (y / gTileSize)
end

--------------------------------
-- central function for moving the map
function moveMap(dx, dy, dt)
  oldMapX = gMapX
  oldMapY = gMapY
	
	if gCurrentScreenShakeTime > 0 then
		gCurrentScreenShakeTime = gCurrentScreenShakeTime - (1 * dt)
		if gCurrentScreenShakeTime < 0 then gCurrentScreenShakeTime = 0 end
		local size = gCurrentScreenShakePower * gCurrentScreenShakeTime / gTotalScreenShakeTime
		dx = dx + ((math.random() * gCurrentScreenShakePower) - gCurrentScreenShakePower / 2)
		dy = dy + ((math.random() * gCurrentScreenShakePower) - gCurrentScreenShakePower / 2)
	end
	
	gMapX = gMapX + dx
  gMapY = gMapY + dy
	
  -- only update if we actually moved
--[[  if math.floor(gMapX) ~= math.floor(oldMapX) 
	or math.floor(gMapY) ~= math.floor(oldMapY) then
    updateTilesetBatch()
  end]]--
end

--------------------
function getRoomFromID(id)
	return roomTable[id]
end
--------------------
function getRoomIDArray()
	local array = {}
	for k, v in pairs(roomTable) do
		table.insert(array, k)
	end
	return array
end

-------------------- 
local gCamX = 0
local gCamY = 0
local offsetX = 0
local offsetY = 0

local lastLookDirectionX = 0
local lastLookDirectionY = 0
local DISTANCE_TO_DOOR_SQ = 5 * 5

function cameraFixedPointUpdate(dt)
  local x_moving = true
  local y_moving = true
  if math.abs(gPlayer.velocity.x) < 0.01 and gPlayer.moveInputX == false then x_moving = false end
  if math.abs(gPlayer.velocity.y) < 0.01 and gPlayer.moveInputY == false then y_moving = false end  

  if x_moving == false then
    offsetX = offsetX * 0.98
  end
  if y_moving == false then
    offsetY = offsetY * 0.98
  end
end

function cameraLookAtPlayer(dt)
	local bias = 0.98
	local inv_bias = 1 - bias
	
	local sx, sy = screenDistanceXYToCellDistanceXY(gCanvasWidth / 2, gCanvasHeight / 2)  
	
	-- We now have an average position of where we are in relation to doors
	if gFirstUpdate then
		gFirstUpdate = false -- DOESN'T STOP FIRST MAP POSITION BEING INVALID
		gCamX = gPlayer.drawX
		gCamY = gPlayer.drawY
		offsetX = 0
		offsetY = 0
		lastLookDirectionX = 0
		lastLookDirectionY = 0
	else
		if gScreenRotate > 0 then
			offsetX = offsetX * 0.7
			offsetY = offsetY * 0.7			
		elseif gPlayer.rezInExplosion == nil then 
			local X_SLEW_SPEED = 3
			local Y_SLEW_SPEED = 3
			local X_RETURN_SPEED = 2
			local Y_RETURN_SPEED = 2
			
			local X_PEEK = 4
			local Y_PEEK = 3

			local x_moving = true
			local y_moving = true
			-- Use the previous acceleration for 'look' direction
			-- But use the velocity to decide if we're actually NOT moving
			if math.abs(gPlayer.velocity.x) < 0.01 and gPlayer.moveInputX == false then x_moving = false end
			if math.abs(gPlayer.velocity.y) < 0.01 and gPlayer.moveInputY == false then y_moving = false end

			if x_moving and gPlayer.lastAcceleration.x ~= 0 then lastLookDirectionX = gPlayer.lastAcceleration.x / math.abs(gPlayer.lastAcceleration.x) end
			if y_moving and gPlayer.lastAcceleration.y ~= 0 then lastLookDirectionY = gPlayer.lastAcceleration.y / math.abs(gPlayer.lastAcceleration.y) end
			
			if x_moving == false then
--				if offsetX < 0 then offsetX = offsetX + dt * X_RETURN_SPEED if offsetX > 0 then offsetX = 0 end end
--				if offsetX > 0 then offsetX = offsetX - dt * X_RETURN_SPEED if offsetX < 0 then offsetX = 0 end end
			else
				offsetX = offsetX + lastLookDirectionX * dt * X_SLEW_SPEED
			end
			
			if y_moving == false then
--				if offsetY < 0 then offsetY = offsetY + dt * Y_RETURN_SPEED if offsetY > 0 then offsetY = 0 end end
--				if offsetY > 0 then offsetY = offsetY - dt * Y_RETURN_SPEED if offsetY < 0 then offsetY = 0 end end
			else
				offsetY = offsetY + lastLookDirectionY * dt * X_SLEW_SPEED
			end
		
			local max_x_peek = X_PEEK-- + (X_PEEK / 2) * (1.0 - math.min(math.abs(gPlayer.velocity.x) / 2, 1))
			local max_y_peek = Y_PEEK-- + (Y_PEEK / 2) * (1.0 - math.min(math.abs(gPlayer.velocity.y) / 2, 1))

			offsetX = math.clamp(offsetX, -max_x_peek, max_x_peek)
			offsetY = math.clamp(offsetY, -max_y_peek, max_y_peek)
		end
		
		gCamX = (gPlayer.drawX + offsetX)
		gCamY = (gPlayer.drawY + offsetY)

	end

	moveMap(gCamX - (gMapX + sx), gCamY - (gMapY + sy), dt)
end

--------------------
function prepareAllDoorsForClosing()
	for i, door in ipairs(doorways) do
		door.isCreatureNear = false
	end
end
--------------------
function getDoorBetweenRoomIDandRoomID(id1, id2)
  for i, v in ipairs(doorways) do
    if (v.roomID1 == id1 and v.roomID2 == id2) or (v.roomID1 == id2 and v.roomID2 == id1) then
      return v
    end
  end
  return nil
end

function doorFixedUpdate()
	for i, v in ipairs(doorways) do
		v:fixedUpdate()
	end
end

function roomFixedUpdate()
	for k, v in pairs(roomTable) do
		v:fixedUpdate()
	end
end

--------------------
local DOOR_OPENING_DIST_ON_AXIS = 2
local DOOR_OPENING_DIST_OFF_AXIS = 3
function setAllDoorsNearCreaturesToOpening()
	for i, v in ipairs(gCreatureArray) do
		if v.isPlayer or gFreezeUntilEnemySpotted == false then -- Stop creatures who spawned near player start point opening door and attacking him due to LOS!
			if v.currentRoomID == 0 then return end
			local room = roomTable[v.currentRoomID]
		
			for i2, door in ipairs(room.doorArray) do
				if door.isHorizontal then
					if math.abs(v.position.x - door.cx) < DOOR_OPENING_DIST_ON_AXIS and math.abs(v.position.y - door.cy) < DOOR_OPENING_DIST_OFF_AXIS then
						door.isCreatureNear = true
					end
				else
					if math.abs(v.position.x - door.cx) < DOOR_OPENING_DIST_OFF_AXIS and math.abs(v.position.y - door.cy) < DOOR_OPENING_DIST_ON_AXIS then
						door.isCreatureNear = true
					end
				end
			end
		end
	end
end

--------------------
function doorFixedUpdate()
	for i, door in ipairs(doorways) do
		door.isVisible = false
	end
		
	for i, door in ipairs(doorways) do
		if roomTable[door.roomID1].isVisible or roomTable[door.roomID2].isVisible then
			door.isVisible = true
		end
		door:fixedUpdate()
	end
end
-------------------- 
function updateTilesetBatch()
  tilesetBatch:clear()
  for x=0, gTilesDisplayWidth-1 do
    for y=0, gTilesDisplayHeight-1 do
			x2 = x+math.floor(gMapX+1)
			y2 = y+math.floor(gMapY+1)
			if x2 > 0 and x2 <= gMapWidth and y2 > 0 and y2 <= gMapHeight then
				local i = map[x2][y2]
				tilesetBatch:add(tileQuads[i.tileIndex + (gNumberOfTileTypes * i.colourIndex)], x*gTileSize, y*gTileSize)
			end
    end
  end
  tilesetBatch:flush()
end

function getRandomRoom()
	return getRandomMember(roomTable)
end


function getRandomRoomAvoidingRoomIDs(room_id_array)
	local rk = nil
	local keys = {}
	for k, v in pairs(roomTable) do
		local found = false
		for i, v2 in ipairs(room_id_array) do
			if v.roomID == v2 then 
				found = true
				break
			end
		end
		if found == false then table.insert(keys, k) end
	end
	
	keys = getRandomisedArray(keys)
	return roomTable[keys[1]]
end

-----------------------------------
function pickRandomNeighbouringRoom(room_id)
	local room = getRoomFromID(room_id)
	local neighbour_id = room:getRandomNeighbourID()
	if neighbour_id ~= 0  then return getRoomFromID(neighbour_id) end
	return nil
end

-----------------------------------
function getFurthestCornerOfRoomWithIDFromPosition(room_id, pos)
	local room = getRoomFromID(room_id)
	local new_pos = pos:clone()
	if math.abs(pos.x-room.x1) > math.abs(pos.x-room.x2) then new_pos.x = room.x1 else new_pos.x = room.x2 end -- Furthest
	if math.abs(pos.y-room.y1) > math.abs(pos.y-room.y2) then new_pos.y = room.y1 else new_pos.y = room.y2 end -- Furthest

	return findValidThingPositionInSpecificRoomNear(room, new_pos)
end

-----------------------------------
function getNearestCornerOfRoomWithIDFromPosition(room_id, pos)
	local room = getRoomFromID(room_id)
	local new_pos = pos:clone()
	if math.abs(pos.x-room.x1) > math.abs(pos.x-room.x2) then new_pos.x = room.x2 else new_pos.x = room.x1 end -- Nearest
	if math.abs(pos.y-room.y1) > math.abs(pos.y-room.y2) then new_pos.y = room.y2 else new_pos.y = room.y1 end -- Nearest

	return findValidThingPositionInSpecificRoomNear(room, new_pos)
end
-----------------------------------
function getOppositeVerticalCornerOfRoomWithIDFromPosition(room_id, pos)
	local room = getRoomFromID(room_id)
	local new_pos = pos:clone()
	if math.abs(pos.x-room.x1) > math.abs(pos.x-room.x2) then new_pos.x = room.x2 else new_pos.x = room.x1 end -- Nearest
	if math.abs(pos.y-room.y1) > math.abs(pos.y-room.y2) then new_pos.y = room.y1 else new_pos.y = room.y2 end -- Furthest

	return findValidThingPositionInSpecificRoomNear(room, new_pos)
end

-----------------------------------
function getOppositeHorizontalCornerOfRoomWithIDFromPosition(room_id, pos)
	local room = getRoomFromID(room_id)
	local new_pos = pos:clone()
	if math.abs(pos.x-room.x1) > math.abs(pos.x-room.x2) then new_pos.x = room.x1 else new_pos.x = room.x2 end
	if math.abs(pos.y-room.y1) > math.abs(pos.y-room.y2) then new_pos.y = room.y2 else new_pos.y = room.y1 end -- Nearest

	return findValidThingPositionInSpecificRoomNear(room, new_pos)
end


-----------------------------------
function findValidThingPositionInSpecificRoomNear(room, pos)
		local nearest_pos = room:getNearestValidMapCellPositionToPosition(pos)
		nearest_pos.x = nearest_pos.x + 0.5
		nearest_pos.y = nearest_pos.y + 0.5
		return nearest_pos
end

-----------------------------------
function findValidThingPositionInAnyRoomNear(pos, avoid_special_rooms)
		local pos2, room = findValidMapPositionAndRoomNear(pos, avoid_special_rooms)
		pos2.x = pos2.x + 0.5
		pos2.y = pos2.y + 0.5
		return pos2, room
end

-----------------------------------
function findValidMapPositionAndRoomNear(pos, avoid_special_rooms)
	local nearest_room, nearest_pos = getNearestRoomAndMapCellPositionToPos(pos, avoid_special_rooms)
	if nearest_room ~= nil then -- Convert map cell to centre of tile
		nearest_pos = nearest_room:getNearestValidMapCellPositionToPosition(nearest_pos)
		return nearest_pos, nearest_room
	end
	
	print("Failed to find valid point within room at position: " .. pos.x ..", " .. pos.y)
	return nil, nil
end

-----------------------------------
function getNearestRoomAndMapCellPositionToPos(pos, avoid_special_rooms)
	local min_dist = 999999999
	local nearest_room = nil
	local nearest_pos = vector(0,0)
	for k, v in pairs(roomTable) do
		if v.roomType == nil or avoid_special_rooms ~= true then
			local room_pos = v:clampPositionToRoom(pos) -- might still be in a pillar, remember
			local dist2 = pos:dist2(room_pos)
			if dist2 < min_dist then 
				nearest_room = v
				nearest_pos = room_pos
				min_dist = dist2
			end
		end
	end
	
	return nearest_room, nearest_pos
end

-----------------------------------------
function drawDoorways()
	local col_r, col_g, col_b = HSL(((gFixedFrameCount * 4) + 128) % 255, 255, 100)
	for i, v in ipairs(doorways) do
		if v.isVisible and v.openFraction < 1 then
			local mx, my = cellXYToScreenXY(math.floor(v.x * gTileSize) / gTileSize, math.floor(v.y * gTileSize) / gTileSize)
		
			local f = 1.0 - v.openFraction
			love.graphics.setColor(col_r * f, col_g * f, col_b * f, 255)

			if v.isHorizontal then
				love.graphics.rectangle("line", mx+1, my  + (gTileSize - DOOR_THICKNESS) / 2, NODE_FLOOR_SIZE * gTileSize - 1, DOOR_THICKNESS)
			else
				love.graphics.rectangle("line", mx + (gTileSize - DOOR_THICKNESS) / 2, my+1, DOOR_THICKNESS, NODE_FLOOR_SIZE * gTileSize - 1)
			end
			
			love.graphics.setColor(col_r * f, col_g * f, col_b * f, 150)

			if v.isLocked then
				if v.isHorizontal then
					love.graphics.rectangle("fill", mx+1, my  + (gTileSize - DOOR_THICKNESS) / 2, NODE_FLOOR_SIZE * gTileSize - 1, DOOR_THICKNESS)
				else
					love.graphics.rectangle("fill", mx + (gTileSize - DOOR_THICKNESS) / 2, my+1, DOOR_THICKNESS, NODE_FLOOR_SIZE * gTileSize - 1)
				end
			end
		end
	end
	love.graphics.setColor(255,255,255)
end

----------------
function wedgeAllDoors()
	for i, v in ipairs(doorways) do
		if v.isLocked == false then
			v:wedge()
		end
	end
end


----------------
function isWalkable(x,y)
	if getMapCell(x,y).isSolid then return false end
	return true
end
----------------
function mapNearbyRooms()
	for k, v in pairs(roomTable) do
		v:map()
		v:draw()
	end
	
--[[	
	local room = roomTable[gPlayer.currentRoomID]
	for k, v in pairs(room.neighbourTable) do
		local room2 = roomTable[k]
		room2:map()
		room2:draw()
		for k2, v2 in pairs(room2.neighbourTable) do
			local room3 = roomTable[k2]
			room3:map()
			room3:draw()
		end
	end]]
end

----------------------
function hideAllRooms()
	for k, v in pairs(roomTable) do
		v.isVisible = false
	end
end

----------------------
function getPixelMapOfLevel()
	-- For mapping purposes we collapse room nodes so that a wall and an empty node area are both one pixel in size
	local pixels_per_node = 4
	local dx, dy = (MAX_NODES_WIDE * pixels_per_node) + 1, (MAX_NODES_HIGH * pixels_per_node) +1
	local data = love.image.newImageData(dx,dy)
	for y = 0, dy-1 do   -- remember: start at 0
		for x = 0, dx-1 do   -- remember: start at 0
			local sx = x * math.floor(NODE_SIZE / pixels_per_node)
			local sy = y * math.floor(NODE_SIZE / pixels_per_node)
			local cell = getMapCell(sx,sy)
			
			if gDebugRoomID ~= 0 and cell.roomID == gDebugRoomID then
				data:setPixel(x, y, 255, 64, 0, 255)
			elseif cell.isMapped == false then
				data:setPixel(x, y, 0, 64, 0, 255)
			elseif cell.isDoor then
				data:setPixel(x, y, 0, 128, 0, 255)
			elseif cell.isSolid then
				data:setPixel(x, y, 0, 255, 0, 255)
			else
				data:setPixel(x, y, 0, 128, 0, 255)
			end
		end
	end
	local img = love.graphics.newImage(data)
	img:setFilter(gCanvasType, gCanvasType)
	return img
end

----------------------
function destroyMapCellsWithinRange(x, y, range)
	local cells_that_were_solid = {}
	local range_sq = range * range
	local sx = x - range
	local sy = y - range
	for dy = 1, range * 2 do
		for dx = 1, range * 2 do
			local nx, ny = sx + dx, sy + dy
			if nx > 0 and nx < gMapWidth-1 and ny > 0 and ny < gMapHeight-1 then
				if math.dist2(x,y, nx,ny) < range_sq then
					if getMapCell(nx,ny).isSolid then table.insert(cells_that_were_solid, {nx, ny}) end
					setMapCellSolidity(nx,ny, false)
				end
			end
		end
	end
	
	for dy = 0, 2 + range * 2 do
		for dx = 0, 2 + range * 2 do
			local nx, ny = sx + dx, sy + dy
			if nx > 0 and nx < gMapWidth-1 and ny > 0 and ny < gMapHeight-1 then
				setMapTileAndColourBasedOnSolidityAndSeenStatus(nx,ny, true, true) -- MOOO
				updateCellConnectionsXY(nx,ny)
			end
		end
	end
	
	local r2 = 2 + math.floor(range * 2)
	for dy = 0, r2, NODE_SIZE do
		for dx = 0, r2, NODE_SIZE do
			local nx, ny = sx + dx, sy + dy
			nx = math.floor(nx / NODE_SIZE)
			ny = math.floor(ny / NODE_SIZE)
			if nx > 0 and nx < MAX_NODES_WIDE-1 and ny > 0 and ny < MAX_NODES_HIGH-1 then
				updateNodeConnectionsXY(nx,ny)
			end
		end
	end
	
	

--[[	for dy = 1, range * 2 do
		for dx = 1, range * 2 do
			local nx, ny = sx + dx, sy + dy
			if nx > 0 and nx < gMapWidth-1 and ny > 0 and ny < gMapHeight-1 then
				if math.dist2(x,y, nx,ny) < range_sq then
					if getMapCell(nx,ny).isSolid then
						setMapTileAndColourBasedOnSolidityAndSeenStatus(nx,ny, true, true)
					end
					setMapTile(nx,ny, 17)
				end
			end
		end
	end
	
	for i, k in ipairs(cells_that_were_solid) do
		setMapTile(k[1],k[2], 16)
	end]]
	
end
----------------------
function destroyDoorsWithoutWalls()
	for i, v in ipairs(doorways) do
		if v.isWedged == false and v:isSupportedByWalls() == false then
			v:wedge()
		end
	end
end

-----------------------
function mapWholeLevel()
	for k, room in pairs(roomTable) do
		room:map()
		room:draw()
		gMapImage = getPixelMapOfLevel()
	end
end
-----------------------
require 'library.fifo'
function getRoomRouteFromTo(room_id_from, room_id_to)
  local visited = {}
	local prev = {}
  for k, v in pairs(roomTable) do visited[k] = false; prev[k] = nil end
  local queue = fifo()
  visited[room_id_from] = true
  queue:push(room_id_from)
  
  while queue:length() ~= 0 do
  -- Dequeue a vertex from queue and print it
    s = queue:pop()

    local room = getRoomFromID(s)
    for k, v in pairs(room.neighbourTable) do
      if visited[k] == false then
        visited[k] = true
				prev[k] = s
        queue:push(k)
      end    
    end
  end
	
	local route = {}
	local current_node = room_id_to
	while current_node ~= nil do
		table.insert(route, 1, current_node)
		current_node = prev[current_node]
	end
		
	return route
end

----------------------
function getViableTreasureRoomAvoidingRoom(avoid_room)
	local min_size = 99999
	local room = nil
	for k, v in pairs(roomTable) do
		if #v.doorArray == 1 and v ~= avoid_room then
--			local dx, dy = (v.x2-v.x1), (v.y2-v.y1)
--			local size = dx * dy
--			if size < min_size then 
				min_size = size; 
				room = v 
--			end
		end
	end
	
	return room
end
----------------------
function getViableShopRoom()
	local max_size = 0
	local room = nil
	for k, v in pairs(roomTable) do
		if #v.doorArray == 1 then
			local dx, dy = (v.x2-v.x1), (v.y2-v.y1)
			if dx > NODE_SIZE and dy > NODE_SIZE then
				local size = dx * dy
				if size > max_size then max_size = size; room = v end
			end
		end
	end
	
	return room
end

function unlockAllDoorsWithpassword(password)
	local result = false
	for k, v in pairs(roomTable) do
		if v:unlockDoorsWithPassword(password) == true then result = true end
	end
	return result
end
