vector = require 'library.hump.vector'
local class = require 'library.middleclass'

CMapCell = class('CMapCell')

gCellTypes = {}

function CMapCell:initialize(t, c)
  self.roomID = 0
  self.tileIndex = 1
  self.colourIndex = 1
  self.isSolid = false
  self.isMapped = false
  self.isDoor = false
  self.isLocked = false
  self.isMapFeature = false
  self.connections = {false, false, false, false, false, false, false, false, }
end

